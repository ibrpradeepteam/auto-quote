<?php
function is_login(){
    $CI =& get_instance();
    if($CI->session->userdata('member_id')||$CI->session->userdata('admin_id')){
        return true;
    }else{
        $current_uri =  curPageURL();
        $CI->session->set_userdata(array('redirect_uri' => $current_uri));
        return false;
    }
}
function curPageURL() {
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"])){ if($_SERVER["HTTPS"]== "on") {$pageURL .= "s";}}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}
function requestDropDown($name='',$id='', $type='', $attr='', $selected='', $isDropDown='', $single=0, $new=0){
    $CI =& get_instance();	
    $dropDown = '';
    $CI->db->select('*');
    $CI->db->from('categories');
    ($id!='') ? $CI->db->where('id', $id) : '';
    ($type!='') ? $CI->db->where('type', encrypt($type)) : $CI->db->where('type', encrypt('ProjectCategory'));
    $CI->db->order_by('name', 'desc');
    $query = $CI->db->get();
    $result = ($single==1) ? $query->row() : $query->result();
    if($isDropDown==1){		
        $dropDown = '<select name="'.$name.'" id="'.str_replace('[]', '', $name).'" '.$attr.'>';
        if(is_array($result)&&count($result)>0){
            $noneopt = ($new==1) ? 'New' : '--None--';
            $noneoptval = ($new==1) ? 'new' : '';
            $dropDown .= '<option value="'.$noneoptval.'">'.$noneopt.'</option>';
            foreach($result as $row){
                $select = ($selected==$row->id) ? 'selected' : '';
                $dropDown .= '<option value="'.$row->id.'" '.$select.'>'.decrypt($row->name).'</option>';
            }
        }else{
            $dropDown .= '<option value="new">New</option>';	
        }
        $dropDown .= '</select>';
        return $dropDown;
    }else{
        return $result;	
    }
}
function get_catlog_dropdown($field_name, $catlog_type='', $attr='', $array_index=0, $other_val=0,  $module_name ='', $selected='',$otr_relative_field='', $otr_relative_field_attr='', $roi_state='', $roi_state_selected='', $other_selected='', $loop_count=''){ 
		
		$CI =& get_instance();
		if(!empty($catlog_type)){
			$CI->db->where('rq_rqf_quote_catlog_type.catlog_type', $catlog_type);
		}
		if(!empty($module_name)){
			$CI->db->where('module_name', $module_name);
		}
		$CI->db->where('rq_rqf_quote_catlog_type.status', 1);
		
		$CI->db->select('rq_rqf_quote_catlogs.catlog_value,rq_rqf_quote_catlogs.id');
		$CI->db->select('rq_rqf_quote_catlogs.is_catalog');
		$CI->db->select('rq_rqf_quote_catlogs.status');
		//$CI->db->select('rq_rqf_quote_catlogs.as_catalog');
		//$CI->db->select('rq_rqf_quote_catlogs.as_catalog');
		
		
		$CI->db->from('rq_rqf_quote_catlogs');
		$CI->db->join('rq_rqf_quote_catlog_type', 'rq_rqf_quote_catlogs.catlog_type = rq_rqf_quote_catlog_type.id', 'inner');
		$CI->db->order_by('rq_rqf_quote_catlog_type.catlog_type', 'asc');
		//$CI->db->where('rq_rqf_quote_catlogs.is_catalog','1');
		$CI->db->where('rq_rqf_quote_catlogs.status','1');
		//$CI->db->where('rq_rqf_quote_catlogs.as_catalog','1');
		$query = $CI->db->get(); //echo $CI->db->last_query();
		
		if($query->num_rows() > 0) {
			$catlog_data = $query->result();
		} else {
			$catlog_data = '';
		}
		
		$catlog ='';
                $field_name_1 = ($array_index) ? $field_name.'[]' : $field_name;
		$catlog .='<select type="text" id="'.$field_name.$loop_count.'" name="'.$field_name_1.'" '.$attr.' >';
		
			$catlog .='<option value="">Select</option>';
			//$select_this = '';
			$selec_opt = '';
			
		//$catlog_data = $this->quote_model->get_catlog_options('commodities_haulted');
		$sel_array = $selected;
		
		if(!is_array($selected) && !empty($selected)){
			$sel_array = array($selected);		
			//print_r($sel_array);	
		}
		if(empty($sel_array)) { $sel_array = array(); }
		$cat_val = array();
		//print_r($catlog_data);
		
		if($catlog_data)
		{
			foreach ($catlog_data as $catlogData){ 
				$select_this = ($selected == $catlogData->id) ? 'selected="selected"' : '';
				
				//if(!empty($select_this)){ $selec_opt = 1; }
				if($catlog_type=='meeting_group')
				{
                                    $catlog .='<option '.$select_this.' value="'.$catlogData->id.'">'.$catlogData->catlog_value.'</option>';
				}
				else {
					$catlog .='<option '.$select_this.' value="'.$catlogData->id.'">'.$catlogData->catlog_value.'</option>';
				}
				$cat_val[] = $catlogData->catlog_value;
			}
		}
		$otr_value = '';
		if($other_val) { 
			$select_otr = '';
			if(!empty($sel_array)){
				foreach($sel_array as $sel_item){
					if(!in_array($sel_item, $cat_val)){
						$select_otr = 'selected="selected"'; 
						$otr_value = $sel_item;
					}
				}
			}
			//$arr_mer = array_merge($cat_val, $sel_array);
			//$diff = array_diff($cat_val, $arr_mer); 
			/* if((!$selec_opt && !empty($selected)) || !empty($diff)){
				$select_otr = 'selected="selected"'; 
			} */
			$catlog .='<option '.$select_otr.' value="other">Other</option>';	
		}	
		$catlog .='</select>';
		if($otr_relative_field){
			if($otr_value) { $style_attr = '';} else { $style_attr = ''; }
				if($otr_relative_field_attr == 1){ $status_rad_of_ope = 1;  $style_roo_br = ''; $style_span = ''; $style_other='span8';} else { $style_roo_br = ''; $status_rad_of_ope = '';  $style_span ='span2'; $style_other='span10'; }
			
			$catlog .= '</div><div class="'.$style_span.' '.$field_name.$loop_count.'_relative_section relative" '.$style_attr.' >';
			if(!$status_rad_of_ope){
			$catlog .= '<label>Specify other</label>';
			}
			$catlog .= '<input class="textinput pull-left '.$style_other.' '.$field_name./*$loop_count.*/'_relative" type="text" id="ren_no_" placeholder="specify other" name="'.$field_name.'_other[]" value="'.$other_selected.'">';
					 if($roi_state){
						if($roi_state_selected){
							$broker_state = $roi_state_selected;
							$class_new = 'chzn-select-other-1';
						}else{						
							//$broker_state = 'CA';
							$broker_state = '';
							$class_new = 'chzn-select-other';
						}
						
						//$broker_state ='CA,AL';
						if(!$status_rad_of_ope){
							$attr = 'multiple class="chzn-select span4"';
						}else{
							$attr = 'multiple class="'.$class_new.' span12"';
						}
						
						$catlog .= '</div>'.$style_roo_br.'<div class="'.$style_span.' '.$field_name.$loop_count.'_relative_section relative" '.$style_attr.' >';
						
						if(!$status_rad_of_ope){
						$catlog .= '<label>Select State</label>'; 
						}
						$catlog .= $style_roo_br;
						$catlog .= get_state_dropdown($field_name.'_state'.$loop_count.'[]', $broker_state, $attr);
					 }
		}
		
		
		return $catlog;
		
	}
function get_active_catlog_dropdown($selected='',$attr='',$field_name,$id=''){ 
		
		
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->where('as_catlog', '1');
		$CI->db->where('status', '1');
		$CI->db->from('rq_rqf_quote_catlog_type');
		
		$CI->db->order_by('catlog_type', 'asc');
		$query = $CI->db->get();
		//echo $this->db->last_query(); die;
		if($query->num_rows() > 0) {
			$catlog_data = $query->result();
		} else {
			$catlog_data = array();
		}
		$res = '';
		$res .='<select type="text" id="'.$id.'" name="'.$field_name.'[]" '.$attr.' >';
		foreach ($catlog_data as $catlogData){ 
				//$select_this = ($selected == $catlogData->catlog_value) ? 'selected="selected"' : '';
				if($catlogData->id == $selected){
					 $select_this = 'selected="selected"';
				}else{
					$select_this = '';
				}
				//if(!empty($select_this)){ $selec_opt = 1; }
				$res .='<option '.$select_this.' value="'.$catlogData->id.'">'.$catlogData->catlog_type.'</option>';
				
			}
		$res .='</select>';
		return $res;
	
	}
function getCatlogName($id){
    $CI =& get_instance();
    if($id) {
        $CI->db->select('catlog_value');
        $CI->db->from('rq_rqf_quote_catlogs');
        $CI->db->where('id', $id);
        $query = $CI->db->get();
        $result = $query->row();
        return isset($result->catlog_value) ? $result->catlog_value : '';
    }
}
function getCatlogColor($id){
    $CI =& get_instance();
    if($id) {
            $CI->db->select('catlog_color');
            $CI->db->from('rq_rqf_quote_catlogs');
            $CI->db->where('id', $id);
            $query = $CI->db->get();
            $result = $query->row();

            return $result->catlog_color;
    }
}
 /* created by ashvin patel 27/may/2014*/
function check_privilage($up_id='', $up_privilage='')
{
	 $CI =& get_instance();
	 $up_privilage1 = array();
	 $admin_privilage = array();
	 $up_privilage1 = explode(",", $up_privilage);
	 $check = 0;
	//print_r($up_privilage1);
	 if($CI->session->userdata('admin_type')==1)
	 {
		return true;
	 }
	 else
	 {
		if(!count($up_privilage1))
		{
			echo 'a';
			 //$privilage = get_profile_id($up_id);
			 //$string = $privilage->up_privileges;
			 $string = '';
			 $admin_privilage = explode(",", $string);
			 foreach($up_privilage1 as $privilage)
			 {				
				 if(in_array($privilage, $admin_privilage))
				  {
						 return true;
				  }
				  else
				  {
						return false;
				  }
			  }
		}
		else
		{
			return false;
		}
		 
	}
}
function userDropdown($name='', $for='', $type='', $attr='', $selected=''){    
    $CI = & get_instance();
    $CI->db->select('*');
    $CI->db->where('status', 1);
    $CI->db->order_by('name', 'asc');
    $CI->db->order_by('middle_name', 'asc');
    $CI->db->order_by('last_name', 'asc');
    $query = $CI->db->get('pmod_siteuser');
    //echo $CI->db->last_query();
    $result = $query->result();
    $option = '<option value="">--Select--</option>';
    if(!empty($result)){
        foreach($result as $value) {
            if(is_array($selected)){
                $selected_1 = in_array($value->id, $selected) ? 'selected' : '';
            }else{
                $selected_1 = ($value->id==$selected) ? 'selected' : ''; 
            }
            if($CI->session->userdata('admin_id')!=$value->id){
                $option .= '<option value="'.$value->id.'" '.$selected_1.'>'.$value->name.' '.$value->middle_name.' '.$value->last_name.'</option>';
            }
        }
        $drop_down = '<select name="'.$name.'" '.$attr.'>';
        $drop_down .= $option;
        $drop_down .= '</select>';
        return $drop_down;
    }
}
function department_managers_drop_down($name='', $for='', $type='', $attr='', $selected=''){    
    $CI = & get_instance();
    $CI->db->select('*');
    $CI->db->where('status', 1);
    $CI->db->where('type', 4);
    $CI->db->order_by('name', 'asc');
    $CI->db->order_by('middle_name', 'asc');
    $CI->db->order_by('last_name', 'asc');
    $query = $CI->db->get('pmod_siteuser');
    //echo $CI->db->last_query();
    $result = $query->result();
    $option = '<option value="">--Select--</option>';
    if(!empty($result)){
        foreach($result as $value) {
            if(is_array($selected)){
                $selected_1 = in_array($value->id, $selected) ? 'selected' : '';
            }else{
                $selected_1 = ($value->id==$selected) ? 'selected' : ''; 
            }
            if($CI->session->userdata('admin_id')!=$value->id){
                $option .= '<option value="'.$value->id.'" '.$selected_1.'>'.$value->name.' '.$value->middle_name.' '.$value->last_name.'</option>';
            }
        }
        $drop_down = '<select name="'.$name.'" '.$attr.'>';
        $drop_down .= $option;
        $drop_down .= '</select>';
        return $drop_down;
    }
}
function location_managers_drop_down($name='', $for='', $type='', $attr='', $selected=''){    
    $CI = & get_instance();
    $CI->db->select('*');
    $CI->db->where('status', 1);
    $CI->db->where('type', 3);
    $CI->db->order_by('name', 'asc');
    $CI->db->order_by('middle_name', 'asc');
    $CI->db->order_by('last_name', 'asc');
    $query = $CI->db->get('pmod_siteuser');
    //echo $CI->db->last_query();
    $result = $query->result();
    $option = '<option value="">--Select--</option>';
    if(!empty($result)){
        foreach($result as $value) {
            if(is_array($selected)){
                $selected_1 = in_array($value->id, $selected) ? 'selected' : '';
            }else{
                $selected_1 = ($value->id==$selected) ? 'selected' : ''; 
            }
            if($CI->session->userdata('admin_id')!=$value->id){
                $option .= '<option value="'.$value->id.'" '.$selected_1.'>'.$value->name.' '.$value->middle_name.' '.$value->last_name.'</option>';
            }
        }
        $drop_down = '<select name="'.$name.'" '.$attr.'>';
        $drop_down .= $option;
        $drop_down .= '</select>';
        return $drop_down;
    }
}
function getUserEmail($user_id){
    $CI = & get_instance();
    $CI->db->select('email');
    $CI->db->where('id', $user_id);
    $query = $CI->db->get('pmod_siteuser');
    $row = $query->row();
    return isset($row->email) ? $row->email : '';
}
function getUserDetail($user_id, $select='*'){
    $CI = & get_instance();
    $CI->db->select($select);
    $CI->db->where('id', $user_id);
    $query = $CI->db->get('pmod_siteuser');
    $row = $query->row();
    return isset($row) ? $row: '';
}
function getUserInfo($user_id, $select='*'){
    $CI = & get_instance();
    $CI->db->select($select);
    $CI->db->where('id', $user_id);
    $query = $CI->db->get('pmod_siteuser');
    $row = $query->row();
    return isset($row->email) ? $row->email : '';
}
function getSmtpSeting(){
    $CI = & get_instance();
    $CI->db->where('type', 'smtp');
    $CI->db->where('status', '1');
    $query = $CI->db->get('setting');
    if($query->num_rows){
        return $query->row();
    }else{ 
         FALSE;
    }
}
function getManagerDepartment($user_id){
    $CI = & get_instance();
    $CI->db->select('dm.department_id, ctl.catlog_value as department');
    $CI->db->where('manager_id', $user_id);
    $CI->db->join('rq_rqf_quote_catlogs as ctl', 'dm.department_id = ctl.id');
    $query = $CI->db->get('department_manager as dm');
    $row = $query->row();
    return isset($row->department_id) ? $row->department_id : '';
}
function sendEmail($data){
    $smtp = getSmtpSeting(); 
    $mail             = new PHPMailer();			
    $mail->IsSMTP(); 		
    $mail->SMTPAuth   = true;   
    $mail->Host       = isset($smtp->smtp_host) ? $smtp->smtp_host : '';
    $mail->Port       = isset($smtp->smtp_port) ? $smtp->smtp_port : '';
    $mail->SMTPSecure = isset($smtp->smtp_secure) ? $smtp->smtp_secure : '';
    $mail->Username   = isset($smtp->smtp_user) ? $smtp->smtp_user : ''; 
    $mail->Password   = isset($smtp->smtp_pass) ? $smtp->smtp_pass : ''; 	
    $from             = isset($smtp->smtp_from) ? $smtp->smtp_from : '';  
    $from_name        = isset($smtp->smtp_from_name) ? $smtp->smtp_from_name : '';	
    if($smtp->smtp_user==''||$smtp->smtp_pass==''){
        echo 'Smtp Username Password not set';
        return ;	
    }
    
    $mail->SetFrom($from, $from_name);		
    $mail->AddReplyTo($from, $from_name);
    $mail->Subject = $data['sub'];
    $mail->MsgHTML($data['msg']);
    if(is_array($data['address'])){
        foreach ($data['address'] as $value) {
          $mail->AddAddress($value, "");  
        }
    }else{
        $address = $data['address'];
        $mail->AddAddress($address, "");  
    }
   
    if(!$mail->Send()){
        $res = "<p style='color:red;text-align: center;margin-top: 28px;text-shadow: 1px 0px 11px red;'>Mailer Error: " . $mail->ErrorInfo."</p>";
    }else{
        $res =  "<h3 style='color:green;text-align: center;margin-top: 28px;text-shadow: 1px 0px 11px green;'>Message sent!</h3>";
    }
    return $res;
    //emailLog($data);
}
//return status name by id
function getStatus($id){
    if($id){
        $CI =& get_instance();
        $CI->db->select('name, properties');
        $CI->db->where('id', $id);
        $query = $CI->db->get('status');
        $row = $query->row();
        return $row;
    }
}
function getMStatus($request_id, $original_status){
    $CI =& get_instance();
    $CI->db->select('status_id');
    $CI->db->where('request_id', $request_id);
    $CI->db->where('responsible_id', $CI->session->userdata('admin_id'));
    $query = $CI->db->get('managment_status');
    $row = $query->row();
    if(isset($row->status_id)){
        return getStatus($row->status_id);
    }else{
        $CI->db->select('status_id');
        $CI->db->where('request_id', $request_id);
        $CI->db->where('status_id', 5);
        $query = $CI->db->get('managment_status');
        $row = $query->row();
        if(isset($row->status_id)){
            return getStatus($row->status_id);
        }else{
            return getStatus($original_status);
        }
    }
}
function getBStatus($request_id, $original_status){
    $CI =& get_instance();
    $CI->db->select('status_id');
    $CI->db->where('request_id', $request_id);
    $CI->db->where('acknowledge_person_id', $CI->session->userdata('admin_id'));
    $query = $CI->db->get('request_acknowledge');
    $row = $query->row();
    if(isset($row->status_id)){
        return getStatus($row->status_id);
    }else{
        $CI->db->select('status_id');
        $CI->db->where('request_id', $request_id);
        $CI->db->where('status_id', 5);
        $query = $CI->db->get('request_acknowledge');
        $row = $query->row();
        if(isset($row->status_id)){
            return getStatus($row->status_id);
        }else{
            return getStatus($original_status);
        }
    }
}
function get_backup_ustatus($request_id, $user_id){
    $CI =& get_instance();
    $CI->db->select('status_id');
    $CI->db->where('request_id', $request_id);
    $CI->db->where('acknowledge_person_id', $user_id);
    $query = $CI->db->get('request_acknowledge');
    $row = $query->row();
    if(isset($row->status_id)){
        return getStatus($row->status_id);
    }else{
        $data = array('name' => 'pending', 'properties' => 'a:2:{s:8:"fg_color";s:7:"#000000";s:8:"bg_color";s:7:"#FFFF00";}');
        return (object) $data;
    }
}
function get_manager_ustatus($request_id, $user_id){
    $CI =& get_instance();
    $CI->db->select('status_id');
    $CI->db->where('request_id', $request_id);
    $CI->db->where('responsible_id', $user_id);
    $query = $CI->db->get('managment_status');
    $row = $query->row();
    if(isset($row->status_id)){
        return getStatus($row->status_id);
    }else{
        $data = array('name' => 'pending', 'properties' => 'a:2:{s:8:"fg_color";s:7:"#000000";s:8:"bg_color";s:7:"#FFFF00";}');
        return (object) $data;
    }
}
function check_request_denied_by_manager($request_id){
    $CI =& get_instance();
    $CI->db->select('status_id');
    $CI->db->where('request_id', $request_id);
    $CI->db->where('status_id', 5);
    $query = $CI->db->get('managment_status');
    $row = $query->row();
    if(isset($row->status_id)){
        return 0;
    }else{
        return 1;
    }
}
?>