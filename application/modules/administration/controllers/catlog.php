<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catlog  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/
	  <method_name> 
 	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		 if(!is_login()){
                    redirect(base_url());
                }	
		$this->load->model('catlog_model');
		$this->load->library('pagination');
		$this->load->model('files_model');
		//$this->load->library('customclass');
	}
	
	public function browse($type){
            $limit = 25;
            $total = $this->catlog_model->count_catlog($type); 

            $data['admins'] = $this->catlog_model->list_catlog($limit, 0, $type, '');
            $data['object'] = $this;

            $config['base_url'] = base_url('administration/catlog');
            $config['total_rows'] = $total;
            $config['per_page'] = $limit;

            $data['total'] = $total;
            $this->pagination->initialize($config);
            $this->load->view('administration/catlog_main', $data);		
    }
	
	public function index()
	{
		$this->browse('1');	
	}
	
	public function categories(){
		$this->browse_categories('1');
	}
	
	public function extras(){
		$this->browse_extras('3');
	}
	
	public function browse_categories($type){
	   $limit = 25;
            $total = $this->catlog_model->count_catlog($type); 

            $data['admins'] = $this->catlog_model->list_catlog($limit, 0, $type, '');
            $data['object'] = $this;

            $config['base_url'] = base_url('administration/catlog/categories');
            $config['total_rows'] = $total;
            $config['per_page'] = $limit;

            $data['total'] = $total;
            $this->pagination->initialize($config);
            $this->load->view('administration/category_catlog_main', $data);
	}
	
	public function browse_extras($type){
	   $limit = 25;
            $total = $this->catlog_model->count_catlog($type); 

            $data['admins'] = $this->catlog_model->list_catlog($limit, 0, $type, '');
            $data['object'] = $this;

            $config['base_url'] = base_url('administration/catlog/extras');
            $config['total_rows'] = $total;
            $config['per_page'] = $limit;

            $data['total'] = $total;
            $this->pagination->initialize($config);
            $this->load->view('administration/extras_catlog_main', $data);
	}
	
	public function ajax_list_catlog($type)
	{
            $array = array('rq_rqf_quote_catlogs.catlog_type' => $type);
            $this->datatables->select("rq_rqf_quote_catlogs.id, rq_rqf_quote_catlog_type.catlog_type, rq_rqf_quote_catlogs.catlog_title, rq_rqf_quote_catlogs.catlog_description, rq_rqf_quote_catlogs.status", FALSE);
            $this->datatables->from('rq_rqf_quote_catlogs');
            $this->datatables->where($array);
            $this->datatables->join('rq_rqf_quote_catlog_type', 'rq_rqf_quote_catlogs.catlog_type = rq_rqf_quote_catlog_type.id', 'inner');
            echo $this->datatables->generate();
		
	}
	
	public function ajax_list_category_catlog_type($type){
		$this->db->select('*');
		$this->db->from('rq_rqf_quote_catlog_type');
		$this->db->where('catlog_type_value',$type);
		$query = $this->db->get();
		$result = $query->row();
		$this->ajax_list_category_catlog($result->id);
	}
	
	public function ajax_list_category_catlog($type)
	{
		
            $array = array('rq_rqf_quote_catlogs.catlog_type' => $type);
            $this->datatables->select("rq_rqf_quote_catlogs.id, rq_rqf_quote_catlog_type.catlog_type, rq_rqf_quote_catlogs.catlog_title, rq_rqf_quote_catlogs.catlog_description, rq_rqf_quote_catlogs.status", FALSE);
            $this->datatables->from('rq_rqf_quote_catlogs');
            $this->datatables->where($array);
            $this->datatables->join('rq_rqf_quote_catlog_type', 'rq_rqf_quote_catlogs.catlog_type = rq_rqf_quote_catlog_type.id', 'inner');
            echo $this->datatables->generate();	
	}
	
	//ADD
	public function catlog_add()
	{
		
		$data['header'] = array('title'=>'Add Catalog');
		
		//$this->form_validation->set_rules('cat_f', 'Type', 'required');
		//$this->form_validation->set_rules('catlog_value', 'Value', 'required');
		
		if ($this->input->post('add_catlog') == TRUE)
		{
			
			//echo $catlog['catlog_type'] = implode(" ", $this->input->post('cat_f'));
			$catlog['catlog_type'] = $this->input->post('cat_f');
			$catlog['addon_keyword'] = $this->input->post('addon_keyword');
			$catlog['addon_price'] = $this->input->post('price');
			$catlog['catlog_title'] = $this->input->post('cat_title');
			$catlog['status'] = $this->input->post('status');
			$catlog['catlog_description'] = $this->input->post('cat_description');
			$catlog['created_on']	=	date('Y-m-d H:i:s');
			$catlog['created_by_id']	= $this->session->userdata('admin_id');
			
			/** add subscription type **/
				if($this->session->userdata('admin_type') == 1)
				{
				$catlog['subscription_type']	= 'Main';	
				}
				if($this->session->userdata('admin_type') == 3 || $this->session->userdata('admin_type') == 2)
				{
				$catlog['subscription_type']	= 'Self-managed';	
				}
			
			
			$file_types = $this->config->item('image_file_types');		
			//$title_img_name = do_upload($file_types,'titl_image','title_image');
			
			//upload multiple files
			$file_name = array();
			$config['upload_path'] = 'uploads/title_image/';
    			$config['allowed_types'] = $file_types;
			$this->load->library('upload');
			$files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {

        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
        $_FILES['userfile']['size']= $files['userfile']['size'][$i];    



    $this->upload->initialize($config);
    $this->upload->do_upload();
    $upload = $this->upload->data();
    $file_name[] = $upload['file_name'];
   }
   $names = implode(',',$file_name);
			
			$catlog['title_image'] = isset($names) ? $names : '';
			$this->catlog_model->insert_update_catlog($catlog);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.reload(true);
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$catlog['catlog_type']).'";
				</script>';
		}
		else
		{
			$this->load->view('administration/catlog_add',$data);
		}
		
	}
	
	//Edit
	public function catlog_edit($id='')
	{
		$data['header'] = array('title'=>'Edit Catalog');
		$data['catlog'] = $this->catlog_model->get_catlog_info($id);		
		//$this->form_validation->set_rules('cat_f', 'Type', 'required');
		//$this->form_validation->set_rules('catlog_value', 'Value', 'required');
		
		if ($this->input->post('add_catlog') == TRUE)
		{
			//$catlog['catlog_type'] = implode(" ", $this->input->post('cat_f'));
			
			$catlog['catlog_type'] = $this->input->post('cat_f');
			$catlog['addon_keyword'] = $this->input->post('addon_keyword');
			$catlog['addon_price'] = $this->input->post('price');
			$catlog['catlog_title'] = $this->input->post('cat_title');
			$catlog['status'] = $this->input->post('status');
			$catlog['catlog_description'] = $this->input->post('cat_description');
			$catlog['created_by_id']	= $this->session->userdata('admin_id');
			
			/** add subscription type **/
				if($this->session->userdata('admin_type') == 1)
				{
				$catlog['subscription_type']	= 'Main';	
				}
				if($this->session->userdata('admin_type') == 3 || $this->session->userdata('admin_type') == 2)
				{
				$catlog['subscription_type']	= 'Self-managed';	
				}
			$file_types = $this->config->item('image_file_types');	
			
			//$name_array = array();
			//$count = count($this->input->post('titl_image'));
						
			//$logo_name = do_upload($file_types,'titl_image','title_image');
			
			// upload multiple images
			$file_name = array();
			$config['upload_path'] = 'uploads/title_image/';
    			$config['allowed_types'] = $file_types;
			$this->load->library('upload');
			$files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {

        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
        $_FILES['userfile']['size']= $files['userfile']['size'][$i];    



    $this->upload->initialize($config);
    $this->upload->do_upload();
    $upload = $this->upload->data();
    $file_name[] = $upload['file_name'];
   }
   if(!empty($file_name))
   {
   $names = implode(',',$file_name);
   }
			
			
			if(isset($id) && $id!="")
			{
			$catlog_detail = $this->get_data_cotlog($id);
			
			$stored_image = isset($catlog_detail[0]['title_image']) ? $catlog_detail[0]['title_image'] : '';	
			}
			$catlog['title_image'] = isset($names) && !empty($names) ? $names : $stored_image;	
		
			$this->catlog_model->insert_update_catlog($catlog,$id);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.reload(true);
				</script>';
		}
		else
		{
			$this->load->view('administration/catlog_add',$data);
		}
	}
	
	//DELETE
	public function ajax_catlog_delete()
	{
		$this->db->where(array('id'=>$this->input->post('id')));
		$query=$this->db->delete('rq_rqf_quote_catlogs'); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	}
	
	
	
	/***** add catlog type  ****/
	public function catlog_type_add() {
		
		if($this->input->post('catlog_type_add') == true)
		{
		$this->catlog_model->catlog_type_add();
		echo '<script> parent.$.fancybox.close(); parent.location.reload(); </script>';
		}
		else
		{
		$this->load->view('catlog_type_add');	
		}
	}
	
	
	public function delete_company_logo()
	{
		$update_data['Company_logo'] = '';
		$this->db->where(array('id'=>$this->input->post('id')));
		$this->db->update('rq_rqf_quote_catlogs ', $update_data);
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	}
	public function get_data_cotlog($id){
		$this->db->select('*');
		$this->db->from('rq_rqf_quote_catlogs');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function delete_catalog_image(){
	$id = $this->input->post('id');
	$del_img = $this->input->post('img');
	
	$this->db->select('*');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('id',$id);
	$query = $this->db->get();
	$result = $query->row();
	
	//print_r($result);
	$images = $result->title_image;
	
	$imgs = explode(',',$images);
	foreach($imgs as $key => $value)
	{
	if(substr($value,0,-4) == $del_img)
	{
	unset($imgs[$key]);;
	}
	}
	//print_r($imgs);
	$images = implode(',',$imgs);
	 
	$this->db->update('rq_rqf_quote_catlogs',array('title_image' => $images),array('id' => $id));
	//echo $this->db->affected_rows();
	if($this->db->affected_rows() != 0)
	{
	echo 1;	
	}	
	}
	
}