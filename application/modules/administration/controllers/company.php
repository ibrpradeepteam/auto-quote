<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company  extends RQ_Controller {
	
	public function __construct(){
		parent::__construct();		
		if(is_login()){			
			$this->load->model('company_model');	
			$this->load->library('parser');	
			
		}else{
			redirect(base_url());
		}
		
	}
	/*
	* Company default controller
	* Listing all company
	*/
	public function index(){
		$data['page_title'] = 'Company';
		$data['title_small'] = 'List';
		$data['base_url'] = base_url();
		$data['header'] = $this->parser->parse('includes/header', $data, true);
		$data['footer'] = $this->parser->parse('includes/footer', $data, true);
		$data['page_header'] = $this->parser->parse('includes/page_header', $data, true);
		$company_list = $this->company_model->get_data_company();
		$company_lists = json_encode($company_list);
		$data['company_lists'] = form_safe_json($company_lists);
		$this->parser->parse('company_list', $data);
	}
	/*
	* Add Company
	* Click add company button show add popup
	* Come with post save data in db.
	* Ashvin Patel 14/Mar/2015 12:23PM 
	*/
	public function add_company($id=''){
		if($this->input->post('add_company')){
			
			$this->company_model->add_update_company();
	        echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.reload(true);
				</script>';
			
		}else{
			$data['page_title'] = 'Add Company';		
			$data['base_url'] = base_url();		
			$data['styles'] = $this->parser->parse('includes/styles', $data, true);
			$data['scripts'] = $this->parser->parse('includes/scripts', $data, true);
			if($id!='')
			{
			$company_detail = $this->company_model->get_data_company($id);
			$data['company_id'] = $id;
			$company_detail = json_encode($company_detail);	
			$data['company_detail'] = form_safe_json($company_detail);
			}
			$this->parser->parse('add_company', $data);
		}
	}
	
	 public function company_delete(){
		 foreach($_POST['delete_check'] as $id){
            $this->db->update('company', array('is_deleted' => 1), array('id'=>$id));
        }
		redirect('administration/company');	 
	}
	
	 public function customer_company_delete(){
		 foreach($_POST['delete_check'] as $id){
            $this->db->update('customer_company', array('is_deleted' => 1), array('customer_company_id'=>$id));
        }
		redirect('administration/customer/customers');	 
	}
	

}	