<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Csv extends CI_Controller {
	 function __construct() {
		 
		 parent::__construct();
		 
		 if(!is_login()){
			redirect(base_url()); 
		 }
		// $this->load->model('product_model'); 
		 $this->load->library( 'parser' );
		 }

public function index(){
	    $data['base_url'] 		= base_url();
	 	$data['header'] 		= $this->load->view('includes/header', '', true);
	 	$data['styles'] 		= $this->load->view('includes/styles', '', true);
	 	$data['scripts'] 		= $this->load->view('includes/scripts', '', true);
		 $this->parser->parse('import_export_csv',$data);
}


public function export_product_csv(){
		
		//echo "sdsaddfsfdfdg";
		// Fetch Record from Database

$output			= "";
$table 			= ""; // Enter Your Table Name
$sql 			= mysql_query("select * from products");
$columns_total 	= mysql_num_fields($sql);

// Get The Field Name

for ($i = 0; $i < $columns_total; $i++) {
	$heading	=	mysql_field_name($sql, $i);
	$output		.= '"'.$heading.'",';
}
$output .="\n";

// Get Records from the table

while ($row = mysql_fetch_array($sql)) {
for ($i = 0; $i < $columns_total; $i++) {
$output .='"'.$row["$i"].'",';
}
$output .="\n";
}

// Download the file

$filename =  "productcsv.csv";
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);

echo $output;
return ;
//exit;
	
		}
	
public function export_customer_csv(){
		
		//echo "sdsaddfsfdfdg";
		// Fetch Record from Database

$output			= "";
$table 			= ""; // Enter Your Table Name
$sql 			= mysql_query("select * from customer_detail");
$columns_total 	= mysql_num_fields($sql);

// Get The Field Name

for ($i = 0; $i < $columns_total; $i++) {
	$heading	=	mysql_field_name($sql, $i);
	$output		.= '"'.$heading.'",';
}
$output .="\n";

// Get Records from the table

while ($row = mysql_fetch_array($sql)) {
for ($i = 0; $i < $columns_total; $i++) {
$output .='"'.$row["$i"].'",';
}
$output .="\n";
}

// Download the file

$filename =  "customercsv.csv";
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);

echo $output;
return ;
//exit;
	
		}
		
public function export_quotation_csv(){
		
		//echo "sdsaddfsfdfdg";
		// Fetch Record from Database

$output			= "";
$table 			= ""; // Enter Your Table Name
$sql 			= mysql_query("select * from quotations");
$columns_total 	= mysql_num_fields($sql);

// Get The Field Name

for ($i = 0; $i < $columns_total; $i++) {
	$heading	=	mysql_field_name($sql, $i);
	$output		.= '"'.$heading.'",';
}
$output .="\n";

// Get Records from the table

while ($row = mysql_fetch_array($sql)) {
for ($i = 0; $i < $columns_total; $i++) {
$output .='"'.$row["$i"].'",';
}
$output .="\n";
}

// Download the file

$filename =  "quotationcsv.csv";
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);

echo $output;
return ;
//exit;
	
		}
		
		public function import_csv_form($csv_file=''){
			$data['file_name'] = $csv_file;
			$this->load->view('import_csv',$data);
		}
		
public function import_csv(){
	
	if(isset($_FILES['product_csv']))
	{
	if ($_FILES['product_csv']['size'] > 0) { 
    //get the csv file 
    $csv_file = $_FILES['product_csv']['tmp_name']; 
  
		if (($getfile = fopen($csv_file, "r")) !== FALSE) { 
        $data = fgetcsv($getfile, 1000, ",");
        while (($data = fgetcsv($getfile, 1000, ",")) !== FALSE) {
         $num = count($data); 
         for ($c=0; $c < $num; $c++) {
             $result = $data; 
             $str = implode(",", $result); 
             $slice = explode(",", $str);
             //$col1 = $slice[0]; 
             $col['product_name'] = $slice[1];
             $col['motor'] = $slice[2];
			 $col['price'] = $slice[3];
             $col['max_pump_pressure'] = $slice[4];
			 $col['pump_set_pressure'] = $slice[5];
             $col['water_temprature'] = $slice[6];
			 $col['product_size'] = $slice[7];
             $col['product_weight'] = $slice[8];
			 $col['product_category'] = $slice[9];
             $col['product_image'] = $slice[10];
			 $col['feature1'] = $slice[11];
             $col['feature2'] = $slice[12];
			 $col['feature3'] = $slice[13];
             $col['feature4'] = $slice[14];
			 $col['feature5'] = $slice[15];
             $col['feature6'] = $slice[16]; 
			 $col['feature7'] = $slice[17];
             $col['feature8'] = $slice[18];
			 $col['feature9'] = $slice[19];
             $col['is_deleted'] = $slice[20];
			 $col['subscription_type'] = $slice[21];
             $col['created_on'] = $slice[22];
			 $col['created_by_id'] = $slice[23];
             $col['updated_on'] = $slice[24];
     }
	 $this->db->insert('products',$col);
   } 
  }
	}
	}
		
		
		
		if(isset($_FILES['customer_csv']))
	{
		if ($_FILES['customer_csv']['size'] > 0) { 
    //get the csv file 
    $csv_file = $_FILES['customer_csv']['tmp_name']; 
  
		if (($getfile = fopen($csv_file, "r")) !== FALSE) { 
        $data = fgetcsv($getfile, 1000, ",");
        while (($data = fgetcsv($getfile, 1000, ",")) !== FALSE) {
         $num = count($data); 
         for ($c=0; $c < $num; $c++) {
             $result = $data; 
             $str = implode(",", $result); 
             $slice = explode(",", $str);
             //$col1 = $slice[0]; 
             $col['customer_f_name'] = $slice[1];
             $col['customer_l_name'] = $slice[2];
			 $col['company_name'] = $slice[3];
             $col['phone_number'] = $slice[4];
			 $col['email_add'] = $slice[5];
             $col['street_add'] = $slice[6];
			 $col['suburb'] = $slice[7];
             $col['state'] = $slice[8];
			 $col['post_code'] = $slice[9];
             $col['created_on'] = $slice[10];
			 $col['created_by'] = $slice[11];
             $col['updated_on'] = $slice[12];
			
     }
	 $this->db->insert('customer_detail',$col);
   } 
  }
	}
	}
		
		
		if(isset($_FILES['quote_csv']))
	{
		if ($_FILES['quote_csv']['size'] > 0) { 
    //get the csv file 
    $csv_file = $_FILES['quote_csv']['tmp_name']; 
  
		if (($getfile = fopen($csv_file, "r")) !== FALSE) { 
        $data = fgetcsv($getfile, 1000, ",");
        while (($data = fgetcsv($getfile, 1000, ",")) !== FALSE) {
         $num = count($data); 
         for ($c=0; $c < $num; $c++) {
             $result = $data; 
             $str = implode(",", $result); 
             $slice = explode(",", $str);
             //$col1 = $slice[0]; 
             $col['customer_id'] = $slice[1];
             $col['product_category'] = $slice[2];
			 $col['product_id'] = $slice[3];
             $col['product_name'] = $slice[4];
			 $col['product_price'] = $slice[5];
             $col['rated_pressure'] = $slice[6];
			 $col['setup_pressure'] = $slice[7];
             $col['sales_person'] = $slice[8];
			 $col['selling_point1'] = $slice[9];
             $col['selling_point2'] = $slice[10];
			 $col['selling_point3'] = $slice[11];
             $col['selling_point4'] = $slice[12];
			 $col['selling_point5'] = $slice[13];
             $col['selling_point6'] = $slice[14];
			 $col['optional_extra1'] = $slice[15];
             $col['optional_extra2'] = $slice[16]; 
			 $col['optional_extra3'] = $slice[17];
             $col['is_deleted'] = $slice[18];
			
     }
	 $this->db->insert('quotations',$col);
   } 
  }
  
		}
	}
	echo '<script> parent.$.fancybox.close();
	parent.location.reload(true);	</script>';	
		
}

public function mail_setting(){
   $data['base_url'] 		= base_url();
   $data['header'] 		= $this->load->view('includes/header', '', true);
  // $data['styles'] 		= $this->load->view('includes/styles', '', true);
  // $data['scripts'] 		= $this->load->view('includes/scripts', '', true);
   $this->db->select('*');
   $this->db->from('manager_mail_setting');
   $query = $this->db->get();
   $data['result'] = $query->row_array();
   //$data['result'] = json_encode($data['result']);
  // print_r($data['result']);
   $this->parser->parse('mail_setting',$data);
}

public function save_mail_setting(){

if($this->input->post('save_setting') == true)
{
$post = $this->input->post();
$setting['quote_detail'] = isset($post['delete_check1']) ? $post['delete_check1'] : 0;
$setting['product_detail'] = isset($post['delete_check2']) ? $post['delete_check2'] : 0;
$setting['customer_detail'] = isset($post['delete_check3']) ? $post['delete_check3'] : 0;

$this->db->update('manager_mail_setting',$setting,array('id' => 1));
}
redirect('administration/csv/mail_setting');
}
}
?>