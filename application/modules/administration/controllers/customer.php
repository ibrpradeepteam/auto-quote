<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer  extends RQ_Controller {
	
	public function __construct(){
		parent::__construct();		
		if(is_login()){			
			$this->load->model('company_model');	
			$this->load->library('parser');	
			
		}else{
			redirect(base_url());
		}
		
	}
	/*
	* Company default controller
	* Listing all company
	*/
	public function index($id=''){
		$data['page_title'] = 'Customer';
		$data['title_small'] = 'List';
		$data['base_url'] = base_url();
		$data['header'] = $this->parser->parse('includes/header', $data, true);
		$data['footer'] = $this->parser->parse('includes/footer', $data, true);
		$data['page_header'] = $this->parser->parse('includes/page_header', $data, true);
		$customer_list = $this->company_model->get_data_customer();
		
		$customer_lists = json_encode($customer_list);
		$data['customer_lists'] = form_safe_json($customer_lists);
		$this->parser->parse('customer_list', $data);
	}
	
	
	
	public function customers(){
	
	  	$data['page_title'] = 'Company';
		$data['title_small'] = 'List';
		$data['base_url'] = base_url();
		$data['header'] = $this->parser->parse('includes/header', $data, true);
		$data['footer'] = $this->parser->parse('includes/footer', $data, true);
		$data['page_header'] = $this->parser->parse('includes/page_header', $data, true);
		
		$company_list = $this->company_model->get_data_customer_company();
		
		$company_lists = json_encode($company_list);
		$data['company_lists'] = form_safe_json($company_lists);
		//print_r($data['company_lists']);
		$this->parser->parse('customers_company', $data);
	
	}
	
	
	
	
	
	/*
	* Add Company
	* Click add company button show add popup
	* Come with post save data in db.
	* Ashvin Patel 14/Mar/2015 12:23PM 
	*/
	public function add_first_company($id=''){
		if($this->input->post('customer_f_name')){
		echo $this->input->post('customer_f_name') ; die();
		}
		
	}
	public function add_last_company($id=''){
	}
	public function add_customer_company($id=''){
		if($this->input->post('company_name')){
			
			$dataid['insert_id1']=$this->company_model->add_update_customers_company();
			if($dataid['insert_id1'] == "updated")
			{
	      echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.reload(true);
				</script>';
			}
			echo $company_info=json_encode($dataid['insert_id1']);
		}else{
			$data['page_title'] = 'Add Company';		
			$data['base_url'] = base_url();		
			$data['styles'] = $this->parser->parse('includes/styles', $data, true);
			$data['scripts'] = $this->parser->parse('includes/scripts', $data, true);
			if($id!='')
			{
			$data['page_title'] = 'Edit Company';
			$company_detail = $this->company_model->get_data_customer_company($id);
			$data['customer_company_id'] = $id;
			$company_detail = json_encode($company_detail);	
			$data['customer_company_detail'] = form_safe_json($company_detail);
			
	        
			//print_r($data['company_detail']);
			}
			
			$this->parser->parse('add_customer_company', $data);
		}
	}
	
	 public function company_delete(){
		 foreach($_POST['delete_check'] as $id){
            $this->db->update('company', array('is_deleted' => 1), array('id'=>$id));
        }
		redirect('administration/company');	 
	}
	
	 public function customer_delete(){
		 foreach($_POST['delete_check'] as $id){
            $this->db->update('customer_detail', array('is_deleted' => 1), array('customer_id'=>$id));
        }
		redirect('administration/customer');	 
	}
	
	public function add_customer($id=''){
	//echo $id;
	 $customer_id = $this->input->get('customer_id');
	if($id != ''){
			
			//$this->session->set_userdata(array('edited_customer'=>$id));
			}
	if($this->input->post('add_customer')){
			
			$this->company_model->add_update_customer();
			if($id == ''){
				
			$this->session->set_userdata(array('addedd_customer'=>'add'));
			
			}
			
	        echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.reload(true);
				</script>';
			
		}else{
	                 $data['title'] 		= 'Add Customer';
			 $data['styles'] 		= $this->load->view('includes/styles', '', true);
			 $data['scripts'] 		= $this->load->view('includes/scripts', '', true);
			 $data['footer'] 		= $this->load->view('includes/footer', '', true);
			 $data['base_url'] 		= base_url();
			 
			 if($customer_id!='')
			{
			
			$customer_detail = $this->company_model->get_data_customer($customer_id);
			foreach($customer_detail as $cd)
			{
			$name = $cd->customer_f_name." ".$cd->customer_l_name;
			$data['company_id'] = $cd->company_id;
			}
			if(isset($name)){
			$data['title'] = "Edit"." ".$name; 
			}
			$data['customer_id'] = $customer_id;
			
			$customer_detail = json_encode($customer_detail);	
			$data['customer_detail'] = form_safe_json($customer_detail);
			
			
			//print_r($data['customer_companies']);
			}
			else{
			$data['company_id'] = '';
			}
			$data['customer_companies'] = $this->get_customer_companies();
			if($id != ''){
			$data['company_id'] = $id;
			}
			$this->parser->parse('add_customer',$data);
			 
			}
			
	}
	
	function get_customer_companies(){
	$this->db->select('*');
	$this->db->from('customer_company');
$this->db->where('is_deleted',0);
	$query = $this->db->get();
	return $query->result();
	}
	

}	