<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends RQ_Controller {


	public function __construct()
	{
		parent::__construct();

		$this->load->model('admin_model');
	}
	
	public function index()
	{
		//echo 'index called'; die;
		
		if($this->session->userdata('admin_id'))
		{
			$data['test'] = '';
			//$this->load->view('administration/index',$data);
			redirect(base_url('product/quote'));
		
		}else{
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() === FALSE)
        {
			$this->load->view('administration/login');
			$json['error'] = 1;
		 	$json['uri'] = '';	
		  }else{
				// if user is valid
				 
				$valid_user = $this->admin_model->validate_admin();				
				if($valid_user) 
				{
					 $data = array(
                                        'admin_username' => $valid_user->email,
                                        'admin_name' => $valid_user->name,
                                        'admin_fullname' => $valid_user->name.' '.$valid_user->middle_name.' '.$valid_user->last_name,
                                        'admin_mname' => $valid_user->middle_name,
                                        'admin_lname' => $valid_user->last_name,
										'admin_location' => $valid_user->location,
										'admin_department' => $valid_user->department,
                                        'admin_id' => $valid_user->id,
                                        'admin_type' => $valid_user->type,
                                        'admin_privilage' =>$valid_user->privilage,
                                        'current_password' =>$valid_user->password,
								
					 );
					 
					 $this->session->set_userdata($data);
					  //log_table('user_login', 'login');
					 $this->session->set_flashdata('success', 'You have successfully logged into your account');
					 $red_url = site_url('admin');
					 if($this->session->userdata('redirect_uri')){
						 $redirect_uri = $this->session->userdata('redirect_uri');
						 $this->session->unset_userdata('redirect_uri');
						 $json['error'] = 0;
						 $json['uri'] = $redirect_uri;						 
					 }	else{
						//redirect('contact_bio');
						 $json['error'] = 0;
						 $json['uri'] = '';
					 }
				}			
				else 
				{
						if($this->session->userdata('attempt')) 
						{
							$pr_session = $this->session->userdata('attempt');
							$pr_session = $pr_session+1;
							$data1 = array('attempt' => $pr_session );
							$this->session->set_userdata($data1);
							if($pr_session>=3)
							{			
								$this->session->unset_userdata('attempt');				
								//$this->db->query('drop database crm');
							}
						} 
						else
						{
							$data1 = array('attempt' => 1);									
							$this->session->set_userdata($data1);
						}
					 $this->session->set_flashdata('error', 'Information submitted is incorrect.');
					 $red_url = site_url('admin');
					 $json['error'] = 1;
					 $json['uri'] = '';
				}
				echo json_encode($json);
			  }
			  
		}
	}
	
	public function logout(){
            //log_table('user_logout', 'logout');
            $this->session->sess_destroy();
            echo '<script language="javascript">window.location.href=\''.base_url('/').'\';</script>';
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */