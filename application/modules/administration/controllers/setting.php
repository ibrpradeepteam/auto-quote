<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		 if(!is_login()){
			redirect(base_url()); 
		 }
		 $this->load->model('setting_model'); 
		 $this->load->library( 'parser' );
	}
	
	
		
	public function index()
	{
		//$this->load->view('remarks');			
		//$data['templat'] = $this->setting_model->list_templats();
		//$this->load->view('view_templats', $data);	
			
	}
	public function smtp()
	{
		$data['smtp'] = $this->setting_model->get_smtp();  
		 $data['header'] 		= $this->load->view('includes/header', '', true);
		 $data['footer'] 		= $this->load->view('includes/footer', '', true);
	 	 $data['base_url'] 		= base_url();
		$this->parser->parse('smtp_view',$data);
	}
	
	public function save_smtp_setting(){
	 if($this->input->post('save_smtp') == true)
	 {
	 $this->setting_model->save_setting();
	 redirect('administration/setting/smtp');
	 }
	}
	
	
	public function change_password()
	{
		//print_r($this->session->all_userdata());
		if(!isset($_POST['save']))
		{
			$this->load->view('change_password');
		}
		else
		{
			if($this->input->post('n_pass')==$this->input->post('cn_pass'))
			{
				$data['result'] = $this->setting_model->change_password($this->input->post());
			}
			else
			{
				$data['result'] = '<h4 style="color:#ff0000;">Confirm Password do not match</h4>';				
			}
			$this->load->view('change_password', $data);
		}
	}
	
	
   
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */