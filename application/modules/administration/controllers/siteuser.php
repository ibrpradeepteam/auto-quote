<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Siteuser  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/
	  <method_name> 
 	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		//$this->require_admin_login();
		if(is_login())
		{
			$this->load->helper('my_helper');
			$this->load->model('siteuser_model');
			$this->load->library('pagination');  
			$this->load->helper('date');
			$this->load->helper('url');
		}
		else
		{
			redirect(base_url());
		}
		//$this->load->library('customclass');
	}
	public function index()
	{
			$limit = 25;
			$total = $this->siteuser_model->count_siteuser(); 
			
			$data['admins'] = $this->siteuser_model->list_siteuser($limit, 0, '', '');
//print_r($data['admins']);
			$data['object'] = $this;
			
			$config['base_url'] = site_url('administration/siteuser');
			$config['total_rows'] = $total;
			$config['per_page'] = $limit;
			
			$data['total'] = $total;
			$this->pagination->initialize($config);	
			//if(check_privilage())
			//{
				$this->load->view('administration/siteuser_main', $data);
			//}
			//else
			//{
				//$this->load->view('administration/permission', $data);
			//}
			
	}
	
	
	public function admin_setup()
	{ 
			$limit = 25;
			$total = $this->siteuser_model->count_admins(); 
			
			$data['admins'] = $this->siteuser_model->list_admins($limit, 0, '', '');
			$data['object'] = $this;
			
			$config['base_url'] = site_url('administration/siteuser/admin_setup');
			$config['total_rows'] = $total;
			$config['per_page'] = $limit;
			
			$data['total'] = $total;
			$this->pagination->initialize($config);	
			if(check_privilage())
			{
				$this->load->view('administration/siteuser_main', $data);
			}
			else
			{
				$this->load->view('administration/permission', $data);
			}
			
	}
	
	// start for header///////
	
	public function header_view($id)
	{
		if($id == 0)
		{
			$res['result'] = $this->templat_model->get_head_logo();
			$this->load->view('administration/header_logo', $res);
		}else{
			$res['row'] = $this->templat_model->get_header_info($id);
			$this->load->view('administration/header_logo', $res);
		}
			
	}
	
	public function insert_data($id)
	{
		$this->form_validation->set_rules('h_logo_title', 'Header Logo Title', 'required');
		if($id == 0){
			if (empty($_FILES['logo_image']['name']))
			{
			$this->form_validation->set_rules('logo_image', 'Header Logo Image', 'required');
			}
		}
		if ($this->form_validation->run() === TRUE)
		{			
			if(isset($_FILES['logo_image']['name']) && $_FILES['logo_image']['name']!=''){
				
				$returnval=$this->upload_mainimage('logo_image');	
					//echo "<pre>";print_r($returnval);
					if(is_array($returnval) && isset($returnval['error'])){
						$this->session->set_flashdata('error', @$returnval['error']);
						redirect(uri_string());exit;
					}
					elseif(is_array($returnval) && isset($returnval['mainimage'])){
						$mainimage=$returnval['mainimage'];
						
					} 
				}
			if($id == 0)
			{
				if($this->input->post('status') == 'active')
				{
					$up_status = $this->templat_model->update_status('real_header_table','pending');
				}
				$data['title'] = $this->input->post('h_logo_title');
				$data['logo_name'] = $mainimage;
				$data['create_date'] = date('Y-m-d H:i:s');
				$data['status'] = $this->input->post('status');
				$this->templat_model->insert_logo($data);
				redirect('administration/template/header_view/0');
			}else{
				if($this->input->post('status') == 'active')
				{
					$up_status = $this->templat_model->update_status('real_header_table','pending');
				}

				if(isset($_FILES['logo_image']['name']) && $_FILES['logo_image']['name'] !='')
				{
					$data['logo_name'] = $mainimage;
					$this->delete_logo($this->input->post('current_logo'), 'header_logo');
				}
				$data['title'] = $this->input->post('h_logo_title');
				$data['create_date'] = date('Y-m-d H:i:s');
				$data['status'] = $this->input->post('status');
				
				
				$this->templat_model->update_logo($data, $id);
				redirect('administration/template/header_view/0');
			}
				//redirect('administration/template/header_view/0');
		} else {
		if($id == 0)
			{
				$res['result'] = $this->templat_model->get_head_logo();
				$this->load->view('administration/header_logo', $res);
			}else{
				$res['row'] = $this->templat_model->get_header_info($id);
				$this->load->view('administration/header_logo', $res);
			}
		}
	}
	
	public function upload_mainimage($fieldname)
	{	
		  $imgPath = realpath(APPPATH . '../uploads/header_logo');
		  $config['upload_path'] = './uploads/header_logo'; /* NB! create this dir! */
		  $config['allowed_types'] = IMAGE_TYPES;
		  $config['max_size']  = '0';
		  $config['max_width']  = '0';
		  $config['max_height']  = '0';	
		  $config['overwrite'] = FALSE;
		  $this->load->library('upload', $config);
		  $configThumb = array();
		  $configThumb['image_library'] = 'gd2';
		  $configThumb['source_image'] = '';
		  $configThumb['create_thumb'] = TRUE;
		  $configThumb['maintain_ratio'] = FALSE;
		  $configThumb['width'] = 100;
		  $configThumb['height'] = 100;		  
		  $this->load->library('image_lib');
		 
		 if(isset($_FILES[$fieldname]['name']) && $_FILES[$fieldname]['name']!=''){		
				$upload = $this->upload->do_upload($fieldname);									
				if(!$upload){
					return array('error' => $this->upload->display_errors());
				}else{
					
					$data = $this->upload->data();
				}
			
						
				if($data['is_image'] == 1) {
				  $configThumb['source_image'] = $data['full_path'];
				  $configThumb['new_image'] = $imgPath . '/thumbs/';
				  $this->image_lib->initialize($configThumb);
				  $this->image_lib->resize();
				}						
			}
			$image_name   = (isset($_REQUEST['main_image_hid']))? $_REQUEST['main_image_hid']:''; 
			$story=array();
			$image_to_delete = '';
			if($image_name!='' && isset($data['file_name']) && $data['file_name']!=''){	
				$thumbImg=explode('.',$image_to_delete);	
				$thumbsimage=($thumbImg[0]!='') ? $thumbImg[0].'.'.$thumbImg[1] :'';
				$story = array('mainimage'=>$data['file_name']);									
			}
			elseif($image_name!='' && (!isset($data['file_name']))){			
				$story = array('mainimage'=>$image_name);
			}
			elseif($image_name=='' && isset($data['file_name']) && $data['file_name']!=''){		
				$story = array('mainimage'=>$data['file_name']);
			} 
			return $story;
		
		}
	//function delete_logo('filename', 'foldername')
	
	public function delete_logo($filename, $foldername)
	{
		echo unlink('./uploads/'.$foldername.'/'.$filename);
		echo unlink('./uploads/'.$foldername.'/'.'thumbs/'.$filename);
	}


	
	public function delete_head_logo($id)
		{
			$row = $this->db->where('id',$id)->get('real_header_table')->row();
			unlink('./uploads/header_logo/'.$row->logo_name);
			unlink('./uploads/header_logo/thumbs/'.$row->logo_name);
			$this->templat_model->delete_head_logo_mod($id);
			redirect('administration/template/header_view/0');
			//redirect($this->config->item('prevous_url'));
			exit;
		}
		
		// End for header///////

	
	
	
	// start for footer///////
	public function footer_edit($id)
	{
		if($id == 0){
		$res['result'] = $this->templat_model->get_footer();
		$this->load->view('administration/footer_e', $res);
		}else{
		$res['row'] = $this->templat_model->get_footer_info($id);
		$this->load->view('administration/footer_e', $res);
		}
	}
	
	
	
	
	public function footer_sub($id)
	{	
			$this->form_validation->set_rules('footer_title', 'Title', 'required');
			$this->form_validation->set_rules('footer_content', 'Footer Description', 'required');
		if ($this->form_validation->run() === TRUE)
		{			
	
			if($this->input->post('status') == 'active')
			{
				$up_status = $this->templat_model->update_status('real_footer_table','pending');
			}
				$data['title'] = $this->input->post('footer_title');
				$data['footer_content'] = $this->input->post('footer_content');
				$data['create_date'] = date('Y-m-d H:i:s');
				$data['status'] = $this->input->post('status');
			if($id == 0){
				$this->templat_model->insert_footer($data);
			}
			else{
				$this->templat_model->update_footer($data, $id);
			}
			redirect('administration/template/footer_edit/0');
		}else{
		
				if($id == 0){
				$res['result'] = $this->templat_model->get_footer();
				$this->load->view('administration/footer_e', $res);
				}else{
				$res['row'] = $this->templat_model->get_footer_info($id);
				$this->load->view('administration/footer_e', $res);
			}
		}
	}
	
	
	
	public function delete_footer($id)
	{
		$this->templat_model->delete_footer($id);
		redirect('administration/template/footer_edit/0');
	}
	// End for Footer///////
	
	
	
	
	
	
	// start for Video///////
	public function video($id)
	{
		$res['page_result'] = $this->templat_model->get_page_for_vdo();
	 if($id == 0)
	 {
		$res['result'] = $this->templat_model->get_videos();
		$this->load->view('administration/video', $res);
		}else
		{
		$res['row'] = $this->templat_model->get_videos_info($id);
		$this->load->view('administration/video', $res);
		}
	}

	public function insert_video($id)
	{ 
		$this->form_validation->set_rules('video_title', 'Video Title', 'required');
		if($id == 0)
		{
			if (empty($_FILES['video']['name']))
			{
				$this->form_validation->set_rules('video', 'Video', 'required');
			}
		} 
		if ($this->form_validation->run() === TRUE)
		{  
			if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '') {
				unset($config);
				 $path = explode('.', $_FILES['video']['name']);
				 $exetention = end($path);
				$time = time();
				$configVideo['upload_path'] = './uploads/videos';
				$configVideo['max_size'] = '100000';
				$configVideo['allowed_types'] = 'flv|3gp|mp4|mp4';
				$configVideo['overwrite'] = FALSE;
				$configVideo['remove_spaces'] = TRUE;
				$video_name = $time.'.'.$exetention;
				$configVideo['file_name'] = $video_name;

				$this->load->library('upload', $configVideo);
				$this->upload->initialize($configVideo);
				if (!$this->upload->do_upload('video')) { 
					$error_msg = $this->upload->display_errors();
					$this->session->set_flashdata('error', @$error_msg);
					redirect(uri_string());exit;
				} else {
					$mainimage = $video_name;
				
				}/*
					$videoDetails = $this->upload->data();
					return 1;
					//$data['result'] = "Successfully Uploaded";
					//$this->load->view('upload',$data);
					*/
			if(isset($_FILES['video']['name']) && $_FILES['video']['name']!=''){
			
				$data['video_name'] = $mainimage;
				if($this->input->post('current_vdo') != ''){
				$this->delete_vdo($this->input->post('current_vdo'));
				}
			}
			$data['vdo_title'] = $this->input->post('video_title');
			$data['page_id'] = $this->input->post('page_id');
			$data['create_date'] = date('Y-m-d H:i:s');
			$data['vdo_status'] = $this->input->post('status');
			
			if($id == 0){
			$res = $this->templat_model->insert_vdo($data);
			redirect('administration/template/video/0');
			}else{
			$res = $this->templat_model->update_vdo($data, $id);
			redirect('administration/template/video/0');
			}	
		} else {
			//$this->input->post('ads_name');
			$data['vdo_title'] = $this->input->post('video_title');
			$data['page_id'] = $this->input->post('page_id');
			//$data['create_date'] = date('Y-m-d H:i:s');
			$data['vdo_status'] = $this->input->post('status');
			$data['modified_date'] = date('Y-m-d H:i:s');
			$this->templat_model->update_vdo($data, $id);
			
			$res['row'] = $this->templat_model->get_videos_info($id);
			$this->load->view('administration/video', $res);
		}
	} else {
		
		$res['page_result'] = $this->templat_model->get_page_for_vdo();
	 if($id == 0)
	 {
		$res['result'] = $this->templat_model->get_videos();
		$this->load->view('administration/video', $res);
		}else
		{
		$res['row'] = $this->templat_model->get_videos_info($id);
		$this->load->view('administration/video', $res);
		}
		//$this->video('');
	}
}

	
	public function delete_video($id)
	{
			$row = $this->db->where('id',$id)->get('real_video_table')->row();
			unlink('./uploads/videos/'.$row->video_name);
			$this->db->set('right_bar', 0);
			$this->db->where('right_bar', 'video-'.$id);
			$this->db->update('real_pages');
			$this->templat_model->delete_video_mod($id);
			redirect('administration/template/video/0');
			
			exit;
	}
	
	
	public function delete_vdo($filename)
	{
		unlink("./uploads/videos/".$filename);
	}

		// End for Video///////
   
   
   	// start for Ads///////
	
	
	public function ads($id)
	{
		if($id == 0)
		{
			$res['result'] = $this->templat_model->get_ads();
			$this->load->view('administration/ads', $res);
		}else{
			$res['row'] = $this->templat_model->get_ads_info($id);
			$this->load->view('administration/ads', $res);
		}
	}
	
	
	public function insert_ads($id)
	{
				
		$this->form_validation->set_rules('ads_name', 'AD Name', 'required');
		$this->form_validation->set_rules('ads_link', 'AD Link', 'required');
		
		if($id == 0){
		if (empty($_FILES['ads_img']['name']))
		{
			$this->form_validation->set_rules('ads_img', 'AD Image', 'required');
		}
		}
		if ($this->form_validation->run() === TRUE)
		{ 
			if(isset($_FILES['ads_img']['name']) && $_FILES['ads_img']['name']!=''){
				$returnval=$this->upload_ads_image('ads_img');	
					//echo "<pre>";print_r($returnval);
					if(is_array($returnval) && isset($returnval['error'])){
						$this->session->set_flashdata('error', @$returnval['error']);
						redirect(uri_string());exit;
					}
					elseif(is_array($returnval) && isset($returnval['mainimage'])){
						$mainimage=$returnval['mainimage'];
					} 
				}
			if($id == 0)
			{
				$data['name'] = $this->input->post('ads_name');
				$data['ads_img'] =$mainimage;
				$data['ads_link'] = $this->input->post('ads_link');
				$data['create_date'] = date('Y-m-d H:i:s');
				$data['status'] = $this->input->post('status');
				$this->templat_model->insert_ads($data);
				redirect('administration/template/ads/0');
			}else{
			if(isset($_FILES['ads_img']['name']) && $_FILES['ads_img']['name'] !='')
			{
				$data['ads_img'] = $mainimage;
				$this->delete_logo($this->input->post('current_logo'),'ads_images');
			}
				$data['name'] = $this->input->post('ads_name');
				$data['ads_link'] = $this->input->post('ads_link');
				$data['create_date'] = date('Y-m-d H:i:s');
				$data['status'] = $this->input->post('status');
				$this->templat_model->update_ads($data, $id);
				redirect('administration/template/ads/0');
			}

		} else {
			if($id == 0)
			{
				$res['result'] = $this->templat_model->get_ads();
				$this->load->view('administration/ads', $res);
			}else{
				$res['row'] = $this->templat_model->get_ads_info($id);
				$this->load->view('administration/ads', $res);
			}
		}
	}
	
	
	public function upload_ads_image($fieldname)
	{	
		  $imgPath = realpath(APPPATH . '../uploads/ads_images');
		  $config['upload_path'] = './uploads/ads_images'; /* NB! create this dir! */
		  $config['allowed_types'] = IMAGE_TYPES;
		  $config['max_size']  = '0';
		  $config['max_width']  = '0';
		  $config['max_height']  = '0';	
		  $config['overwrite'] = FALSE;
		  $this->load->library('upload', $config);
		  $configThumb = array();
		  $configThumb['image_library'] = 'gd2';
		  $configThumb['source_image'] = '';
		  $configThumb['create_thumb'] = TRUE;
		  $configThumb['maintain_ratio'] = FALSE;
		  $configThumb['width'] = 100;
		  $configThumb['height'] = 100;		  
		  $this->load->library('image_lib');
		 
		 if(isset($_FILES[$fieldname]['name']) && $_FILES[$fieldname]['name']!=''){		
				$upload = $this->upload->do_upload($fieldname);									
				if(!$upload){
					return array('error' => $this->upload->display_errors());
				}else{
					$data = $this->upload->data();
				}
			
						
				if($data['is_image'] == 1) {
				  $configThumb['source_image'] = $data['full_path'];
				  $configThumb['new_image'] = $imgPath . '/thumbs/';
				  $this->image_lib->initialize($configThumb);
				  $this->image_lib->resize();
				}						
			}
			$image_name   = (isset($_REQUEST['main_image_hid']))? $_REQUEST['main_image_hid']:''; 
			$story=array();
			$image_to_delete = '';
			if($image_name!='' && isset($data['file_name']) && $data['file_name']!=''){	
				$thumbImg=explode('.',$image_to_delete);	
				$thumbsimage=($thumbImg[0]!='') ? $thumbImg[0].'.'.$thumbImg[1] :'';
				$story = array('mainimage'=>$data['file_name']);									
			}
			elseif($image_name!='' && (!isset($data['file_name']))){			
				$story = array('mainimage'=>$image_name);
			}
			elseif($image_name=='' && isset($data['file_name']) && $data['file_name']!=''){		
				$story = array('mainimage'=>$data['file_name']);
			} 
			return $story;
		
		}

		public function delete_ads($id)
		{
			$row = $this->db->where('id',$id)->get('real_ads')->row();
			unlink('./uploads/ads_images/'.$row->ads_img);
			unlink('./uploads/ads_images/thumbs/'.$row->ads_img);
			$this->db->set('right_bar', 0);
			$this->db->where('right_bar', 'ad-'.$id);
			$this->db->update('real_pages');
			$this->templat_model->delete_ads($id);
			redirect('administration/template/ads/0');
			//redirect($this->config->item('prevous_url'));
			exit;
		}
			// End for Ads///////

		public function add_video(){
			if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '') {
				unset($config);
				$date = date("ymd");
				$configVideo['upload_path'] = './uploads/videos';
				$configVideo['max_size'] = '10240';
				$configVideo['allowed_types'] = 'jpg|avi|flv|wmv|mp3|3gp';
				$configVideo['overwrite'] = FALSE;
				$configVideo['remove_spaces'] = TRUE;
				$video_name = $date.$_FILES['video']['name'];
				$configVideo['file_name'] = $video_name;

				$this->load->library('upload', $configVideo);
				$this->upload->initialize($configVideo);
				if (!$this->upload->do_upload('video')) {
					echo $this->upload->display_errors();
					return 0;
				} else {
					$videoDetails = $this->upload->data();
					return 1;
					//$data['result'] = "Successfully Uploaded";
					//$this->load->view('upload',$data);

				}
			}
	}
		
		public function show_video()
		{
			$this->load->view('upload');
		}
		
	public function ajax_list_siteuser()
	{
	//echo $this->uri->segment(3);
	$type = $this->input->get('type');
		$array = array('delete_status' => 0,
						'role' => '','type' =>3						
						);
						
						if($type == 'admin_setup'){
					$array1 = array('type'=>2);
					$array = array_merge($array,$array1);
					
						}
if(($this->session->userdata('admin_type') == 1) && $type == 'admin_setup'){
			$this->datatables
         ->select("id,name,email,status,updated_date,type", FALSE)
		 ->from('pmod_siteuser')
		 ->where('type',1)
->or_where('type',2);
        echo $this->datatables->generate();		
					
						}
else{
		$this->datatables
         ->select("id,name,email,status,updated_date,type", FALSE)
		 ->from('pmod_siteuser')
		 ->where($array);
        echo $this->datatables->generate();
}
	}
	
	public function ajax_status()
	{
		$status['status']=$this->input->post('status');
		$query=$this->db->update('pmod_siteuser', $status, array('id'=>$this->input->post('id')));
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	}
	
	public function siteuser_add()
	{
		$data['header'] = array('title'=>'Add user');
		$data['up_name'] = $this->siteuser_model->get_profiles();
		$data['companies'] = $this->siteuser_model->get_company_list();
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		//$this->form_validation->set_rules('privilage_profile', 'Profile', 'required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$privilage = array();
			$privilage = $this->input->post('privilage');
			$siteuser['type'] = $this->input->post('user_type');
			$siteuser['name'] = $this->input->post('name');
			$siteuser['middle_name'] = $this->input->post('middle_name');
			$siteuser['last_name'] = $this->input->post('last_name');
			$siteuser['phone_no'] = implode('-',$this->input->post('phone_no'));
			$siteuser['fax_no'] = implode('-',$this->input->post('fax_no'));
			$siteuser['email'] = $this->input->post('email');		
			$siteuser['location'] = $this->input->post('location');
			$siteuser['department'] = $this->input->post('department');	
			$siteuser['password'] = base64_encode($this->input->post('password'));
			$siteuser['assigned_company'] = $this->input->post('company_assigned');
			$siteuser['add_product_status'] = $this->input->post('add_product_status');
			$siteuser['delete_status'] = "0";
			$siteuser['group'] = ($this->input->post('groups')) ? implode(',', $this->input->post('groups')) : '';
			
			if($siteuser['type']==1)
			{			
				$siteuser['privilage'] = 'All';
				$siteuser['profile'] = 0;
			}
			else
			if($siteuser['type']==2)
			{
				$siteuser['profile'] = 1;
				$siteuser['privilage'] = ($this->input->post('privilage_profile')) ? implode(',', $this->input->post('privilage_profile')) : '';	
			}
			$siteuser['created_date'] = date('Y-m-d H:i:s');
			$siteuser['updated_date'] = date('Y-m-d H:i:s');
			
			$validateuser = $this->siteuser_model->check_user_exist($siteuser['email']);
			if($validateuser)
			{
				if($validateuser->delete_status==0)
				{
					echo "The email is already exists please try different email";
				}
				else
				{
					 $user_id = $validateuser->id;
					$this->siteuser_model->insert_update_siteuser($siteuser, $user_id);
					echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.site_url('administration/siteuser').'";
				</script>';
				}
				
			}
			else
			{
				$this->siteuser_model->insert_update_siteuser($siteuser);
				echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.site_url('administration/siteuser').'";
				</script>';
			}
			
		}
		else
		{
			
			$this->load->view('administration/siteuser_add',$data);
		}
	}
	public function chekh_email_id($id='')
	{
		$email = $this->input->post('email');
		$result=$this->siteuser_model->chekh_emailid_model($email,$id);
		
		echo  $result; 
	}
	public function siteuser_edit($id='')
	{
		$data['header'] = array('title'=>'Edit user');
		$data['siteuser']=$this->siteuser_model->get_siteuser_info($id);
		$data['up_name'] = $this->siteuser_model->get_profiles();
		$data['companies'] = $this->siteuser_model->get_company_list();
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$privilage = array();
			$privilage = $this->input->post('privilage');
			$siteuser['name'] = $this->input->post('name');
			$siteuser['middle_name'] = $this->input->post('middle_name');
			$siteuser['last_name'] = $this->input->post('last_name');
			$siteuser['phone_no'] = implode('-',$this->input->post('phone_no'));
			$siteuser['fax_no'] = implode('-',$this->input->post('fax_no'));
			$siteuser['location'] = $this->input->post('location');
			$siteuser['department'] = $this->input->post('department');	
			$siteuser['type'] = $this->input->post('user_type');
			$siteuser['email'] = $this->input->post('email');
			$siteuser['privilage'] = ($this->input->post('privilage_profile')) ? implode(',', $this->input->post('privilage_profile')) : '';
			$siteuser['group'] = ($this->input->post('groups')) ? implode(',', $this->input->post('groups')) : '';			
			$siteuser['assigned_company'] = $this->input->post('company_assigned');
			
			if($this->input->post('password')!='')
			$siteuser['password'] = base64_encode($this->input->post('password'));
			
			$siteuser['status'] = $this->input->post('status');
			$siteuser['add_product_status'] = $this->input->post('add_product_status');
			$siteuser['updated_date'] = date('Y-m-d H:i:s');
			
			$this->siteuser_model->insert_update_siteuser($siteuser,$id);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.site_url('administration/siteuser').'";
				</script>';
		}
		else{	
                    $this->load->view('administration/siteuser_add',$data);
		}
	}
	
	/* public function ajax_profile_delete()
	{
		//$status['delete_status']=1; 
		$this->db->delete('user_profile', array('id' => $this->input->post('id'))); 
		//$query=$this->db->update('pmod_siteuser', $status, array('id'=>$this->input->post('id')));
		log_table('Delete Profile', 'deletion');
		if($this->db->affected_rows()==1)
		{
			echo "done";
		} 
	}	 */
	
	public function ajax_siteuser_delete()
	{
		$status['delete_status']=1; 
		$query=$this->db->update('pmod_siteuser', $status, array('id'=>$this->input->post('id')));
		//log_table('Delete User', 'deletion');
		if($this->db->affected_rows()==1)
		{
			echo "done";
		} 
	}
	
	
public function changepassword()
		{ 
			
			if(!$this->session->userdata('admin_id')){
				redirect('/');
			}		
			$id = $this->session->userdata('admin_id');
			
			$this->form_validation->set_rules('old_passwords', 'Old Password', 'required');
			$this->form_validation->set_rules('new_password', 'New Password', 'required');	
			$this->form_validation->set_rules('re_password', 'Re-Password', 'required|matches[new_password]');
			
			$pww = $this->input->post('old_passwords');
			if(!empty($pww)){
				if($this->session->userdata('current_password')!= 	($this->input->post('old_passwords'))){
					$this->form_validation->set_rules('old_passwords', 'Old Password', 'matches[Existing Password]');
				}
			}
			
			if ($this->form_validation->run() === TRUE)
			{						
				$currentDateTime=($this->input->post('create_date')!='' && $this->input->post('create_date')!='0000-00-00 00:00:00') ? $this->input->post('create_date') :date("Y-m-d : H:i:s", time());

				$data = array(
								'password' => $this->input->post('new_password')									
							);
							
				if($this->siteuser_model->change_password($data, $id)){
					$this->session->set_flashdata('success', 'Your Password has been changed successfully '); 	
					redirect('../administration/siteuser/changepassword');	
				}
							
				/* if($this->input->post('change_password'))
				{
					if($this->members_model->change_password($data, $id)){
						
					}
				} 		
					$this->session->set_flashdata('success', 'Your Password has been changed successfully '); 
					*/
			}
				$this->template->write('title', "Member Change Pasword");
				$data['metaKeywords'] = "Member Change Pasword";		
				$data['member']=$this->siteuser_model->get_siteuser_info($this->session->userdata('admin_id'));	
			
				//$this->template->write_view('content', 'changepassword', $data);
				//$this->template->render();
				$this->load->view('changepassword',$data);

		}

}	
/* End of file test_post.php */
/* Location: ./application/controllers/test_post.php */