<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Admin_model extends CI_Model {       
        function __construct(){
            // Call the Model constructor
            parent::__construct();
            
        }
	/**
	 * _delete function
	 *
	 * @access	protected
	 * @params	integer id, string table name
	 * @return	boolean
	 */
	protected function _delete($id, $table)
	{
		$this->db->where('id', $id);
		return $this->db->delete($table); 
	}
	/**
	 * _insert function
	 *
	 * @access	protected
	 * @params	array data, string table name
	 * @return	boolean
	 */
	protected function _insert($data, $table)
	{
		$this->db->set($data);
		if($this->db->insert($table) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * _update function
	 *
	 * @access	protected
	 * @params	integer id, array data, string table name
	 * @return	boolean
	 */
	protected function _update($id, $data, $table)
	{
		$this->db->where('id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * validate_admin function
	 *
	 * @access	public
	 * @return	array
	 */
	public function validate_admin()
	{
         
	  $this->db->where('email', $this->input->post('username'));
	  $this->db->where('password', ($this->input->post('password')));
	  $this->db->where('status', '1');
	  $query = $this->db->get('pmod_siteuser');	  
	 
	  if($query->num_rows == 1) 
	  {
			return $query->row();
	  }
	  else  
	  { 
			return FALSE;
	  }
	}
	
	public function count_all_property()
	{
		return $this->db->count_all_results($this->config->item('property_listing_table'));
	}	
	
	public function count_active_property()
	{
		$this->db->where('request_status','active');
		return $this->db->count_all_results($this->config->item('property_listing_table'));
	}	
	
	public function count_pending_property()
	{
		$this->db->where('request_status','pending');
		return $this->db->count_all_results($this->config->item('property_listing_table'));
	}	
	
	public function count_cancelled_property()
	{
		$this->db->where('request_status','cancelled');
		return $this->db->count_all_results($this->config->item('property_listing_table'));
	}
	
	public function top_members_by_listings()
	{
		$property_listing_table = $this->config->item('property_listing_table');
		$query = $this->db->query("SELECT posted_by, count(`posted_by`) as result FROM $property_listing_table group by `posted_by` order by result desc limit 5");
		return $query->result();
	}	
	
	public function top_locations_by_listings()
	{
		$property_listing_table = $this->config->item('property_listing_table');
		$query = $this->db->query("SELECT `country`, count(`country`) as result FROM $property_listing_table group by `country` order by result desc limit 5");
		return $query->result();
	}	
	
	public function top_marketvalue_listings()
	{
		$property_listing_table = $this->config->item('property_listing_table');
		$query = $this->db->query("SELECT `id`, `property_title`, `current_market_value` FROM $property_listing_table order by current_market_value desc limit 5");
		return $query->result();
	}

/**
 * count_pages function
 *
 * @access	public
 * @return	integer
 */
	public function count_pages()
	{
		return $this->db->count_all_results($this->config->item('pages_table'));
	}
/**
 * list_pages function
 *
 * @access	public
 * @param	integer limit, integer offset
 * @return	array
 */
	
	
/**
 * count_admins function
 *
 * @access	public
 * @return	integer
 */
	public function count_admins()
	{
		return $this->db->count_all_results($this->config->item('admin_table'));
	}
/**
 * list_admins function
 *
 * @access	public
 * @param	integer limit, integer offset
 * @return	array
 */
	public function list_admins($limit, $offset)
	{
		
		$query = $this->db->get($this->config->item('admin_table'));
		return $query->result();
	}
	/**
	 * insert_update_admins function
	 *
	 * @access	public
	 * @params	array data, integer id
	 * @return	boolean
	 */
	
	public function insert_update_admins($data, $id=NULL)
	{
		if($id)
			return $this->_update($id, $data, $this->config->item('admin_table'));
		else
			return $this->_insert($data, $this->config->item('admin_table'));
	}
/**  
 * get_admins_info function
 *
 * @access	public
 * @param	integer id
 * @return	array
 */
	
	public function get_admins_info($id, $code='')
	{
			$this->db->where('id',$id);
			
		if( $query = $this->db->get($this->config->item('admin_table')) )
		{
			
			if ($query->num_rows() > 0)
			{
				return $query->row();
			}
		}
		return FALSE;
	}
/**
 * delete_admins function
 *
 * @access	public
 * @param	integer id
 * @return	boolean
 */
	
	public function delete_admins($id)
	{
		return $this->_delete($id, $this->config->item('admin_table'));
	}


}