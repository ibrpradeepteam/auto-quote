<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Company_model extends CI_Model {

	/**
	 * _delete function
	 *
	 * @access	protected
	 * @params	integer id, string table name
	 * @return	boolean
	 */
	protected function _delete($id, $table)
	{
		$this->db->where('id', $id);
		return $this->db->delete($table); 
	}
	/**
	 * _insert function
	 *
	 * @access	protected
	 * @params	array data, string table name
	 * @return	boolean
	 */
	protected function _insert($data, $table)
	{
		$this->db->set($data);
		if($this->db->insert($table) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * _update function
	 *
	 * @access	protected
	 * @params	integer id, array data, string table name
	 * @return	boolean
	 */
	protected function _update($id, $data, $table)
	{
		$this->db->where('id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	public function add_update_company(){
		$post = $this->input->post();
		$file_types = $this->config->item('image_file_types');				
		$logo_name = do_upload($file_types,'logo','docs');
		if(isset($post['company_id']) && $post['company_id']!="{company_id}")
		{
		$company_detail = $this->get_data_company($post['company_id']);
		$stored_image =  isset($company_detail[0]['logo']) ? $company_detail[0]['logo'] : NULL;	
		}
		$logo= isset($logo_name['file_name']) ? $logo_name['file_name'] : $stored_image;	
		$tbl_data = array(
			'name'					=> $this->input->post('name'),
			'address' 				=> $this->input->post('address'),
			'default_shippig_text' 	=> $this->input->post('default_shippig_text'),
			'footer_text' 			=> $this->input->post('footer_text'),
			'cover_text_1' 			=> $this->input->post('cover_text_1'),
			'cover_text_2' 			=> $this->input->post('cover_text_2'),
			'logo' 					=> $logo,
			'created_on' 			=> date('Y-m-d H:i:s'),
			'created_by_id' 		=> $this->session->userdata('admin_id')
		);
		if(isset($post['company_id']) && $post['company_id']!="{company_id}")
				{
			return $this->db->update('company',$tbl_data, array('id'=>$post['company_id']));
		}	
		else{	
		return $this->_insert($tbl_data, 'company');
		}
	}
	
	public function add_update_customers_company(){
		$post = $this->input->post();
		
		if(isset($post['customer_company_id']) && $post['customer_company_id']!="{customer_company_id}")
		{
		$company_detail = $this->get_data_customer_company($post['customer_company_id']);
		}
		
		$tbl_data = array(
			'company_name' => $this->input->post('company_name'),
			'company_address' => $this->input->post('company_address'),
			'created_on' => date('Y-m-d H:i:s'),
			'created_by' => $this->session->userdata('admin_id')
		);
		if(isset($post['customer_company_id']) && $post['customer_company_id']!="{customer_company_id}")
				{
			
			 $this->db->update('customer_company',$tbl_data, array('customer_company_id'=>$post['customer_company_id']));
			return "updated";
		}	
		else{	
		 $this->_insert($tbl_data, 'customer_company');
		 
		$insert_id = $this->db->insert_id();
		 //$value=$this->session->set_userdata(array('customer_company_id_session' => $insert_id));	
					$this->db->where('customer_company_id', $insert_id);
		            $insert_id1  =$this->db->get('customer_company') ;   
					
		   return $insert_id1->row();
		
		}
	}
	
public function get_data_company($id=''){
if($this->session->userdata('admin_type') == 2){
// get assigned company
$this->db->select('assigned_company');
$this->db->from('pmod_siteuser');
$this->db->where('id',$this->session->userdata('admin_id'));
$query = $this->db->get();
$company = $query->row();
$company_assigned = $company->assigned_company;
}
		//echo $this->session->userdata('admin_id');
        $this->db->select('*');
         $this->db->from('company');
		 $this->db->where('is_deleted',0);
		 if($id!='')
		 {
		 $this->db->where('id',$id);	 
		 }
if(isset($company_assigned) && $company_assigned != '')
{
$this->db->where('id',$company_assigned);	 
}
			    $query = $this->db->get();
		        $row = $query->result_array();
               return $row;  
	}
	
	public function get_data_customer_company($id=''){
		
         $this->db->select('*');
         $this->db->from('customer_company');
		 $this->db->where('is_deleted',0);
		 if($id!='')
		 {
		 $this->db->where('customer_company_id',$id);	 
		 }
			$query = $this->db->get();
		        $row = $query->result_array();
				
               return $row;   
	}
	
	
	public function add_update_customer(){
		$post = $this->input->post();
		
		if(isset($post['customer_id']) && $post['customer_id']!="{customer_id}")
		{
		$customer_detail = $this->get_data_customer($post['customer_id']);
		}
		
		$tbl_data = array(
			'customer_f_name' => $this->input->post('customer_f_name'),
			'customer_l_name' => $this->input->post('customer_l_name'),
			'company_id' => $this->input->post('company_id'),
			'phone_number' => $this->input->post('phone_number'),
			'email_add' => $this->input->post('email_add'),
			'street_add' => $this->input->post('street_add'),
			'suburb' => $this->input->post('suburb'),
			'state' => $this->input->post('state'),
			'post_code' => $this->input->post('post_code'),
			'created_on' => date('Y-m-d H:i:s'),
			'created_by' => $this->session->userdata('admin_id'),
			'title' => $this->input->post('title'),
			'work_phone' => $this->input->post('work_phone'),
			'owner' => $this->input->post('owner')
		);
		if(isset($post['customer_id']) && $post['customer_id']!="{customer_id}")
				{
				//print_r($tbl_data); 
				//echo $post['customer_id'];
				//die;
				$this->session->set_userdata(array('edited_customer'=>$post['customer_id']));
			return $this->db->update('customer_detail',$tbl_data, array('customer_id'=>$post['customer_id']));
		}	
		else{	
		return $this->_insert($tbl_data, 'customer_detail');
		}
	}
	
	public function get_data_customer($id=''){
	$this->db->select('customer_detail.customer_id,
	customer_detail.customer_f_name,
	customer_detail.customer_l_name,
	customer_detail.company_id,
	customer_detail.phone_number,
	customer_detail.email_add,
	customer_detail.street_add,
	customer_detail.suburb,
	customer_detail.state,
	customer_detail.work_phone,
	customer_detail.owner,
	customer_detail.post_code,
	customer_company.company_name');
	$this->db->from('customer_detail');
	$this->db->join('customer_company','customer_detail.company_id = customer_company.customer_company_id');
	$this->db->where('customer_detail.is_deleted',0);
	if($id!='')
	{
	$this->db->where('customer_id',$id);
	}
	$query = $this->db->get();
	$result = $query->result();
	return $result;
	}
	
	
}