<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Profile_model extends CI_Model {


	protected function _delete($id, $table)
	{
		$this->db->where('id', $id);
		return $this->db->delete($table); 
	}
	/**
	 * _insert function
	 *
	 * @access	protected
	 * @params	array data, string table name
	 * @return	boolean
	 */
	protected function _insert($data, $table)
	{
		$this->db->set($data);
		if($this->db->insert($table) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * _update function
	 *
	 * @access	protected
	 * @params	integer id, array data, string table name
	 * @return	boolean
	 */
	protected function _update($id, $data, $table)
	{
		$this->db->where('id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * validate_admin function
	 *
	 * @access	public
	 * @return	array
	 */
	
	/*public function list_siteuser($limit, $offset, $sort_by, $sort_order)
	{
		$sort_order = ($sort_order == 'asc') ? 'asc' : 'desc';
		$sort_columns = array('name', 'id');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'id';
		
		$this->db->limit($limit, $offset);
		$this->db->where('delete_status','0');
		$this->db->from('pmod_siteuser');
		$this->db->order_by($sort_by, $sort_order);
		
		
		$query = $this->db->get();
		return $query->result();
	}	

	
	public function count_siteuser()
	{
		
		return $this->db->count_all_results('pmod_siteuser');
	}*/
	public function get_profile_info($id)
	{	
	  $siteuser_table = 'user_profile';
	  $this->db->select("*");	
	  $this->db->where("$siteuser_table.up_id", $id);	  
	  $query = $this->db->get('user_profile');
	  
	  if($query->num_rows == 1) 
	  {
			return $query->row();
	  }
	  else 
	  {
			return FALSE;
	  }
	}
	public function insert_update_profile($data, $id='')
	{
		
		if($id!='')
		{
			$this->db->update('user_profile',$data, array('up_id'=>$id));
			//log_table('edit_profile', 'upadtion');
		}
		else
		{
			$this->db->insert('user_profile',$data);
			//log_table('add_profile', 'insertion');
		}
	}	
	public function check_profile_exist($up_name)
	{
	  $this->db->where('up_name', $up_name);	 
	  $query = $this->db->get('user_profile');	
	 /* echo $query->num_rows;*/
		if($query->num_rows) 
	  {
			return TRUE;
	  }
	  else  
	  { 
			return FALSE;
	  }  
	}
	
}