<?php
class Setting_model extends CI_Model {

public function get_smtp($id='')
{
	//echo "hello";
	
	$this->db->select('id,smtp_host,smtp_port,smtp_secure,smtp_user,smtp_pass,smtp_from_name,smtp_from');
	$this->db->from('setting');
	$this->db->where('status',1);
	$this->db->where('type','smtp');
	if($id!='')
	{
		$this->db->where('id', $id);
	}
	$query = $this->db->get();
   
	$result = $query->row_array();
	//print_r($result);
	return $result;
}

 public function save_setting(){
	$post = $this->input->post();
	$smtp['smtp_host'] = $post['smtp_host'];
	$smtp['smtp_port'] = $post['smtp_port'];
	$smtp['smtp_secure'] = $post['smtp_secure'];
	$smtp['smtp_user'] = $post['smtp_user'];
	$smtp['smtp_pass'] = $post['smtp_password'];
	$smtp['smtp_from_name'] = $post['mail_from_name'];
	$smtp['smtp_from'] = $post['mail_from'];
	
	$this->db->update('setting',$smtp,array('id' => $post['smtp_id']));
	return 1;
 }

}
?>