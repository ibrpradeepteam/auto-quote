<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Siteuser_model extends CI_Model {

	/**
	 * _delete function
	 *
	 * @access	protected
	 * @params	integer id, string table name
	 * @return	boolean
	 */
	protected function _delete($id, $table)
	{
		$this->db->where('id', $id);
		return $this->db->delete($table); 
	}
	/**
	 * _insert function
	 *
	 * @access	protected
	 * @params	array data, string table name
	 * @return	boolean
	 */
	protected function _insert($data, $table)
	{
		$this->db->set($data);
		if($this->db->insert($table) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * _update function
	 *
	 * @access	protected
	 * @params	integer id, array data, string table name
	 * @return	boolean
	 */
	protected function _update($id, $data, $table)
	{
		$this->db->where('id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * validate_admin function
	 *
	 * @access	public
	 * @return	array
	 */
	
	
	
	
	public function list_siteuser($limit, $offset, $sort_by, $sort_order)
	{
		$sort_order = ($sort_order == 'asc') ? 'asc' : 'desc';
		$sort_columns = array('name', 'id');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'id';
		
		$this->db->limit($limit, $offset);
		$this->db->where('delete_status','0');
		$this->db->from('pmod_siteuser');
		$this->db->order_by($sort_by, $sort_order);
		
		
		$query = $this->db->get();
		return $query->result();
	}	

	
	public function count_siteuser()
	{
		
		return $this->db->count_all_results('pmod_siteuser');
	}
	
	public function list_admins($limit, $offset, $sort_by, $sort_order)
	{
		$sort_order = ($sort_order == 'asc') ? 'asc' : 'desc';
		$sort_columns = array('name', 'id');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'id';
		
		$this->db->limit($limit, $offset);
		$this->db->where('delete_status','0');
		$this->db->where('type',2);
		$this->db->from('pmod_siteuser');
		$this->db->order_by($sort_by, $sort_order);
		
		
		$query = $this->db->get();
		return $query->result();
	}
	
	public function count_admins()
	{
		$this->db->where('type',2);
		$this->db->from('pmod_siteuser');
		return $this->db->count_all_results();
	}
	
	
	
	
	//OLD
	public function get_property_info($id)
	{
			$this->db->where('id',$id);
			
		if( $query = $this->db->get($this->config->item('property_listing_table')) )
		{
			
			if ($query->num_rows() > 0)
			{
				return $query->row();
			}
		}
		return FALSE;
	}
	
	public function insert_update_property_listings($data, $id=NULL)
	{
		if($id)
			
			return $this->_update($id, $data, $this->config->item('property_listing_table'));
		else
			$this->_insert($data, $this->config->item('property_listing_table'));
			return $this->db->insert_id();
	}
	
	public function insert_update_realestate_valuation($data, $id=NULL)
	{
	
		$this->db->where('property_id',$id);
		$query = $this->db->get('real_estate_valuation');
		if ($query->num_rows() > 0)
		{
			$this->db->where('property_id', $id);
			return $this->db->update('real_estate_valuation', $data); 

		}else{

			return $this->db->insert('real_estate_valuation', $data); 
		}
	}
	
	public function get_realestate_valuation_info($id)
	{
		$this->db->where('property_id',$id);
			
		if( $query = $this->db->get($this->config->item('realestate_valuation_table')))
		{
			
			if ($query->num_rows() > 0)
			{
			
				return $query->row();
			}
		}
		return FALSE;
	}
	
	
	
	public function delete_property_listing($id)
	{
		$this->_delete($id, $this->config->item('property_listing_table'));
		return $this->db->delete($this->config->item('realestate_valuation_table'), array('property_id' => $id)); 
	}
	
	public function delete_mainimage($id,$data)
	{
		
		return $this->_update($id, $data, $this->config->item('property_listing_table'));
		
	}
	
	
	/**
	 * insert_update_pages function
	 *
	 * @access	public
	 * @params	array data, integer id
	 * @return	boolean
	 */
	
	public function insert_update_pages($data, $id=NULL)
	{
		if($id)
			return $this->_update($id, $data, $this->config->item('pages_table'));
		else
			return $this->_insert($data, $this->config->item('pages_table'));
	}
	
/**
 * delete_page function
 *
 * @access	public
 * @param	integer id
 * @return	boolean
 */
	
	public function delete_page($id)
	{
		return $this->_delete($id, $this->config->item('pages_table'));
	}

/**
 * count_admins function
 *
 * @access	public
 * @return	integer
 */
	/*public function count_admins()
	{
		return $this->db->count_all_results($this->config->item('admin_table'));
	}*/
/**
 * list_admins function
 *
 * @access	public
 * @param	integer limit, integer offset
 * @return	array
 */
	/*public function list_admins($limit, $offset)
	{
		
		$query = $this->db->get($this->config->item('admin_table'));
		return $query->result();
	}*/
	/**
	 * insert_update_admins function
	 *
	 * @access	public
	 * @params	array data, integer id
	 * @return	boolean
	 */
	

/**  
 * get_admins_info function
 *
 * @access	public
 * @param	integer id
 * @return	array
 */
	
	
/**
 * delete_admins function
 *
 * @access	public
 * @param	integer id
 * @return	boolean
 */
	


	public function count_records($table){		
		$total;
		foreach($table as $tablename){			
			$total[]=$this->db->count_all_results($this->config->item($tablename));			
		}
		return $total;
	}
	
	public function count_activerecords($table){		
		$active;
		foreach($table as $tablename){						
			$this->db->where('status',1);
			$active[]=$this->db->count_all_results($this->config->item($tablename));
		}
		return $active;
	}
	
	public function count_inactiverecords($table){		
		$inactive;
		foreach($table as $tablename){									
			$this->db->where('status',0);
			$inactive[]=$this->db->count_all_results($this->config->item($tablename));
		}

		return $inactive;		
	}
	
	public function count_assetrecords($table){		
			$asset;												
			$asset[]=$this->db->count_all_results($this->config->item($table));		

		return $asset;		
	}

	public function count_updatesrecords($table){		
			$asset;												
			$asset[]=$this->db->count_all_results($this->config->item($table));		

		return $asset;		
	
	}
	
	public function get_story_list(){		
		$this->db->select('acms_articles.id,acms_articles.articles_content');
		$this->db->from('acms_articles');
		//$this->db->limit(5);
		$this->db->order_by('id','desc');			
		$this->db->where('status',1);
		$this->db->where('main_image','');
		$query = $this->db->get();  						
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		
	}
	
	/* By Sunil Chouhan */
	public function get_siteuser_info($id)
	{	
	  $siteuser_table = $this->config->item('siteuser_table');
	  $this->db->select("*");	
	  $this->db->from('pmod_siteuser');
	  $this->db->where("id", $id);	  
	  $query = $this->db->get();
	  
	  if($query->num_rows == 1) 
	  {
			return $query->row();
	  }
	  else 
	  {
			return FALSE;
	  }
	}
	public function get_user_groups($id){		
	  $this->db->select("pmod_siteuser.id,pmod_siteuser.group");	
	  $this->db->from('pmod_siteuser');
	  $this->db->where("pmod_siteuser.id", $id);	  
	  $query = $this->db->get();	
	  //print_r($query->result());  
	  if($query->num_rows == 1) 
	  {
			return $query->row();
	  }
	  else 
	  {
			return FALSE;
	  }	
	}
	public function chekh_emailid_model($data, $id=''){
	  if($id!=''){
		  $this->db->select('email');
		  $this->db->from('pmod_siteuser');
		   $this->db->where('id !=', $id);
		   $this->db->where('email', $data);
		 $count = $this->db->get();
	 $this->db->last_query();
	 $row=$count->row('email');
	 
		  if($row)
		 {
			echo "true";
		 }
		 else{
		 echo "false";
		 }
	  }
	  
	}
	public function insert_update_siteuser($data, $id=''){
	  if($id!=''){
		$this->db->update('pmod_siteuser',$data, array('id'=>$id));
		$user_id = $id;
		//log_table('edit_siteuser', 'updation'); 
	  }else{
		$this->db->insert('pmod_siteuser',$data);
		$user_id = $this->db->insert_id();
		//log_table('add_siteuser', 'insertion');
	  }
	 /* if($this->input->post('user_type')==3){
		$this->save_user_location($user_id, $this->input->post('location'));  
	  }elseif($this->input->post('user_type')==4){
		$this->save_user_department($user_id, $this->input->post('department'));  
	  }*/
	  
	}
	public function save_user_department($user, $departments){
	  $this->db->delete('department_manager', array('manager_id' => $user));			
	  if(!empty($departments)) {
		foreach ($departments as $value) {
			$tbl = array('department_id' => $value, 'manager_id' => $user);
			$this->db->insert('department_manager', $tbl);
		}			
	  }
	}
	public function save_user_location($user, $locations){
	  $this->db->delete('location_manager', array('manager_id' => $user));						
	  if(!empty($locations)) {			
		foreach ($locations as $value) {
			$tbl = array('location_id' => $value, 'manager_id' => $user);
			$this->db->insert('location_manager', $tbl);
		}			
	  }
	}
	public function check_user_exist($email)
	{
		 $this->db->where('email', $email);		 	 
	  $query = $this->db->get('pmod_siteuser');	
	 /* echo $query->num_rows;*/
	  if($query->num_rows) 
	  {
			$result = $query->row();
			return $result;
	  }
	  else  
	  { 
			return FALSE;
	  }  
	}
	public function check_profile_exist($up_name)
	{
		 $this->db->where('up_name', $up_name);	 
	  $query = $this->db->get('user_profile');	
	 /* echo $query->num_rows;*/
		if($query->num_rows) 
	  {
			return TRUE;
	  }
	  else  
	  { 
			return FALSE;
	  }  
	}
	public function get_profiles()
	{
		 return $this->db->get('user_profile')->result();
		
		 
	}
	public function change_password($data, $id=NULL)
	{		
		
		if($id){
			$this->_update($id, $data, 'pmod_siteuser');
			//log_table('change_password', 'updation'); 
			return true;
		}
		else{
			return FALSE;
		}

		
	}
	
	public function get_company_list(){
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where('is_deleted',0);
		$this->db->order_by('name');
		$query = $this->db->get();
		return $query->result();	
	}
}