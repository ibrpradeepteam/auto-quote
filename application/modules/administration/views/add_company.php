<link rel="stylesheet" type="text/css" href="{base_url}css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="{base_url}css/auto_quote.css">
{scripts}
<script src="{base_url}ng-table/ng-table.js"></script>
<script>
var company_details = '{company_detail}';
</script>
<div ng-app="add_company">
<div class="row-fluid" ng-controller="CompanyCtrl" ng-init="init(company_details)">
	<form action="{base_url}administration/company/add_company/" method="post" enctype="multipart/form-data">
        <div class="row-fluid">
        <input type="hidden" name="company_id" value="{company_id}" />
            <label>Company Name</label>
            <input type="text" name="name" class="form-control span12" ng-model="company.name" required="required">
        </div>
        <div class="row-fluid">
            <label>Address</label>
            <textarea name="address" rows="3" class="form-control span12" ng-model="company.address" required="required"></textarea>
        </div>
        <div class="row-fluid">
            <label>Default Shipping Text</label>
            <textarea name="default_shippig_text" class="form-control span12" required="required">{{company.default_shippig_text}}</textarea>
            <!--<input type="number" name="default_shippig_text" class="form-control span12" value="{{company.default_shippig_text}}" required="required">-->
        </div>
        
        <div class="row-fluid">
            <label>Page Footer Text </label>
           <input type="text" name="footer_text" class="form-control span12" ng-model="company.footer_text" required="required">
        </div>
        
        <div class="row-fluid">
            <label>Cover Page, Paragraph 1 Text</label>
            <textarea name="cover_text_1" class="form-control span12" required="required">{{company.cover_text_1}}</textarea>
            <!--<input type="text" name="cover_text_1" class="form-control span12" ng-model="company.cover_text_1" required="required">-->
        </div>
        
        <div class="row-fluid">
            <label>Cover Page, Paragraph 2 Text</label>
            <textarea name="cover_text_2" class="form-control span12" required="required">{{company.cover_text_2}}</textarea>
            <!--<input type="text" name="cover_text_2" class="form-control span12" ng-model="company.cover_text_2" required="required">-->
            
        </div>
        
        <div class="row-fluid">
            <label>Logo</label>
            <div ng-if="company.logo">
            <img ng-src="{base_url}/uploads/docs/{{company.logo}}" width="100px" height="100px"/>
             <input type="file" name="logo" class="span12"  >
            </div>
            
          <div ng-if="!company.logo">
            <input type="file" name="logo" class="span12"  required>
          </div>
        </div>
        <div class="row-fluid top10">    	
            <input type="submit" name="add_company" value="Submit" class="btn btn-primary">
        </div>
    </form>
</div>
</div>