{styles}
{scripts}
<script type="text/javascript">

function close_pop_up(){
    parent.$.fancybox.close();
}
var details = '{customer_detail}';
 $(document).ready(function () { 
$('.chosen-choices').css('padding','5px');
});
function add_new_company(value){
if(value == "add")
{
url = base_url+'administration/customer/add_customer_company';
viewFancybox(url,300,300);
}
}
function myListdata(varid)
{
$("#myList").trigger("chosen:updated");
$('select').val(varid);
$('select').trigger("chosen:updated");
}

</script>


<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<h2> {title} </h2>
<div ng-app="add_cust">
<form action="{base_url}administration/customer/add_customer" method="post" >
<div class="col-md-12" ng-controller="AddCustCtrl" ng-init="init(details)">
							
							<div class="row">
                                <div class="col-md-2" style="width:70%;">
                                    <label>Company Name<span class="red-cl">*</span></label>
                                   <!-- <input type="text" class="form-control input-sm" name="company_id" id="company_name" required="required" ng-model="customer.company_name"> -->
<?php
								   if($company_id == ''){
								  
								   ?>
                                   <select class="form-control input-sm chzn-select" id="myList" name="company_id" ng-model="customer.company_id"  onchange="add_new_company(this.value)" readonly>
                                   <option ></option>
                                   <option value="add"> --Add new--</option>
								   
                                   <?php
                                   foreach($customer_companies as $cc)
                                   {
                                   ?>
								   <option  value="<?php echo $cc->customer_company_id ?>" <?php if(($this->session->userdata('customer_company_id_session') == $cc->customer_company_id)){  echo "selected='selected' "  ;   } if(isset($customer_id)){ if($customer_id == $cc->customer_company_id) { echo "selected = 'selected'"; }}?> >
<?php echo $cc->company_name ?></option>
                                                                      <?php
                                   }
								   
                                   ?>
                                   </select>
								  
								   <?php
								   }
								   if(isset($company_id) && $company_id != ''){
								    //echo $company_id;
								   foreach($customer_companies as $cc)
                                   {
								   if($company_id == $cc->customer_company_id){
								   ?>
								   <br>
								   <input type="text" name="company_id_view" value="<?php echo $cc->company_name; ?>" readonly />
								   <input type="hidden" name="company_id" value="<?php echo $cc->customer_company_id; ?>" readonly />
								  <?php
								  }
								  }
								  }
								  ?>
                                </div>
                                </div>
                                 <?php  echo $this->session->unset_userdata('customer_company_id_session');?>
                            <div class="row">
                               <div class="col-md-2" style="width:70%;">  
                               <input type="hidden" name="customer_id" value="{customer_id}" />
                                   <label>First Name<span class="red-cl">*</span></label>
                           <input type="text" class="form-control" name="customer_f_name" value=""
						   id="customer_f_name" required="required"  ng-model="customer.customer_f_name" >
                             
                              </div>  
                                </div>
                                
                                <div class="row">
                                <div class="col-md-2" style="width:70%;">
                                    <label>Last Name<span class="red-cl">*</span></label>
                                   
                                    <input type="text" class="form-control input-sm" name="customer_l_name" id="customer_l_name" required="required" ng-model="customer.customer_l_name" >
                                </div>
                                </div>
                                
                                

                               <!-- <div class="row">
                                <div class="col-md-2" style="width:70%;">
                                    <label>Title<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="title" id="title" required="required" ng-model="customer.title">
                                </div>
                                </div> -->
                                
                                <div class="row">
                                <div class="col-md-2" style="width:70%;">
                                    <label>Primary  Phone<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="phone_number" id="phone_number" required="required" ng-model="customer.phone_number">
                                </div>
                                </div>
                                
							
                                <div class="row">
                                <div class="col-md-2" style="width:70%;">
                                    <label>Other Phone<span class="red-cl"></span></label>
                                    <input type="text" class="form-control input-sm" name="work_phone" id="work_phone"  ng-model="customer.work_phone">
                                </div>
                                </div>
                                
                                <div class="row">
                                <div class="col-md-2" style="width:70%;">
                                    <label>Email Address<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="email_add" id="email_add" required="required" ng-model="customer.email_add">
                                </div>
                                </div>
                                
                                <div class="row">
                                <div class="col-md-2" style="width:70%;">
                                    <label>Street Address</label>
                                    <input type="text" class="form-control input-sm" name="street_add" id="street_add" ng-model="customer.street_add">
                                </div>
                                </div>
                            
                           
                            <div class="row">
                            <div class="col-md-1" style="width:70%;">
                                    <label>Suburb<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="suburb" id="suburb" ng-model="customer.suburb">
                                </div>
                                </div>
                                
                              <div class="row">
                                <div class="col-md-1" style="width:70%;">
                                    <label>State<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="state" id="state" ng-model="customer.state">
                                </div>
                                </div>
                                
                                <div class="row">
                                <div class="col-md-2" style="width:70%;">
                                    <label>Post Code<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="post_code" id="post_code" ng-model="customer.post_code">
                                </div>
                                 </div>
                                 
                                <div class="row">
                                <div class="col-md-2" style="width:70%;">
                                    <label>Owner<span class="red-cl"></span></label>
                                    <input type="text" class="form-control input-sm" name="owner" id="owner"  value="<?php echo $this->session->userdata('admin_fullname') ?>" readonly>
                                </div>
                                </div>
                               
                                
                               <div class="row"> 
                                <div class="col-md-3" style="margin-top:20px;">
                                  
                                   
                                        <input type="submit" name="add_customer" value="Submit" class="btn btn-danger" />
                                  
                                </div>
                                </div>
                                
                                
                            </div>
                  </form>
                  </div>          

</body>