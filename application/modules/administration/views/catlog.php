<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catlog  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/
	  <method_name> 
 	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->require_admin_login();
		$this->load->helper('my_helper');
		$this->load->model('catlog_model');
		$this->load->library('pagination');  
		$this->load->helper('date');
		$this->load->helper('url');
		$this->load->model('files_model');
		//$this->load->library('customclass');
	}
	
	public function browse($type)
	{
		$limit = 25;
		$total = $this->catlog_model->count_catlog($type); 
		
		$data['admins'] = $this->catlog_model->list_catlog($limit, 0, $type, '');
		$data['object'] = $this;
		
		$config['base_url'] = base_url('administration/catlog');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		
		$data['total'] = $total;
		$this->pagination->initialize($config);
		if(check_privilage())
		{
			$this->load->view('administration/catlog_main', $data);
		}
		else
		{
			$this->load->view('administration/permission', $data);
		}
	}
	
	public function index()
	{
		$this->browse('1');	
	}
	
	public function ajax_list_catlog($type)
	{
		$array = array('rq_rqf_quote_catlogs.catlog_type' => $type);
		$this->datatables->select("rq_rqf_quote_catlogs.id, rq_rqf_quote_catlog_type.catlog_type, rq_rqf_quote_catlogs.catlog_value, rq_rqf_quote_catlogs.status, rq_rqf_quote_catlogs.module_name", FALSE);
		$this->datatables->from('rq_rqf_quote_catlogs');
		$this->datatables->where($array);
		$this->datatables->join('rq_rqf_quote_catlog_type', 'rq_rqf_quote_catlogs.catlog_type = rq_rqf_quote_catlog_type.id', 'inner');
        echo $this->datatables->generate();
		
	}
	
	//ADD
	public function catlog_add()
	{
		$data['header'] = array('title'=>'Add Catlog');
		
		$this->form_validation->set_rules('cat_f', 'Type', 'required');
		$this->form_validation->set_rules('catlog_value', 'Value', 'required');
		
		if ($this->form_validation->run() === TRUE)
		{
			//echo $catlog['catlog_type'] = implode(" ", $this->input->post('cat_f'));
			$catlog['catlog_type'] = $this->input->post('cat_f');
			$catlog['catlog_value'] = $this->input->post('catlog_value');
			
			
			
			$config['upload_path'] = './uploads/companylogo/';
		$config['allowed_types'] = 'gif|JPEG|PNG|doc|docx|pdf|txt|jpeg|jpg';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('userfile')) {
                $data['message'] = $this->upload->data();                       
            } else {
                $data['error'] = $this->upload->display_errors('<div class="alert alert-error">', '</div>');                
            }
		
		if(!isset($data['error']))
		{
		
		$catlog['Company_logo'] = $data['message']['file_name'];
		}
		else
		{
		echo $data['error'];
		die();
		}
     
			$catlog['module_name'] = 'policy_issue';
			$catlog['status'] = 1;
			$this->catlog_model->insert_update_catlog($catlog);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$catlog['catlog_type']).'";
				</script>';
		}
		else
		{
			$this->load->view('administration/catlog_add',$data);
		}
	}
	
	//Edit
	public function catlog_edit($id='')
	{
		$data['header'] = array('title'=>'Edit Catlog');
		$data['catlog']=$this->catlog_model->get_catlog_info($id);
		
		$this->form_validation->set_rules('cat_f', 'Type', 'required');
		$this->form_validation->set_rules('catlog_value', 'Value', 'required');
		
		if ($this->form_validation->run() === TRUE)
		{
			//$catlog['catlog_type'] = implode(" ", $this->input->post('cat_f'));
			$catlog['catlog_type'] = $this->input->post('cat_f');;
			$catlog['catlog_value'] = $this->input->post('catlog_value');
			$catlog['module_name'] = 'policy_issue';
			
			$catlog['status'] = $this->input->post('status');
			
			$config['upload_path'] = './uploads/companylogo/';
		$config['allowed_types'] = 'gif|JPEG|PNG|doc|docx|pdf|txt|jpeg|jpg';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('userfile')) {
                $data['message'] = $this->upload->data();                       
            } else {
                $data['error'] = $this->upload->display_errors('<div class="alert alert-error">', '</div>');                
            }
		
		if(!isset($data['error']))
		{
		
		$catlog['Company_logo'] = $data['message']['file_name'];
		}
		else
		{
		echo $data['error'];
		die();
		}
			
			$this->catlog_model->insert_update_catlog($catlog,$id);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$catlog['catlog_type']).'";
				</script>';
		}
		else
		{
			$this->load->view('administration/catlog_edit',$data);
		}
	}
	
	//DELETE
	public function ajax_catlog_delete()
	{
		$this->db->where(array('id'=>$this->input->post('id')));
		$query=$this->db->delete('rq_rqf_quote_catlogs'); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	}
	
	public function delete_company_logo()
	{
		$update_data['Company_logo'] = '';
		$this->db->where(array('id'=>$this->input->post('id')));
		$this->db->update('rq_rqf_quote_catlogs ', $update_data);
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	}
}