<link rel="stylesheet" type="text/css" href="<?= base_url('css/bootstrap.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('css/auto_quote.css'); ?>">
<?php $this->load->view('includes/scripts'); ?>
<?php if(isset($_GET['cat_type']) && ($_GET['cat_type'] > 0) ) { $cat_type_v = $_GET['cat_type']; } else {  $cat_type_v = 0; }

 ?>
<?php
$id	= isset($_POST['catlogId']) ? $_POST['catlogId'] : (isset($catlog->id)?$catlog->id :NULL);
$idpp	= isset($_POST['catlogId']) ? $_POST['catlogId'] : (isset($catlog->id)?$catlog->id :NULL);
$catlog_type	= isset($_POST['catlog_type']) ? $_POST['catlog_type'] : (isset($catlog->catlog_type)?$catlog->catlog_type :$cat_type_v );
$catlog_title	= isset($_POST['cat_title']) ? $_POST['cat_title'] : (isset($catlog->catlog_title)?$catlog->catlog_title :NULL);
$catlog_description	= isset($_POST['cat_description']) ? $_POST['cat_description'] : (isset($catlog->catlog_description)?$catlog->catlog_description :NULL);
$status	= isset($_POST['status']) ? $_POST['status'] : (isset($catlog->status)?$catlog->status : 1);
$title_image	= isset($_POST['titl_image']) ? $_POST['titl_image'] : (isset($catlog->title_image)?$catlog->title_image : NULL);
$add_on	= isset($_POST['addon_keyword']) ? $_POST['addon_keyword'] : (isset($catlog->addon_keyword)?$catlog->addon_keyword : NULL);
$add_on_price	= isset($_POST['price']) ? $_POST['price'] : (isset($catlog->addon_price)?$catlog->addon_price : NULL);

?>
<script type="text/javascript">
function close_pop_up(){
    parent.$.fancybox.close();
}
</script>


<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<div class="row-fluid">
  <div class="content">
  <?php $catlog_name = getCatlogName($cat_type_v);
	$catlog_type_value = getCatlogTypeValue($cat_type_v);
   ?>
    <!-- Underwriter List -->
    <div class="span12">
      <h2><?php echo $header['title']; ?></h2>
      <form action="" method="post" name="frmCatlog" id="frmCatlog"  enctype="multipart/form-data">
          <div class="row-fluid">
            <label>Name <em style="color:#FF0000">*</em></label>
            <?php echo get_active_catlog_dropdown($selected=$cat_type_v,$attr='disabled="disabled"','cat_f_n',$id='cat_f_n',$catlog_type_value= $catlog_type_value); ?>
            <input type="hidden" name="cat_f" value="<?php echo $cat_type_v; ?>">
            <?php echo form_error('insured_state','<span style="color:red"">','</span>'); ?>
          </div>
          
           <?php 
          if($cat_type_v==16){
          ?>
          <div class="row-fluid">
              <div class="span8">
                <label>Addon Keyword <em style="color:#FF0000">*</em></label>
                <?php $managers = isset($managers) ? $managers : ''; ?>
                 <input type="text" name="addon_keyword" id="addon_keyword" value="<?= $add_on; ?>" class="span8" required="required">
              </div>
          </div>  
          
          <div class="row-fluid">
              <div class="span8">
                <label>Price <em style="color:#FF0000">*</em></label>
                <?php $managers = isset($managers) ? $managers : ''; ?>
                 <input type="text" name="price" id="price" value="<?= $add_on_price; ?>" class="span8" required="required">
              </div>
          </div>  
        <?php
        }?>
          
          <div class="row-fluid">
            <label>Title <em style="color:#FF0000">*</em></label>
            <input type="text" name="cat_title" id="title" value="<?= $catlog_title; ?>" class="span8" required>
            <?php echo form_error('catlog_title','<span style="color:red"">','</span>'); ?>
          </div>
          
        <div class="row-fluid">
            <label>Description <em style="color:#FF0000">*</em></label>
           
            <textarea rows="10" cols="40" name="cat_description" id="description" class="span8" required> <?= $catlog_description; ?> </textarea>
            <?php echo form_error('catlog_description','<span style="color:red"">','</span>'); ?>
          </div>
        
        <div class="row-fluid">
            <label>Upload Image <em style="color:#FF0000">*</em></label>
            <?php if($title_image != NULL) {
            $images = explode(',',$title_image);
            foreach($images as $img)
            {
            ?>
            <div style="float:left; margin-left:10px; width:100px; height:100px;">
            <img src="<?php echo base_url('uploads/title_image/'.$img); ?>" width="100px" height="100px" id="<?php echo substr($img,0,-4); ?>" style="width:100px; height:70px;" >
            <img src="<?php echo base_url('css'); ?>/delete.png" style="margin-top:-100px; cursor:pointer; float:right;" onclick="delete_catalog_image(<?php echo $catlog->id; ?>,'<?php echo substr($img,0,-4); ?>')"  title="Delete Image" id="<?php echo substr($img,0,-4); ?>"/>
            </div>
            <?php } } ?>
           <input type="file" name="userfile[]" value="" multiple>
            
          </div>
          
          <div class="row-fluid">
          <label >Status <em style="color:#FF0000">*</em></label>        <input type="radio" name="status" id="status_active" value="1" <?php if ($status==1 || (!isset($catlog->id))) echo 'checked'; ?> />        &nbsp;Active&nbsp;        <input type="radio" name="status" id="status_inactive" value="0" <?php if ($status==0) echo 'checked'; ?> />        &nbsp;Inactive <br />
          </div>
        
        			
        <input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="<?php if(isset($catlog->id)) { echo 'Edit'; } else { echo 'Add';}  ?>" class="btn btn-primary pull-right" style="margin-right:10px" name="add_catlog">
        <div class="clearfix"></div>
      </form>
    </div>
  </div>
</div>
    <script>
    $(document).ready(function(){
        $('html, body').css('min-height', '20px');
    });
    </script>
</body>