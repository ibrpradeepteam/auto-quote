<script>
function deletelogo(id,value)
{
	
	var r=confirm("Are you sure want to delete Company Image ..?");
	if (r==true)
  	{
	$.ajax({
		type: "POST",
		url: "<?php echo base_url().'administration/catlog/delete_company_logo'; ?>",
		data:{id:id,value:value},
		success:function(data){
			
			if(data=="done")
			{
				document.getElementById('filesvalues').innerHTML = ''; 
				
			} 
			
	}});
	}
}
</script>
<?php 
$this->load->view('includes/styles');
$this->load->view('includes/scripts');

$id	= isset($_POST['catlogId']) ? $_POST['catlogId'] : (isset($catlog->id)?$catlog->id :NULL);
$idpp	= isset($_POST['catlogId']) ? $_POST['catlogId'] : (isset($catlog->id)?$catlog->id :NULL);
$catlog_type	= isset($_POST['catlog_type']) ? $_POST['catlog_type'] : (isset($catlog->catlog_type)?$catlog->catlog_type :NULL);
$catlog_value	= isset($_POST['catlog_value']) ? $_POST['catlog_value'] : (isset($catlog->catlog_value)?$catlog->catlog_value :NULL);
$status	= isset($_POST['status']) ? $_POST['status'] : (isset($catlog->status)?$catlog->status :NULL);
$compant_logo	= isset($_POST['Company_logo']) ? $_POST['Company_logo'] : (isset($catlog->Company_logo)?$catlog->Company_logo :NULL);
$catlog_color	= isset($_POST['catlog_color']) ? $_POST['catlog_color'] : (isset($catlog->catlog_color)?$catlog->catlog_color :NULL);
?>
<script type="text/javascript">
function close_pop_up()
{
	parent.$.fancybox.close();
}
</script>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<div class="container">
  <div class="content">
  <?php  $catlog_name = getCatlogName($catlog_type); ?>
    <div class="span3">
      <h2><?php echo $header['title'] ?></h2>
      <form action="" method="post" name="frmCatlog" id="frmCatlog"  enctype="multipart/form-data">
        
		<input type="hidden" name="catlogId" id="catlogId" value="<?php echo $id ?>" />
        
		<label>Catlog Type <em style="color:#FF0000">*</em></label>
         <?php
		  $selected = $catlog_type;
    	echo get_active_catlog_dropdown($selected, $attr='disabled="disabled"','cat_f_n',$id='cat_f_n');
	?>
	<input type="hidden" name="cat_f" value="<?php echo $selected; ?>">
       <!-- <select name="catlog_type" id="catlog_type" class="span3">
			<option value="nature_of_business" <?php echo($catlog_type=='nature_of_business' ? 'selected="selected"': '');?>>Nature of Business</option>
			<option value="trailer_type" <?php echo($catlog_type=='trailer_type' ? 'selected="selected"': '');?>>Trailer Type</option>
			<option value="commodities_haulted" <?php echo($catlog_type=='commodities_haulted' ? 'selected="selected"': '');?>>Commodities Haulted</option>
            <option value="trac_truck_model" <?php echo($catlog_type=='trac_truck_model' ? 'selected="selected"': '');?>>Truck & Tractor Models</option>
            <option value="number_of_axles" <?php echo($catlog_type=='number_of_axles' ? 'selected="selected"': '');?>>Number of Axles</option>
		</select>-->
        <?php echo form_error('insured_state','<span style="color:red"">','</span>'); ?>
        
		<label>Catalog Value<em style="color:#FF0000">*</em></label>
        <input type="text" name="catlog_value" id="catlog_value" value="<?php echo $catlog_value;?>" class="span3">
        <?php echo form_error('catlog_value','<span style="color:red"">','</span>'); ?>
        
        <?php if($catlog_type == 1)
		{
		?>
        <label>Company logo <em style="color:#FF0000">*</em></label>
       <input type="file" name="userfile123" id="userfile123"  value="Company Logo">
        <?php  if($compant_logo != NULL)
		{
		?>
		
        <div id="filesvalues" style="margin-bottom:10px;">
		<?php 
        echo '<br>'.$compant_logo .'&nbsp;&nbsp; '; ?><a onClick="deletelogo('<?php echo $idpp; ?>','<?php echo  $compant_logo;?>');" style="margin-bottom:10px;cursor:pointer;">Delete</a>
		</div>
		<?php 
		
		}
		?>
        <input type="hidden" name="filesyes" id="filesyes" value="1" />
		<?php 
		}
		else
		{
		?>
        <input type="hidden" name="filesyes" id="filesyes" value="0"/>
        <?php 
		}
		?>
        <?php if($catlog_name=='meeting_group') { ?>
		<label>Catlog Color<em style="color:#FF0000">*</em></label>
		<input type="text" name="catlog_color" id="catlog_color"  value="<?php echo $catlog_color; ?>" class="span2">
		<?php echo form_error('catlog_color','<span style="color:red"">','</span>'); ?>
		<?php } ?>
		<label >Status <em style="color:#FF0000">*</em></label>
        <input type="radio" name="status" id="status_active" value="1" <?php if ($status==1) echo 'checked'; ?> />
        &nbsp;Active&nbsp;
        <input type="radio" name="status" id="status_inactive" value="0" <?php if ($status==0) echo 'checked'; ?> />
        &nbsp;Inactive <br />
        <input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="Save" class="btn btn-primary pull-right" style="margin-right:10px">
        <div class="clearfix"></div>
      </form>
    </div>
  </div>
</div>
</body>
