<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');
}

?>
<script type="text/javascript">
$(document).ready(function () {
	<?php if(isset($_GET['cat_type']) && ($_GET['cat_type'] > 0) ) {
	
	 ?>
		load_catlog_list('<?php echo $_GET['cat_type']; ?>');
	<?php  } else { ?>
		load_catlog_list('1');
	<?php } ?>
    
	/* $('.fancybox-add').click(function(){ alert('d');
		//add_catlog_item();
		edit_catlog('9')
	}) */
	
});

function add_catlog_item(){
	var cat_type = $('#cat_f').val();
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url('administration/catlog/catlog_add?cat_type=') ?>'+cat_type,
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '400',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}

function load_catlog_list(type) {
    $("#catlogList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo base_url(); ?>administration/catlog/ajax_list_catlog/"+type,

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [
              
           

            {"sClass": "center"},
            {  
                "mRender": function (data, type, oObj) {
                    var b = oObj[1];
                    var a = b.replace( new RegExp('_', 'g'), ' ' );
                    return (a);

                }

            }, {  
                "mRender": function (data, type, oObj) {
                    var a = oObj[2]+'<span style="background-color:#'+oObj[3]+';width: 16px;height: 16px;display: inline-block;border-radius: 11px;" class="left5"></span>';
                    return (a);

                }

            }, {  
                "mRender": function (data, type, oObj) {
                    var a = oObj[4];
                    if (a == 1) {
                        a = '<img src="<?php echo base_url('images/green-dot.png') ?>" onClick="ajax_status(0,' + oObj[0] + ')" class="cursor-pointer"/>&nbsp;(Active)';
                    } else {
                        a = '<img src="<?php echo base_url('images/red-dot.png') ?>" onClick="ajax_status(1,' + oObj[0] + ')" class="cursor-pointer"/>&nbsp;(Disable)';
                    }
                    return (a);

                }

            }, {  
                "mRender": function (data, type, oObj) {
                    var a = oObj[4];
                    var jobId = oObj[0];
                    a = '<a href="javascript:void(0);" onClick="edit_catlog('+oObj[0]+')" data-toggle="modal"><button class="btn btn- btn-phone-block"><icon class="icon-pencil icon-white"></icon><span class="hidden-phone">Edit</span></button></a>&nbsp;<a href="javascript:void(0)" onClick="catlog_delete('+oObj[0]+')" data-toggle="modal"><button class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><span class="hidden-phone">Delete</span></button></a>';
                    return (a);

                }
            }
        ]
    });

}


$(document).ready(function(){


		load_catlog_list($("#cat_f").val());
	$("#cat_f").change(function(){
		load_catlog_list($(this).val());
	});
	
});

	
function edit_catlog(id)
{	var cat_type = $('#cat_f').val();
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url() ?>administration/catlog/catlog_edit/'+id+'?cat_type='+cat_type,
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '350',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}

function catlog_delete(id)
{ 	var cat_type = $('#cat_f').val();
	var r=confirm("Are you sure want to delete catlog ..?");
	if (r==true)
  	{
	$.ajax({
		type: "POST",
		url: "<?php echo base_url().'administration/catlog/ajax_catlog_delete'; ?>",
		data:{id:id},
		success:function(data){
			
			if(data=="done")
			{
				load_catlog_list(cat_type); 
			} 
			
	}});
	}
}
function ajax_status(status,id){
	/*$.ajax({
		type: "POST",
		url: "<?php echo site_url('administration/siteuser/ajax_status'); ?>",
		data:{status:status,id:id},
		success:function(data){			
			if(data=="done"){
				load_siteuser_list(); 
			} 		
		}
	});*/
}

function add_catlog_type(){
	//var cat_type = $('#cat_f').val();
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url('administration/catlog/catlog_type_add?cat_type=') ?>',
    	autoSize: false,
    	closeBtn: true,
    	width: '300',
    	height: '200',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}
</script>
<style>
#catlogList tr td:nth-child(2){
text-transform:capitalize;
}
</style>
<aside class="right-side">
    <div class="col-md-12">
        <h1>Manage Catalogs</h1>
        <div class="table"> 
        
        <?php if(isset($_GET['cat_type']) && ($_GET['cat_type'] > 0) ) { $cat_type_selected = $_GET['cat_type'];  } else { $cat_type_selected=''; } ?>
       
        <div class="row">   
            	
          <div class="col-md-3" style="float:left">  
              <label>Select Catalog Type</label>
               <?php
             
              echo get_active_catlog_dropdown($selected=$cat_type_selected,$attr='class="form-control input-sm"','cat_f',$id='cat_f',$catlog_type_value='2');
              
          ?>
          
          </div>    
           <a data-toggle="modal" onclick="add_catlog_type()"  class="fancybox-add_ btn btn-primary" href="" style="margin: 25px 0 10px 10px; float:left;">Add Catalog type</a>        
        </div>
       
        <a data-toggle="modal" onclick="add_catlog_item()"  class="fancybox-add_ btn btn-primary" href="">Add New</a>
        
          <!-- Catlog List -->
          <table id="catlogList" width="100%" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th class="center" width="10%" style="display:none;" >ID</th>
                <th width="22%">Catalog Type</th>
                <th width="22%">Catalog Option Value</th>
                <th width="22%">Status</th>
                <th width="22%">Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          <!-- Catlog List -->
        </div><style>#catlogList tbody tr td:first-child { display:none; }</style>
    </div>
</aside>