<?php
$this->load->view('includes/styles');
$this->load->view('includes/scripts');
?>
<html>
<link rel="stylesheet" type="text/css" href="<?= base_url('css/bootstrap.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('css/auto_quote.css'); ?>">

<script type="text/javascript">
function close_pop_up(){
    parent.$.fancybox.close();
}
</script>
<style>
html,body {
	min-height:200px !important;
}
</style>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<div class="row-fluid">
  <div class="content">
  <?php  ?>
    <!-- Underwriter List -->
    <div class="span12">
      <h2><?php  ?></h2>
      <form action="<?php echo base_url('administration') ?>/catlog/catlog_type_add" method="post" name="frmCatlog" id="frmCatlog"  enctype="multipart/form-data">
          <div class="row-fluid">
          
          </div>
          <div class="row-fluid">
            <label>Catalog Type <em style="color:#FF0000">*</em></label>
            <input type="text" name="catlog_type" id="catlog_type" value="" class="span8" required>
            
          </div>
          
       	<br>
        			
        <input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="<?php if(isset($catlog->id)) { echo 'Edit'; } else { echo 'Add';}  ?>" class="btn btn-primary pull-right" style="margin-right:10px" name="catlog_type_add">
        <div class="clearfix"></div>
      </form>
    </div>
  </div>
</div>
<script>
    $(document).ready(function(){
        $('html, body').css('min-height', '20px');
    });
    </script>
</body>
</html>