<?php
$this->load->view('include/style_and_script');
?>
<div class="row-fluid">
	<h4 class="text-center">Change profile photo</h4>
	<form action="<?= site_url('administration/siteuser/change_photo')?>" name="change_photo" id="change_profile_photo" enctype="multipart/form-data" method="post">
    	<div class="row-fluid">
        	<label>Select Photo</label>
        	<input type="file" name="profile_photo">
        </div>
        <div class="row-fluid">
        	<input type="submit" name="submit" value="Save" class="btn btn-primary">
        </div>
    </form>
</div>