<?php if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest'){	
	$this->load->view('include/header');}?>

<?php 	$newdata = array('current_password'  => "$member->password" );
		$this->session->set_userdata($newdata);
?>

<?php
if($this->session->userdata('admin_id')){		
?>
<style>
input { height:30px !important;} 
</style>
<script>
$(document).ready(function(e) {
	$('.close').click(function(e) {
        $('.alert-error').remove();
    });    
});
</script>

<?php
$flash_success = '';
if ($this->session->flashdata('error')){    
		//echo '<div class="error"><p>'.$this->session->flashdata('error').'</p></div>';
		$flash_error = $this->session->flashdata('error');
	}
	if ($this->session->flashdata('success')){    
		$flash_success = '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; 
	}
	// Validation Errors
	  $error_message = (validation_errors()?'<div class="error">'.validation_errors().'</div>':'');
	  
    if(!empty ($error_message)) {
		$error_message_div = '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="javascript:void(0);">x</a>'.$error_message.'</div>';
	} else {
		$error_message_div = '';
	}	
	
    if(!empty ($flash_error)) {
		$error_message_div .= '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$flash_error.'</div>';
	} 
	
	$password_changed=isset($password_changed) ? $password_changed:'';
	$old_passwords = isset($_POST['old_passwords']) ? $_POST['old_passwords'] : NULL;
	
?>
  


 <div class="row top30">
    <div class="span4 offset4">
		<div class="well">
		<legend>Change Password</legend>
			<form id="frmChangePassword" name="frmChangePassword" method="post" action="<?php echo base_url(uri_string());?>">
			<?php echo $error_message_div ?>
			<!--div class="success" ><strong><?=$password_changed?></strong></div-->
			<?=$flash_success ?>
			<input class="span3" placeholder="Old Password" name="old_passwords" type="password" id="old_passwords" value="<?php echo $old_passwords;?>" >
			
			<input class="span3" placeholder="New Password" name="new_password" type="password"  id="new_password">
			<input class="span3" placeholder="Confirm Password" name="re_password" type="password"  id="re_password">
			<br/>
			<button class="btn btn-primary" name="change_password" type="submit">Modify</button>
			</form>
		</div>
    </div>
</div>
</div>
  <?php 
}else{
	//$this->load->view('access_denied');
}
?>


<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('include/footer');
}
?>
      