{header}
<script src="{base_url}ng-table/ng-table.js"></script>
<script>
$(document).ready(function(e) {
    $("#datatable").dataTable();	
});
</script>
<link rel="stylesheet" href="{base_url}ng-table/ng-table.css">    
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    {page_header}    
     <!-- Main content -->
      <form action="{base_url}administration/customer/customer_delete/" method="post" onsubmit="return checkBox('customer')">
    <section class="content" ng-app="customer"> 
    	<div class="row">
        	<div class="col-md-3">
            	<a class="btn btn-primary add-customer" onclick="javascript:void(0)" href="">Add New</a>
				<?php
				if($this->session->userdata('admin_type') == 1 || $this->session->userdata('admin_type') == 2) {
				?>
                 <input type="submit" name="delete" value="Delete" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon></button>
				 <?php
				 }
				 ?>
            </div>
        </div>   	
        <div class="row">        	
        	<div ng-controller="CustomerCtrl" class="demo-reponsiveTable clearfix">
                <table show-filter="flase" class="table table-bordered table-striped" id="datatable">
                <thead>
                    <tr>
                    <th width="5%">&nbsp;<input type="checkbox" name="check_all" id="check_all" /></th>
                        <th>Customer Name</th>
                        <th>Company</th>
                        <th>Phone No.</th>
                        <th>Email</th>
                        <th>Address</th> 
                        <th>Action</th>                                
                    </tr>
                </thead>
                <tbody>
                <tr ng-repeat="customer in data">
               		<td title="'Name'" title-alt="'Name'">
               		<input type="checkbox" name="delete_check[]" value="{{customer.customer_id}}" class="check" />
            		</td>
                    <td title="'Company'" title-alt="'Company'" sortable="'company'" filter="{ 'company': 'text' }">
                        {{customer.customer_f_name}}&nbsp;{{customer.customer_l_name}}
                    </td>
                    <td title="'Address'" sortable="'address'" filter="{ 'address': 'text' }">
                        {{customer.company_name}}
                    </td>
                    <td title="'Address'" sortable="'address'" filter="{ 'address': 'text' }">
                        {{customer.phone_number}}
                    </td>
                    <td title="'Logo'" sortable="'logo'" filter="{ 'logo': 'text' }">
                        {{customer.email_add}}
                    </td>
                     <td title="'Logo'" sortable="'logo'" filter="{ 'logo': 'text' }">
                        {{customer.street_add}}
                    </td>
                    <td title="'Pump Set Pressure'" sortable="'pump_set_pressure'" filter="{ 'pump_set_pressure': 'text' }">
                       <a ng-click="edit_customer(customer.customer_id)">
                        <button class="btn btn- btn-phone-block"><icon class="fa fa-pencil icon-white"></icon>
                        <span class="hidden-phone">Edit</span>
                        </button>
                        </a>
                    </td>           
                </tr>
                </tbody>
            </table>
           
            
<script>
var customer_list = '{customer_lists}';
var app = angular.module('customer', ['ngTable']);
function CustomerCtrl($scope, $filter, NgTableParams) {
	data = JSON.parse(customer_list);
	$scope.data = [];
	$scope.data = data;
	$scope.edit_customer = function(customer_id){
	console.log(customer_id);
		var url = base_url+'administration/customer/add_customer/?customer_id='+customer_id;
        viewFancybox(url, 400, 600);
	}
	//console.log($scope.data);		   
}
$('#check_all').click(function(e){
	
	if($(this).is(':checked')){
		$(".check").each(function(){
			this.checked = true;
			});	
	}
	if(!$(this).is(':checked')){
		$(".check").each(function(){
			this.checked = false;
			});	
	}
	
});           
</script>

<!--<script>
var app = angular.module('main', ['ngTable']);
app.controller('DemoCtrl', ['$scope', '$http', function($scope,$http,$filter, NgTableParams) {
  $http.get("http://localhost/auto_quote/administration/company/get_data")
  .success(function (data) {

 $scope.data = [];
 $scope.data = data;
 console.log($scope.data);
 });
}]);
</script>-->
		</div> 
       </div>
   </section>
   </form>    
  </aside>
</div>
{footer}