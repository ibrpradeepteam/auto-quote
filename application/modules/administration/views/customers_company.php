{header}
<script src="{base_url}ng-table/ng-table.js"></script>
<script>
$(document).ready(function(e) {
    $("#datatable").dataTable();	
});
</script>
<link rel="stylesheet" href="{base_url}ng-table/ng-table.css">    
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    {page_header}    
     <!-- Main content -->
      <form action="{base_url}administration/company/customer_company_delete/" method="post" onsubmit="return checkBox('company')">
    <section class="content" ng-app="cust_comp"> 
    	<div class="row">
        	<div class="col-md-3">
            	<a class="btn btn-primary add-customer-company" onclick="javascript:void(0)" href="">Add New</a>
                 <input type="submit" name="delete" value="Delete" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon></button>
            </div>
        </div>   	
        <div class="row">        	
        	<div ng-controller="CustCompCtrl" class="demo-reponsiveTable clearfix">
                <table show-filter="flase" class="table table-bordered table-striped" id="datatable">
                <thead>
                    <tr>
                    <th width="5%">&nbsp;<input type="checkbox" name="check_all" id="check_all" /></th>
                        <th>Company</th>
                        <th>Address</th>
                        
                        <th>Action</th>                                
                    </tr>
                </thead>
                <tbody>
                <tr ng-repeat="company in data">
               		<td title="'Name'" title-alt="'Name'">
               		<input type="checkbox" name="delete_check[]" value="{{company.customer_company_id}}" class="check" />
            		</td>
                    <td title="'Company'" title-alt="'Company'" sortable="'company'" filter="{ 'company': 'text' }">
                        {{company.company_name}}
                    </td>
                    <td title="'Address'" sortable="'address'" filter="{ 'address': 'text' }">
                        {{company.company_address}}
                    </td>
                   
                    <td title="'Pump Set Pressure'" sortable="'pump_set_pressure'" filter="{ 'pump_set_pressure': 'text' }">
                       <a ng-click="edit_company(company.customer_company_id)">
                        <button class="btn btn- btn-phone-block"><icon class="fa fa-pencil icon-white"></icon>
                        <span class="hidden-phone">Edit</span>
                        </button>
                        </a>
                    </td>           
                </tr>
                </tbody>
            </table>
           
            
<script>
var company_list = '{company_lists}';
var app = angular.module('cust_comp', ['ngTable']);
function CustCompCtrl($scope, $filter, NgTableParams) {
	data = JSON.parse(company_list);
	$scope.data = [];
	$scope.data = data;
	$scope.edit_company = function(company_id){
		var url = base_url+'administration/customer/add_customer_company/'+company_id;
        viewFancybox(url, 300, 300);
	}
	//console.log($scope.data);		   
}
$('#check_all').click(function(e){
	
	if($(this).is(':checked')){
		$(".check").each(function(){
			this.checked = true;
			});	
	}
	if(!$(this).is(':checked')){
		$(".check").each(function(){
			this.checked = false;
			});	
	}
	
});           
</script>

<!--<script>
var app = angular.module('main', ['ngTable']);
app.controller('CustCompCtrl', ['$scope', '$http', function($scope,$http,$filter, NgTableParams) {
  $http.get("http://localhost/auto_quote/administration/company/get_data")
  .success(function (data) {

 $scope.data = [];
 $scope.data = data;
 console.log($scope.data);
 });
}]);
</script>-->
		</div> 
       </div>
   </section>
   </form>    
  </aside>
</div>
{footer}