<?php
echo $this->load->view('includes/styles');
echo $this->load->view('includes/scripts');
?>
<aside class="right-side">
 <form action="<?php echo base_url(); ?>administration/csv/import_csv" method="post" enctype="multipart/form-data">
                                                          
                           
                            <div class="row">
                            
                                
                                <div class="col-md-2" style="margin-top:10px;">
                                    <label>Select file</label>
                                    <?php if($file_name == 'products'){ ?>
                                    <input type="file" name="product_csv" required />
                                    <?php } ?>
                                     <?php if($file_name == 'customers'){ ?>
                                    <input type="file" name="customer_csv" required />
                                    <?php } ?>
                                     <?php if($file_name == 'quotations'){ ?>
                                    <input type="file" name="quote_csv" required />
                                    <?php } ?>
                                </div>
                               
                           </div>
                         
                            <br />
                            <div class="row">
                                <div class="col-md-2">
                                <?php if($file_name == 'products'){ ?>
                                    <input type="submit" name="import_product_csv" value="Submit" class="btn btn-primary">  
                                    <?php } ?>      
                                    
                                    <?php if($file_name == 'customers'){ ?>
                                    <input type="submit" name="import_customer_csv" value="Submit" class="btn btn-primary">  
                                    <?php } ?>      
                                    
                                    <?php if($file_name == 'quotations'){ ?>
                                    <input type="submit" name="import_quotation_csv" value="Submit" class="btn btn-primary">  
                                    <?php } ?>                     
                                </div>
                            </div>
                             </form>
                             </aside>