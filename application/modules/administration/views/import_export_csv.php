{header}
<script>
function import_csv(csv_file){
	jQuery.fancybox({
        type: 'iframe',
        href: '{base_url}administration/csv/import_csv_form/'+csv_file,
        autoSize: false,
        closeBtn: true,
        width: 300,
        height: 200,
        closeClick: true,
        enableEscapeButton: true,
        beforeLoad: function () {},
    });	
	
	
}

</script>
              <aside class="right-side">        
                          <!-- general form elements -->   
                           
                             
                             <h3>CSV Import Export </h3>
                             <table ng-table="tableParams" show-filter="flase" class="table table-bordered table-striped" id="products">
    
    <thead>
        	<tr>
            	
            	<th width="5%">Export CSV</th>
                <th width="5%">Import CSV</th>
             
            </tr>
        </thead>
       <tbody>
        <tr>
       
        	<td>
             <a href="{base_url}administration/csv/export_product_csv" data-toggle="modal" class="btn btn- btn-phone-block"> Export Products</a>
            </td>
            <td title="'Name'" title-alt="'Name'" sortable="'product_name'">
             <a onclick="import_csv('products')" data-toggle="modal" class="btn btn- btn-phone-block"> Import Products</a> 
            </td>
          
        </tr>
        
         <tr>
       
        	<td title="'Name'" title-alt="'Name'">
            <a href="{base_url}administration/csv/export_customer_csv" data-toggle="modal" class="btn btn- btn-phone-block">  Export Customers</a>
            </td>
            <td title="'Name'" title-alt="'Name'" sortable="'product_name'" filter="{ 'product_name': 'text' }">
             <a onclick="import_csv('customers')" data-toggle="modal" class="btn btn- btn-phone-block"> Import Customers </a>
            </td>
          
        </tr>
         <tr>
       
        	<td title="'Name'" title-alt="'Name'">
            <a href="{base_url}administration/csv/export_quotation_csv" data-toggle="modal" class="btn btn- btn-phone-block">  Export Quotations</a>
            </td>
            <td title="'Name'" title-alt="'Name'" sortable="'product_name'" filter="{ 'product_name': 'text' }">
            <a onclick="import_csv('quotations')" data-toggle="modal" class="btn btn- btn-phone-block">  Import Quotations </a>
            </td>
          
        </tr>
        </tbody>
    </table>
                     </aside><!-- /.right-side -->      