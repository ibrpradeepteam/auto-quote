<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('include/header');
}
?>
<script src="<?= base_url('js/instrument.js'); ?>" type="text/javascript"></script>
         <?php
          $dash_menu['add_title'] = 'Add Item';
          $dash_menu['manage_title'] = 'Manage Item';
          $dash_menu['show_add_menu'] = 1;
          $dash_menu['show_manage_menu'] = 1;
         ?>
       <?php $this->load->view('include/dash-menu', $dash_menu); ?>
       
       <div class="rt_cont">
        <div class="title" style="display:none;">
         <div class="title_lt">
         	<div class="row-fluid">
            	<div class="span1">Filter:</div>
                <div class="span11 item_filter">
                    <div class="span2">
                        <Label>Type</Label>
                        <select name="type" class="span12" id="type">
                            <option <?= isset($get['type']) ? ($get['type'] == 'Instrument') ? 'selected="selected"' : '' : ''; ?>>Instrument</option>
                            <option <?= isset($get['type']) ? ($get['type'] == 'Lab Resources') ? 'selected="selected"' : '' : ''; ?>>Lab Resources</option>
                        </select>
                    </div>
                    <div class="span2">
                        <label>Status</label>
                        <select name="status" id="status" class="span12">
                            <option <?= (strtolower($filter) == 'all') ? 'selected="selected"' : ''; ?>>All</option>
                            <option <?= (strtolower($filter) == 'available') ? 'selected="selected"' : ''; ?>>Available</option>
                            <option <?= (strtolower($filter) == 'notavailable') ? 'selected="selected"' : ''; ?>>Not Available</option>
                        </select>
                    </div>
                    <div class="span2">
                        <label>Building#</label>
                        <input type="text" name="building" id="building" class="span12" value="<?= isset($get['building']) ? $get['building'] : ''; ?>">
                    </div>
                    <div class="span1">
                        <label>Floor#</label>
                        <input type="text" name="floor" id="floor" class="span12" value="<?= isset($get['floor']) ? $get['floor'] : ''; ?>">
                    </div>
                    <div class="span2">
                        <label>Lab Number#</label>
                        <input type="text" name="lab_number" id="lab_number" class="span12" value="<?= isset($get['lab_number']) ? $get['lab_number'] : ''; ?>">
                    </div>
                    <div class="pull-left left5">
                        <label>&nbsp;</label>
                        <!--<input type="button" name="filter"  id="filter" value="Go" class="btn btn-yellow">-->
                        <a href="javascript:;" class="btn btn-yellow" id="filter" >Go</a>
                    </div>
                </div>
            </div>
         </div>
         <div class="title_rt"><a href="javascript:void(0);" class="filter_ar">Add / Remove filter</a></div>
         
         
         <div class="clear"></div>
        </div>
         <div class="filter_listing">
           <div class="img_list">          
           <?php if(!empty($instruments)){?>
             <ul>
             <?php foreach($instruments as $instrument){ ?>
             <?php $avail_status = check_avail_status($instrument->id); ?>
             <?php
			 $status = ($avail_status) ? 'available' : 'notavailable';
			 if($filter){
             	$allow = (strtolower($filter) == 'all') ? true : (strtolower($status) == strtolower($filter)) ? true : '';
			 }else{
				$allow = true;	 
			}
			 if($allow){
			 ?>
              <li>
              <?php $images = json_decode($instrument->images); ?>
              <div class="img">
              	<img src="<?= base_url('upload/'.$images[0].''); ?>" alt="<?= $images[0]; ?>">
                <a href="javascript:void(0);" onclick="reserveInstrument(<?= $instrument->id?>)" class=""><div class="mask"></div></a>
              </div>
              <div class="pro_detail">
			  	<?php 
				if($this->input->get('search')){
					$search = strtolower($this->input->get('search'));					
					$ins_name = str_replace($search, "<b>$search</b>", strtolower($instrument->name));
				} else{
					$ins_name =$instrument->name; 	
				}
				?>
               <div class="pro_name"><?= ucfirst($ins_name); ?></div>
               <div class="bottom_box">               
                <div class="<?= ($avail_status) ? 'available' : 'not_available' ; ?>"><?= ($avail_status) ? 'Available' : '<a href="javascript:void();" onclick="not_avaialble_range('.$instrument->id.')">Not Available</a>' ; ?></div>
                 <div class="reserve" onclick="reserveInstrument(<?= $instrument->id?>)">Reserve &nbsp;&nbsp;&nbsp;&nbsp;<span><img src="<?= base_url('images/date_icon.png'); ?>"></span></div>
                
                <div class="clear"></div>
               </div>
              
              </div>
              </li>
              <?php } ?>
              <?php } ?>
             </ul>
           <?php } ?>
           </div>
         
         </div>
       
       </div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('include/footer');
}
?>