<?php $this->load->view('includes/header'); ?>
<script>
$(document).ready(function(e) {
    $('#login_form').submit(function(e) {
        e.preventDefault();
		var form_data = $('#login_form').serialize();
		var username = $('#username').val();
		var password = $('#password').val();
		 $('.error').fadeOut('fast');
		
		//console.log(form_data);
		if(username!=''&&password!=''){
			 $('#singin').val('Signing.....');
			$.ajax({
				url: '<?php echo base_url('index.php');?>',
				type: 'post',
				data: form_data,
				success:function(json){
					 var obj = jQuery.parseJSON(json);
					 console.log(obj);
					 if(obj['error']==0){
						 if(obj['uri']!=''){
							window.location = obj['uri'];
						 }else{
							window.location.reload(true); 
						 }
					 }else{
						 $('.error').fadeIn('fast');
					 }
					  $('#singin').val('Sign me in');
				}
			});
		}
    });
});
</script>
</head>
<body class="bg-gray">
<style>
.btn1{
display: inline-block;
padding: 4px 12px;
margin-bottom: 0;
font-size: 14px;
line-height: 20px;
text-align: center;
/*text-shadow: 0 1px 1px rgba(255,255,255,0.75);*/
vertical-align: middle;
cursor: pointer;
font-weight: 500;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;
border: 1px solid transparent;
-webkit-box-shadow: inset 0px -2px 0px 0px rgba(0, 0, 0, 0.09);
-moz-box-shadow: inset 0px -2px 0px 0px rgba(0, 0, 0, 0.09);
box-shadow: inset 0px -1px 0px 0px rgba(0, 0, 0, 0.09);
}
.form-group:first-child{
	margin-top:10px!important;	
}
</style>
        <div class="form-box" id="login-box">
            <div class="header bg-light-blue">Auto Quote</div>
            <form action="<?php echo base_url('administration/index');?>" method="post" id="login_form">
                <div class="body" style="background-color: #DDDDDD !important;">
                    <div class="form-group row-fluid">
                    	<div class="error" style="display:none;">Incorrect username or password</div>
                        <input type="text" name="username" id="username" class="form-control span12" placeholder="User ID" required/>
                    </div>
                    <div class="form-group row-fluid">
                        <input type="password" name="password" id="password" class="form-control span12" placeholder="Password" required/>
                    </div> 
                    <?php if($this->config->item('remembers_me_on')){?>         
                   <div class="form-group row-fluid">
                        <input type="checkbox" name="remember_me"/> Remember me
                    </div>
                    <?php } ?>
                </div>
                <div class="footer">                                                               
                    <input type="submit" class="btn1 bg-light-blue btn-block" id="singin" value="Sign me in"> 
                    
                   <!-- <p><a href="#">I forgot my password</a></p>
                    
                    <a href="register.html" class="text-center">Register a new membership</a>-->
                </div>
            </form>

          <!--  <div class="margin text-center">
                <span>Sign in using social networks</span>
                <br/>
                <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
                <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
                <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

            </div>-->
        </div> 
    </body>
</html>
