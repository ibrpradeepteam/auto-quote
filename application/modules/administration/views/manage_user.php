<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('include/header');
}
?>
<?php
$dash_menu['add_title'] = 'Add User';
$dash_menu['manage_title'] = 'Manage User';
$dash_menu['show_add_menu'] = 0;
$dash_menu['show_manage_menu'] = 1;
$dash_menu['manage_click'] = 'onClick="edit_siteuser('.$siteuser->id.')"';
?>
<?php $this->load->view('include/dash-menu', $dash_menu); ?>
<?php
    //print_r($siteuser);
?>
<script>
function edit_siteuser(id)
{
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo site_url() ?>/administration/siteuser/siteuser_edit/'+id,
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '350',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}
</script>
<div class="row-fluid ">
	<div class=" left36">
        <table class="table table-bordered table-hover ">
            <thead>
                <tr>
                    <th>Full name</th>
                    <th>Email</th>
                    <th>Password</th>
                    <!--<th>Phone no.</th>
                    <th>Fax no.</th>-->
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $siteuser->name.' '.$siteuser->middle_name.' '.$siteuser->last_name; ?></td>
                    <td><?php echo $siteuser->email; ?></td>
                    <td>********</td>
                    <!--<td><?php /*echo $siteuser->phone_no; ?></td>
                    <td><?php echo $siteuser->fax_no;*/ ?></td>-->
                </tr>            
            </tbody>
        </table>
   </div>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('include/footer');
}
?>