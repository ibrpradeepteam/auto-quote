<?php $this->load->view('include/style_and_script'); ?>
<script type="text/javascript">
function close_pop_up()
{
	parent.$.fancybox.close();
}

function validate()
{
	var user_type = document.getElementById("user_type").value;
	if(user_type==0)
	{
		document.getElementById("user-r").innerHTML = 'Please select any one user';
		return false;
	}
}
var source;
checked1=false;
function checkAll(source)
 {
 
	var array = document.getElementsByTagName("input");
	 if (checked1 == false)
          {
           checked1 = true
          }
        else
          {
          checked1 = false
          }

for(var ii = 0; ii < array.length; ii++)
{

   if(array[ii].type == "checkbox")
   {
      if(array[ii].className == source)
       {
        array[ii].checked = checked1;

       }


   }
}
}
checked=false;
function checkAll1(source)
 {
 
	var array = document.getElementsByTagName("input");
	//alert(array.className);
	 if (checked == false)
          {
           checked = true
          }
        else
          {
          checked = false
          }

for(var ii = 0; ii < array.length; ii++)
{

   if(array[ii].type == "checkbox")
   {
      if(array[ii].className == source)
       {
        array[ii].checked = checked;

       }


   }
   }
 
}
</script>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<style>
.container1{
padding-left:30px;
}
.content{
	overflow:visible;	
}
</style>
<div class="container1">
  <div class="content">
    <!-- siteuser List -->
    
      <h2><?php echo $header['title'] ?></h2>
      <form action="" method="post" name="frmprofile" id="frmprofile" autocomplete="on">    
      <div class="row-fluid">
      	<div class="span6">
            <label>Profile Name <em style="color:#FF0000">*</em></label>
            <input type="text" name="profile_name" id="name" value="<?php echo set_value('name') ?>" class="span312">
            <?php echo form_error('profile_name','<span style="color:red"">','</span>'); ?>
        </div>
   	 	</div>
        <style>
		.chosen-container-multi .chosen-choices li.search-field input[type="text"]{
			height:25px;	
		}
		</style>
        <div class="row-fluid">
        	<div class="span12">
              <label>Groups *</label>    
              <?php $u_groups = isset($account[0]->group) ? explode(',', decrypt($account[0]->group)) : array();?>
              <select name="groups[]" id="groups" multiple class="chzn-select span10">
              <option value="all">All</option>
              <?php if(isset($groups)){?>
                  <?php 
                  foreach($groups as $group){
                      $selected = is_array($u_groups) ? in_array($group->id, $u_groups) ? 'selected' : '' : '';
                      echo '<option value="'.$group->id.'" '.$selected.'>'.$group->group_name.'</option>';						
                  }
                  ?>
              <?php } ?>
              </select>
            </div>
        </div>
		<!--<label>Email Address <em style="color:#FF0000">*</em></label>
        <input type="text" name="email" id="email" value="<?php echo set_value('email') ?>" class="span3">
		<?php echo form_error('email','<span style="color:red"">','</span>'); ?>
		
		<label>Password <em style="color:#FF0000">*</em></label>
        <input type="text" name="password" id="password" class="span3">
		<?php echo form_error('password','<span style="color:red"">','</span>'); ?>-->
       <div id="privilages" style="display:none;">
       		<label>Select Profile Privilage <em style="color:#FF0000">*</em></label>
             <div class="maintainance">Certificate Privileges</div>           
              <?php echo form_error('privilage','<span style="color:red"">','</span>'); ?></br> 
             <input type="checkbox" name="cheall" onClick="checkAll('policy_privilages')">&nbsp; All
            <table class="privilage_table">
				<tr>
				  <td><input type="checkbox" name="privilage[]" value="list_policies" class="policy_privilages" style="display:none;">&nbsp; Full Search </td>
				   <td>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="privilage[]" value="restricted_search_policies" class="policy_privilages" style="display:none;">&nbsp; Restricted Search </td>
				</tr>
                <tr>
                    <td><input type="checkbox" name="privilage[]" value="add_policies" class="policy_privilages" style="display:none;">&nbsp; Add </td>
                  
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="privilage[]" value="modify_policies" class="policy_privilages" style="display:none;">&nbsp; Modify </td>
                   
                    <td><input type="checkbox" name="privilage[]" value="cancel_policies" class="policy_privilages" style="display:none;">&nbsp; Cancel </td>
                  </tr>
                <tr>            	
                    <td><input type="checkbox" name="privilage[]" value="reprint_policies" class="policy_privilages" style="display:none;">&nbsp; Reprint </td>
                    <td><input type="checkbox" name="privilage[]" value="sub_certificate" class="policy_privilages" style="display:none;">&nbsp; Add/Modify Sub Certificate </td>
                 </tr>
               </table>
               <div class="maintainance">Maintenance Privileges</div>
                <input type="checkbox" name="cheall" onClick="checkAll1('main_privilages')" style="display:none;">&nbsp; All
               <table class="privilage_table">
                 <tr>
                    <td><input type="checkbox" name="privilage[]" value="list_transactions log" class="main_privilages" style="display:none;">&nbsp; List transactions log</td>
                </tr>
                <!--tr>
                    <td><input type="checkbox" name="privilage[]" value="list_reprint log" class="main_privilages">&nbsp; List reprint log</td>
                    <td><input type="checkbox" name="privilage[]" value="add_documents templates" class="main_privilages">&nbsp; Add documents templates</td>
                </tr-->
                <tr>
                    <!--td><input type="checkbox" name="privilage[]" value="modify_documents templates" class="main_privilages">&nbsp; Modify documents templates</td-->
                    <td><input type="checkbox" name="privilage[]" value="access_to_Policy" class="main_privilages" style="display:none;">&nbsp; Access</td>
                </tr>
                <!--tr>            	
                    <td><input type="checkbox" name="privilage[]" value="vehicle_schedule" class="main_privilages">&nbsp; Vehicle Schedule</td>
                    <td><input type="checkbox" name="privilage[]" value="driver_schedule_and_exclusions_table_functionality" class="main_privilages">&nbsp; Driver Schedule and Exclusions table functionality</td>
                </tr-->
				 <!--tr>            	
                    <td><input type="checkbox" name="privilage[]" value="manage_catalogs" class="main_privilages">&nbsp; Manage Catalogs</td>
                 
                </tr-->
				
                <tr>
                  <td><input type="checkbox" name="privilage[]" value="roles_maintenance" class="main_privilages" style="display:none;">&nbsp; Roles maintenance</td>
                 </tr>
            </table>
           
          </div>
          <div class="row-fluid top10">
            <input type="button" value="Cancel" class="btn btn-primary pull-left" onClick="close_pop_up();">
            <input type="submit" value="Add" class="btn btn-primary pull-left" style="margin-left:10px">
          </div>
        <div class="clearfix"></div>
        
      </form>
    </div>
    <!-- siteuser List -->
  </div>
</div>
</body>
