<?php 
$this->load->view('include/style_and_script');

$id	= isset($_POST['profileId']) ? $_POST['profileId'] : (isset($profile->up_id)?$profile->up_id :NULL);
$profile_name	= isset($_POST['profile_name']) ? $_POST['name'] : (isset($profile->up_name)?$profile->up_name :NULL);
$privilage	= isset($_POST['privilage']) ? $_POST['privilage'] : (isset($profile->up_privileges)?$profile->up_privileges :NULL);
$u_groups	= isset($_POST['groups']) ? $_POST['groups'] : (isset($profile->up_groups)? explode(',', $profile->up_groups) :NULL);
?>
<script type="text/javascript">
function close_pop_up()
{
	parent.$.fancybox.close();
}
var source;
checked=false;
function checkAll(source)
 {

	var array = document.getElementsByTagName("input");
	//alert(array.className);
	 if (checked == false)
          {
           checked = true
          }
        else
          {
          checked = false
          }

for(var ii = 0; ii < array.length; ii++)
{

   if(array[ii].type == "checkbox")
   {
      if(array[ii].className == source)
       { //alert(  array[ii].checked);
		 $("#main_checkbox").prop("checked", checked);
        array[ii].checked = checked;

       }


   }
   }
 
}
checked=true;
function checkAll1(source)
 {
 
	var array = document.getElementsByTagName("input");
	//alert(array.className);
	 if (checked == false)
          {
           checked = true
          }
        else
          {
          checked = false
          }

for(var ii = 0; ii < array.length; ii++)
{

   if(array[ii].type == "checkbox")
   {
      if(array[ii].className == source)
       {
        $("#main_checkbox_maintenance").prop("checked", checked);
		array[ii].checked = checked;

       }


   }
   }
 
}

</script>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<style>
.container1{
padding-left:30px;
}
.content{
	overflow:visible;	
}
</style>
<div class="container1">
  <div class="content">
    <!-- siteuser List -->

      <h2><?php echo $header['title'] ?></h2>
      <form action="" method="post" name="frmsiteuser" id="frmsiteuser" autocomplete="on">
      <div class="row-fluid">
      	<div class="span6">
            <input type="hidden" name="profileId" id="profileId" value="<?php echo $id ?>" />
            <label>Profile Name <em style="color:#FF0000">*</em></label>
            <input type="text" name="profile_name" id="name" value="<?php echo $profile_name; ?>" class="span12">
            <?php echo form_error('profile_name','<span style="color:red"">','</span>'); ?>
        </div>
      </div>  
       <style>
		.chosen-container-multi .chosen-choices li.search-field input[type="text"]{
			height:25px;	
		}
		</style>
        <div class="row-fluid">
        	<div class="span12">
              <label>Groups *</label>                 
              <select name="groups[]" id="groups" multiple class="chzn-select span10">
              <option value="all" <?php  echo ($u_groups) ? in_array('all', $u_groups) ? 'selected' : '' : '';?>>All</option>
              <?php if(isset($groups)){?>
                  <?php 
                  foreach($groups as $group){
                      $selected = is_array($u_groups) ? in_array($group->id, $u_groups) ? 'selected' : '' : '';
                      echo '<option value="'.$group->id.'" '.$selected.'>'.$group->group_name.'</option>';						
                  }
                  ?>
              <?php } ?>
              </select>
            </div>
        </div>
		<!--<label>Email Address <em style="color:#FF0000">*</em></label>
        <input type="text" name="email" id="email" value="<?php echo set_value('email') ?>" class="span3">
		<?php echo form_error('email','<span style="color:red"">','</span>'); ?>
		
		<label>Password <em style="color:#FF0000">*</em></label>
        <input type="text" name="password" id="password" class="span3">
		<?php echo form_error('password','<span style="color:red"">','</span>'); ?>-->
       <div id="privilages" style="display:none;">
       		<label>Select Profile Privilage <em style="color:#FF0000">*</em></label>
             <div class="maintainance">Certificate Privileges</div>    
             <?php
			 $profiles = array();
            $profiles = (explode(",",$privilage));
			 ?>         
             <input type="checkbox" id="main_checkbox" name="cheall" onClick="checkAll('policy_privilages')">&nbsp; All
            <table class="privilage_table">
				
				<tr>
					  <td><input type="checkbox" name="privilage[]" value="list_policies" class="policy_privilages"
                     <?php 
					if(in_array("list_policies", $profiles)){
        				echo 'checked';} ?>
                    >&nbsp; Full Search </td>
					
					<td>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="privilage[]" value="restricted_search_policies" class="policy_privilages"
                     <?php 
					if(in_array("restricted_search_policies", $profiles)){
        				echo 'checked';}?>
                    >&nbsp; Restricted Search </td>
				</tr>	

			   <tr>
                    <td><input type="checkbox" name="privilage[]" value="add_policies" class="policy_privilages"
                    <?php 
					if(in_array("add_policies", $profiles)){
        				echo 'checked';
   						 }?>
    >&nbsp; Add </td>
                  
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="privilage[]" value="modify_policies" class="policy_privilages" 
                     <?php 
					if(in_array("modify_policies", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; Modify </td>
                    <td><input type="checkbox" name="privilage[]" value="cancel_policies" class="policy_privilages"
                     <?php 
					if(in_array("cancel_policies", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; Cancel </td>
                  </tr>
                <tr>            	
                    <td><input type="checkbox" name="privilage[]" value="reprint_policies" class="policy_privilages"
                     <?php 
					if(in_array("reprint_policies", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; Reprint </td>
					
					 <td><input type="checkbox" name="privilage[]" value="sub_certificate" class="policy_privilages" 
                     <?php 
					if(in_array("sub_certificate", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; Add/Modify Sub Certificate</td>
					
                 </tr>
				 
               </table>
               <div class="maintainance">Maintenance Privileges</div>
                <input type="checkbox" name="cheall"  id="main_checkbox_maintenance" onClick="checkAll1('main_privilages')">&nbsp; All
               <table class="privilage_table">
                 <tr>
                    <td><input type="checkbox" name="privilage[]" value="list_transactions_log" class="main_privilages"
                     <?php 
					if(in_array("list_transactions_log", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; List transactions log</td>
                </tr>
                <!--tr>
                    <td><input type="checkbox" name="privilage[]" value="list_reprint_log" class="main_privilages"
                     <?php 
					if(in_array("list_reprint_log", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; List reprint log</td>
                    <td><input type="checkbox" name="privilage[]" value="add_documents_templates" class="main_privilages"
                     <?php 
					if(in_array("add_documents_templates", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; Add documents templates</td>
                </tr-->
                <tr>
                    <!--td><input type="checkbox" name="privilage[]" value="modify_documents_templates" class="main_privilages"
                     <?php 
					if(in_array("modify_documents_templates", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; Modify documents templates</td-->
                    <td><input type="checkbox" name="privilage[]" value="access_to_Policy" class="main_privilages"
                     <?php 
					if(in_array("access_to_Policy", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; Access </td>
                </tr>
                <!--tr>            	
                    <td><input type="checkbox" name="privilage[]" value="vehicle_schedule" class="main_privilages"
                     <?php 
					if(in_array("vehicle_schedule", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; Vehicle Schedule</td>
                    <td><input type="checkbox" name="privilage[]" value="driver_schedule_and_exclusions_table_functionality" class="main_privilages"
                     <?php 
					if(in_array("driver_schedule_and_exclusions_table_functionality", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; Driver Schedule and Exclusions table functionality</td>
                </tr-->
				
				<!--tr>            	
                    <td><input type="checkbox" name="privilage[]" value="vehicle_schedule" class="main_privilages"
                     <?php 
					if(in_array("manage_catalog", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; Manage Catalog</td>
				</tr-->
                <tr>
                   <td><input type="checkbox" name="privilage[]" value="roles_maintenance" class="main_privilages"
                     <?php 
					if(in_array("roles_maintenance", $profiles)){
        				echo 'checked';
   						 }?>
                    >&nbsp; Roles maintenance</td>
                </tr>
                </table>
        
          </div>
          <div class="row-fluid top10">
            <input type="button" value="Cancel" class="btn btn-primary pull-left" onClick="close_pop_up();">
            <input type="submit" value="Update" class="btn btn-primary pull-left" style="margin-left:10px">
          </div>
        <div class="clearfix"></div>
      </form>
    </div>
    <!-- siteuser List -->
  </div>
</div>
</body>
