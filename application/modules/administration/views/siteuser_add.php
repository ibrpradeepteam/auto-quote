<link rel="stylesheet" type="text/css" href="<?= base_url('css/bootstrap.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('css/auto_quote.css'); ?>">
<?php $this->load->view('includes/scripts'); ?>
<?php 
$id	= isset($_POST['siteuserId']) ? $_POST['siteuserId'] : (isset($siteuser->id)?$siteuser->id :NULL);

$name	= isset($_POST['name']) ? $_POST['name'] : (isset($siteuser->name)?$siteuser->name :NULL);
$middle_name	= isset($_POST['middle_name']) ? $_POST['middle_name'] : (isset($siteuser->middle_name)?$siteuser->middle_name :NULL);
$last_name	= isset($_POST['last_name']) ? $_POST['last_name'] : (isset($siteuser->last_name)?$siteuser->last_name :NULL);

$email	= isset($_POST['email']) ? $_POST['email'] : (isset($siteuser->email)?$siteuser->email :NULL);
$location	= isset($_POST['location']) ? $_POST['location'] : (isset($siteuser->location)?$siteuser->location :NULL);
$department	= isset($_POST['department']) ? $_POST['department'] : (isset($siteuser->department)?$siteuser->department :NULL);
$status	= isset($_POST['status']) ? $_POST['status'] : (isset($siteuser->status) ? $siteuser->status : 1);
$password	= isset($_POST['password']) ? $_POST['password'] : (isset($siteuser->password)?$siteuser->password :NULL);

$phone_no_val = isset($_POST['phone_no']) ? implode('-', $_POST['phone_no']) : (isset($siteuser->phone_no)?$siteuser->phone_no :NULL);
if(!empty($phone_no_val)){
	$phone_no = explode('-',$phone_no_val);
}else{
	$phone_no = array('','','','');
}

$fax_no_val = isset($_POST['fax_no']) ? implode('-', $_POST['fax_no']) : (isset($siteuser->fax_no) ? $siteuser->fax_no :NULL);
if(!empty($fax_no_val)){
	$fax_no = explode('-', $fax_no_val);
}else{
	$fax_no = array('','','','');
}

$privilage	= isset($_POST['privilage_profile']) ? $_POST['privilage_profile'] : (isset($siteuser->privilage)? explode(',', $siteuser->privilage) :NULL);
$u_groups	= isset($_POST['groups']) ? $_POST['groups'] : (isset($siteuser->group)? explode(',', $siteuser->group) :NULL);
$type = isset($_POST['user_type']) ? $_POST['user_type'] : (isset($siteuser->type)? $siteuser -> type :NULL);
$company_assigned = isset($_POST['company_assigned']) ? $_POST['company_assigned'] : (isset($siteuser->assigned_company)? $siteuser -> assigned_company :NULL);
$add_product_status	= isset($_POST['add_product_status']) ? $_POST['add_product_status'] : (isset($siteuser->add_product_status) ? $siteuser->add_product_status: 0);

?>
<script type="text/javascript">
function close_pop_up()
{
	parent.$.fancybox.close();
}

function validate()
{
	var user_type = document.getElementById("user_type").value;
	if(user_type==0)
	{
		document.getElementById("user-r").innerHTML = 'Please select any one user';
		return false;
	}
	var privilage_profile = document.getElementById("privilage_profile").value;
	if(user_type==0)
	{
		document.getElementById("profile").innerHTML = 'Please select any one profile';
		return false;
	}
}
var source;
checked=false;
function checkAll(source)
 {
	var array = document.getElementsByTagName("input");
	 if (checked == false)
          {
           checked = true
          }
        else
          {
          checked = false
          }

for(var ii = 0; ii < array.length; ii++)
{

   if(array[ii].type == "checkbox")
   {
      if(array[ii].className == source)
       {
        array[ii].checked = checked;

       }


   }
}
}
$(document).ready(function(e) {
  $('#user_type').change(function(e) {
	var val = $(this).val();
	if(val==3){
	  $('.location_dropdown').show();	
	  $('.department_dropdown').hide();	
	}else if(val==4){
	  $('.location_dropdown').hide();	
	  $('.department_dropdown').show();		
	}else{
	  $('.location_dropdown').hide();	
	  $('.department_dropdown').hide();		
	}
  });
});
$(document).ready(function(e) {
  $('#email').change(function(e) {
	  var email = $(this).val();
	  var id=<?php if(isset($id)){ echo $id ; } ?>;
	  $.ajax({
		type: "POST",
		url: "<?php echo site_url() ?>/administration/siteuser/chekh_email_id/"+id,
		data:{email:email},
		success:function(data){
			//console.log(data);
			$('#add').show();
			if(data == 'true')
			{
				document.getElementById("email1").innerHTML = 'Email id is aleady availabel';
				$('#add').hide();	
			} 
			else
			{
				document.getElementById("email1").innerHTML = 'Availabel';
				$('#add').show();
			}
			
	}});
	
	
  });
});


</script>

<style>
.container1{
padding-left:30px;
}
.chosen-container{
width:216px!important;
}
</style>
<div class="container1">
  <div class="content">
    <!-- siteuser List -->
   
      <h2><?php echo $header['title'] ?></h2>
      <form action="" method="post" name="frmsiteuser" id="frmsiteuser" autocomplete="off" onSubmit="return validate()">
      <?php if($this->session->userdata('admin_type') == 1) {?>
	 <div class="row-fluid">
      	<div class="span6">
        <label>User Type</label>
            <select name="user_type" class="span12" id="user_type"  <?php if($this->session->userdata('admin_type') != 1){ echo "readonly" ;} ?>>
         
			<option value="">--Select--</option>
		 
            <?php if($this->session->userdata('admin_type') == 1){ ?>
            	<option value="1" <?php echo ($type==1) ?	'selected' : ''; ?>>Super Admin</option>
            	
                <option value="2" <?php echo ($type==2) ?	'selected' : ''; ?>>Admin</option>
                <?php } if($this->session->userdata('admin_type') == 1 || $this->session->userdata('admin_type') == 2 || $this->session->userdata('admin_type') == 3){   ?>
                <option value="3" <?php echo ($type==3) ?	'selected' : ''; ?>>User(Sales Person)</option>  
                <?php } ?>             
            </select>
            <span id="user-r" style="color:red"></span>
            <?php echo form_error('user_type','<span style="color:red"">','</span>'); ?>
           </div>
        </div>
        
        <div class="row-fluid">
      	<div class="span6">
        <label>Company</label>
            <select name="company_assigned" class="span12" id="company_name"  <?php if($this->session->userdata('admin_type') != 1){ echo "readonly" ;} ?>>
             
			<option value="" >--Select--</option>
			
                <?php  foreach($companies as $company){ echo $company->id. "=". $company_assigned;?>
                <option value="<?= $company->id;?>" <?php if($company_assigned == $company->id){ ?> selected="selected" <?php } ?>><?= $company->name ?></option>
                <?php } ?>  
			
            </select>
            <span id="user-r" style="color:red"></span>
            <?php echo form_error('user_type','<span style="color:red"">','</span>'); ?>
           </div>
        </div>
         <?php } ?>
        <div class="row-fluid">
        	<div class="span8">
                <label>Name <em style="color:#FF0000">*</em></label>
                <input type="text" name="name" id="name" value="<?php echo $name ?>" class="span4">
                <input type="text" placeholder="Middle Name" name="middle_name" id="middle_name" value="<?php echo $middle_name; ?>" class="span4">
                <input type="text" placeholder="Last Name" name="last_name" id="last_name" value="<?php echo $last_name; ?>" class="span4">                
                <?php echo form_error('name','<span style="color:red"">','</span>'); ?>
            </div>
        </div>
        <div class="row-fluid">
        	<div class="span6">
                <label>Email Address <em style="color:#FF0000">*</em></label>
                <input type="text" name="email" id="email" value="<?php echo $email ?>" class="span12"  	>
                <?php echo form_error('email','<span style="color:red"">','</span>'); ?><span id="email1"></span>
            </div>
		</div>
		
		
		<div class="row-fluid">
        	<div class="span6">
                <label>Password <em style="color:#FF0000">*</em></label>
                <input type="password" name="password" id="password" class="span12" value="<?php echo base64_decode($id); ?>"  >
                <?php echo form_error('password','<span style="color:red"">','</span>'); ?>
            </div>
        </div>        
       
        <div id="privilages" style="display:none;">      
            <div class="row-fluid"> 
            	<div class="span12">
                	<label>Profiles</label> 
                     <select name="privilage_profile[]" id="privilage_profile" multiple class="chzn-select span12">
                        <option>Select Privilages Profile</option>
                         <?php 				 
                            foreach($up_name as $up_name)
                            {
                            ?>						
                                <option value="<?php echo $up_name->up_id; ?>"><?php echo $up_name->up_name; ?></option>
                            <?php
                            }
                            ?>               
                     </select></br>
                     <span id="profile-r" style="color:red"></span>
                 </div>
              </div>
		  </div>
          <style>
		  .chosen-container-multi .chosen-choices li.search-field input[type="text"]{
			  height:22px;  
		  }
		  </style>
		  <div class="row-fluid" style="display:none;">
          	<label>Groups *</label>           
            <select name="groups[]" id="groups" multiple class="chzn-select span12" style="display:none;">
            <option value="">Select</option>
            <?php if(isset($groups)){?>
            	<?php 
				foreach($groups as $group){
					echo '<option value="'.$group->id.'">'.$group->group_name.'</option>';						
				}
				?>
            <?php } ?>
            </select>
          </div>
		<div class="row-fluid top7" >
			<div class="span12">
			<label>Mobile No.<em style="color:#FF0000"></em></label>
			<!--<input type="text" name="phone_no[]" id="" value="<?php echo $phone_no[0]; ?>" class="custom_fields span2"  />
			<input type="text" name="phone_no[]" id="" value="<?php echo $phone_no[1]; ?>" class="custom_fields span2">
			<input type="text" name="phone_no[]" id="" value="<?php echo $phone_no[2]; ?>" class="custom_fields span2">
			
			<input type="text" placeholder="Ext" name="phone_no[]" id="" value="<?php echo $phone_no[3]; ?>" class="custom_fields span2"> -->
			<input type="text" name="phone_no[]" id="" value="<?php echo $phone_no[0]; ?>" class="custom_fields span8" />
			</div>
		</div>
		
		<div class="row-fluid" >
			<div class="span12">
			<label>Landline No.<em style="color:#FF0000"></em></label>
			<!--<input type="text"  name="fax_no[]" id="" value="<?php echo $fax_no[0]; ?>" class="custom_fields span2" />
			<input type="text" name="fax_no[]" id="" value="<?php echo $fax_no[1]; ?>" class="custom_fields span2">
			<input type="text" name="fax_no[]" id="" value="<?php echo $fax_no[2]; ?>" class="custom_fields span2">
			<input type="text" placeholder="Ext" name="fax_no[]" id="" value="<?php echo $fax_no[3]; ?>" class="custom_fields span2"> -->
			<input type="text"  name="fax_no[]" id="" value="<?php echo $fax_no[0]; ?>" class="custom_fields span8" />
			
			</div>
		</div>
		 <?php if($this->session->userdata('admin_type') == 1){ ?>
		<div class="row-fluid">
        	<div class="span6">
                <label>Status <em style="color:#FF0000">*</em></label>
                <input type="radio" name="status" id="status_active" value="1" <?php if ($status==1) echo 'checked'; ?> />
                &nbsp;Active&nbsp;
                <input type="radio" name="status" id="status_inactive" value="0" <?php if ($status==0) echo 'checked'; ?> />
                &nbsp;Inactive <br />  
        	</div>
        </div>
	
        
        
        <!-- restriction for adding more then one products -->
        <div class="row-fluid" style="margin-top: 15px;">
        	<div class="span6">
                <label>Add more then one products <em style="color:#FF0000">*</em></label>
                <input type="radio" name="add_product_status" id="add_product_status_active" value="1" <?php if ($add_product_status==1) echo 'checked'; ?> />
                &nbsp;Allow&nbsp;
                <input type="radio" name="add_product_status" id="add_product_status_inactive" value="0" <?php if ($add_product_status==0) echo 'checked'; ?> />
                &nbsp;Disallow<br />  
        	</div>
        </div>
			 <?php } ?>
		<div class="row-fluid top10">
        	<input type="hidden" name="siteuserId" id="siteuserId" value="<?php echo $id ?>" />
            <input type="button" value="Cancel" class="btn btn-primary pull-left" onClick="close_pop_up();">
            <input type="submit" value="Add" class="btn btn-primary pull-left" id="add" style="margin-left:10px">
        </div>
        <div class="clearfix"></div>
        
      </form>
    </div>
    <!-- siteuser List -->
  </div>
</div>
</body>


<style>
.custom_fields{
	float:left; 
	width:50px;
	margin-left:7px;
}
</style>