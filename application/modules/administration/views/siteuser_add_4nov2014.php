<?php $this->load->view('includes/styles'); ?>
<?php $this->load->view('includes/scripts'); ?>
<script type="text/javascript">
function close_pop_up()
{
	parent.$.fancybox.close();
}
function seletPrivilage()
{
	var user_type = document.getElementById("user_type").value;
	if(user_type==2)
	{
		//document.getElementById("privilages").style.display = 'block';
		
	}
	if(user_type==1)
	{
		if(document.getElementById("privilages").style.display = 'block')
		{
			//document.getElementById("privilages").style.display = 'none';
		}
	}
	if(user_type==0)
	{
		if(document.getElementById("privilages").style.display = 'block')
		{
			//document.getElementById("privilages").style.display = 'none';
		}
	}
}
function validate()
{
	var user_type = document.getElementById("user_type").value;
	if(user_type==0)
	{
		document.getElementById("user-r").innerHTML = 'Please select any one user';
		return false;
	}
	var privilage_profile = document.getElementById("privilage_profile").value;
	if(user_type==0)
	{
		document.getElementById("profile").innerHTML = 'Please select any one profile';
		return false;
	}
}
var source;
checked=false;
function checkAll(source)
 {
	var array = document.getElementsByTagName("input");
	 if (checked == false)
          {
           checked = true
          }
        else
          {
          checked = false
          }

for(var ii = 0; ii < array.length; ii++)
{

   if(array[ii].type == "checkbox")
   {
      if(array[ii].className == source)
       {
        array[ii].checked = checked;

       }


   }
}
}
</script><body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<style>
.container1{
padding-left:30px;
}
.chosen-container{
width:216px!important;
}
</style>
<div class="container1">
  <div class="content">
    <!-- siteuser List -->
   
      <h2><?php echo $header['title'] ?></h2>
      <form action="" method="post" name="frmsiteuser" id="frmsiteuser" autocomplete="off" onSubmit="return validate()">
      <div class="row-fluid">
      	<div class="span6">
            <select name="user_type" onChange="seletPrivilage()" class="span12" id="user_type">
                <option value="0">Select User type</option>
                <option value="6">Super Admin</option>
                <option value="1">Admin</option>
                <option value="2">User</option>
                <option value="3">Location Manager</option>
                <option value="4">Department Manager</option>
                <option value="5">Supervisor</option>
            </select>
            <span id="user-r" style="color:red"></span>
            <?php echo form_error('user_type','<span style="color:red"">','</span>'); ?>
           </div>
        </div>
        <div class="row-fluid">
        	<div class="span8">
                <label>Name <em style="color:#FF0000">*</em></label>
                <input type="text" name="name" id="name" value="<?php echo set_value('name') ?>" class="span4">
                <input type="text" placeholder="Middle Name" name="middle_name" id="middle_name" value="<?php echo set_value('middle_name') ?>" class="span4">
                <input type="text" placeholder="Last Name" name="last_name" id="last_name" value="<?php echo set_value('last_name') ?>" class="span4">
                
                <?php echo form_error('name','<span style="color:red"">','</span>'); ?>
            </div>
        </div>
        <div class="row-fluid">
        	<div class="span6">
                <label>Email Address <em style="color:#FF0000">*</em></label>
                <input type="text" name="email" id="email" value="<?php echo set_value('email') ?>" class="span12">
                <?php echo form_error('email','<span style="color:red"">','</span>'); ?>
            </div>
		</div>
		
		
		<div class="row-fluid">
        	<div class="span6">
                <label>Password <em style="color:#FF0000">*</em></label>
                <input type="text" name="password" id="password" class="span12">
                <?php echo form_error('password','<span style="color:red"">','</span>'); ?>
            </div>
        </div>
        <div id="privilages" style="display:none;">      
            <div class="row-fluid"> 
            	<div class="span12">
                	<label>Profiles</label> 
                     <select name="privilage_profile[]" id="privilage_profile" multiple class="chzn-select span12">
                        <option>Select Privilages Profile</option>
                         <?php 				 
                            foreach($up_name as $up_name)
                            {
                            ?>						
                                <option value="<?php echo $up_name->up_id; ?>"><?php echo $up_name->up_name; ?></option>
                            <?php
                            }
                            ?>               
                     </select></br>
                     <span id="profile-r" style="color:red"></span>
                 </div>
              </div>
		  </div>
          <style>
		  .chosen-container-multi .chosen-choices li.search-field input[type="text"]{
			  height:22px;  
		  }
		  </style>
		  <div class="row-fluid" style="display:none;">
          	<label>Groups *</label>           
            <select name="groups[]" id="groups" multiple class="chzn-select span12" style="display:none;">
            <option value="">Select</option>
            <?php if(isset($groups)){?>
            	<?php 
				foreach($groups as $group){
					echo '<option value="'.$group->id.'">'.$group->group_name.'</option>';						
				}
				?>
            <?php } ?>
            </select>
          </div>
		<div class="row-fluid top7" >
			<div class="span12">
			<label>Phone No.<em style="color:#FF0000"></em></label>
			<input type="text" name="phone_no[]" id="" value="<?php echo set_value('phone_no') ?>" class="custom_fields span2" />
			<input type="text" name="phone_no[]" id="" value="<?php echo set_value('phone_no') ?>" class="custom_fields span2">
			<input type="text" name="phone_no[]" id="" value="<?php echo set_value('phone_no') ?>" class="custom_fields span2">
			<input type="text" placeholder="Ext" name="phone_no[]" id="" value="<?php echo set_value('phone_no') ?>" class="custom_fields span2">
			</div>
		</div>
		
		<div class="row-fluid" >
			<div class="span12">
			<label>Fax No.<em style="color:#FF0000"></em></label>
			<input type="text"  name="fax_no[]" id="" value="<?php echo set_value('fax_no') ?>" class="custom_fields span2" />
			<input type="text" name="fax_no[]" id="" value="<?php echo set_value('fax_no') ?>" class="custom_fields span2">
			<input type="text" name="fax_no[]" id="" value="<?php echo set_value('fax_no') ?>" class="custom_fields span2">
			<input type="text" placeholder="Ext" name="fax_no[]" id="" value="<?php echo set_value('fax_no') ?>" class="custom_fields span2">
			</div>
		</div>
		
		
		<div class="row-fluid">
            <input type="button" value="Cancel" class="btn btn-primary pull-left" onClick="close_pop_up();">
            <input type="submit" value="Add" class="btn btn-primary pull-left" style="margin-left:10px">
        </div>
        <div class="clearfix"></div>
        
      </form>
    </div>
    <!-- siteuser List -->
  </div>
</div>
</body>

<style>
.custom_fields{
	float:left; 
	width:50px;
	margin-left:7px;
}
</style>
