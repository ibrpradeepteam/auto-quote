<?php 
$this->load->view('includes/styles');

$id	= isset($_POST['siteuserId']) ? $_POST['siteuserId'] : (isset($siteuser->id)?$siteuser->id :NULL);

$name	= isset($_POST['name']) ? $_POST['name'] : (isset($siteuser->name)?$siteuser->name :NULL);
$middle_name	= isset($_POST['middle_name']) ? $_POST['middle_name'] : (isset($siteuser->middle_name)?$siteuser->middle_name :NULL);
$last_name	= isset($_POST['last_name']) ? $_POST['last_name'] : (isset($siteuser->last_name)?$siteuser->last_name :NULL);

$email	= isset($_POST['email']) ? $_POST['email'] : (isset($siteuser->email)?$siteuser->email :NULL);
$status	= isset($_POST['status']) ? $_POST['status'] : (isset($siteuser->status)?$siteuser->status :NULL);
$password	= isset($_POST['password']) ? $_POST['password'] : (isset($siteuser->password)?$siteuser->password :NULL);

$phone_no_val = isset($_POST['phone_no']) ? implode('-', $_POST['phone_no']) : (isset($siteuser->phone_no)?$siteuser->phone_no :NULL);
if(!empty($phone_no_val)){
	$phone_no = explode('-',$phone_no_val);
}else{
	$phone_no = array('','','','');
}

$fax_no_val = isset($_POST['fax_no']) ? implode('-', $_POST['fax_no']) : (isset($siteuser->fax_no) ? $siteuser->fax_no :NULL);
if(!empty($fax_no_val)){
	$fax_no = explode('-', $fax_no_val);
}else{
	$fax_no = array('','','','');
}

$privilage	= isset($_POST['privilage_profile']) ? $_POST['privilage_profile'] : (isset($siteuser->privilage)? explode(',', $siteuser->privilage) :NULL);
$u_groups	= isset($_POST['groups']) ? $_POST['groups'] : (isset($siteuser->group)? explode(',', $siteuser->group) :NULL);
$type = isset($_POST['user_type']) ? $_POST['user_type'] : (isset($siteuser->type)? $siteuser -> type :NULL);

?>
<script type="text/javascript">
function close_pop_up()
{
	parent.$.fancybox.close();
}
var source;
checked=false;
function checkAll(source)
 {
	var array = document.getElementsByTagName("input");
	 if (checked == false)
          {
           checked = true
          }
        else
          {
          checked = false
          }

for(var ii = 0; ii < array.length; ii++)
{

   if(array[ii].type == "checkbox")
   {
      if(array[ii].className == source)
       {
        array[ii].checked = checked;

       }


   }
}
}

function seletPrivilage()
{
	var user_type = document.getElementById("user_type").value;
	if(user_type==2)
	{
		//document.getElementById("privilages").style.display = 'block';
	}
	if(user_type==1)
	{
		if(document.getElementById("privilages").style.display = 'block')
		{
			//document.getElementById("privilages").style.display = 'none';
		}
	}
	if(user_type==0)
	{
		if(document.getElementById("privilages").style.display = 'block')
		{
			//document.getElementById("privilages").style.display = 'none';
		}
	}
}
</script><body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<style>
.container1{
padding-left:30px;
}
.chosen-container{
width:216px!important;
}
</style>
<div class="container1">
  <div class="content">
    <!-- siteuser List -->
  
      <h2><?php echo $header['title'] ?></h2>
      
      <form action="" method="post" name="frmsiteuser" id="frmsiteuser" autocomplete="on">
        
		<input type="hidden" name="siteuserId" id="siteuserId" value="<?php echo $id ?>" />
        <div class="row-fluid">
        	<div class="span6">
                <select name="user_type" onChange="seletPrivilage()" id="user_type"  class="span12"value='<?php echo $type ?>'>
                    <option value="0">Select User type</option>
                    <option value="6" <?php echo ($type==6) ?	'selected' : ''; ?>>Super Admin</option>
                    <option value="1" <?php echo ($type==1) ?	'selected' : ''; ?>>Admin</option>
                    <option value="2" <?php echo ($type==2) ?	'selected' : ''; ?>>User</option>
                    <option value="3" <?php echo ($type==3) ?	'selected' : ''; ?>>Location Manager</option>
                    <option value="4" <?php echo ($type==4) ?	'selected' : ''; ?>>Department Manager</option>
                    <option value="5" <?php echo ($type==5) ?	'selected' : ''; ?>>Supervisor</option>
                </select>
        	</div>
        </div>
        <div class="row-fluid">
        	<div class="span8">
                <label>Name <em style="color:#FF0000">*</em></label>
                <input type="text" name="name" id="name" value="<?php echo $name ?>" class="span4">
                <input type="text" placeholder="Middle Name" name="middle_name" id="middle_name" value="<?php echo $middle_name; ?>" class="span4">
                <input type="text" placeholder="Last Name" name="last_name" id="last_name" value="<?php echo $last_name; ?>" class="span4">
                <?php echo form_error('name','<span style="color:red"">','</span>'); ?>
        	</div>
        </div>
        <div class="row-fluid">
        	<div class="span6">
                <label>Email Address <em style="color:#FF0000">*</em></label>
                <input type="text" name="email" id="email" value="<?php echo $email ?>" class="span12">
                <?php echo form_error('email','<span style="color:red"">','</span>'); ?>
            </div>
        </div>
        <div class="row-fluid">
        	<div class="span6">
                <label>Password</label>
                <input type="text" name="password" id="password" class="span12" value="<?php echo $password ?>">
            </div>
        </div>
        <div class="row-fluid">
        	<div class="span6">
                <label>Status <em style="color:#FF0000">*</em></label>
                <input type="radio" name="status" id="status_active" value="1" <?php if ($status==1) echo 'checked'; ?> />
                &nbsp;Active&nbsp;
                <input type="radio" name="status" id="status_inactive" value="0" <?php if ($status==0) echo 'checked'; ?> />
                &nbsp;Inactive <br />  
        	</div>
        </div>
          <style>
		  .chosen-container-multi .chosen-choices li.search-field input[type="text"]{
			  height:22px;  
		  }
		  </style>
		  <div class="row-fluid" >
			<div class="span12">
			<label >Phone No.<em style="color:#FF0000"></em></label>
			<input type="text" name="phone_no[]" id="" value="<?php echo $phone_no[0]; ?>" class="custom_fields span2" />
			<input type="text" name="phone_no[]" id="" value="<?php echo $phone_no[1]; ?>" class="custom_fields span2">
			<input type="text" name="phone_no[]" id="" value="<?php echo $phone_no[2]; ?>" class="custom_fields span2">
			<input type="text" placeholder="Ext" name="phone_no[]" id="" value="<?php echo $phone_no[3]; ?>" class="custom_fields span2">
			</div>
		</div>
		
		<div class="row-fluid" >
			<div class="span12">
			<label >Fax No.<em style="color:#FF0000"></em></label>
			<input type="text"  name="fax_no[]" id="" value="<?php echo $fax_no[0]; ?>" class="custom_fields span2" />
			<input type="text" name="fax_no[]" id="" value="<?php echo $fax_no[1]; ?>" class="custom_fields span2">
			<input type="text" name="fax_no[]" id="" value="<?php echo $fax_no[2]; ?>" class="custom_fields span2">
			<input type="text" placeholder="Ext" name="fax_no[]" id="" value="<?php echo $fax_no[3]; ?>" class="custom_fields span2">
			</div>
		</div>
		<div class="row-fluid">
            <input type="button" value="Cancel" class="btn btn-primary pull-left" onClick="close_pop_up();">
            <input type="submit" value="Update" class="btn btn-primary pull-left" style="margin-left:10px">
        </div>
        <div class="clearfix"></div>
      </form>
    </div>
    <!-- siteuser List -->
  </div>
</div>
</body>


<style>
.custom_fields{
	float:left; 
	width:50px;
	margin-left:7px;
}
</style>
