<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');
	
}
?>

<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
    load_siteuser_list();
	
	$(".fancybox-add").fancybox({
    	type: 'iframe',
    	href: '<?php echo site_url('administration/siteuser/siteuser_add') ?>',
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '550',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
});

function load_siteuser_list() {
    $("#siteuserList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
	"aaSorting": [[ 4, "desc" ]],
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                   // $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo site_url(); ?>/administration/siteuser/ajax_list_siteuser?type=<?php echo $this->uri->segment(3); ?>",

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [

            //{"sClass": "center"},
            {
                "mRender": function (data, type, oObj) {
                    var a = oObj[1];
                    return (a);

                }

            }, {
                "mRender": function (data, type, oObj) {
                    var a = oObj[2];;
                    return (a);

                }

            }, {
                "mRender": function (data, type, oObj) {
                    var a = oObj[3];;
                    if (a == 1) {
                        a = '<img src="<?php echo base_url('images/green-dot.png') ?>" onClick="ajax_status(0,' + oObj[0] + ')" class="cursor-pointer"/>&nbsp;(Active)';
                    } else {
                        a = '<img src="<?php echo base_url('images/red-dot.png') ?>" onClick="ajax_status(1,' + oObj[0] + ')" class="cursor-pointer"/>&nbsp;(Disable)';
                    }
                    return (a);

                }

            }, {
                "mRender": function (data, type, oObj) {
                    // var a = oObj.aData[2];
                    var jobId = oObj[0];
                    a = '<a href="" onClick="edit_siteuser('+oObj[0]+')" data-toggle="modal"><button class="btn btn- btn-phone-block"><icon class="icon-pencil icon-white"></icon><span class="hidden-phone">Edit</span></button></a>&nbsp;<a href="" onClick="siteuser_delete('+oObj[0]+')" data-toggle="modal"><button class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><span class="hidden-phone">Delete</span></button></a>';
                    return (a);

                }

            }
        ]
    });

}



function siteuser_delete(id)
{
	var r=confirm("Are you sure want to delete siteuser ..?");
	if (r==true)
  	{
	$.ajax({
		type: "POST",
		url: "<?php echo site_url('administration/siteuser/ajax_siteuser_delete'); ?>",
		data:{id:id},
		success:function(data){
			
			if(data=="done")
			{
				load_siteuser_list(); 
			} 
			
	}});
	}
}

function edit_siteuser(id)
{
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo site_url() ?>/administration/siteuser/siteuser_edit/'+id,
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '550',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}
function ajax_status(status,id){
	$.ajax({
		type: "POST",
		url: "<?php echo site_url('administration/siteuser/ajax_status'); ?>",
		data:{status:status,id:id},
		success:function(data){			
			if(data=="done"){
				load_siteuser_list(); 
			} 		
		}
	});
}
</script>
<aside class="right-side">
    <div class="col-md-12">
        <h1>Manage user</h1>
        <div class="table">
            <a class="fancybox-add btn btn-primary" onclick="javascript:void(0)" href="">Add New</a>
            <label style="font-size:13px; color:green"><?php echo $this->session->flashdata('success') ?></label>
              <!-- siteuser List -->
              <table id="siteuserList" width="100%" class="dataTable table table-striped table-bordered">
                <thead>
                  <tr>
                    
                    <th width="22%">Name</th>
                    <th width="22%">Email</th>
                    <th width="22%">Status</th>
                    <th width="22%">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              <!-- siteuser List -->
        </div>
    </div>
</aside>