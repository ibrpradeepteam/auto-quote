<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('include/header');
}
?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
    load_siteuser_list();
	
	$(".fancybox-add").fancybox({
    	type: 'iframe',
    	href: '<?php echo site_url('administration/profile/profile_add') ?>',
    	autoSize: false,
    	closeBtn: true,
    	width: '450',
    	height: '480',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
});

function load_siteuser_list() {
    $("#siteuserList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
		"aaSorting": [[ 4, "desc" ]],
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo site_url(); ?>/administration/profile/ajax_list_profile",

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [{
                "sClass": "center"
            },

            //{"sClass": "center"},
            {
                "fnRender": function (oObj) {
                    var a = oObj.aData[1];
                    return (a);

                }

            },  {
                "fnRender": function (oObj) {
                    var a = oObj.aData[3];;
                    if (a == 1) {
                        a = '<img src="<?php echo base_url('images/green-dot.png') ?>" onClick="ajax_status(0,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Active)';
                    } else {
                        a = '<img src="<?php echo base_url('images/red-dot.png') ?>" onClick="ajax_status(1,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Disable)';
                    }
                    return (a);

                }

            }, {
                "fnRender": function (oObj) {
                    // var a = oObj.aData[2];
                    var jobId = oObj.aData[0];
                    a = '<a href="javascript:void(0);" onClick="edit_siteuser('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn- btn-phone-block"><icon class="icon-pencil icon-white"></icon><span class="hidden-phone">Edit</span></button></a>&nbsp;<a href="javascript:void(0)" onClick="profile_delete('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><span class="hidden-phone">Delete</span></button></a>';
                    return (a);

                }

            }
        ]
    });

}

function ajax_status(status,id)
{
	$.ajax({
		type: "POST",
		url: "<?php echo site_url('administration/siteuser/profile_ajax_status'); ?>",
		data:{status:status,id:id},
		success:function(data){
			
			if(data=="done")
			{
				load_siteuser_list(); 
			} 
			
	}});
}

function profile_delete(id)
{
	var r=confirm("Are you sure want to delete siteuser ..?");
	if (r==true)
  	{
	$.ajax({
		type: "POST",
		url: "<?php echo site_url('administration/profile/ajax_profile_delete'); ?>",
		data:{id:id},
		success:function(data){
			
			if(data=="done")
			{
				load_siteuser_list(); 
			} 
			
	}});
	}
}

function edit_siteuser(id)
{
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo site_url() ?>/administration/profile/profile_edit/'+id,
    	autoSize: false,
    	closeBtn: true,
    	width: '450',
    	height: '480',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}
</script>
<br>
<h1>Manage Profile</h1>
<div class="table"> <!--<img src="<?php // echo site_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> <!--<img src="<?php //echo site_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<a class="fancybox-add btn btn-primary" onclick="javascript:void(0)" href="">Add New</a>
	<label style="font-size:13px; color:green"><?php echo $this->session->flashdata('success') ?></label>
  <!-- siteuser List -->
  <table id="siteuserList" width="100%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">Sr.</th>
        <th width="22%">Profile Name</th>
       
        <th width="22%">Status</th>
        <th width="22%">Action</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <!-- siteuser List -->
</div>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	//$this->load->view('administration/includes/footer');
}
?>
