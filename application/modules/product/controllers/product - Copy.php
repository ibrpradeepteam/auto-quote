<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {
	
	 function __construct() {
		 
		 parent::__construct();
		 
		 if(!is_login()){
			redirect(base_url()); 
		 }
		 $this->load->model('product_model'); 
		 $this->load->library( 'parser' );
		 //$this->load->library( 'UploadHandler' );
		 }
		 
		 public function get_products(){
		 $this->db->select('products.product_name,products.motor,products.price,rq_rqf_quote_catlogs.catlog_title');
		 $this->db->from('products');
		 $this->db->leftjoin('rq_rqf_quote_catlogs','products.product_category = rq_rqf_quote_catlogs.id');
		 $query = $this->db->get();
		 echo json_encode($query->result());
		 }
		 
		 public function upload_image(){
		 
		 }
		 
		 public function save_row(){
		 $id = $this->input->post('id');
		 $products['product_name'] = $this->input->post('name');
		 $products['motor'] = $this->input->post('motor');
		 $products['price'] = $this->input->post('price');
		 $products['product_size'] = $this->input->post('size');
		 $products['product_weight'] = $this->input->post('weight');
		 $products['water_temprature'] = $this->input->post('water_temp');
		 $products['max_pump_pressure'] = $this->input->post('max_pump_pressure');
		 $products['pump_set_pressure'] = $this->input->post('pump_set_pressure');
		 
		 $products['product_category'] = $this->input->post('category');
		 $products['feature1'] = $this->input->post('feature1');
		 $products['feature2'] = $this->input->post('feature2');
		 $products['feature3'] = $this->input->post('feature3');
		 $products['feature4'] = $this->input->post('feature4');
		 $products['feature5'] = $this->input->post('feature5');
		 $products['feature6'] = $this->input->post('feature6');
		 $products['feature7'] = $this->input->post('feature7');
		 $products['feature8'] = $this->input->post('feature8');
		 $products['feature9'] = $this->input->post('feature9');
		 $products['product_image'] = $this->input->post('image');
		 $products['product_image'] = substr($products['product_image'],12);
		 $this->db->update('products',$products,array('id'=>$id));
		 if($this->db->affected_rows() != 0){
		 echo true;
		 }
		 }
		 
	 public function index() { 
		$data['page_title'] = 'Product';
		$data['title_small'] = 'List';
		$data['base_url'] = base_url();
		$data['header'] = $this->parser->parse('includes/header', $data, true);
	    	$data['footer'] = $this->parser->parse('includes/footer', $data, true);
		$data['page_header'] = $this->parser->parse('includes/page_header', $data, true);
		$data['products']    = $this->product_model->get_products_test();
		$products    = $this->product_model->get_products();
		$json = json_encode($products);
		$data['product'] = form_safe_json($json);
		
		$data['category_catlog'] = $this->get_category_catlog();
		$data['feature1'] = $this->get_feature1();
		$data['feature2'] = $this->get_feature2();
		$data['feature3'] = $this->get_feature3();
		$data['feature4'] = $this->get_feature4();
		$data['feature5'] = $this->get_feature5();
		$data['feature6'] = $this->get_feature6();
		$data['feature7'] = $this->get_feature7();
		$data['feature8'] = $this->get_feature8();
		$data['feature9'] = $this->get_feature9();
		
		//$data['category_catlog'] = json_encode($category_catlog);
		//$data['category_catlog'] = form_safe_json($category_catlog_json);
		$this->parser->parse('product_list', $data);	 
		}	 
		 
	public function get_category_catlog(){
	$this->db->select('id');
	$this->db->from('rq_rqf_quote_catlog_type');
	$this->db->where('catlog_type','category');
	$query = $this->db->get();
	$result = $query->row_array();
	
	$this->db->select('rq_rqf_quote_catlogs.id,rq_rqf_quote_catlogs.catlog_title');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('catlog_type',$result['id']);
	$query = $this->db->get();
	$data = $query->result();
	return $data;
	}
	public function get_feature1(){
	$this->db->select('id');
	$this->db->from('rq_rqf_quote_catlog_type');
	$this->db->where('catlog_type','Specification');
	$query = $this->db->get();
	$result = $query->row_array();
	
	$this->db->select('rq_rqf_quote_catlogs.id,rq_rqf_quote_catlogs.catlog_title');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('catlog_type',$result['id']);
	$query = $this->db->get();
	$data = $query->result();
	//print_r($data);
	return $data;
	}
	public function get_feature2(){
	$this->db->select('id');
	$this->db->from('rq_rqf_quote_catlog_type');
	$this->db->where('catlog_type','Motor / Engine');
	$query = $this->db->get();
	$result = $query->row_array();
	
	$this->db->select('rq_rqf_quote_catlogs.id,rq_rqf_quote_catlogs.catlog_title');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('catlog_type',$result['id']);
	$query = $this->db->get();
	$data = $query->result();
	//print_r($data);
	return $data;
	}
	public function get_feature3(){
	$this->db->select('id');
	$this->db->from('rq_rqf_quote_catlog_type');
	$this->db->where('catlog_type','Pump');
	$query = $this->db->get();
	$result = $query->row_array();
	
	$this->db->select('rq_rqf_quote_catlogs.id,rq_rqf_quote_catlogs.catlog_title');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('catlog_type',$result['id']);
	$query = $this->db->get();
	$data = $query->result();
	//print_r($data);
	return $data;
	}
	public function get_feature4(){
	$this->db->select('id');
	$this->db->from('rq_rqf_quote_catlog_type');
	$this->db->where('catlog_type','Boiler');
	$query = $this->db->get();
	$result = $query->row_array();
	
	$this->db->select('rq_rqf_quote_catlogs.id,rq_rqf_quote_catlogs.catlog_title');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('catlog_type',$result['id']);
	$query = $this->db->get();
	$data = $query->result();
	//print_r($data);
	return $data;
	}
	public function get_feature5(){
	$this->db->select('id');
	$this->db->from('rq_rqf_quote_catlog_type');
	$this->db->where('catlog_type','Frame');
	$query = $this->db->get();
	$result = $query->row_array();
	
	$this->db->select('rq_rqf_quote_catlogs.id,rq_rqf_quote_catlogs.catlog_title');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('catlog_type',$result['id']);
	$query = $this->db->get();
	$data = $query->result();
	//print_r($data);
	return $data;
	}
	public function get_feature6(){
	$this->db->select('id');
	$this->db->from('rq_rqf_quote_catlog_type');
	$this->db->where('catlog_type','Water Tank');
	$query = $this->db->get();
	$result = $query->row_array();
	
	$this->db->select('rq_rqf_quote_catlogs.id,rq_rqf_quote_catlogs.catlog_title');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('catlog_type',$result['id']);
	$query = $this->db->get();
	$data = $query->result();
	//print_r($data);
	return $data;
	}
	public function get_feature7(){
	$this->db->select('id');
	$this->db->from('rq_rqf_quote_catlog_type');
	$this->db->where('catlog_type','By-Pass');
	$query = $this->db->get();
	$result = $query->row_array();
	
	$this->db->select('rq_rqf_quote_catlogs.id,rq_rqf_quote_catlogs.catlog_title');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('catlog_type',$result['id']);
	$query = $this->db->get();
	$data = $query->result();
	//print_r($data);
	return $data;
	}
	
	public function get_feature8(){
	$this->db->select('id');
	$this->db->from('rq_rqf_quote_catlog_type');
	$this->db->where('catlog_type','Control Box');
	$query = $this->db->get();
	$result = $query->row_array();
	
	$this->db->select('rq_rqf_quote_catlogs.id,rq_rqf_quote_catlogs.catlog_title');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('catlog_type',$result['id']);
	$query = $this->db->get();
	$data = $query->result();
	//print_r($data);
	return $data;
	}
	public function get_feature9(){
	$this->db->select('id');
	$this->db->from('rq_rqf_quote_catlog_type');
	$this->db->where('catlog_type','Hose / Gun');
	$query = $this->db->get();
	$result = $query->row_array();
	
	$this->db->select('rq_rqf_quote_catlogs.id,rq_rqf_quote_catlogs.catlog_title');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('catlog_type',$result['id']);
	$query = $this->db->get();
	$data = $query->result();
	//print_r($data);
	return $data;
	}
	
	/*** add product into database ***/	 
	 public function add_product($product_id='') {
		
		 if($this->input->post()){	 		 
			 $this->product_model->add_product();
			 redirect(base_url('product'));	
		 }else{
			 $data['title'] 		= 'Add product';
			 $data['header'] 		= $this->load->view('includes/header', '', true);
			 $data['footer'] 		= $this->load->view('includes/footer', '', true);
			 $data['base_url'] 		= base_url();
			 if($product_id!='')
			 { 
			$product_detail			= $this->product_model->get_product_detail($product_id);
			$data['product_id']		= $product_detail->id;
			$detail_json			= json_encode($product_detail);
			$data['detail'] = form_safe_json($detail_json); 
			 }
			 $data['category'] = get_active_catlog_value_dropdown($type='Category',$selected=isset($product_detail->product_category) ? $product_detail->product_category : '',$attr='class="form-control input-sm"','product_category',$id='cat_f');
	
			 $data['specification'] = get_active_catlog_value_dropdown($type='Specification',$selected=isset($product_detail->feature1) ? $product_detail->feature1 : '',$attr='class="form-control input-sm"','feature1',$id='cat_f');
			 $data['motor'] 		= get_active_catlog_value_dropdown($type='Motor / Engine',$selected=isset($product_detail->feature2) ? $product_detail->feature2 : '',$attr='class="form-control input-sm"','feature2',$id='cat_f');
			 $data['pump'] 			= get_active_catlog_value_dropdown($type='Pump',$selected=isset($product_detail->feature3) ? $product_detail->feature3 : '',$attr='class="form-control input-sm"','feature3',$id='cat_f');
			 $data['boiler'] 		= get_active_catlog_value_dropdown($type='Boiler',$selected=isset($product_detail->feature4) ? $product_detail->feature4 : '',$attr='class="form-control input-sm"','feature4',$id='cat_f');
			 $data['frame'] 		= get_active_catlog_value_dropdown($type='Frame',$selected=isset($product_detail->feature5) ? $product_detail->feature5 : '',$attr='class="form-control input-sm"','feature5',$id='cat_f');
			 $data['water_tank'] 	= get_active_catlog_value_dropdown($type='Water Tank',$selected=isset($product_detail->feature6) ? $product_detail->feature6 : '',$attr='class="form-control input-sm"','feature6',$id='cat_f');
			 $data['by_pass'] 		= get_active_catlog_value_dropdown($type='By-Pass',$selected=isset($product_detail->feature7) ? $product_detail->feature7 : '',$attr='class="form-control input-sm"','feature7',$id='cat_f');
			 $data['control_box'] 	= get_active_catlog_value_dropdown($type='Control Box',$selected=isset($product_detail->feature8) ? $product_detail->feature8 : '',$attr='class="form-control input-sm"','feature8',$id='cat_f');
			 $data['hose_gun'] 		= get_active_catlog_value_dropdown($type='Hose / Gun',$selected=isset($product_detail->feature9) ? $product_detail->feature9 : '',$attr='class="form-control input-sm"','feature9',$id='cat_f');
			 
			 $this->parser->parse('add_product', $data);	 
		 }
	 }
	 
	 public function products_delete(){
		 foreach($_POST['delete_check'] as $id){
            $this->db->update('products', array('is_deleted' => 1), array('id'=>$id));
        }
		redirect('product');	 
	}
	
	public function delete_bulk_products(){
	$products = $this->input->post('products');
	 foreach($products as $id){
            $this->db->update('products', array('is_deleted' => 1), array('id'=>$id));
        }
	}
	
	public function get_product_features(){
		$ids = $this->input->post('id');
		//$ids = explode(',',$ids);
		$this->db->select('*');
		$this->db->from('products');
		$this->db->where('id',$ids);
		$query = $this->db->get();
		//echo $this->db->last_query();
		echo json_encode($query->row());
		}
		
	public function delete_product_image(){
	$id = $this->input->post('id');
	$this->db->update('products',array('product_image' => ''),array('id' => $id));
	//echo $this->db->affected_rows();
	if($this->db->affected_rows() != 0)
	{
	echo 1;	
	}	
	}
	
	public function get_catlog_dropdown(){
	$selected_type = $this->input->post('selected');
	$options = array();
	$this->db->select('id');
	$this->db->from('rq_rqf_quote_catlog_type');
	$this->db->where('catlog_type',$selected_type);
	$query = $this->db->get();
	$result = $query->row_array();
	
	$this->db->select('rq_rqf_quote_catlogs.id,rq_rqf_quote_catlogs.catlog_title');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('catlog_type',$result['id']);
	$query = $this->db->get();
	$data = $query->result();
	$options[] = '<option value="">--Select--</option>';
	foreach($data as $d){
	$options[] .= '<option value="'.$d->id.'">'.$d->catlog_title.'</option>';
	}
	echo json_encode($options);
	}
	
	public function update_bulk_products(){
	$ids = $this->input->post('product_ids');
	$field_value = $this->input->post('change_field');
	$field_name = $this->input->post('field_name');
	foreach($ids as $id){
	if($field_name == "Category"){
	$this->db->update('products',array('product_category'=>$field_value),array('id'=>$id));
	}
	if($field_name == "Specification"){
	$this->db->update('products',array('feature1'=>$field_value),array('id'=>$id));
	}
	if($field_name == "Motor / Engine"){
	$this->db->update('products',array('feature2'=>$field_value),array('id'=>$id));
	}
	if($field_name == "Pump"){
	$this->db->update('products',array('feature3'=>$field_value),array('id'=>$id));
	}
	if($field_name == "Boiler"){
	$this->db->update('products',array('feature4'=>$field_value),array('id'=>$id));
	}
	if($field_name == "Frame"){
	$this->db->update('products',array('feature5'=>$field_value),array('id'=>$id));
	}
	if($field_name == "Water Tank"){
	$this->db->update('products',array('feature6'=>$field_value),array('id'=>$id));
	}
	if($field_name == "By-Pass"){
	$this->db->update('products',array('feature7'=>$field_value),array('id'=>$id));
	}
	if($field_name == "Control Box"){
	$this->db->update('products',array('feature8'=>$field_value),array('id'=>$id));
	}
	if($field_name == "Hose / Gun"){
	$this->db->update('products',array('feature9'=>$field_value),array('id'=>$id));
	}
	}
	if($this->db->affected_rows() != 0){
	echo true;
	}
	}
	
	public function update_bulk_products_text(){
	$ids = $this->input->post('product_ids');
	$field_value = $this->input->post('change_field');
	$field_name = $this->input->post('field_name');
	
	foreach($ids as $id){
	if($field_name == 'Product Name'){
	$this->db->update('products',array('product_name'=>$field_value),array('id'=>$id));
	}
	if($field_name == 'Motor'){
	$this->db->update('products',array('motor'=>$field_value),array('id'=>$id));
	}
	if($field_name == 'Price'){
	$this->db->update('products',array('price'=>$field_value),array('id'=>$id));
	}
	if($field_name == 'Max Pump Pressure'){
	$this->db->update('products',array('max_pump_pressure'=>$field_value),array('id'=>$id));
	}
	if($field_name == 'Pump Set Pressure'){
	$this->db->update('products',array('pump_set_pressure'=>$field_value),array('id'=>$id));
	}
	if($field_name == 'Water Temprature'){
	$this->db->update('products',array('water_temprature'=>$field_value),array('id'=>$id));
	}
	if($field_name == 'Size'){
	$this->db->update('products',array('product_size'=>$field_value),array('id'=>$id));
	}
	if($field_name == 'Weight'){
	$this->db->update('products',array('product_weight'=>$field_value),array('id'=>$id));
	}
	}
	if($this->db->affected_rows() != 0){
	echo true;
	}
	}
	
	public function export_csv(){
		
		//echo "sdsaddfsfdfdg";
		// Fetch Record from Database

$output			= "";
$table 			= ""; // Enter Your Table Name
$sql 			= mysql_query("select * from products");
$columns_total 	= mysql_num_fields($sql);

// Get The Field Name

for ($i = 0; $i < $columns_total; $i++) {
	$heading	=	mysql_field_name($sql, $i);
	$output		.= '"'.$heading.'",';
}
$output .="\n";

// Get Records from the table

while ($row = mysql_fetch_array($sql)) {
for ($i = 0; $i < $columns_total; $i++) {
$output .='"'.$row["$i"].'",';
}
$output .="\n";
}

// Download the file

$filename =  "productcsv.csv";
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);

echo $output;
return ;
//exit;
	
		}
	
	}

?>