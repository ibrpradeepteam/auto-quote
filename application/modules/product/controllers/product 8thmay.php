<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {
	
	 function __construct() {
		 
		 parent::__construct();
		 
		 if(!is_login()){
			redirect(base_url()); 
		 }
		 $this->load->model('product_model'); 
		 $this->load->library( 'parser' );
		 }
		 
		 
	 public function index() { 
		$data['page_title'] = 'Product';
		$data['title_small'] = 'List';
		$data['base_url'] = base_url();
		$data['header'] = $this->parser->parse('includes/header', $data, true);
	    	$data['footer'] = $this->parser->parse('includes/footer', $data, true);
		$data['page_header'] = $this->parser->parse('includes/page_header', $data, true);
		
		$products    = $this->product_model->get_products();
		$json = json_encode($products);
		$data['product'] = form_safe_json($json);
		$this->parser->parse('product_list', $data);	 
		}	 
		 
	
	/*** add product into database ***/	 
	 public function add_product($product_id='') {
		
		 if($this->input->post()){	 		 
			 $this->product_model->add_product();
			 redirect(base_url('product'));	
		 }else{
			 $data['title'] 		= 'Add product';
			 $data['header'] 		= $this->load->view('includes/header', '', true);
			 $data['footer'] 		= $this->load->view('includes/footer', '', true);
			 $data['base_url'] 		= base_url();
			 if($product_id!='')
			 { 
			$product_detail			= $this->product_model->get_product_detail($product_id);
			$data['product_id']		= $product_detail->id;
			$detail_json			= json_encode($product_detail);
			$data['detail'] = form_safe_json($detail_json); 
			 }
			 $data['category'] = get_active_catlog_value_dropdown($type='Category',$selected=isset($product_detail->product_category) ? $product_detail->product_category : '',$attr='class="form-control input-sm"','product_category',$id='cat_f');
	
			 $data['specification'] = get_active_catlog_value_dropdown($type='Specification',$selected=isset($product_detail->feature1) ? $product_detail->feature1 : '',$attr='class="form-control input-sm"','feature1',$id='cat_f');
			 $data['motor'] 		= get_active_catlog_value_dropdown($type='Motor / Engine',$selected=isset($product_detail->feature2) ? $product_detail->feature2 : '',$attr='class="form-control input-sm"','feature2',$id='cat_f');
			 $data['pump'] 			= get_active_catlog_value_dropdown($type='Pump',$selected=isset($product_detail->feature3) ? $product_detail->feature3 : '',$attr='class="form-control input-sm"','feature3',$id='cat_f');
			 $data['boiler'] 		= get_active_catlog_value_dropdown($type='Boiler',$selected=isset($product_detail->feature4) ? $product_detail->feature4 : '',$attr='class="form-control input-sm"','feature4',$id='cat_f');
			 $data['frame'] 		= get_active_catlog_value_dropdown($type='Frame',$selected=isset($product_detail->feature5) ? $product_detail->feature5 : '',$attr='class="form-control input-sm"','feature5',$id='cat_f');
			 $data['water_tank'] 	= get_active_catlog_value_dropdown($type='Water Tank',$selected=isset($product_detail->feature6) ? $product_detail->feature6 : '',$attr='class="form-control input-sm"','feature6',$id='cat_f');
			 $data['by_pass'] 		= get_active_catlog_value_dropdown($type='By-Pass',$selected=isset($product_detail->feature7) ? $product_detail->feature7 : '',$attr='class="form-control input-sm"','feature7',$id='cat_f');
			 $data['control_box'] 	= get_active_catlog_value_dropdown($type='Control Box',$selected=isset($product_detail->feature8) ? $product_detail->feature8 : '',$attr='class="form-control input-sm"','feature8',$id='cat_f');
			 $data['hose_gun'] 		= get_active_catlog_value_dropdown($type='Hose / Gun',$selected=isset($product_detail->feature9) ? $product_detail->feature9 : '',$attr='class="form-control input-sm"','feature9',$id='cat_f');
			 
			 $this->parser->parse('add_product', $data);	 
		 }
	 }
	 
	 public function products_delete(){
		 foreach($_POST['delete_check'] as $id){
            $this->db->update('products', array('is_deleted' => 1), array('id'=>$id));
        }
		redirect('product');	 
	}
	
	public function get_product_features(){
		$ids = $this->input->post('id');
		//$ids = explode(',',$ids);
		$this->db->select('*');
		$this->db->from('products');
		$this->db->where('id',$ids);
		$query = $this->db->get();
		//echo $this->db->last_query();
		echo json_encode($query->row());
		}
		
	public function delete_product_image(){
	$id = $this->input->post('id');
	$this->db->update('products',array('product_image' => ''),array('id' => $id));
	//echo $this->db->affected_rows();
	if($this->db->affected_rows() != 0)
	{
	echo 1;	
	}	
	}
	
	public function export_csv(){
		
		//echo "sdsaddfsfdfdg";
		// Fetch Record from Database

$output			= "";
$table 			= ""; // Enter Your Table Name
$sql 			= mysql_query("select * from products");
$columns_total 	= mysql_num_fields($sql);

// Get The Field Name

for ($i = 0; $i < $columns_total; $i++) {
	$heading	=	mysql_field_name($sql, $i);
	$output		.= '"'.$heading.'",';
}
$output .="\n";

// Get Records from the table

while ($row = mysql_fetch_array($sql)) {
for ($i = 0; $i < $columns_total; $i++) {
$output .='"'.$row["$i"].'",';
}
$output .="\n";
}

// Download the file

$filename =  "productcsv.csv";
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);

echo $output;
return ;
//exit;
	
		}
	
	}

?>