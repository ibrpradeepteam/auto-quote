<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quote extends CI_Controller {
	
	 function __construct() {
		 
		 parent::__construct();
		 
		 if(!is_login()){
			redirect(base_url()); 
		 }
		 $this->load->model('quote_model'); 
		 $this->load->model('product_model'); 
		 $this->load->library( 'parser' );
		 }
		 
		 
	 public function index() { 
		$data['page_title'] = 'Quote';
		$data['title_small'] = 'List';
		$data['base_url'] = base_url();
		$data['header'] = $this->parser->parse('includes/header', $data, true);
	    $data['footer'] = $this->parser->parse('includes/footer', $data, true);
		$data['page_header'] = $this->parser->parse('includes/page_header', $data, true);
		
		$data['quote']  = $this->session->userdata('quote');
		
		$products    = $this->quote_model->get_quotes();
		$data['product'] = json_encode($products);
		$this->parser->parse('quote_list', $data);	 
		}	 
		 
	
	/*** add product into database ***/	 
	 public function add_quote($quote_id='') {
	 
		
		 if($this->input->post()){	 		 
			 $this->product_model->add_product();
			 redirect(base_url('product'));	
		 }else{
			 $data['title'] 		= 'Add quote';
			 $data['header'] 		= $this->load->view('includes/header', '', true);
			 $data['footer'] 		= $this->load->view('includes/footer', '', true);
			 $data['base_url'] 		= base_url();
			 if($quote_id!='')
			 { 
			$quote_detail			= $this->quote_model->get_quote_detail($quote_id);
			$data['quote_id']		= $quote_detail->id;
			$data['customer_id']		= $quote_detail->customer_id;
			
			//get company of customer
			
			$data['customer_company'] 	= $this->quote_model->get_customers_company($quote_detail->customer_id);
			//echo $quote_detail->customer_id;
			
			$data['detail']			= json_encode($quote_detail);
			$product_detail			= $this->quote_model->get_quote_products($quote_id);
			 
			$product_detail_json	= json_encode($product_detail); 
			
			$data['product_detail'] = form_safe_json($product_detail_json);
			//print_r($product_detail);
			
			foreach($product_detail as $pdetail)
			{
				//echo $pdetail->product_category;
			// $data['category'] = get_active_catlog_value_dropdown($type='Category',$selected='',$attr='class="form-control input-sm" onchange="get_machines(this.value,this.id)"  required="required" selected="selected"','product_category[]',$id='cat_f_{{$index}}');	
			 // $data['machines'] = $this->get_category_machines($pdetail->product_category);
			}
			 }
			//if($quote_id=='')
			//{
			 $data['category'] = get_active_catlog_value_dropdown($type='Category',$selected='',$attr='class="form-control input-sm" ng-model="row.product_category" onchange="get_machines(this.value,this.id)" required="required"','product_category[]',$id='cat_f_{{$index}}');
			 
			 
			 
			 
			//}
			 
			 $data['add_on1'] = get_active_catlog_value_dropdown($type='add_ons',$selected=isset($quote_detail->optional_extra1) ? $quote_detail->optional_extra1 : '',$attr='class="form-control input-sm" onchange="get_price1(this.value)" ','optional_extra1',$id='cat_f_{{$index}}');
			 
			 $data['add_on2'] = get_active_catlog_value_dropdown($type='add_ons',$selected=isset($quote_detail->optional_extra2) ? $quote_detail->optional_extra2 : '',$attr='class="form-control input-sm" onchange="get_price2(this.value)"  ','optional_extra2',$id='cat_f_{{$index}}');
			 
			 $data['add_on3'] = get_active_catlog_value_dropdown($type='add_ons',$selected=isset($quote_detail->optional_extra3) ? $quote_detail->optional_extra3 : '',$attr='class="form-control input-sm" onchange="get_price3(this.value)" ','optional_extra3',$id='cat_f_{{$index}}');
			
			 if($this->session->userdata('addedd_customer'))
			 {
			 $last_customer_id1 = $this->get_last_customer_id();
			 $last_customer_id = $last_customer_id1->customer_id;
			 }
			 if($this->session->userdata('edited_customer'))
			 {
			$last_customer_id = $this->session->userdata('edited_customer');
			 }
			//echo $last_customer_id->customer_id;
			 $data['customers'] = $this->get_customer_dropdown($attr='id="selected_customer" class="form-control input-sm" onchange="auto_fill(this.value)"','customer',$selected=isset($quote_detail->customer_id) ? $quote_detail->customer_id : (isset($last_customer_id) ? $last_customer_id : ''));
			 
			 $this->session->unset_userdata('addedd_customer');
			 $this->session->unset_userdata('edited_customer');
			 
			 
			 $data['sales_persons'] = $this->get_sales_persons($attr='class="form-control input-sm" required="required" id="sales_person_{{$index}}"','sales_person',$selected=isset($quote_detail->sales_person) ? $quote_detail->sales_person : '');
			 
			 $permission = $this->get_permission_status($this->session->userdata('admin_id'));
			
			 $data['add_more_products'] = $permission->add_product_status; 
			 $this->parser->parse('add_quote', $data);	 
		 }
	 }
	 
	 public function get_last_customer_id(){
	 $this->db->select('customer_id');
	 $this->db->from('customer_detail');
	 $this->db->where('is_deleted',0);
	 $this->db->order_by('customer_id','DESC');
	 $query = $this->db->get();
	 return $query->row();
	 }
	 
	 public function get_permission_status($user_id){
	 $this->db->select('add_product_status');
	 $this->db->from('pmod_siteuser');
	 $this->db->where('id',$user_id);
	 $query = $this->db->get();
	 return $query->row();
	 }
	 
	 public function quots_delete(){
		 foreach($_POST['delete_check'] as $id){
            $this->db->update('quotations', array('is_deleted' => 1), array('id'=>$id));
        }
		redirect('product/quote');	 
	}
	
	public function get_category_machines(){
		$category_id = $this->input->post('cat_id');
		$div_id = $this->input->post('id');
		$pro_id = $this->input->post('product_id');
		$this->db->select('*');
		$this->db->from('products');
		$this->db->where('product_category',$category_id);
		$query = $this->db->get();
		$result = $query->result();
		$drop[] = '<option value="">--Select--</option>';
		foreach($result as $res)
		{
		
		if($pro_id == $res->id)
			{
			$selected= 'selected="selected"';
			}
			else{
			$selected = '';	
			}
		$drop[] .= '<option value='.$res->id.' '.$selected.'>'.$res->product_name.'</option>';	
		}
		echo json_encode($drop).$div_id;
		}
		
	public function get_sales_persons($attr,$field_name,$selected=''){
		$this->db->select('*');
		$this->db->from('pmod_siteuser');
		$this->db->where('type',3);
		$query = $this->db->get();
		$result = $query->result();
		if($query->num_rows() > 0) {
			$data = $query->result();
		} else {
			$data = array();
		}
		$res = '';

		$res .='<select type="text" name="'.$field_name.'" '.$attr.' >';
		$res .='<option value="">--Select--</option>';
		foreach($data as $dt){
			if($dt->id == $selected){
				 $select_this = 'selected="selected"';
				}else{
					$select_this = '';
				}
			$res .='<option '.$select_this.' value="'.$dt->id.'">'.$dt->name.'</option>';
		}
		$res .='</select>';
		return $res;
		}
		
		public function save_quote_data(){
		if($this->input->post('view_html') == true)
		{
			$data['save'] = $this->quote_model->save_quote_data();
			if($data['save'] != 0)
			{
			$this->session->set_userdata(array('quote' => $data['save']));	
			}
			redirect('product/quote/');
		}
		else{
			
		}
	}
	
	public function get_customer_dropdown($attr,$field_name,$selected=''){
		$this->db->select('*');
		$this->db->from('customer_detail');
		//$this->db->where('type',3);
		//$this->db->join('quotations','quotations.customer_id = customer_detail.customer_id');
		$this->db->where('is_deleted',0);
		$query = $this->db->get();
		$result = $query->result();
		if($query->num_rows() > 0) {
			$data = $query->result();
		} else {
			$data = array();
		}
		$res = '';

		$res .='<select type="text" name="'.$field_name.'" '.$attr.' >';
		$res .='<option value="">--Select--</option>';
		$res .='<option value="add">Add New Customer</option>';
		foreach($data as $dt){
			if($dt->customer_id == $selected){
				 $select_this = 'selected="selected"';
				}else{
					$select_this = '';
				}
			$res .='<option '.$select_this.' value="'.$dt->customer_id.'">'.$dt->customer_f_name.' '.$dt->customer_l_name.'</option>';
		}
		
		$res .='</select>';
		return $res;
		}
		
	public function get_customer_detail(){
		$customer_id = $this->input->post('id');
		$this->db->select('customer_id,customer_f_name,customer_l_name,company_id,phone_number,email_add,street_add,suburb,state,post_code,title,work_phone,owner,customer_company.company_name');
		$this->db->from('customer_detail');
		$this->db->join('customer_company','customer_detail.company_id = customer_company.customer_company_id','left');
		$this->db->where('customer_id',$customer_id);
		$query = $this->db->get();
		$result = $query->row();
		echo json_encode($result);
	}
	
	public function view_html($quote_id=''){ 
		if($quote_id != 0)
		{
		// get already saved html
		$edited_html = $this->get_edited_html($quote_id);
		
		$edited = isset($edited_html->quote_data) ? $edited_html->quote_data : '';
		
		
		if($edited != '')
		{
		$data['page_title'] = 'Quote';
		$data['base_url'] = base_url();
		$data['styles'] = $this->parser->parse('includes/styles', $data, true);
	   	$data['scripts'] = $this->parser->parse('includes/scripts', $data, true);
		$data['edited_id'] = $quote_id;
		$data['replace']     = true;
		$data['edited'] = $edited ;
		
		//get quote id and customer email for sending mail
		$customer_id = $this->get_customer_id($quote_id);
		$data['customer_id'] = $customer_id;
		$customer_data = $this->get_customer_mail($customer_id);
		$data['customer_email'] = $customer_data->email_add;
		$data['customer_f_name'] = $customer_data->customer_f_name;
		echo $this->session->userdata('admin_firstname') ;die();
		//$data['customer_f_name_f']=substr($this->session->userdata('admin_firstname'), 0);
		//$data['customer_f_name_f']=substr($customer_data->customer_f_name, 0);$this->session->userdata('admin_id')
		$data['customer_l_name'] = $customer_data->customer_l_name;
		$data['customer_l_name_l']=substr($customer_data->customer_l_name, 0);
		
		// get company detail for header and footer
		$sales_person_id = $this->get_salesperson($quote_id);
		$assigned_company = $this->get_assigned_company($sales_person_id);
		$company_id = $assigned_company->assigned_company;
		$company_data = $this->get_company_data($company_id);
		$data['company_name'] = isset($company_data->name) ? $company_data->name :'';
		$data['footer_text'] = isset($company_data->footer_text) ? $company_data->footer_text :'';
		$data['company_address'] = isset($company_data->address) ? nl2br($company_data->address) : '';
		$data['company_logo'] = isset($company_data->logo) ? $company_data->logo : '';
		}
		
		
		if($edited == ''){
		
		$data['page_title'] = 'Quote';
		$data['base_url'] = base_url();
		$data['angular'] = '<script src="{base_url}ng-table/ng-table.js"> </script>';
		$data['styles'] = $this->parser->parse('includes/styles', $data, true);
	   	$data['scripts'] = $this->parser->parse('includes/scripts', $data, true);
		$quota_data = $this->quote_model->get_quote_data($quote_id);
		
		$data['quota_data'] = $quota_data;
		//change date format in html view Pradeep 21-04-2015
		$myDateTime = new DateTime($quota_data['created_on']);
		$quota_data['created_on'] = $myDateTime->format('d-m-Y');
		$quota_data['created_on_year'] = substr($myDateTime->format('Y'), 2, 2);
		$quota_data['created_on_day'] = $myDateTime->format('d');
		$quota_data['created_on_month'] = $myDateTime->format('m');
		
		//get customers company
		//echo $quota_data['customer_id'];
		$data['customer_company'] = $this->quote_model->get_customers_company($quota_data['customer_id']);
		//print_r($data['customer_company']);
		
		$addon_data   = $this->quote_model->get_addon_data($quota_data['optional_extra1'],$quota_data['optional_extra2'],$quota_data['optional_extra3']);
		
		$data['test_addon']  = $addon_data;
		$addon_json   = json_encode($addon_data); 
		$data['addons_data'] = $this->form_safe_json($addon_json);
		
		
		$product_data = $this->quote_model->get_product_data($quote_id);
		
		//echo $quota_data['assigned_company'];
		$company_data = $this->quote_model->get_company_data($quota_data['assigned_company']);
		$data['test_company'] = $company_data;
		
		//print_r($company_data);
		$product_details = array();
		//$product_featuress = array();
		foreach($product_data as $pd)
		{
		//$quote_data = array_push($quota_data,$product_data);
		$product_featuress = $pd['feature1'].','.$pd['feature2'].','.$pd['feature3'].','.$pd['feature4'].','.$pd['feature5'].','.$pd['feature6'].','.$pd['feature7'].','.$pd['feature8'].','.$pd['feature9'];
		$product_features[] = $this->quote_model->get_product_features($product_featuress,$pd['product_name']);
		//$data['product_features'] = array_push($product_features,$data['product_feature']);
		//array_push($product_featuress,$product_features);
		//echo "<br>";
		}
		if(!empty($product_featuress))
		{
		
		}
		$data['test_features'] = array_unique($product_features,SORT_REGULAR);
		
		for($i = 0; $i<(count($data['test_features'])-1); $i++)
		{
		for($j=0; $j<(count($data['test_features'])-1); $j++)
		{
		$data['test_features'][$i][$j]['catlog_title'] = $data['test_features'][$i][$j]['catlog_title'];
		}
		}
		
		
		$json = json_encode($product_features);
		//echo $json; echo "<br>".'-----------------------------------------';
		$data['product_features'] = $this->form_safe_json($json);
		
		
		
		
		//$data['product_features'] = str_replace('\\\n',' <br>',$data['product_features']);;
		//echo $data['product_features'];
		$data['uncoded_product_data'] = $product_data;
		$product_json = json_encode($product_data);
		$data['product_data'] = form_safe_json($product_json);
		$data['quote_data'] = json_encode($quota_data);	
		$data['company_data_show'] = $company_data;
		$company_data = json_encode($company_data);
		$data['company_data'] = form_safe_json($company_data);
		
		
		//$data['product_features'] = $data['product_feature'];
		$data['full_html'] = $this->parser->parse('view_html',$data,true);
		
		$permission = $this->get_permission_status($this->session->userdata('admin_id'));
			
		$data['add_more_products'] = $permission->add_product_status; 
		
		}
		
		$this->parser->parse('view_html',$data);
		}
		
}
				
		function get_customer_id($quote_id){
		$this->db->select('customer_id');
		$this->db->from('quotations');
		$this->db->where('id',$quote_id);
		$query = $this->db->get();
		$result = $query->row();
		return $result->customer_id;
		}
		
		function get_customer_mail($customer_id){
		$this->db->select('*');
		$this->db->from('customer_detail');
		$this->db->where('customer_id',$customer_id);
		$query = $this->db->get();
		$result = $query->row();
		return $result;
		}
		
		function get_salesperson($quote_id){
		$this->db->select('sales_person');
		$this->db->from('quotations');
		$this->db->where('id',$quote_id);
		$query = $this->db->get();
		$result = $query->row();
		return $result->sales_person;
		}
		
		function get_assigned_company($sales_person){
		$this->db->select('*');
		$this->db->from('pmod_siteuser');
		$this->db->where('id',$sales_person);
		$query = $this->db->get();
		return $query->row();
		}
		
		function get_company_data($company_id){
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where('id',$company_id);
		$query = $this->db->get();
		return $query->row();
		}
		
		
		
		function form_safe_json($json) {
    $json = empty($json) ? '[]' : $json ;
     $search = array('\\',"\n","\r","\f","\t","\b","'") ;
    $replace = array('\\\\',"\\n", "\\r","\\f","\\t","\\b", "&#039");
    $json = str_replace($search,$replace,$json);
    
    return strip_tags($json);
}

	 public function GeneratePDF()
	{
		if (!class_exists('phpmailerException')) {
	   $this->load->view('PHPMailer_5.2.4/class.phpmailer.php');
	  }
		//$this->load->library('form_validation');
		//$this->quote_model->get_quote($quote_id);
		//echo $html = $this->parser->parse('view_html.php',$pdf_id,true);
		
		$name = $this->input->get('name');
		$email = $this->input->get('email');
		$quote_id = $this->input->get('quote_id');
		
		/*$name = $this->input->post('customer_name');
		$email = $this->input->post('customer_email');
		$quote_id = $this->input->post('quote_id');
		$manager_email = $this->input->post('manager_email');
		$msg = $this->input->post('message');
		
		$subject = $this->input->post('subject');*/
               
		if($name){ 	
					
				$this->load->view('MPDF53/mpdf.php');	
				$mpdf=new mPDF('en-x','A4','','',0,0,10,30,0,0,'L');
 
				
				//$filecont = file_get_contents('files/html.htm');
				
				//$filehead = file_get_contents('files/header.htm');
				
				//$filefoot = file_get_contents('files/footer.htm');
				
				//$first_page = file_get_contents('files/first_page.htm');
				
				//$first_page_header = file_get_contents('files/first_page_header.htm');
				
				//$first_page_footer = file_get_contents('files/first_page_footer.htm');
				
				// select edited html from database
				$edited_html = $this->get_edited_html($quote_id);
				
				$full_html = $edited_html->quote_data;
				$final_html = explode('devide',$full_html);
				$filecont = $final_html[1];
				$first_page = '<style type="text/css"> .first_page_header{display:none;} </style>'.$final_html[0];
				
				$filehead = $edited_html->pdf_header;
				$filefoot = $edited_html->pdf_footer;
				$first_page_header = $edited_html->first_page_header;
				$first_page_footer = $edited_html->first_page_footer;
				
					
				$mpdf->mirrorMargins = 2;	// Use different Odd/Even headers and footers and mirror margins
                                $mpdf->useOddEven = false;
                               // $mpdf->SetHTMLHeader($filehead);
                              
				$stylsheet = file_get_contents('css/auto_quote.css');
				$stylsheet1 = file_get_contents('css/bootstrap.min.css');
				$mpdf->WriteHTML($stylsheet,1);
				$mpdf->WriteHTML($stylsheet1,1);
				
				//$mpdf->SetHeader('Title');
				//$mpdf->SetHTMLFooter($filefoot);
				
				//$mpdf->SetAutoFont(AUTOFONT_THAIVIET);
				//$mpdf->ignore_invalid_utf8 = true;
				$mpdf->SetHTMLHeader($first_page_header);
				$mpdf->SetHTMLFooter($first_page_footer);
				$mpdf->WriteHTML($first_page);
				
				//$mpdf->WriteHTML($filecont);
				
				//$mpdf->SetHTMLHeader('');
				//$mpdf->SetFooter('');
				
				
				$mpdf->SetHTMLHeader($filehead);
				
				$mpdf->AddPage();
				$mpdf->SetHTMLFooter($filefoot);
				$mpdf->WriteHTML($filecont);
				
							
				$mpdf->Output('quote_pdf/'.'150306-PS'.$quote_id.'.pdf','F');
				//$mpdf->Output();
				/*$customer_mail_data = array('sub' => 'Quote',
							  'msg' => 'Hello '.$name,
							  'address' => $email,
							  'file' => 'quote_pdf/'.$name.$quote_id.'.pdf');
				
				
				
							  
				$manager_mail_data = array('sub' => $subject,
							  'msg' => $msg,
							  'address' => $manager_email,
							  'file' => 'quote_pdf/'.$name.$quote_id.'.pdf'
							  );
							  
				 
				 if(isset($manager_mail_data) && $this->input->post('send_mail') == true)
				 {
				 sendEmail($manager_mail_data);
				 }
				 
				if(isset($customer_mail_data))
				 { 
				 sendEmail($customer_mail_data);
				 }*/
				 
				 echo '<script>
				 parent.$.fancybox.close();</script>';
				 $mpdf->output('150306-PS'.$quote_id.'.pdf');
				 $mpdf->Output('150306-PS'.$quote_id.'.pdf','D'); 
				
				 //$this->send_mail_to_manager($mail_data);
				 
							
			}
	}
	
	public function send_mail_to_manager($mail_data=''){
	
	$data['name'] = $this->input->get('name');
	$data['email'] = $this->input->get('email');
	$data['quote_id'] = $this->input->get('quote_id');	
	
	// send mail to manager as the setting selected by manager
				$this->db->select('quotations.id,quotations.customer_id,quotations.product_category,quotations.product_id,quotations.product_price,quotations.rated_pressure,quotations.setup_pressure,quotations.sales_person,quotations.selling_point1,quotations.selling_point2,quotations.selling_point3,quotations.selling_point4,quotations.selling_point5,quotations.selling_point6,quotations.optional_extra1,quotations.optional_extra2,quotations.optional_extra3,quotations.optional_extra1_price,quotations.optional_extra2_price,quotations.optional_extra3_price,quotations.created_on,customer_detail.customer_f_name,customer_detail.customer_l_name,customer_detail.company_name,customer_detail.phone_number,customer_detail.email_add,customer_detail.street_add,customer_detail.suburb,customer_detail.state,customer_detail.post_code,quote_products.product_name,quote_products.product_price,quote_products.rated_pressure,quote_products.setup_pressure,quote_products.quote_date');
				$this->db->from('quotations');
				$this->db->join('customer_detail','quotations.customer_id = customer_detail.customer_id');
				$this->db->join('quote_products','quotations.id = quote_products.quote_id');	
				$this->db->where('quotations.id',$data['quote_id']);	
				$query = $this->db->get();	  
			        $data = $query->result();
			       
			       $quote_detail = '';
			       $customer_detail = '';
			       $quote_products = array();
			        foreach($data as $d)
			        {
			        $quote_detail = 'Quote Number -'.$d->id.'<br>'.'Sales Person -'.$d->sales_person.'<br>'.'Selling Point1 -'.$d->selling_point1.'<br>'.'Selling Point2 -'.$d->selling_point2.'<br>'.'Selling Point3 -'.$d->selling_point3.'<br>'.'Created Date -'.$d->created_on;
			        
			        
			        
			        $customer_detail = 'Customer Name -'.$d->customer_f_name.' '.$d->customer_l_name.'<br>'.'Company Name -'.$d->company_name.'<br>'.'Phone Number -'.$d->phone_number.'<br>'.'Email Address -'.$d->email_add;
			        
			        $customer_email = $d->email_add;
			        $customer_name = $d->customer_f_name.' '.$d->customer_l_name;
			        
			        $quote_products[] = 'Product Name -'.$d->product_name.'<br>'.'Product Price -'.$d->product_price.'<br>'.'Rated Pressure -'.$d->rated_pressure.'<br>'.'Setup Pressure -'.$d->setup_pressure;
			        }
			        
			        // get the manager mail setting
			        $this->db->select('*');
			        $this->db->from('manager_mail_setting');
			        $query = $this->db->get();
			        $setting = $query->row();
			        $manager_msg = '';
			        if($setting->quote_detail == 1){
			        $manager_msg .= 'Quote Detail -<br>'.$quote_detail.'</br>';
			        }
			        if($setting->customer_detail == 1){
			        $manager_msg .= 'Customer Detail -<br>'.$customer_detail.'</br>';
			        }
			        if($setting->product_detail == 1){
			        foreach($quote_products as $qp){
			        $manager_msg .= 'Product Detail -<br>'.$qp.'</br>';
			        }
			        }
	
	
	$data['manager_msg'] = $manager_msg;
	$data['name'] = $this->input->get('name');
	$data['email'] = $this->input->get('email');
	$data['quote_id'] = $this->input->get('quote_id');
	//$data['email'] = $customer_email;
	
		//$data['base_url'] = base_url();
		//$data['styles'] = $this->parser->parse('includes/styles', $data, true);
	    //$data['scripts'] = $this->parser->parse('includes/scripts', $data, true);
		//$data['mail_data'] = $mail_data;
		
		$this->load->view('mail_to_manager',$data);
	}
	
	public function mail_to_managers(){
	if (!class_exists('phpmailerException')) {
	   $this->load->view('PHPMailer_5.2.4/class.phpmailer.php');
	  }
	  
	if($this->input->post('send_mail') == true)
	{
		$data['quote_id'] = $this->input->post('quote_id');
		$data['customer_name'] = $this->input->post('customer_name');
		$data['address'] = $this->input->post('manager_email');
		$data['sub'] = $this->input->post('subject');
		$data['msg'] = $this->input->post('message');
		$data['file'] = './quote_pdf/'.$data['customer_name'].$data['quote_id'].'.pdf';
		sendEmail($data);
		echo '<script>parent.$.fancybox.close();</script>';
	}	
	}
	
	function get_view_path($view_name)
	{
		
		$target_file= APPPATH.'modules/product/views/'.$view_name.'.php';
		if(file_exists($target_file)) return $target_file;
	}
	
	public function file_write(){
	
	$html = $this->input->post('data');	
	$header = $this->input->post('pdf_head');
	$footer = $this->input->post('pdf_foot');
	$first_page = $this->input->post('first_page');
	$first_page_header = $this->input->post('first_head');
	$first_page_footer = $this->input->post('first_foot');
	
	// save edited quote into database
        $quote_id = $this->input->post('quote_id');
        $edited_quotes['quote_data'] = $html;
        $edited_quotes['quote_id'] = $quote_id;
        $edited_quotes['pdf_header'] = $header;
        $edited_quotes['pdf_footer'] = $footer;
        $edited_quotes['first_page_header'] = $first_page_header;
        $edited_quotes['first_page_footer'] = $first_page_footer;
        $edited_quotes['created_on'] = date('d-m-Y');
		//print_r($edited_quotes['created_on']); die;
        $edited_quotes['created_by'] = $this->session->userdata('admin_id');
        
        $this->db->select('*');
        $this->db->from('edited_quotations');
        $this->db->where('quote_id',$quote_id);
        $query = $this->db->get();
        if($query->num_rows() != 0)
        {
        $this->db->update('edited_quotations',$edited_quotes,array('quote_id' =>$quote_id));
        }
        else
        {
        $this->db->insert('edited_quotations',$edited_quotes);
        }
	
	 $new_html = explode('devide',$html);
	 $file_name = fopen('files/html.htm',"w") or die("unable to open");
	 $file_name1 = fopen('files/header.htm',"w") or die("unable to open");
	 $file_name2 = fopen('files/footer.htm',"w") or die("unable to open");
	 $file_name3 = fopen('files/first_page.htm',"w") or die("unable to open");
	 $file_name4 = fopen('files/first_page_header.htm',"w") or die("unable to open");
	 $file_name5 = fopen('files/first_page_footer.htm',"w") or die("unable to open");
	 
	 fwrite($file_name,$new_html[1]);
	 fwrite($file_name1,$header);
	 fwrite($file_name2,$footer);
	 fwrite($file_name3,'<style type="text/css"> .first_page_header{display:none;} </style>'.$new_html[0]);
	 fwrite($file_name4,$first_page_header);
	 fwrite($file_name5,$first_page_footer);
	  
	fclose($file_name);
        fclose($file_name1);
        fclose($file_name2);
        fclose($file_name3);
        fclose($file_name4);
        fclose($file_name5);
        
        
	}
	
	public function get_edited_html($quote_id){
	$this->db->select('*');
	$this->db->from('edited_quotations');
	$this->db->where('quote_id',$quote_id);
	$query = $this->db->get();
	$result = $query->row();
	return $result;
	}
	
	public function get_addon_price(){
	$id = $this->input->post('id');
	$this->db->select('*');
	$this->db->from('rq_rqf_quote_catlogs');
	$this->db->where('id',$id);
	$query = $this->db->get();
	echo json_encode($query->row());
	}
	
	public function add_customer(){
	
	                 $data['title'] 		= 'Add Customer';
			 $data['styles'] 		= $this->load->view('includes/styles', '', true);
			 $data['scripts'] 		= $this->load->view('includes/scripts', '', true);
			 $data['footer'] 		= $this->load->view('includes/footer', '', true);
			 $data['base_url'] 		= base_url();
			 $this->parser->parse('add_customer',$data);
	}
	
	}

?>