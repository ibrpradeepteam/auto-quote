<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quote extends CI_Controller {
	
	 function __construct() {
		 
		 parent::__construct();
		 
		 if(!is_login()){
			redirect(base_url()); 
		 }
		 $this->load->model('quote_model'); 
		 $this->load->model('product_model'); 
		 $this->load->library( 'parser' );
		 }
		 
		 
	 public function index() { 
		$data['page_title'] = 'Quote';
		$data['title_small'] = 'List';
		$data['base_url'] = base_url();
		$data['header'] = $this->parser->parse('includes/header', $data, true);
	    $data['footer'] = $this->parser->parse('includes/footer', $data, true);
		$data['page_header'] = $this->parser->parse('includes/page_header', $data, true);
		
		$data['quote']  = $this->session->userdata('quote');
		
		$products    = $this->quote_model->get_quotes();
		$data['product'] = json_encode($products);
		$this->parser->parse('quote_list', $data);	 
		}	 
		 
	
	/*** add product into database ***/	 
	 public function add_quote($quote_id='') {
		
		 if($this->input->post()){	 		 
			 $this->product_model->add_product();
			 redirect(base_url('product'));	
		 }else{
			 $data['title'] 		= 'Add product';
			 $data['header'] 		= $this->load->view('includes/header', '', true);
			 $data['footer'] 		= $this->load->view('includes/footer', '', true);
			 $data['base_url'] 		= base_url();
			 if($quote_id!='')
			 { 
			$quote_detail			= $this->quote_model->get_quote_detail($quote_id);
			$data['quote_id']		= $quote_detail->id;
			$data['detail']			= json_encode($quote_detail);
			$product_detail			= $this->quote_model->get_quote_products($quote_id); 
			$data['product_detail']	= json_encode($product_detail); 
			 }
			 $data['category'] = get_active_catlog_value_dropdown($type='Category',$selected=isset($quote_detail->product_category) ? $quote_detail->product_category : '',$attr='class="form-control input-sm" onchange="get_machines(this.value,this.id)" required="required"','product_category[]',$id='cat_f_{{$index}}');
			 
			  $data['add_on1'] = get_active_catlog_value_dropdown($type='add_ons',$selected=isset($product_detail->optional_extra1) ? $product_detail->optional_extra1 : '',$attr='class="form-control input-sm" ','optional_extra1',$id='cat_f_{{$index}}');
			 
			 $data['add_on2'] = get_active_catlog_value_dropdown($type='add_ons',$selected=isset($product_detail->optional_extra2) ? $product_detail->optional_extra2 : '',$attr='class="form-control input-sm"  ','optional_extra2',$id='cat_f_{{$index}}');
			 
			 $data['add_on3'] = get_active_catlog_value_dropdown($type='add_ons',$selected=isset($product_detail->optional_extra3) ? $product_detail->optional_extra3 : '',$attr='class="form-control input-sm" ','optional_extra3',$id='cat_f_{{$index}}');
			 
			 $data['customers'] = $this->get_customer_dropdown($attr='class="form-control input-sm" onchange="auto_fill(this.value)"','customer',$selected=isset($quote_detail->customer_id) ? $quote_detail->customer_id : '');
			 
			 $data['sales_persons'] = $this->get_sales_persons($attr='class="form-control input-sm" required="required" id="sales_person_{{$index}}"','sales_person',$selected=isset($quote_detail->sales_person) ? $quote_detail->sales_person : '');		 
			  
			 $this->parser->parse('add_quote', $data);	 
		 }
	 }
	 
	 public function quots_delete(){
		 foreach($_POST['delete_check'] as $id){
            $this->db->update('quotations', array('is_deleted' => 1), array('id'=>$id));
        }
		redirect('product/quote');	 
	}
	
	public function get_category_machines(){
		$category_id = $this->input->post('cat_id');
		$this->db->select('*');
		$this->db->from('products');
		$this->db->where('product_category',$category_id);
		$query = $this->db->get();
		$result = $query->result();
		echo '<option value="">--Select--</option>';
		foreach($result as $res)
		{
		echo '<option value='.$res->id.'>'.$res->product_name.'</option>';	
		}
		}
		
	public function get_sales_persons($attr,$field_name,$selected=''){
		$this->db->select('*');
		$this->db->from('pmod_siteuser');
		$this->db->where('type',3);
		$query = $this->db->get();
		$result = $query->result();
		if($query->num_rows() > 0) {
			$data = $query->result();
		} else {
			$data = array();
		}
		$res = '';

		$res .='<select type="text" name="'.$field_name.'" '.$attr.' >';
		$res .='<option value="">--Select--</option>';
		foreach($data as $dt){
			if($dt->id == $selected){
				 $select_this = 'selected="selected"';
				}else{
					$select_this = '';
				}
			$res .='<option '.$select_this.' value="'.$dt->id.'">'.$dt->name.'</option>';
		}
		$res .='</select>';
		return $res;
		}
		
		public function save_quote_data(){
		if($this->input->post('view_html') == true)
		{
			$data['save'] = $this->quote_model->save_quote_data();
			if($data['save'] != 0)
			{
			$this->session->set_userdata(array('quote' => $data['save']));	
			}
			redirect('product/quote/');
		}
		else{
			
		}
	}
	
	public function get_customer_dropdown($attr,$field_name,$selected=''){
		$this->db->select('*');
		$this->db->from('customer_detail');
		//$this->db->where('type',3);
		$query = $this->db->get();
		$result = $query->result();
		if($query->num_rows() > 0) {
			$data = $query->result();
		} else {
			$data = array();
		}
		$res = '';

		$res .='<select type="text" name="'.$field_name.'" '.$attr.' >';
		$res .='<option value="">--Select--</option>';
		$res .='<option value="">Add New Customer</option>';
		foreach($data as $dt){
			if($dt->customer_id == $selected){
				 $select_this = 'selected="selected"';
				}else{
					$select_this = '';
				}
			$res .='<option '.$select_this.' value="'.$dt->customer_id.'">'.$dt->customer_f_name.' '.$dt->customer_l_name.'</option>';
		}
		
		$res .='</select>';
		return $res;
		}
		
	public function get_customer_detail(){
		$customer_id = $this->input->post('id');
		$this->db->select('*');
		$this->db->from('customer_detail');
		$this->db->where('customer_id',$customer_id);
		$query = $this->db->get();
		$result = $query->row();
		echo json_encode($result);
	}
	
	public function view_html($quote_id=''){
		if($quote_id != 0)
		{
		$data['page_title'] = 'Quote';
		$data['base_url'] = base_url();
		$data['styles'] = $this->parser->parse('includes/styles', $data, true);
	    $data['scripts'] = $this->parser->parse('includes/scripts', $data, true);
		$quota_data = $this->quote_model->get_quote_data($quote_id);
		//print_r($quota_data);
		//get company detail
		//foreach($quota_data)
		//$sales_persons = $this->quote_model->get_sales_persons();
		//$company_id = $quota_data['assigned_company'];
		
		
		$product_data = $this->quote_model->get_product_data($quote_id);
		
		//echo $quota_data['assigned_company'];
		$company_data = $this->quote_model->get_company_data($quota_data['assigned_company']);
		//print_r($company_data);
		$product_details = array();
		//$product_featuress = array();
		foreach($product_data as $pd)
		{
		//$quote_data = array_push($quota_data,$product_data);
		$product_featuress = $pd['feature1'].','.$pd['feature2'].','.$pd['feature3'].','.$pd['feature4'].','.$pd['feature5'].','.$pd['feature6'].','.$pd['feature7'].','.$pd['feature8'].','.$pd['feature9'];
		$product_features = $this->quote_model->get_product_features($product_featuress);
		//$data['product_features'] = array_push($product_features,$data['product_feature']);
		//array_push($product_featuress,$product_features);
		//echo "<br>";
		}
		if(!empty($product_featuress))
		{
		//$unique = array_unique($product_featuress);
		//echo "<pre>";
		//print_r($unique);
		}
		$json = json_encode($product_features);
		$data['product_features'] = $this->form_safe_json($json);
		//print_r($data['product_features']);
		//print_r($product_data);
		$data['product_data'] = json_encode($product_data);
		$data['quote_data'] = json_encode($quota_data);	
		$data['company_data'] = json_encode($company_data);
		
		//$data['product_features'] = $data['product_feature'];
		$data['full_html'] = $this->parser->parse('view_html',$data,true);
		//$this->set_data($data);
		//$data['product_features1'] = str_replace("\u","v",$data['product_features']);	
		//$data['product_features2'] = str_replace("\n","",$data['product_features1']);	
		//$data['product_features3'] = str_replace("u","",$data['product_features2']);	

		}
		$this->parser->parse('view_html',$data);
		}
		
		
		function form_safe_json($json) {
    $json = empty($json) ? '[]' : $json ;
    $search = array('\\',"\n","\r","\f","\t","\b","'") ;
    $replace = array('\\\\',"\\n", "\\r","\\f","\\t","\\b", "&#039");
    $json = str_replace($search,$replace,$json);
    return strip_tags($json);
}

	 public function GeneratePDF($pdf_id = '')
	{
		//$this->load->library('form_validation');
		//$this->quote_model->get_quote($quote_id);
		//echo $html = $this->parser->parse('view_html.php',$pdf_id,true);
		if($pdf_id){
				//$this->load->view('pdf', $this->data);
			//}else { 		
				
				$this->load->view('MPDF53/mpdf.php');	
				$mpdf=new mPDF('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0);
 
				$mpdf->SetDisplayMode('fullpage');
				 
				$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
				 
				/* $php = base_url().'pdfreport.php';
									;*/
				//$stylesheet = base_url('css').'/style.css';
				//$stylesheetcont = file_get_contents($stylesheet);
				
				//$data['ajax_req'] = TRUE;
				
				//$filecont = $this->load->view('view_html.php',true);
				//$stylesheetcont = file_get_contents($stylesheet);
				//$path_to_view = $this->get_view_path('view_html');
				$filecont = file_get_contents('files/html.htm');
				
				//$mpdf->WriteHTML($stylesheetcont,1);			
				//$mpdf->WriteHTML(file_get_contents(base_url().'application/modules/views/view_html.php'));
			
				$mpdf->WriteHTML($filecont);
				 
				$mpdf->Output('quote_pdf/'.$pdf_id.'.pdf','F');
				$mpdf->Output();
							
			}
	}
	
	function get_view_path($view_name)
	{
		
		$target_file= APPPATH.'modules/product/views/'.$view_name.'.php';
		if(file_exists($target_file)) return $target_file;
	}
	
	public function file_write(){
	
	$html = $this->input->post('data');	
	
	 $file_name = fopen('files/html.htm',"w") or die("unable to open");
	 fwrite($file_name,$html);
	 fclose($file_name);
	}
	
	}

?>