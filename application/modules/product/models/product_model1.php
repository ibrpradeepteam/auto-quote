<?php
class product_model extends CI_Model {
	
	public function add_product($id='') {
	
		if($this->input->post('add_product') == true)
		{
				$post = $this->input->post();
				
				$product['product_name'] 	 	= $post['product_name'];
				$product['motor'] 				= $post['motor_name'];
				$product['price'] 				= $post['price'];
				$product['max_pump_pressure'] 	= $post['max_pump_pressure'];
				$product['pump_set_pressure'] 	= $post['pump_set_pressure'];
				$product['water_temprature'] 	= $post['water_temprature'];
				$lenght 						= $post['product_sizel'];
				$widht 							= $post['product_sizew'];
				$hight 							= $post['product_sizeh'];
				$lwh							=array($lenght,$widht,$hight);
				$product['product_size'] 		= implode("x", $lwh);
				//$product['product_size'] 		= $post['product_size'];
				$product['product_weight'] 		= $post['product_weight'];
				$product['product_category'] 	= $post['product_category'];
				$product['feature1'] 			= $post['feature1'];
				$product['feature2'] 			= $post['feature2'];
				$product['feature3'] 			= $post['feature3'];
				$product['feature4'] 			= $post['feature4'];
				$product['feature5'] 			= $post['feature5'];
				$product['feature6'] 			= $post['feature6'];
				$product['feature7'] 			= $post['feature7'];
				$product['feature8'] 			= $post['feature8'];
				$product['feature9'] 			= $post['feature9'];
				$product['created_by_id']		= $this->session->userdata('admin_id');
				
				/** add subscription type **/
				if($this->session->userdata('admin_type') == 1)
				{
				$product['subscription_type']	= 'Main';	
				}
				if($this->session->userdata('admin_type') == 3 || $this->session->userdata('admin_type') == 2)
				{
				$product['subscription_type']	= 'Self-managed';	
				}
				/*** upload product image ****/
				$file_types = $this->config->item('image_file_types');				
				$logo_name = do_upload($file_types,'product_img','product_image');
				if(isset($post['product_id']) && $post['product_id']!="{product_id}")
				{
				$product_detail = $this->get_product_detail($post['product_id']);
				$stored_image = isset($product_detail->product_image) ? $product_detail->product_image : '';	
				}
				$product['product_image'] = isset($logo_name['file_name']) ? $logo_name['file_name'] : $stored_image;	
				if(isset($post['product_id']) && $post['product_id']!="{product_id}")
				{
				$this->db->update('products',$product,array('id'=>$post['product_id']));	
				}
				if(isset($post['product_id']) && $post['product_id']=="{product_id}")
				{
				$product['created_on']			= date('Y-m-d H:i:s');
				$this->db->insert('products',$product);
				}
		}	
	}
	
	public function get_features() {
		$this->db->select('*');
		$this->db->from('rq_rqf_quote_catlog_type');
		$query = $this->db->get();
		$result = $query->result();
		return $result;	
	}
	
	public function get_products() {
		$this->db->select('*');
		$this->db->from('products');
		$this->db->where('is_deleted',0);
		$query = $this->db->get();
		$result = $query->result();
		return $result;	
	}
	
	public function get_product_detail($id) {
		$this->db->select('*');
		$this->db->from('products');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->row();
		return $result;		
	}	
}
?>