<?php
class quote_model extends CI_Model {
	
	public function add_product($id='') {
	
		if($this->input->post('add_product') == true)
		{
				$post = $this->input->post();
				
				$product['product_name'] 	 	= $post['product_name'];
				$product['motor'] 				= $post['motor_name'];
				$product['max_pump_pressure'] 	= $post['max_pump_pressure'];
				$product['pump_set_pressure'] 	= $post['pump_set_pressure'];
				$product['water_temprature'] 	= $post['water_temprature'];
				$product['product_size'] 		= $post['product_size'];
				$product['product_weight'] 		= $post['product_weight'];
				$product['product_category'] 	= $post['product_category'];
				$product['feature1'] 			= $post['feature1'];
				$product['feature2'] 			= $post['feature2'];
				$product['feature3'] 			= $post['feature3'];
				$product['feature4'] 			= $post['feature4'];
				$product['feature5'] 			= $post['feature5'];
				$product['feature6'] 			= $post['feature6'];
				$product['feature7'] 			= $post['feature7'];
				$product['feature8'] 			= $post['feature8'];
				$product['feature9'] 			= $post['feature9'];
				$product['created_by_id']		= $this->session->userdata('admin_id');
				
				/** add subscription type **/
				if($this->session->userdata('admin_type') == 1 || $this->session->userdata('admin_type') == 2)
				{
				$product['subscription_type']	= 'Main';	
				}
				if($this->session->userdata('admin_type') == 3)
				{
				$product['subscription_type']	= 'Self-managed';	
				}
				/*** upload product image ****/
				$file_types = $this->config->item('image_file_types');				
				$logo_name = do_upload($file_types,'product_img','product_image');
				if(isset($post['product_id']) && $post['product_id']!="{product_id}")
				{
				$product_detail = $this->get_product_detail($post['product_id']);
				$stored_image = isset($product_detail->product_image) ? $product_detail->product_image : '';	
				}
				$product['product_image'] = isset($logo_name['file_name']) ? $logo_name['file_name'] : $stored_image;	
				if(isset($post['product_id']) && $post['product_id']!="{product_id}")
				{
				$this->db->update('products',$product,array('id'=>$post['product_id']));	
				}
				if(isset($post['product_id']) && $post['product_id']=="{product_id}")
				{
				$product['created_on']			= date('Y-m-d H:i:s');
				$this->db->insert('products',$product);
				}
		}	
	}
	
	public function get_features() {
		$this->db->select('*');
		$this->db->from('rq_rqf_quote_catlog_type');
		$query = $this->db->get();
		$result = $query->result();
		return $result;	
	}
	
	public function get_quotes() {
		$this->db->select('quotations.id,customer_detail.customer_f_name,customer_detail.customer_l_name,customer_detail.company_name,customer_detail.phone_number,customer_detail.email_add,pmod_siteuser.name');
		$this->db->from('quotations');
		$this->db->join('customer_detail','quotations.customer_id = customer_detail.customer_id');
		//$this->db->join('quote_products','quotations.id = quote_products.quote_id');
		$this->db->join('pmod_siteuser','quotations.sales_person = pmod_siteuser.id');
		$this->db->where('quotations.is_deleted',0);
		$query = $this->db->get();
		$result = $query->result();
		return $result;	
	}
	
	public function get_quote_detail($id) {
		$this->db->select('*');
		$this->db->from('quotations');
		$this->db->join('customer_detail','quotations.customer_id = customer_detail.customer_id');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->row();
		return $result;		
	}
	
	public function save_quote_data(){
		$post = $this->input->post();
		/*echo "<pre>";
		print_r($post); die;*/
		$customer_data['customer_f_name'] 	 = $post['customer_f_name'];
		$customer_data['customer_l_name'] 	 = $post['customer_l_name'];
		$customer_data['company_name']		 = $post['company_name'];
		$customer_data['phone_number']		 = $post['phone_number'];
		$customer_data['email_add']		 	 = $post['email_add'];
		$customer_data['street_add']		 = $post['street_add'];
		$customer_data['suburb']			 = $post['suburb']; 
		$customer_data['state']			 = $post['state']; 
		$customer_data['post_code']		 = $post['post_code']; 
		$customer_data['created_on']	 = date('Y-m-d H:i:s'); 
		$customer_data['created_by']	 = $this->session->userdata('admin_id');
		
		$quote_data['sales_person']	 	 = $post['sales_person'];
		$quote_data['selling_point1']	 = $post['selling_point1'];	
		$quote_data['selling_point2']	 = $post['selling_point2'];	
		$quote_data['selling_point3']	 = $post['selling_point3'];	
		$quote_data['selling_point4']	 = $post['selling_point4'];	
		$quote_data['selling_point5']	 = $post['selling_point5'];	
		$quote_data['selling_point6']	 = $post['selling_point6'];
		
		$quote_data['optional_extra1']	 = $post['optional_extra1'];	
		$quote_data['optional_extra2']	 = $post['optional_extra2'];	
		$quote_data['optional_extra3']	 = $post['optional_extra3'];
		
		/** insert customer detals  ***/
		$this->db->insert('customer_detail',$customer_data);
		$customer_id = $this->db->insert_id();
		$quote_data['customer_id']		= $customer_id;
		
		/** insert quote detals  ***/
		$this->db->insert('quotations',$quote_data);
		$quote_insert_id = $this->db->insert_id();
		
		if(isset($post['product_name']) && $post['product_name']!='')
		{
		$no_of_products = count($post['product_name']);
		//echo "<br>---".$no_of_products;
		//echo "<pre>";
		;
		for($i=0; $i<$no_of_products; $i++)
		{
		$quote_product_data['product_category']	 = !empty($post['product_category'][$i]) ? $post['product_category'][$i]: '';
		//$quote_product_data['sales_person']	 	 = !empty($post['sales_person'][$i]) ? $post['sales_person'][$i]: '';	
		$quote_product_data['product_id']		=  !empty($post['product_id'][$i]) ? $post['product_id'][$i]: '';
		$quote_product_data['product_name']		=  !empty($post['product_name'][$i]) ? $post['product_name'][$i]: '';
		$quote_product_data['product_price']	=  !empty($post['product_price'][$i]) ? $post['product_price'][$i]: '';
		$quote_product_data['rated_pressure']	=  !empty($post['rated_pressure'][$i]) ? $post['rated_pressure'][$i]: '';
		$quote_product_data['setup_pressure']	=  !empty($post['setup_pressure'][$i]) ? $post['setup_pressure'][$i]: '';
		$quote_product_data['quote_id']			=  $quote_insert_id;
		$this->db->insert('quote_products',$quote_product_data);
		}
		}
		
		if($quote_insert_id != '')
		{
		return $quote_insert_id;
		} else {
		return 0;	
		}
	}
	
	public function get_quote_data($quote_id){
		$this->db->select('quotations.id,quotations.customer_id,quotations.sales_person,quotations.selling_point1,quotations.selling_point2,quotations.selling_point3,quotations.selling_point4,quotations.selling_point5,quotations.selling_point6,quotations.optional_extra1,quotations.optional_extra2,quotations.optional_extra3,pmod_siteuser.name,pmod_siteuser.last_name,pmod_siteuser.email,pmod_siteuser.assigned_company,customer_detail.customer_f_name,customer_detail.customer_l_name,customer_detail.company_name,customer_detail.email_add,customer_detail.street_add,customer_detail.suburb,customer_detail.state,customer_detail.post_code');
		$this->db->from('quotations');
		//$this->db->join('quote_products','quotations.id = quote_products.quote_id');
		$this->db->join('pmod_siteuser','quotations.sales_person = pmod_siteuser.id');
		$this->db->join('customer_detail','quotations.customer_id = customer_detail.customer_id');
		//$this->db->join('products','quotations.product_id = products.id');
		$this->db->where('quotations.id',$quote_id);
		$query = $this->db->get();
		return $query->row_array();
	}
	
	public function get_addon_data($add_on1='',$add_on2='',$add_on3=''){
	       $add_ons = $add_on1.','.$add_on2.','.$add_on3;
	       $this->db->select('*');
	       $this->db->from('rq_rqf_quote_catlogs');
	       $this->db->where_in('id',$add_ons);
	       $query = $this->db->get();
	       return $query->result();
	}
		
	public function get_product_data($quote_id){
		$this->db->select('*');
		$this->db->from('quote_products');
		//$this->db->join('quote_products','quotations.id = quote_products.quote_id');
		//$this->db->join('pmod_siteuser','quote_products.sales_person = pmod_siteuser.id');
		$this->db->join('products','quote_products.product_id = products.id');
		$this->db->where('quote_id',$quote_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function get_company_data($company_id){
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where('id',$company_id);
		$query = $this->db->get();
		return $query->row();
	}	
	
	public function get_product_features($features_id,$product_name){
		$features_ids = explode(',',$features_id);
		$this->db->select('*');
		$this->db->from('rq_rqf_quote_catlogs');
		$this->db->join('rq_rqf_quote_catlog_type','rq_rqf_quote_catlogs.catlog_type = rq_rqf_quote_catlog_type.id');
		$this->db->where_in('rq_rqf_quote_catlogs.id',$features_ids);
		$query = $this->db->get();
		$result_array = $query->result_array();
		$final1 = array();
		//$result_array = array_unique($result_array1);
		foreach($result_array as $ra)
		{
		$final['catlog_type'] = $ra['catlog_type'];
		$final['catlog_title'] = $ra['catlog_title'];
		$final['catlog_description'] = $ra['catlog_description'];
		$final['title_image'] = $ra['title_image'];
		$final['product_name'] = $product_name;
		$final1[] = $final;
		}
		
		return $final1;
		
	}
	
	public function get_quote_products($quote_id){
		$this->db->select('*');
		$this->db->from('quote_products');
		$this->db->where('quote_id',$quote_id);
		$query = $this->db->get();
		return $query->result();
	}	
}
?>