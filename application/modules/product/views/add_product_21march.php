{header} 
<script src="{base_url}ng-table/ng-table.js"></script>
        <div class="wrapper row-offcanvas row-offcanvas-left">

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side" ng-app="add_product">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Product(Machines)                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Product(Machines)</a></li>                        
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <script>
						var details = '{detail}';
						</script>
                        <div ng-controller="ProductCtrl" class="col-md-12" ng-init="init(details)">
                          <!-- general form elements -->   
                            <form action="{base_url}product/add_product/" method="post" enctype="multipart/form-data">
                            <div class="row">
                            <input type="hidden" name="product_id" value="{product_id}" />
                                <div class="col-md-2">
                                    <label>Name</label>
                                    <input type="text" class="form-control input-sm" name="product_name" required="required"  ng-value="detail.product_name" ng-model="product.product_name">
                                </div>
                                <div class="col-md-2">
                                    <label>Motor</label>
                                    <input type="text" required="required" class="form-control input-sm" name="motor_name" ng-value="detail.motor" ng-model="product.motor">
                                </div>
                                 <div class="col-md-2">
                                    <label>Price</label>
                                    <input type="text" required="required" class="form-control input-sm" name="price" ng-model="product.price">
                                </div>
                                <div class="col-md-2">
                                    <label>Max Pump Pressure</label>
                                    <input type="text" required="required" class="form-control input-sm" name="max_pump_pressure" ng-model="product.max_pump_pressure">
                                </div>
                                <div class="col-md-2">
                                    <label>Pump Set Pressure</label>
                                    <input type="text" class="form-control input-sm" name="pump_set_pressure" required="required" ng-model="product.pump_set_pressure">
                                </div>
                                <div class="col-md-2">
                                    <label>Water Temperature</label>
                                    <input type="text" class="form-control input-sm" name="water_temprature" required="required" ng-model="product.water_temprature">
                                </div>
                                
                            </div>
                            <div class="row">
                            <div class="col-md-1">
                                    <label>Size</label>
                                    <input type="text" class="form-control input-sm" name="product_size" required="required" ng-model="product.product_size">
                                </div>
                                <div class="col-md-1">
                                    <label>Weight</label>
                                    <input type="text" class="form-control input-sm" name="product_weight" required="required" ng-model="product.product_weight">
                                </div>
                                <div class="col-md-2">
                                    <label>Category</label>
                                   {category}
                                </div>
                                
                                <div class="col-md-2" style="margin-top:10px;">
                                    <label>Select Image</label>
                                    <input type="file" name="product_img" />
                                </div>
                                <div ng-if="product.product_image" class="col-md-2" style="margin-top:10px;">
           							 <img ng-src="{base_url}/uploads/product_image/{{product.product_image}}" width="100px" height="100px"/>
            						</div>
                                
                            </div>
                            <div class="row"> 
                                <div class="col-md-3">
                                    <h4 class="box-title">Features</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Specification</label>
                                 {specification}
                                </div>
                               
                                <div class="col-md-2">
                                    <label>Motor / Engine</label>
                                   {motor}
                                </div>
                                <div class="col-md-2">
                                    <label>Pump</label>
                                    {pump}
                                </div>
                                <div class="col-md-2">
                                    <label>Boiler</label>
                                    {boiler}
                                </div>
                                <div class="col-md-2">
                                    <label>Frame</label>
                                   {frame}
                                </div>
                                <div class="col-md-2">
                                    <label>Water Tank</label>
                                     {water_tank}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <label>By-Pass</label>
                                   {by_pass}
                                </div>
                                <div class="col-md-2">
                                    <label>Control Box</label>
                                    {control_box}
                                </div>
                                <div class="col-md-2">
                                    <label>Hose / Gun</label>
                                   {hose_gun}
                                </div>
                            </div>
                           <!-- <div class="row">
                                <div class="col-md-3">
                                    <h4 class="box-title">Add-ons</h4>
                                </div>
                            </div>-->
                            <!--<div class="row" ng-controller="addons">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <a href="javascript:void(0);" ng-click="add_row()" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <table class="table">
                                            <thead>
                                                <th>Title</th>
                                                <th>Text</th>
                                                <th>Price</th>
                                                <th>Image</th>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in addonrow track by $index">
                                                    <td><input type="text" class="form-control input-sm"></td>
                                                    <td><input type="text" class="form-control input-sm"></td>
                                                    <td><input type="text" class="form-control input-sm"></td>
                                                    <td><input type="file" class="form-control input-sm"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>-->
                            <br />
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="submit" name="add_product" value="Submit" class="btn btn-primary">
                                    
                                    </form>
                                     
                                </div>
                            </div>
                           
                            
                        </div><!--/.col (left) -->                        
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
       
   {footer}