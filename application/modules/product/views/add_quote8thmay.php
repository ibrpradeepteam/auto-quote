{header} 
<script src="{base_url}ng-table/ng-table.js"></script>
<script>
 $(document).ready(function () { 
$('.chosen-choices').css('padding','5px');

$("#date1").datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true,
      startDate: new Date(),
      todayHighlight: true
    });
    
var permission = $("#permission").val();
if(permission == 0)
{
$("#remove_row").attr('disabled','disabled');
$("#add_row").attr('disabled','disabled');
}
var selected_customer = $('select#selected_customer option:selected').val();
   if(selected_customer !="add" && selected_customer != ""){
   auto_fill(selected_customer);
   }
 });
 

</script>
<style>
.disclose > span:before {
    content: '\25B6';
}

.disclose > span:before {
    content: '\25E2';
}
</style>				
<div class="wrapper row-offcanvas row-offcanvas-left">
       <form action="{base_url}product/quote/save_quote_data" method="post" id="quote_form" enctype="multipart/form-data">   
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side" ng-app="add_quote">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Quote                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Quote</a></li>                        
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                    <script>
						var details = '{detail}';
						var products = '{product_detail}';
						</script>
                        <!-- left column -->
                        <div class="col-md-12" ng-controller="QuotationCtrl" ng-init="init(details,products)">
                            <!-- general form elements -->   
                            <div class="row">
                             <input type="hidden" name="quote_id" value="{quote_id}" />
                             <input type="hidden" name="customer_id" value="{customer_id}" id="customer_id" />
                             
                            
                                <div class="col-md-3">
                                    <h4 class="box-title">CUSTOMER DETAILS</h4>
                                </div>
                                 
                            </div>
                            <br />
                            <div class="row">
                            <div class="col-md-2">
                                    <label>Select Customers</label>
                                    {customers}
                                </div>
                                
                                <div class="col-md-4 well" id="customer_info_required" style="margin:20px 0 0 20px; display:none;">
                            <span id="contact_info" style="display:none; font:bold;">Contact Info</span>
                            
                            <p id="customer_first_name" style="color:#000; font-size: 20px;"> </p>
                            <p id="customer_last_name" style="color:#000; font-size: 20px;"> </p>
                           
                            <p id="owner" style="color:#989898; font-size: 11px; display:block; margin:0px; padding:1px;"> </p>
                            
                            <p id="title" style="color:#000; display:block; margin:0px; padding:1px;"> </p>
                            
                            <p id="work_phone" style="color:#21B5D8; font-size: 16px; display:block; margin:0px; padding:1px;"> </p>
                            
                            <p id="phone_number1" style="color:#21B5D8; font-size: 16px; display:block; margin:0px; padding:1px;"> </p>
                            
                            <p id="email_add1" style="color:#21B5D8; font-size: 16px; display:block; margin:0px; padding:1px;"> </p>
                            
                            <p id="address1" style="color:#000; font-size: 16px; display:block; margin:0px; padding:1px;"> </p>
                            <p id="address2" style="color:#000; font-size: 16px;  margin:0px; padding:1px;"> </p>
                            <p id="address3" style="color:#000; font-size: 16px;  margin:0px; padding:1px;"> </p>
                            <p id="address4" style="color:#000; font-size: 16px;  margin:0px; padding:1px;"> </p>
                            
                            <p><input type="button" name="lookup" value="Edit Customer" class="btn" ng-click="edit_customer()" id="edit_customer" style="margin-left:50px;" ></p>
                            </div>
                            
                            <div class="col-md-2">
                           
                            </div>
                            </div>
                            
                            <br>
                            
                            <!-- customer info  --->
                            
                            
                          <!--  <div class="row">
                            
                                <div class="col-md-2">
                                    <label>First Name<span class="red-cl">*</span></label>
                                    
                                    <input type="text" class="form-control input-sm" name="customer_f_name" id="customer_f_name" required="required" ng-model="quote.customer_f_name" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>>
                                </div>
                                <div class="col-md-2">
                                    <label>Last Name<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="customer_l_name" id="customer_l_name" required="required" ng-model="quote.customer_l_name" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>>
                                </div>
                                <div class="col-md-2">
                                    <label>Company Name<span class="red-cl">*</span></label>
                                    
                                    <input type="text" class="form-control input-sm" name="company_name" id="company_name" required="required" value="<?php echo isset($customer_company['company_name']) ? $customer_company['company_name'] : ''; ?>" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>>
                                </div>
                                <div class="col-md-2">
                                    <label>Phone Number<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="phone_number" id="phone_number" required="required" ng-model="quote.phone_number" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>>
                                </div>
                                <div class="col-md-2">
                                    <label>Email Address<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="email_add" id="email_add" required="required" ng-model="quote.email_add" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>>
                                </div>
                                <div class="col-md-2">
                                    <label>Street Address</label>
                                    <input type="text" class="form-control input-sm" name="street_add" id="street_add" ng-model="quote.street_add" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>>
                                </div>
                            
                            </div>
                            <div class="row">
                            <div class="col-md-1">
                                    <label>Suburb<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="suburb" id="suburb" ng-model="quote.suburb" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>>
                                </div>
                                <div class="col-md-1">
                                    <label>State<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="state" id="state" ng-model="quote.state" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>>
                                </div>
                                <div class="col-md-2">
                                    <label>Post Code<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="post_code" id="post_code" ng-model="quote.post_code" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>>
                                </div>
                                <div class="col-md-3" style="margin-top: 25px;">
                                    <div class="col-md-7">
                                        <label>&nbsp;</label>
                                        <!--<input type="hidden" name="customer_id" value="" id="customer_id" />-->
                                        <!-- <input type="hidden" name="customer_id1" id="customer_id1" ng-model="quote.customer_id" value="{customer_id}" />
                                        <input type="button" name="lookup" value="Edit Customer" class="btn" ng-click="edit_customer()" id="edit_customer">
                                    </div>
                                    <div class="col-md-5">
                                        <label>&nbsp;</label>
                                       <!-- <input type="button" name="lookup" value="RESET SHEET" class="btn btn-danger" onclick="reset_values()"> -->
                                    <!--</div>
                                </div>
                            </div> -->
                           <!-- <div class="row"> 
                                <div class="col-md-3">
                                    <h4 class="box-title">Product(Machine)</h4>
                                </div>
                            </div>-->
                            <div class="row" id="product_row" ng-controller="addons" ng-init="init(details,products)">
                          
                            <div class="row product" ng-repeat="row in addonrow" style="float:left; width:65%; margin-top:10px; margin-left:2px;">
                           
                                <div class="col-md-6">
                               
                                    <label>CATEGORY<span class="red-cl">*</span></label>
                                    {category}
                                </div>
                                
                                <div class="col-md-5">
                                    <label>MACHINE<span class="red-cl">*</span></label>
                                    <select class="form-control input-sm" ng-model="row.product_id" id="machine_dropdown_{{$index}}" onchange="check_limit(this.value,this.id)" name="product_added[]" required="required">
                                        <option value="">--Select--</option>
                                    </select> 
                                </div>
                              
                                
                                 <div class="col-md-1">
                                <input type="hidden" id="permission" value="{add_more_products}" />
                                   &nbsp;<button href="javascript:void(0);" ng-click="remove_row($index)" id="remove_row"  class="btn btn-primary btn-sm" style="margin-top:28px; margin-left:-7px;"><span class="fa fa-times" style="cursor:pointer;"></span></botton>
                             </div>
                              
                            </div> 
                            
                            
                             <div style=" width:30%; float:left; margin-top:38px;">
                            
                            &nbsp;<button href="javascript:void(0);" ng-click="add_row()" id="add_row" class="btn btn-primary btn-sm" style="margin-left:8px;"><span class="fa fa-plus" style="cursor:pointer;"></span></button> 
                           
                           </div>                               
                         </div>
                               
                          <div class="row">
                          <!--  <div class="col-md-4 pull-left">
                                    <label>SALES PERSON<span class="red-cl">*</span></label>
                                    {sales_persons}
                                   
                                </div> -->
                               
                              </div> 
                            
                            
                            <!----- product detail onchang ---->
                           
                            <div class="product_detail" id="product_detail_main" >
                          	         
                            </div>
                            
                         
                          <div ng-if="product[0].quote_product_id">
                       
                          <div class="detail_length" style="width:400px; float:left;" ng-repeat="product in product" id="product_detail_{{$index}}">
                           
                                <div class="col-md-12">                                   
                                    <div class="col-md-12"> 
                                     <h4 class="box-title" id="product_title_{{$index}}">Product Detail- {{product.product_name}}                                       </h4>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td align="right">PRICE</td>
                                                    <input type="hidden" name="product_id[]" value="" id="product_id_{{$index}}" />
                                                    
                                    <input type="hidden" name="product_name[]" value="" id="product_name_{{$index}}" /> </h4>
                                                    <td><input type="text" value="" id="product_price_{{$index}}" name="product_price[]" class="form-control input-sm" ng-model="product.product_price" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Pump Rated Pressure</td>
                                                    <td><input type="text" value="" id="rated_pressure_{{$index}}" name="rated_pressure[]" class="form-control input-sm" ng-model="product.rated_pressure" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Pump Setup Pressure</td>
                                                    <td><input type="text" value="" id="setup_pressure_{{$index}}" name="setup_pressure[]" class="form-control input-sm" ng-model="product.setup_pressure" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Size</td>
                                                    <td><input type="text" value="" id="size_{{$index}}" name="size[]" ng-model="product.size" class="form-control input-sm" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Weight</td>
                                                    <td><input type="text" value="" id="weight_{{$index}}" name="weight[]" ng-model="product.weight" class="form-control input-sm" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                </tr>
                                                 <tr>
                                                    <td align="right">Installation Cost</td>
                                                    <td><input type="text" value="" id="cost_{{$index}}" name="cost[]" ng-model="product.installation_cost" class="form-control input-sm" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Freight</td>
                                                    <td><input type="text" value="" id="desc_{{$index}}" name="desc[]" ng-model="product.installation_desc" class="form-control input-sm" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                </tr>
                                                
                                                 <tr>
                                                    <td align="right">Image</td>
                                                    <input type="hidden" id="image_{{$index}}" value="" />
                                                    <input type="hidden" ng-model="product.product_image" id="product_main_image_{{$index}}">
                                                
                                                    <td>
                                                    <span ng-if="product.quote_product_image" >
                                                    <img ng-src="{base_url}uploads/product_image/{{product.quote_product_image}}" id="image_src_{{$index}}" width="100px" height="100px" />
                                                    <script>
                                                    $('.image_src').css('display','none');
                                                    </script>
                                                    </span>
                                                    <img ng-src="{base_url}uploads/product_image/{{product.product_image}}" id="image_src_{{$index}}" class="image_src" width="100px" height="100px" />
                                                    
                                                    </td>          
                                                </tr>
                                                
                                                <tr>
                                                    <td align="right"></td>                                                  
                                                    <td>
                                                    <input type="file" name="new_product_image_{{$index}}" id="new_product_image_{{$index}}" />
                                                    </td>          
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                           
                            </div>  
                            
                            <div class="row">
                                <div class="col-md-12">                                   
                                    <div class="col-md-4">
                                    <label style="margin-top:20px;">Machine Selling Points</label>                                        
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Point 1</td>
                                                    <td><input type="text" value="" class="form-control input-sm" name="selling_point1" ng-model="quote.selling_point1" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                </tr>
                                                <tr>
                                                    <td>Point 2</td>
                                                    <td><input type="text" value="" class="form-control input-sm" name="selling_point2"  ng-model="quote.selling_point2" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                </tr>
                                                <tr>
                                                    <td>Point 3</td>
                                                    <td><input type="text" value="" class="form-control input-sm" name="selling_point3"  ng-model="quote.selling_point3" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                </tr>
                                                <tr>
                                                    <td>Point 4</td>
                                                    <td><input type="text" value="" class="form-control input-sm" name="selling_point4" ng-model="quote.selling_point4" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                </tr>
                                                <tr>
                                                    <td>Point 5</td>
                                                    <td><input type="text" value="" class="form-control input-sm" name="selling_point5" ng-model="quote.selling_point5" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                </tr>
                                                <tr>
                                                    <td>Point 6</td>
                                                    <td><input type="text" value="" class="form-control input-sm" name="selling_point6" ng-model="quote.selling_point6" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">                                   
                                    <div class="col-md-12">                                        
                                        <table class="table">
										
                                            <tbody>
                                                <tr>
                                                    <td align='right' class="col-md-3">Optional Extra 1</td>
                                                    <td class="col-md-5">
                                                       
                                                        {add_on1}
                                                    </td>
                                                    <td class="col-md-4"><input type="text" value="" ng-model="quote.optional_extra1_price" class="form-control input-sm" id="add_on1" name="add_on1" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
                                                     <span ng-if="quote_detail.optional_extra1_img != ''">
													 <td class="col-md-4"><img  id="add_on12" class="image_src" width="50px" height="30px" value="{{quote_detail.optional_extra1_img}}" /><input type="hidden" id="add_ion12"  name="add_on_dimg1" ng-value="quote.optional_extra1_img" ng-model="quote.optional_extra1_img" /></td>  
													 </span>
													 
													 <td class="col-md-4"><input type="file" name="add_on_img1" width="50px" height="30px"/></td>
													 </td>

												</tr>
                                                <tr>
                                                   <td align='right' class="col-md-3">Optional Extra 2</td>
                                                    <td class="col-md-5">
                                                        {add_on2}
                                                    </td>
                                                    <td class="col-md-4"><input type="text" value="" ng-model="quote.optional_extra2_price" class="form-control input-sm" id="add_on2" name="add_on2" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
													 <td class="col-md-4"><img  id="add_on22" class="image_src" width="50px" height="30px"  /><input type="hidden" id="add_ion22" name="add_on_dimg2" ng-value="quote.optional_extra2_img" ng-model="quote.optional_extra2_img" /></td>  
													 <td class="col-md-4"><input type="file" name="add_on_img2" width="50px" height="30px"/></td>

											   </tr>	
                                                <tr>
                                                    <td align='right' class="col-md-3">Optional Extra 3</td>
                                                    <td class="col-md-5">
                                                       {add_on3}
                                                    </td>
                                                    <td class="col-md-4"><input type="text" value="" ng-model="quote.optional_extra3_price" class="form-control input-sm" id="add_on3" name="add_on3" <?php if(isset($quote_id) && $quote_id != ''){ echo "readonly";} ?>></td>
													 <td class="col-md-4"><img  id="add_on32" class="image_src" width="50px" height="30px"   /><input type="hidden" id="add_ion32" name="add_on_dimg3" ng-value="quote.optional_extra3_img" ng-model="quote.optional_extra3_img"/></td>  
													 <td class="col-md-4"><input type="file" name="add_on_img3" width="50px" height="30px"/></td>

												</tr>                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input type="checkbox" checked="checked">
                                                <label>View PDF after export</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox">
                                                <label>Send Email after export</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox">
                                                <label>BCC: Pipeline Deals</label>
                                            </td>
                                        </tr>                                                
                                    </tbody>
                                </table>
                                </div>
                                <!--<input type="submit" name="submit" value="Export PDF" class="btn btn-success btn-lg">-->
                               <div ng-if="quote.id">
                               <script>
							   //$('#view_html').css('display','none');
							   </script>
                               </div> 
                               
                               
                               <input type="submit" name="view_html" id="view_html" value="Configure Quote" class="btn btn-success btn-lg"  title="You are not allowed to configure quote">
                                 
                              <!-- <button name="" value="View html" class="btn btn-success btn-lg" onclick="view_html()">Configure Quote</button>-->
                              
                               
                            </div>
                        </div><!--/.col (left) -->                        
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div>
        
         </form> 

</div>
</div>
</body>
</html>


<div class="product_detail_N" id="save_html" style="display:none; float:left;">
<div class="detail_length" id="product_detail_N" style="width:400px;float:left;">
                            <div class="row" style="width:400px;">
                                <div class="col-md-12">
                                    <h4 class="box-title" id="product_title_N">
                                </div>
                            </div>
                            <div class="row" style="width:400px; float:left;">
                                <div class="col-md-12">                                   
                                    <div class="col-md-12">                                        
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td align="right">PRICE</td>
                                                    <input type="hidden" name="product_id[]" value="" id="product_id_N" />
                                    <input type="hidden" name="product_name[]" value="" id="product_name_N" /> </h4>
                                    
                                                    <td><input type="text" value="" id="product_price_N" name="product_price[]" class="form-control input-sm"></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Pump Rated Pressure</td>
                                                    <td><input type="text" value="" id="rated_pressure_N" name="rated_pressure[]" class="form-control input-sm"></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Pump Setup Pressure</td>
                                                    <td><input type="text" value="" id="setup_pressure_N" name="setup_pressure[]" class="form-control input-sm"></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Size</td>
                                                    <td><input type="text" value="" id="size_N" name="size[]"  class="form-control input-sm"></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Weight</td>
                                                    <td><input type="text" value="" id="weight_N" name="weight[]"  class="form-control input-sm"></td>
                                                </tr>
                                                 <tr>
                                                    <td align="right">Installation Cost</td>
                                                    <td><input type="text" value="" id="cost_N" name="cost[]" class="form-control input-sm"></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Freight</td>
                                                    <td><!--<textarea id="desc_N" name="desc[]"  class="form-control input-sm" cols="10">
                                                    
                                                    </textarea>-->
                                                    
                                                    <input type="text" id="desc_N" name="desc[]" class="form-control input-sm">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Image</td>
                                                    <input type="hidden" id="image_N" value="" />
                                                    
                                                    <td><img id="image_src_N" width="100px" height="100px" /> 
                                                    </td>          
                                                </tr>
                                                
                                                <tr>
                                                    <td align="right"></td>                                                  
                                                    <td>
                                                    <input type="file" name="new_product_image_N" id="new_product_image_N" />
                                                    </td>          
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            </div>
                         
                            </div>
                            <?php
                            if($this->session->userdata('admin_type') == 1){
                               ?>
                                 <script>
                                
							   $('#view_html').removeAttr("type");
							   </script>
                               <?php 
                               }
                                if($this->session->userdata('admin_type') != 1){
                               ?>
                                <script>
                                
							   $('#view_html').removeAttr("title");
							   </script>
							   <?php
							   }
							   ?>