{header} 
<script src="{base_url}ng-table/ng-table.js"></script>
<script>
 $(document).ready(function () { 
$('.chosen-choices').css('padding','5px');
 });
 
function reset_values(){
$('#customer_f_name').val('');
$('#customer_l_name').val('');
$('#phone_number').val('');
$('#company_name').val('');
 $('#email_add').val('');
 $('#street_add').val('');
 $('#state').val('') ;
 $('#post_code').val('');
  $('#suburb').val('');
} 
</script>
 						
<div class="wrapper row-offcanvas row-offcanvas-left">
       <form action="{base_url}product/quote/save_quote_data" method="post" id="quote_form">   
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side" ng-app="add_quote">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Quote                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Quote</a></li>                        
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                    <script>
						var details = '{detail}';
						</script>
                        <!-- left column -->
                        <div class="col-md-12" ng-controller="QuoteCtrl" ng-init="init(details)">
                            <!-- general form elements -->   
                            <div class="row">
                                <div class="col-md-3">
                                    <h4 class="box-title">CUSTOMER DETAILS</h4>
                                </div>
                            </div>
                            <div class="row">
                            
                                <div class="col-md-2">
                                    <label>First Name<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="customer_f_name" id="customer_f_name" required="required" ng-model="quote.customer_f_name">
                                </div>
                                <div class="col-md-2">
                                    <label>Last Name<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="customer_l_name" id="customer_l_name" required="required" ng-model="quote.customer_l_name">
                                </div>
                                <div class="col-md-2">
                                    <label>Company Name<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="company_name" id="company_name" required="required" ng-model="quote.company_name">
                                </div>
                                <div class="col-md-2">
                                    <label>Phone Number<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="phone_number" id="phone_number" required="required" ng-model="quote.phone_number">
                                </div>
                                <div class="col-md-2">
                                    <label>Email Address<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="email_add" id="email_add" required="required" ng-model="quote.email_add">
                                </div>
                                <div class="col-md-2">
                                    <label>Street Address</label>
                                    <input type="text" class="form-control input-sm" name="street_add" id="street_add" ng-model="quote.street_add">
                                </div>
                            
                            </div>
                            <div class="row">
                            <div class="col-md-1">
                                    <label>Suburb<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="suburb" id="suburb" ng-model="quote.suburb">
                                </div>
                                <div class="col-md-1">
                                    <label>State<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="state" id="state" ng-model="quote.state">
                                </div>
                                <div class="col-md-2">
                                    <label>Post Code<span class="red-cl">*</span></label>
                                    <input type="text" class="form-control input-sm" name="post_code" id="post_code" ng-model="quote.post_code">
                                </div>
                                <div class="col-md-3 pull-right" style="margin-right: 70px;">
                                    <div class="col-md-7">
                                        <label>&nbsp;</label>
                                       <!-- <input type="button" name="lookup" value="QUOTE LOOKUP" class="btn">-->
                                    </div>
                                    <div class="col-md-5">
                                        <label>&nbsp;</label>
                                        <input type="button" name="lookup" value="RESET SHEET" class="btn btn-danger" onclick="reset_values()">
                                    </div>
                                </div>
                            </div>
                           <!-- <div class="row"> 
                                <div class="col-md-3">
                                    <h4 class="box-title">Product(Machine)</h4>
                                </div>
                            </div>-->
                            <div class="row">
                                <div class="col-md-2">
                                    <label>MACHINE CATEGORY<span class="red-cl">*</span></label>
                                    {category}
                                </div>
                                <div class="col-md-2">
                                    <label>MACHINE<span class="red-cl">*</span></label>
                                    <select class="form-control input-sm chzn-select" id="machine_dropdown" onChange="check_limit(this.value)" multiple name="product_added">
                                        <option value="">--Select--</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label>SALES PERSON<span class="red-cl">*</span></label>
                                    {sales_persons}
                                </div>
                                
                            </div>
                            
                            
                            
                            <!----- product detail onchang ---->
                            <div class="product_detail" id="product_detail_main" >
                          	<div ng-if="quote.product_id">
                            <script>
							//$('#save_html').css('display','block');
							</script>
                           
                            </div>           
                            </div>
                          
                            
                            <div class="row">
                                <div class="col-md-12">                                   
                                    <div class="col-md-4">
                                    <label style="margin-top:20px;">Machine Selling Points</label>                                        
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Point 1<span class="red-cl">*</span></td>
                                                    <td><input type="text" value="" class="form-control input-sm" name="selling_point1" required="required" ng-model="quote.selling_point1"></td>
                                                </tr>
                                                <tr>
                                                    <td>Point 2<span class="red-cl">*</span></td>
                                                    <td><input type="text" value="" class="form-control input-sm" name="selling_point2" required="required" ng-model="quote.selling_point2"></td>
                                                </tr>
                                                <tr>
                                                    <td>Point 3<span class="red-cl">*</span></td>
                                                    <td><input type="text" value="" class="form-control input-sm" name="selling_point3" required="required" ng-model="quote.selling_point3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Point 4</td>
                                                    <td><input type="text" value="" class="form-control input-sm" name="selling_point4" ng-model="quote.selling_point4"></td>
                                                </tr>
                                                <tr>
                                                    <td>Point 5</td>
                                                    <td><input type="text" value="" class="form-control input-sm" name="selling_point5" ng-model="quote.selling_point5"></td>
                                                </tr>
                                                <tr>
                                                    <td>Point 6</td>
                                                    <td><input type="text" value="" class="form-control input-sm" name="selling_point6" ng-model="quote.selling_point6"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">                                   
                                    <div class="col-md-6">                                        
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td align='right' class="col-md-3">Optional Extra 1</td>
                                                    <td class="col-md-5">
                                                        <select class="form-control input-sm"></select>
                                                    </td>
                                                    <td class="col-md-4"><input type="text" value="" class="form-control input-sm"></td>
                                                </tr>
                                                <tr>
                                                   <td align='right' class="col-md-3">Optional Extra 2</td>
                                                    <td class="col-md-5">
                                                        <select class="form-control input-sm"></select>
                                                    </td>
                                                    <td class="col-md-4"><input type="text" value="" class="form-control input-sm"></td>
                                                </tr>
                                                <tr>
                                                    <td align='right' class="col-md-3">Optional Extra 3</td>
                                                    <td class="col-md-5">
                                                        <select class="form-control input-sm"></select>
                                                    </td>
                                                    <td class="col-md-4"><input type="text" value="" class="form-control input-sm"></td>
                                                </tr>                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input type="checkbox" checked="checked">
                                                <label>View PDF after export</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox">
                                                <label>Send Email after export</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox">
                                                <label>BCC: Pipeline Deals</label>
                                            </td>
                                        </tr>                                                
                                    </tbody>
                                </table>
                                </div>
                                <!--<input type="submit" name="submit" value="Export PDF" class="btn btn-success btn-lg">-->
                               <div ng-if="quote.id">
                               <script>
							   $('#view_html').css('display','none');
							   </script>
                               </div> 
                               <input type="submit" name="view_html" id="view_html" value="View html" class="btn btn-success btn-lg" onclick="view_html()">
                                 
                              <!-- <button name="" value="View html" class="btn btn-success btn-lg" onclick="view_html()">View html</button>-->
                              
                               
                            </div>
                        </div><!--/.col (left) -->                        
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div>
        
         </form> 

</div>
</div>
</body>
</html>


<div class="product_detail_N" id="save_html" style="display:none; float:left;">
<div class="detail_length" id="product_detail_N" style="width:400px;float:left;">
                            <div class="row" style="width:400px;">
                                <div class="col-md-12">
                                    <h4 class="box-title" id="product_title_N">
                                </div>
                            </div>
                            <div class="row" style="width:400px; float:left;">
                                <div class="col-md-12">                                   
                                    <div class="col-md-12">                                        
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td align="right">PRICE</td>
                                                    <input type="hidden" name="product_id[]" value="" id="product_id_N" />
                                    <input type="hidden" name="product_name[]" value="" id="product_name_N" /> </h4>
                                                    <td><input type="text" value="" id="product_price_N" name="product_price[]" class="form-control input-sm"></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Pump Rated Pressure</td>
                                                    <td><input type="text" value="" id="rated_pressure_N" name="rated_pressure[]" class="form-control input-sm"></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Pump Setup Pressure</td>
                                                    <td><input type="text" value="" id="setup_pressure_N" name="setup_pressure[]" class="form-control input-sm"></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Quote Date</td>
                                                    <td><input type="text" value="" id="quote_date_N" name="quote_data[]" class="form-control input-sm"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div>