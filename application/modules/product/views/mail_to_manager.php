<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<?php $this->load->view('includes/styles');
$this->load->view('includes/scripts');
 ?>

<script src="<?php echo base_url('js/ckeditor');?>/ckeditor.js"></script>
<script src="<?php echo base_url('js');?>/adapters/jquery.js"></script>
<script>
$( document ).ready( function() {
	$( '#editor').ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.	
} );
</script>
<script>
function mail_to_customer(){
		var customer_name = $('#customer_name').val();
		var customer_email = $('#customer_email').val();
		var quote_id = $('#quote_id').val();
		
window.location.href='{base_url}product/quote/GeneratePDF/?name='+customer_name+'&email='+customer_email+'&quote_id='+quote_id;	
}

function mail_to_both(){
		var customer_name = $('#customer_name').val();
		var customer_email = $('#customer_email').val();
		var quote_id = $('#quote_id').val();
		
window.location.href='{base_url}product/quote/GeneratePDF/?name='+customer_name+'&email='+customer_email+'&quote_id='+quote_id;	
}
</script>
</head>

<body>
<form action="<?php echo base_url('product/quote/GeneratePDF') ?>" method="post">
<div class="row">
<div class="col-md-12">
<label> To &nbsp; </label>
 <input type="hidden" name="customer_name" value="<?php echo isset($name) ? $name : '';  ?>" id="customer_name" />
 <input type="hidden" name="quote_id" value="<?php echo isset($quote_id) ? $quote_id : '';  ?>" id="quote_id" />
 <input type="hidden" name="customer_email" value="<?php echo isset($email) ? $email : '';  ?>" id="customer_email" />
 
<input type="text" name="manager_email" value="<?php echo isset($email) ? $email : ''; ?>" class="form-control span12" style="width:80%;" required="required" id="manager_email" />
</div>
</div>

<div class="row">
<div class="col-md-12">
<label> Subject &nbsp; </label>
<input type="text" name="subject" value="" class="form-control span12" style="width:80%;" id="manager_sub" />
</div>
</div>

<div class="row">
<div class="col-md-8">
<label> Message: &nbsp; </label>
<textarea id="editor" rows="5" cols="10" name="message" style="width:80%;" required="required">
<?php echo isset($manager_msg) ? $manager_msg :''; ?>
</textarea>
</div>
</div>
<br />
<div class="row">

<input type="submit" name="send_mail" id="send_mail" value="Send To Both,Manager and Customer" class="btn btn-success btn-lg" style="margin-left:50px;">
<button name="" value="" id="" class="btn btn-success btn-danger" style="margin-left:30px;" onclick="mail_to_customer()">Send To Customer</button>

</div>
</form>
</body>
</html>