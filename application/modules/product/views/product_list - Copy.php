{header}
<script src="{base_url}ng-table/ng-table.js"></script>
<!--<script src="{base_url}js/table_edit/jquery.tabledit.js"></script>-->
<script src="{base_url}js/dataTables.tableTools.min.js"></script>
<!--<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" language="javascript" src="../../js/dataTables.editor.js"></script>-->
<script>
  
var editor; // use a global for the submit and return data rendering in the examples
 
$(document).ready(function () {
/*$('#example').Tabledit({
    url: 'example.php',
    columns: {
        identifier: [0, 'id'],
        editable: [[1, 'username'], [2, 'email'], [3, 'avatar', '{"1": "Black Widow", "2": "Captain America", "3": "Iron Man"}']]
    },
    onDraw: function() {
        console.log('onDraw()');
    },
    onSuccess: function(data, textStatus, jqXHR) {
        console.log('onSuccess(data, textStatus, jqXHR)');
        console.log(data);
        console.log(textStatus);
        console.log(jqXHR);
    },
    onFail: function(jqXHR, textStatus, errorThrown) {
        console.log('onFail(jqXHR, textStatus, errorThrown)');
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
    },
    onAlways: function() {
        console.log('onAlways()');
    },
    onAjax: function(action, serialize) {
        console.log('onAjax(action, serialize)');
        console.log(action);
        console.log(serialize);
    }*/



    // Activate an inline edit on click of a table cell
    $("#products").dataTable({
	   "sPaginationType": "full_numbers",
       "aoColumnDefs" : [ {
       'bSortable' : false,
       'aTargets' : [ -1,0 ],
	  
      } ],
	  
		});	
		
		$("#example").dataTable({
	   "sPaginationType": "full_numbers",
       "aoColumnDefs" : [ {
       'bSortable' : false,
       'aTargets' : [ -1,0 ],
	  
      } ],
	  
		});	

});

function edit_row(id){

$("#save_edited_"+id).css('display','inline-block');
$("#edit_row_"+id).css('display','none');

name = $('#name_'+id).text();
$('#name_'+id).html("<input type='text' value='" + name + "' name='product_name_"+id+"' id='product_name_"+id+"' />");

motor = $('#motor_'+id).text();
$('#motor_'+id).html("<input type='text' value='" + motor + "' name='motor_"+id+"' id='product_motor_"+id+"' />");

price = $('#price_'+id).text();
$('#price_'+id).html("<input type='text' value='" + price + "' name='price_"+id+"' id='product_price_"+id+"' />");

max_pump_pressure = $('#max_pump_pressure_'+id).text();
$('#max_pump_pressure_'+id).html("<input type='text' value='" + max_pump_pressure + "' name='max_pump_pressure_"+id+"' id='product_max_pump_pressure_"+id+"' />");

pump_set_pressure = $('#pump_set_pressure_'+id).text();
$('#pump_set_pressure_'+id).html("<input type='text' value='" + pump_set_pressure + "' name='pump_set_pressure_"+id+"' id='product_pump_set_pressure_"+id+"' />");

water_temp = $('#water_temp_'+id).text();
$('#water_temp_'+id).html("<input type='text' value='" + water_temp + "' name='water_temp_"+id+"' id='product_water_temp_"+id+"' />");

size = $('#size_'+id).text();
$('#size_'+id).html("<input type='text' value='" + size + "' name='product_size_"+id+"' id='product_size_"+id+"' />");

weight = $('#weight_'+id).text();
$('#weight_'+id).html("<input type='text' value='" + weight + "' name='product_weight_"+id+"' id='product_weight_"+id+"' />");

$("#category_view_"+id).css('display','none');
$("#category_edit_"+id).css('display','block');

$("#product_image_"+id).css('display','block');

$("#feature1_view_"+id).css('display','none');
$("#feature1_edit_"+id).css('display','block');

$("#feature2_view_"+id).css('display','none');
$("#feature2_edit_"+id).css('display','block');

$("#feature3_view_"+id).css('display','none');
$("#feature3_edit_"+id).css('display','block');

$("#feature4_view_"+id).css('display','none');
$("#feature4_edit_"+id).css('display','block');

$("#feature5_view_"+id).css('display','none');
$("#feature5_edit_"+id).css('display','block');

$("#feature6_view_"+id).css('display','none');
$("#feature6_edit_"+id).css('display','block');

$("#feature7_view_"+id).css('display','none');
$("#feature7_edit_"+id).css('display','block');

$("#feature8_view_"+id).css('display','none');
$("#feature8_edit_"+id).css('display','block');

$("#feature9_view_"+id).css('display','none');
$("#feature9_edit_"+id).css('display','block');
}

function save_row(id){
$('#save_edited_'+id).css('display','none');
$('#edit_row_'+id).css('display','inline');
var file_data = $("#product_image_"+id).prop("files")[0];

var form_data = new FormData();                  
    form_data.append("file", file_data)
 
$.ajax({
						url:base_url+'ajax/upload_image.php',
						dataType: 'text',
						cache: false,
						contentType: false,
						processData: false,
						data: form_data,  
						type :'post',						
						success:function(json){
						}
						});	
						
$("edit_row_"+id).css('display','inline');

product_name = $("#product_name_"+id).val(); 
motor = $("#product_motor_"+id).val(); 
price = $("#product_price_"+id).val(); 
size = $("#product_size_"+id).val(); 
weight = $("#product_weight_"+id).val(); 
water_temp = $("#product_water_temp_"+id).val(); 
max_pump_pressure = $("#product_max_pump_pressure_"+id).val(); 
pump_set_pressure = $("#product_pump_set_pressure_"+id).val(); 

product_category = $("#category_edit_"+id).val();
category_text = $("#category_edit_"+id+" option:selected").text(); 

product_image = $("#product_image_"+id).val();
//console.log(product_image);

feature1 = $("#feature1_edit_"+id).val(); 
feature1_text = $("#feature1_edit_"+id+" option:selected").text();

feature2 = $("#feature2_edit_"+id).val(); 
feature2_text = $("#feature2_edit_"+id+" option:selected").text();

feature3 = $("#feature3_edit_"+id).val(); 
feature3_text = $("#feature3_edit_"+id+" option:selected").text();

feature4 = $("#feature4_edit_"+id).val(); 
feature4_text = $("#feature4_edit_"+id+" option:selected").text();

feature5 = $("#feature5_edit_"+id).val(); 
feature5_text = $("#feature5_edit_"+id+" option:selected").text();

feature6 = $("#feature6_edit_"+id).val(); 
feature6_text = $("#feature6_edit_"+id+" option:selected").text();

feature7 = $("#feature7_edit_"+id).val(); 
feature7_text = $("#feature7_edit_"+id+" option:selected").text();

feature8 = $("#feature8_edit_"+id).val(); 
feature8_text = $("#feature8_edit_"+id+" option:selected").text();

feature9 = $("#feature9_edit_"+id).val(); 
feature9_text = $("#feature9_edit_"+id+" option:selected").text();
$.ajax({
				type:'post',
				url:base_url+'product/save_row',
				data:{id:id,name:product_name,motor:motor,price:price,size:size,weight:weight,water_temp:water_temp, max_pump_pressure:max_pump_pressure,pump_set_pressure:pump_set_pressure,category:product_category,image:product_image,feature1:feature1,feature2:feature2,feature3:feature3,feature4:feature4,feature5:feature5,feature6:feature6,feature7:feature7,feature8:feature8,feature9:feature9,},
				success:function(json){
				
				$("#name_"+id).html(product_name);
				$("#motor_"+id).html(motor);
				$("#price_"+id).html(price);
				$("#size_"+id).html(size);
				$("#weight_"+id).html(weight);
				$("#water_temp_"+id).html(water_temp);
				$("#max_pump_pressure_"+id).html(max_pump_pressure);
				$("#pump_set_pressure_"+id).html(pump_set_pressure);
				
				$("#category_view_"+id).css('display','inline');
				$("#category_view_"+id).html(category_text);
				$("#category_edit_"+id).css('display','none');
				
				$("#product_image_new_"+id).attr('src',base_url+'uploads/product_image/'+product_image.substring(12));
				
				$("#feature1_view_"+id).css('display','inline');
				if(feature1 != ''){
				$("#feature1_view_"+id).html(feature1_text);
				}
				$("#feature1_edit_"+id).css('display','none');
				
				$("#feature2_view_"+id).css('display','inline');
				if(feature2 != ''){
				$("#feature2_view_"+id).html(feature2_text);
				}
				$("#feature2_edit_"+id).css('display','none');
				
				$("#feature3_view_"+id).css('display','inline');
				if(feature3 != ''){
				$("#feature3_view_"+id).html(feature3_text);
				}
				$("#feature3_edit_"+id).css('display','none');
				
				$("#feature4_view_"+id).css('display','inline');
				if(feature4 != ''){
				$("#feature4_view_"+id).html(feature4_text);
				}
				$("#feature4_edit_"+id).css('display','none');
				
				$("#feature5_view_"+id).css('display','inline');
				if(feature5 != ''){
				$("#feature5_view_"+id).html(feature5_text);
				}
				$("#feature5_edit_"+id).css('display','none');
				
				$("#feature6_view_"+id).css('display','inline');
				if(feature6 != ''){
				$("#feature6_view_"+id).html(feature6_text);
				}
				$("#feature6_edit_"+id).css('display','none');
				
				$("#feature7_view_"+id).css('display','inline');
				if(feature7 != ''){
				$("#feature7_view_"+id).html(feature7_text);
				}
				$("#feature7_edit_"+id).css('display','none');
				
				$("#feature8_view_"+id).css('display','inline');
				if(feature8 != ''){
				$("#feature8_view_"+id).html(feature8_text);
				}
				$("#feature8_edit_"+id).css('display','none');
				
				$("#feature9_view_"+id).css('display','inline');
				if(feature9 != ''){
				$("#feature9_view_"+id).html(feature9_text);
				}
				$("#feature9_edit_"+id).css('display','none');
				}
});
}
 

</script>
<style>
.newscroll{
	overflow-x:scroll !important;
	 min-height:0px !important; 
}
</style>

<div class="wrapper row-offcanvas row-offcanvas-left" style="min-height:auto !important;">
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    {page_header}
     <!-- Main content -->
     
    <section class="content" ng-app="main">    	
        <div class="row">
        	<div ng-controller="DemoCtrl" class="demo-reponsiveTable clearfix">  
<div class="newscroll"> 
<div class="col-md-2">
	 <select class="form-control input-sm" data-width="145px" id="get_dropdown">
                          <option value="">--Select--</option>                            
                          <option value="1">Product Name</option>
                          <option value="1">Motor</option>
                          <option value="1">Price</option>
                          <option value="1">Max Pump Pressure</option>
                          <option value="1">Pump Set Pressure</option>
                          <option value="1">Water Temprature</option> 
						  <option value="1">Size</option> 
						   <option value="1">Weight</option> 
						  <option value="Category">Category</option>
						  <option value="Specification">Specification</option>
						  <option value="Motor / Engine">Motor / Engine</option>
						  <option value="Pump">Pump</option>
						  <option value="Boiler">Boiler</option>
						  <option value="Frame">Frame</option>
						  <option value="Water Tank">Water Tank</option>
						  <option value="By-Pass">By-Pass</option>
						  <option value="Control Box">Control Box</option>
						  <option value="Hose / Gun">Hose / Gun</option>								  
                        </select>  
	</div>
	
	<div class="col-md-2">
	<input type="text" name="common_field" id="common_field" class="form-control input-sm" style="display:none;" />
	</div>
	
	<div class="col-md-2">
	<button class="btn btn-success update_value" id="update_product" style="display:none;">Submit</button>
	</div>
	
	<div class="col-md-2">
	<select id="common_dropdown" name="common_dropdown" class="form-control input-sm" style="display:none; margin-left:-342px;">
	</select>
	</div>
	<br>
	<br>
	<div class="col-md-2">
	<button class="btn btn-danger delete_products" id="delete_products">Delete Products</button>
	</div>
	
	<br>
	<br>
	<div class="col-md-2">
	<button class="btn btn-danger bulk_edit" id="bulk_edit">Edit Products</button>
	</div>
	
	
	<table id="example" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
					<th width="5%">&nbsp;<input type="checkbox" name="check_all" id="check_all" /></th>
					    <th>Name</th>
						<th>Motor</th>
						<th>Price</th>
						<th>Max Pump Pressure</th>
						<th>Pump Set Pressure</th>
						<th>Water Temp</th>						
						<th>Size</th>
						<th>Weight</th>
						<th>Category</th>
						<th>Image</th>
						<th>Specification</th>
						<th>Motor/Engine</th>
						<th>Pump</th>
						<th>Boiler</th>
						<th>Frame</th>
						<th>Water Tank</th>
						<th>By-pass</th>
						<th>Control Box</th>
						<th>Hose/gun</th>
						<th>Action</th>	
					</tr>
				</thead>
				<tbody>
				<?php
				foreach($products as $p){
				?>
				<tr id="product_row_<?php echo $p->id; ?>">
				<td title="'Name'" title-alt="'Name'">
                <input type="checkbox" name="delete_check[]" value="<?php echo $p->id; ?>" class="check" />
				</td>
				<td id="name_<?php echo $p->id; ?>"><?php echo $p->product_name; ?></td>
				<td id="motor_<?php echo $p->id; ?>"><?php echo $p->motor; ?></td>
				<td id="price_<?php echo $p->id; ?>"><?php echo $p->price; ?></td>
				<td id="max_pump_pressure_<?php echo $p->id; ?>"><?php echo $p->max_pump_pressure; ?></td>
				<td id="pump_set_pressure_<?php echo $p->id; ?>"><?php echo $p->pump_set_pressure; ?></td>
				<td id="water_temp_<?php echo $p->id; ?>"><?php echo $p->water_temprature; ?></td>
				<td id="size_<?php echo $p->id; ?>"><?php echo $p->product_size; ?></td>
				<td id="weight_<?php echo $p->id; ?>"><?php echo $p->product_weight; ?></td>
				
				
				
				<td id='category_<?php echo $p->id; ?>'>
				<span id="category_view_<?php echo $p->id; ?>">
				<?php echo $p->catlog_title; ?>
				</span>
				<select id="category_edit_<?php echo $p->id; ?>" style="display:none;" name="product_category_<?php echo $p->id; ?>">
				<?php foreach($category_catlog as $cc){ ?>
				<option value="<?php echo $cc->id ?>" <?php if(isset($p->product_category)){ if($p->product_category == $cc->id){ ?> selected="selected" <?php }} ?>><?php echo $cc->catlog_title; ?></option>
				<?php
				}
				?>
				</select>
				</td>
			<form enctype="multipart/form-data">
			
				<td id="image_<?php echo $p->id; ?>">
				<?php if(isset($p->product_image) && ($p->product_image != '')){
				?>
				<img src="{base_url}uploads/product_image/<?php echo $p->product_image; ?>" width="50" height="50" id="product_image_new_<?php echo $p->id; ?>" />
				<?php
				}
				?>
				<input type="file" name = "product_image_<?php echo $p->id; ?>" id="product_image_<?php echo $p->id; ?>" style="display:none; width:95px;"></td>
			</form>	
				<td id="specification_<?php echo $p->id; ?>">
				<span id="feature1_view_<?php echo $p->id; ?>">
				<?php foreach($feature1 as $f1){
				if($f1->id == $p->feature1){
				echo $f1->catlog_title; 
				}
				}
				?>
				</span>
				<select id="feature1_edit_<?php echo $p->id; ?>" style="display:none;" name="product_feature1_<?php echo $p->id; ?>">
				<option value="">--select--</option>
				<?php foreach($feature1 as $f1){ ?>
				<option value="<?php echo $f1->id ?>" <?php if(isset($p->feature1)){ if($p->feature1 == $f1->id){ ?> selected="selected" <?php }} ?>><?php echo $f1->catlog_title; ?></option>
				<?php
				}
				?>
				</select>
				</td>
				
				<td id="motor/engine_<?php echo $p->id; ?>">
				<span id="feature2_view_<?php echo $p->id; ?>">
				<?php foreach($feature2 as $f2){
				if($f2->id == $p->feature2){
				echo $f2->catlog_title; 
				}
				}
				?>
				</span>
				<select id="feature2_edit_<?php echo $p->id; ?>" style="display:none;" name="product_feature2_<?php echo $p->id; ?>">
				<option value="">--select--</option>
				<?php foreach($feature2 as $f2){ ?>
				<option value="<?php echo $f2->id ?>" <?php if(isset($p->feature2)){ if($p->feature2 == $f2->id){ ?> selected="selected" <?php }} ?>><?php echo $f2->catlog_title; ?></option>
				<?php
				}
				?>
				</select>
				</td>
				
				<td id="pump_<?php echo $p->id; ?>">
				<span id="feature3_view_<?php echo $p->id; ?>">
				<?php foreach($feature3 as $f3){
				if($f3->id == $p->feature3){
				echo $f3->catlog_title; 
				}
				}
				?>
				</span>
				<select id="feature3_edit_<?php echo $p->id; ?>" style="display:none;" name="product_feature3_<?php echo $p->id; ?>">
				<option value="">--select--</option>
				<?php foreach($feature3 as $f3){ ?>
				<option value="<?php echo $f3->id ?>" <?php if(isset($p->feature3)){ if($p->feature3 == $f3->id){ ?> selected="selected" <?php }} ?>><?php echo $f3->catlog_title; ?></option>
				<?php
				}
				?>
				</select>
				</td>
				
				<td id="boiler_<?php echo $p->id; ?>">
				<span id="feature4_view_<?php echo $p->id; ?>">
				<?php foreach($feature4 as $f4){
				if($f4->id == $p->feature4){
				echo $f4->catlog_title; 
				}
				}
				?>
				</span>
				<select id="feature4_edit_<?php echo $p->id; ?>" style="display:none;" name="product_feature4_<?php echo $p->id; ?>">
				<option value="">--select--</option>
				<?php foreach($feature4 as $f4){ ?>
				<option value="<?php echo $f4->id ?>" <?php if(isset($p->feature4)){ if($p->feature4 == $f4->id){ ?> selected="selected" <?php }} ?>><?php echo $f4->catlog_title; ?></option>
				<?php
				}
				?>
				</select>
				</td>
				
				<td id="frame_<?php echo $p->id; ?>">
				<span id="feature5_view_<?php echo $p->id; ?>">
				<?php foreach($feature5 as $f5){
				if($f5->id == $p->feature5){
				echo $f5->catlog_title; 
				}
				}
				?>
				</span>
				<select id="feature5_edit_<?php echo $p->id; ?>" style="display:none;" name="product_feature5_<?php echo $p->id; ?>">
				<option value="">--select--</option>
				<?php foreach($feature5 as $f5){ ?>
				<option value="<?php echo $f5->id ?>" <?php if(isset($p->feature5)){ if($p->feature5 == $f5->id){ ?> selected="selected" <?php }} ?>><?php echo $f5->catlog_title; ?></option>
				<?php
				}
				?>
				</select>
				</td>
				
				<td id="water_tank_<?php echo $p->id; ?>">
				<span id="feature6_view_<?php echo $p->id; ?>">
				<?php foreach($feature6 as $f6){
				if($f6->id == $p->feature6){
				echo $f6->catlog_title; 
				}
				}
				?>
				</span>
				<select id="feature6_edit_<?php echo $p->id; ?>" style="display:none;" name="product_feature6_<?php echo $p->id; ?>">
				<option value="">--select--</option>
				<?php foreach($feature6 as $f6){ ?>
				<option value="<?php echo $f6->id ?>" <?php if(isset($p->feature6)){ if($p->feature6 == $f6->id){ ?> selected="selected" <?php }} ?>><?php echo $f6->catlog_title; ?></option>
				<?php
				}
				?>
				</select>
				</td>
				
				<td id="by_pass_<?php echo $p->id; ?>">
				<span id="feature7_view_<?php echo $p->id; ?>">
				<?php foreach($feature7 as $f7){
				if($f7->id == $p->feature7){
				echo $f7->catlog_title; 
				}
				}
				?>
				</span>
				<select id="feature7_edit_<?php echo $p->id; ?>" style="display:none;" name="product_feature7_<?php echo $p->id; ?>">
				<option value="">--select--</option>
				<?php foreach($feature7 as $f7){ ?>
				<option value="<?php echo $f7->id ?>" <?php if(isset($p->feature7)){ if($p->feature7 == $f7->id){ ?> selected="selected" <?php }} ?>><?php echo $f7->catlog_title; ?></option>
				<?php
				}
				?>
				</select>
				</td>
				
				<td id="control_box_<?php echo $p->id; ?>">
				<span id="feature8_view_<?php echo $p->id; ?>">
				<?php foreach($feature8 as $f8){
				if($f8->id == $p->feature8){
				echo $f8->catlog_title; 
				}
				}
				?>
				</span>
				<select id="feature8_edit_<?php echo $p->id; ?>" style="display:none;" name="product_feature8_<?php echo $p->id; ?>">
				<option value="">--select--</option>
				<?php foreach($feature8 as $f8){ ?>
				<option value="<?php echo $f8->id ?>" <?php if(isset($p->feature8)){ if($p->feature8 == $f8->id){ ?> selected="selected" <?php }} ?>><?php echo $f8->catlog_title; ?></option>
				<?php
				}
				?>
				</select>
				</td>
				
				<td id="hose_gun_<?php echo $p->id; ?>">
				<span id="feature9_view_<?php echo $p->id; ?>">
				<?php foreach($feature9 as $f9){
				if($f9->id == $p->feature9){
				echo $f9->catlog_title; 
				}
				}
				?>
				</span>
				<select id="feature9_edit_<?php echo $p->id; ?>" style="display:none;" name="product_feature9_<?php echo $p->id; ?>">
				<option value="">--select--</option>
				<?php foreach($feature9 as $f9){ ?>
				<option value="<?php echo $f9->id ?>" <?php if(isset($p->feature9)){ if($p->feature9 == $f9->id){ ?> selected="selected" <?php }} ?>><?php echo $f9->catlog_title; ?></option>
				<?php
				}
				?>
				</select>
				</td>
				<td><input type="button" name="edit_row" value="Edit" onclick="edit_row('<?php echo $p->id ?>')" style="display:inline;" class="btn btn- btn-phone-block" id="edit_row_<?php echo $p->id; ?>">
				<input type="submit" name="edit_submit" value="Save" id="save_edited_<?php echo $p->id; ?>" style="display:none;" onclick="save_row('<?php echo $p->id; ?>')" class="btn btn-success">
				</td>
				
				
				</tr>
				<?php
				}
				?>
				</tbody>
			</table>
			
			</div>			
    <!--<table ng-table="tableParams" show-filter="flase" class="table table-bordered table-striped" id="products">
    
  <input type="submit" name="delete" value="Delete" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon></button>
    
    <thead>
        	<tr>
            	<th width="5%">&nbsp;<input type="checkbox" name="check_all" id="check_all" /></th>
            	<th width="5%">Name</th>
                <th width="5%">Category</th>
                <th width="20%">Max Pump Pressure</th>
                <th width="20%">Pump Set Pressure</th>
                <th width="20%">Water Temperature</th>
                <th width="10%">Size</th>
                <th width="10%">Weight</th>
                <th width="5%">Action</th>                
            </tr>
        </thead>
       <tbody>
        <tr ng-repeat="product in data track by $index">
        	<td title="'Name'" title-alt="'Name'">
               <input type="checkbox" name="delete_check[]" value="{{product.id}}" class="check" />
            </td>
            <td title="'Name'" title-alt="'Name'" sortable="'product_name'" filter="{ 'product_name': 'text' }">
                {{product.product_name}}
            </td>
            <td title="'Category'" sortable="'product_category'" filter="{ 'product_category': 'text' }">
                {{product.catlog_title}}
            </td>
            <td title="'Max Pump Pressure'" sortable="'max_pump_pressure'" filter="{ 'max_pump_pressure': 'text' }">
                {{product.max_pump_pressure}}
            </td>
            <td title="'Pump Set Pressure'" sortable="'pump_set_pressure'" filter="{ 'pump_set_pressure': 'text' }">
                {{product.pump_set_pressure}}
            </td>
            <td title="'Water Temperature'" sortable="'water_temprature'" filter="{ 'water_temprature': 'text' }">
                {{product.water_temprature}}
            </td>
            <td title="'Size'" sortable="'product_size'" filter="{ 'product_size': 'text' }">
                {{product.product_size}}
            </td>
            <td title="'Weight'" sortable="'product_weight'" filter="{ 'product_weight': 'text' }">
                {{product.product_weight}}
            </td>
             <td title="'Action'">
              	<a ng-click="edit_product(product.id)" data-toggle="modal"><button class="btn btn- btn-phone-block"><icon class="icon-pencil icon-white"></icon><span class="hidden-phone">Edit</span></button></a>&nbsp;
            </td>
        </tr>
        </tbody>
    </table>
    </form>-->
<script>
	var data = '{product}';
    var app = angular.module('main', ['ngTable']);
	function DemoCtrl($scope, $filter, NgTableParams) {
		
                data = JSON.parse(data);
                $scope.data = data;
				$scope.edit_product = function(product_id){
					window.location.href = '{base_url}product/add_product/'+product_id
				}
				$scope.product_delete = function(product_id){
					alert("are you sure want to delete this product?"+product_id);
				}
			   
	}

$('#check_all').click(function(e){
	if($(this).is(':checked')){
		$(".check").each(function(){
			this.checked = true;
			});	
	}
	if(!$(this).is(':checked')){
		$(".check").each(function(){
			this.checked = false;
			});	
	}
	
});
</script>

</div> 
        </div>
    </section>
    
  </aside>
</div>


{footer}