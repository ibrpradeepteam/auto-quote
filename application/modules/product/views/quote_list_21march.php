{header}
<script src="{base_url}ng-table/ng-table.js"></script>
<script>
$(document).ready(function(e) {
    $("#products").dataTable({
		"sPaginationType": "full_numbers",
       "aoColumnDefs" : [ {
       'bSortable' : false,
       'aTargets' : [ -1,0 ],
	  
      } ],
		});	
});
</script>
<style>
.wrapper{
max-height:1000px !important;	
min-height:800px !important;	
}
</style>

<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    {page_header}
     <!-- Main content -->
     <form action="{base_url}product/quote/quots_delete" method="post" onsubmit="return checkBox('product')">
    <section class="content" ng-app="main">  
    <div class="row">
        	<div class="col-md-3">
            	<a class="btn btn-primary add-quote" onclick="javascript:void(0)" href="">Add New</a>
                 <input type="submit" name="delete" value="Delete" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon></button>
            </div>
        </div>    	
        <div class="row">
        	<div ng-controller="QuoteCtrl" class="demo-reponsiveTable clearfix">        
    <table ng-table="tableParams" show-filter="flase" class="table table-bordered table-striped" id="products">
    
    <thead>
        	<tr>
            	<th width="5%">&nbsp;<input type="checkbox" name="check_all" id="check_all" /></th>
            	<th width="5%">Customer Name</th>
                <th width="5%">Company Name</th>
                <th width="20%">Rated Pressure</th>
                <th width="20%">Setup Pressure</th>
                <th width="20%">Product Name</th>
                <th width="5%">Action</th>           
            </tr>
        </thead>
       <tbody>
        <tr ng-repeat="product in data track by $index">
        	<td title="'Name'" title-alt="'Name'">
               <input type="checkbox" name="delete_check[]" value="{{product.id}}" class="check" />
            </td>
            <td title="'Name'" title-alt="'Name'" sortable="'product_name'" filter="{ 'product_name': 'text' }">
                {{product.customer_l_name}}&nbsp;{{product.customer_f_name}}
            </td>
            <td title="'Category'" sortable="'product_category'" filter="{ 'product_category': 'text' }">
                {{product.company_name}}
            </td>
            <td title="'Max Pump Pressure'" sortable="'max_pump_pressure'" filter="{ 'max_pump_pressure': 'text' }">
                {{product.rated_pressure}}
            </td>
            <td title="'Pump Set Pressure'" sortable="'pump_set_pressure'" filter="{ 'pump_set_pressure': 'text' }">
                {{product.setup_pressure}}
            </td>
            <td title="'Water Temperature'" sortable="'water_temprature'" filter="{ 'water_temprature': 'text' }">
                {{product.product_name}}
            </td>
          <td title="'Action'">
              	<a ng-click="view_quote(product.id)" data-toggle="modal"><button class="btn btn- btn-phone-block"><icon class="icon-pencil icon-white"></icon><span class="hidden-phone">View Quote</span></button></a>&nbsp;
            </td>
        </tr>
        </tbody>
    </table>
    </form>
<script>
	var data = '{product}';
    var app = angular.module('main', ['ngTable']);
	function QuoteCtrl($scope, $filter, NgTableParams) {
		
                data = JSON.parse(data);
                $scope.data = data;
				
				$scope.view_quote = function(quote_id){
					window.location.href = '{base_url}product/quote/add_quote/'+quote_id
				}
				$scope.product_delete = function(product_id){
					alert("are you sure want to delete this product?"+product_id);
				}
			   
	}

$('#check_all').click(function(e){
	if($(this).is(':checked')){
		$(".check").each(function(){
			this.checked = true;
			});	
	}
	if(!$(this).is(':checked')){
		$(".check").each(function(){
			this.checked = false;
			});	
	}
	
});
</script>

</div> 
        </div>
    </section>
    
  </aside>
</div>
 
{quote}
{footer}

<script>
var quote_id = '{quote}';
if(quote_id != '')
{
$(document).ready(function () {
 width = 900;
 height = 400;
 left  = ($(window).width()/2)-(width/2),
 top   = ($(window).height()/2)-(height/2);	
 window.open ('{base_url}product/quote/view_html/'+quote_id, "popup", "width=900, height=600, top="+top+", left="+left+", 'dialog=yes'");
});
}
</script>
<?php
$this->session->unset_userdata('quote');
?>