<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{page_title}</title>
{styles}
{scripts}


<!--{angular}-->
<script src="{base_url}ng-table/ng-table.js"></script>
<script src="<?php echo base_url('js/ckeditor');?>/ckeditor.js"></script>
<script src="<?php echo base_url('js');?>/adapters/jquery.js"></script>


<!--ckeditor api -->
<!--<link href="<?php //echo base_url('js/ckfinder');?>/sample.css" rel="stylesheet" type="text/css" />-->
<script src="<?php echo base_url('js/ckfinder');?>/ckeditor.js"></script>
<script src="<?php echo base_url('js/ckfinder');?>/ckfinder.js"></script>

<script>
//CKEDITOR.disableAutoInline = true;


function view_html(){
	$( '#edit_full_page').ckeditor();
	 var $div=$('#main_div'), isEditable=$div.is('.editable');
	 if(!isEditable)
	 {
		 alert('You can edit the quote');
	 }
	 else
	 {
		alert("You can't edit the quote"); 
	  }
     $('#editable_html').prop('contenteditable',!isEditable).toggleClass('editable')
	 $('#editable_html').prop('imageeditable',!isEditable).toggleClass('editable')	
	}
	
function generate_pdf(){	
		//var full_page = $('#editor1').val();
		$('.pdf_header').css('display','block');
		$('.first_page_header').css('display','none');
		var data = CKEDITOR.instances.editor1.getData();
		
		//alert(CKEDITOR.instances.editor1.getData());
		var first_page = $(".first_page").html();
		//var data = $('#main_div').html();
		var pdf_head = $('.pdf_header').html();
		var pdf_foot = $('.pdf_footer').html();
		
		var first_head = $('.first_page_header').html();
		var first_foot = $('.first_page_footer').html();
		
		var customer_name = $('#customer_name').val();
		
		var customer_email = $('#customer_email').val();
		var quote_id = $('#quote_id').val();
		
		var customer_id = $('#customer_id').val();
		
		if(customer_email == '')
		{
		var customer_name = $('#customer_name1').val();
		
		var customer_email = $('#customer_email1').val();
		var quote_id = $('#quote_id1').val();
		var customer_id = $('#customer_id1').val();
		
		var pdf_head = $('.pdf_header1').html();
		var pdf_foot = $('.pdf_footer1').html();
		var first_head = $('.first_page_header1').html();
		
		}
		
		
		$.ajax({
		type:'post',
		url:'{base_url}product/quote/file_write',
		data:{data:data,pdf_head:pdf_head,pdf_foot:pdf_foot,first_page:first_page,first_head:first_head,first_foot:first_foot,quote_id:quote_id},
		success:function(json){
			//alert(json);
			window.location.href='{base_url}product/quote/GeneratePDF/?name='+customer_name+'&email='+customer_email+'&quote_id='+quote_id;	
		}	
		});
		//alert(customer_name);
		//open_popup(customer_name,customer_email,quote_id);
		
		
		//window.location.href='{base_url}product/quote/GeneratePDF/?name='+customer_name+'&email='+customer_email+'&quote_id='+quote_id;
		
		
}

function open_popup(customer_name,customer_email,quote_id){
	jQuery.fancybox({
        type: 'iframe',
        href: '{base_url}product/quote/send_mail_to_manager/?name='+customer_name+'&email='+customer_email+'&quote_id='+quote_id,
        autoSize: false,
        closeBtn: true,
        width: 900,
        height: 500,
        closeClick: true,
        enableEscapeButton: true,
        beforeLoad: function () {},
    });		
}

</script>



</head>

<body>
<textarea id="editor1" name="editor1" rows="100" cols="80" style="width:100%; height:800px"><?php echo isset($edited) ? $edited : ''; ?></textarea>
                   
                   
                   <script type="text/javascript">
				   
$( document ).ready( function() {	
 var replace = '{replace}';
if(replace == 1){


//$('#main_div').html('{edited_id}');
} 			   


// This is a check for the CKEditor class. If not defined, the paths must be checked.
if ( typeof CKEDITOR == 'undefined' )
{
	document.write(
		'<strong><span style="color: #ff0000">Error</span>: CKEditor not found</strong>.' +
		'This sample assumes that CKEditor (not included with CKFinder) is installed in' +
		'the "/ckeditor/" path. If you have it installed in a different place, just edit' +
		'this file, changing the wrong paths in the &lt;head&gt; (line 5) and the "BasePath"' +
		'value (line 32).' ) ;
}
else
{
	
	var config = {};
	config.height = '400px';
	config.resize_enabled = true;
	config.extraAllowedContent = ['div(col-md-4,col-md-8,col-md-12,col-md-2,container-fluid,row,pull-left,pull-right,title,title1,titl2,product_name,product_description,feature_desc,heading-feature,feature_img,heading_color,editable_html,main_div,top,pdf_header,pdf_footer,pdf_header1,pdf_footer1,first_page,first_page_header,first_page_header1,first_page_footer)','style','<br>'];
	config.allowedContent = true;
	
	config.contentsCss = ['{base_url}css/bootstrap.min.css','{base_url}css/auto_quote.css'];
	config['filebrowserBrowseUrl']='{base_url}ckeditor_images/quote/ckfinder.html';
	config['filebrowserImageBrowseUrl']='{base_url}ckeditor_images/quote/ckfinder.html?type=Images';
	config['filebrowserFlashBrowseUrl']='{base_url}ckeditor_images/quote/ckfinder.html?type=Flash';
	config['filebrowserUploadUrl'] = '{base_url}ckeditor_images/quote/core/connector/php/connector.php?command=QuickUpload&type=Files';
	config['filebrowserImageUploadUrl'] = '{base_url}ckeditor_images/quote/core/connector/php/connector.php?command=QuickUpload&type=Images';
	config['filebrowserFlashUploadUrl'] = '{base_url}ckeditor_images/quote/core/connector/php/connector.php?command=QuickUpload&type=Flash';
	
	var editor = CKEDITOR.replace( 'editor1',config);	
	
	//editor.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );
	var main_div = $('#main_div').html();
	if(replace != 1)
	{
	$('#editor1').val(main_div);
	}
	// Just call CKFinder.setupCKEditor and pass the CKEditor instance as the first argument.
	// The second parameter (optional), is the path for the CKFinder installation (default = "/ckfinder/").
	CKFinder.setupCKEditor( editor, '../' ) ;

	
	$('#main_div').css('display','none');
	

}
});
		</script>
						<script>
						var quots_data = '{quote_data}';
						var companies_data = '{company_data}';
						
						var product_features = '{product_features}';
						var product_datas = '{product_data}';
						
						var addons_data = '{addons_data}';
						
					
 
				
</script>
                        
                       <!-- <textarea id="edit_full_page">-->
               
  		   <input type="hidden" name="customer_id" value="{customer_id}" id="customer_id1" />
                  
                  <input type="hidden" name="quote_id" value="{edited_id}" id="quote_id1" />
                  
                   <input type="hidden" name="customer_name" value="{customer_f_name}{customer_l_name}" id="customer_name1" />
                   
                   <input type="hidden" name="customer_email" value="{customer_email}" id="customer_email1" /> 
                   
                   <!-- header of quote pdf (contains company logo) --->
                  <div class="row pdf_header1" style="display:none">
                  <table style="background:url('{base_url}images/new_header.jpg'); background-repeat:no-repeat; width:100%; margin-right:0px; margin-left: -26px">
                  
                            <tr>
                            <td width="40%">  </td>
                                 <td width="40%" style="text-align:right; height:180px;"><img src="{base_url}uploads/docs/{company_logo}" width="250px"  height="80px" style="margin-top:-70px;"  /></td>
                           </tr>
                   </table>
                  <!-- <hr style="border:1px #ccc solid;"/> -->
                   </div>
                   <!-- end of header of quote pdf (contains company logo) --->
                   
                    <!-- footer of quote pdf (contains company logo) --->
                  <div class="row pdf_footer1" style="display:none">
                  <!--<table style="background:url('{base_url}images/new_footer.jpg'); background-repeat:no-repeat;">
                  
                            <tr>
                            <td width="60%">  </td>
                                 <td width="40%" style="text-align:right; height:180px;"><img src="{base_url}uploads/docs/{company_logo}" width="150px"  height="70px" /></td>
                           </tr>
                   </table>-->
                <img  src="{base_url}images/new_footer.jpg" width="100%" style="margin-left: 10px; margin-right:-40px;"/> 
               <table style="height:100px; width:100%; background-color:#8BCFF2; margin-top:-50px; margin-bottom:20px; "><tr><td style="text-align:center; color:#fff"> <p style="text-align:center; bachground-color:#8BCFF2;">{footer_text}</p></td></tr></table>
                <!-- <div style="width:100%; height:20px; background-color:#8BCFF2;">
<h3 style=" text-align:center;">Company Description</h3> 
</div> -->
                   </div>
                   <!-- end of foote of quote pdf (contains company logo) --->
                   <div class="row first_page_header1" style="display:none">

<table style="background:url('{base_url}images/new_header.jpg'); background-repeat:no-repeat; width:90%; height:100%; margin-left: -26px">
<tr>

<td style="height:150px;"><div style="margin-left:30px; background:url(''); width:100%; height:150px; background-repeat:no-repeat;  background-position: center; ">

<img src="{base_url}uploads/docs/{company_logo}" width="300px" height="100px" style="z-index:-11111; margin-left:50px;" />
 
</div></td>
<td style="text-align:right; width:100px; height:auto;">
{company_address}
 </td>
</tr>
</table>
</div>

                   
                   
                                        
 
 <div id="main_div" class="main_div">
<style>
body{
overflow-x:hidden;
}
</style>
<section class="content" ng-app="editable" id="editable_html" class="editable_html" style="margin-left:20px;">
  

                    <div class="row top" ng-controller="EditableCtrl" ng-init="init(quots_data,companies_data,product_features,product_datas,addons_data)">
                    
                  <input type="hidden" name="customer_id" value="{{quote_detail.customer_id}}" id="customer_id" />
                  
                  <input type="hidden" name="quote_id" value="{{quote_detail.id}}" id="quote_id" />
                  
                   <input type="hidden" name="customer_name" value="{{quote_detail.customer_f_name}}{{quote_detail.customer_l_name}}" id="customer_name" />
                   
                   <input type="hidden" name="customer_email" value="{{quote_detail.email_add}}" id="customer_email" /> 
                   
                   
                   <!-- header of quote pdf (contains company logo) --->
                  <div class="row pdf_header" style="display:none">
                  <table style="background:url('{base_url}images/new_header.jpg'); background-repeat:no-repeat; width:100%; margin-right:0px; margin-left: -26px">
                  
                            <tr>
                            <td width="40%">  </td>
                                 <td width="40%" style="text-align:right; height:180px;"><div ng-if="company_data.logo"> <img ng-src="{base_url}uploads/docs/{{company_data.logo}}" width="250px"  height="80px" style="margin-top:-70px; margin-left:40px;"  /></div></td>
                           </tr>
                   </table>
                  <!-- <hr style="border:1px #ccc solid;"/> -->
                   </div>
                   <!-- end of header of quote pdf (contains company logo) --->
                   
                    <!-- footer of quote pdf (contains company logo) --->
                  <div class="row pdf_footer" style="display:none">
                  <!--<table style="background:url('{base_url}images/new_footer.jpg'); background-repeat:no-repeat;">
                  
                            <tr>
                            <td width="40%">  </td>
                                 <td width="40%" style="text-align:right; height:180px;"><div ng-if="company_data.logo"><img ng-src="{base_url}uploads/docs/{{company_data.logo}}" width="150px"  height="70px" /></div></td>
                           </tr>
                   </table>-->
                <img  src="{base_url}images/new_footer.jpg" width="100%" style="margin-right: -70px; margin-left:10px;"/> 
               <table style="height:100px; width:100%; background-color:#8BCFF2; margin-top:-50px; margin-bottom:20px;"><tr><td style="text-align:center; color:#fff"> <p style="text-align:center; bachground-color:#8BCFF2;">{{company_data.footer_text}}</p></td></tr></table>
                <!-- <div style="width:100%; height:20px; background-color:#8BCFF2;">
<h3 style=" text-align:center;">Company Description</h3> 
</div> -->
                   </div>
                   <!-- end of foote of quote pdf (contains company logo) --->
                   
                
                    <!-- footer of first page of quote pdf (contains company logo) --->
                  <div class="row first_page_footer" style="display:none">
                  <!--<table style="background:url('{base_url}images/new_footer.jpg'); background-repeat:no-repeat;">
                  
                            <tr>
                            <td width="40%">  </td>
                                 <td width="40%" style="text-align:right; height:180px;"><div ng-if="company_data.logo"><img ng-src="{base_url}uploads/docs/{{company_data.logo}}" width="150px"  height="70px" /></div></td>
                           </tr>
                   </table>-->
                <img  src="{base_url}images/new_footer.jpg" width="100%" style="margin-right: -40px;"/> 
               
                <!-- <div style="width:100%; height:20px; background-color:#8BCFF2;">
<h3 style=" text-align:center;">Company Description</h3> 
</div> -->
                   </div>
                   <!-- end of foote of first page of quote pdf (contains company logo) --->
                   
                   
               
            
<!-- right side of pdf contains company details --->
<div class="first_page">   
                 
             <DIV id="page_1">
<div class="row first_page_header">

<table style="background:url('{base_url}images/new_header.jpg'); background-repeat:no-repeat; width:95%; height:100%; margin-left: -26px">
<tr>

<td style="height:150px;"><div style="margin-left:30px; background:url(''); width:100%; height:150px; background-repeat:no-repeat;  background-position: center; ">

<img ng-src="{base_url}uploads/docs/{{company_data.logo}}" width="300px" height="100px" style="z-index:-11111;margin-left:35px;" />
 
</div></td>
<td style="text-align:right; width:100px; height:auto;">
 <?php
 if(isset($test_company) && !empty($test_company))
 {
 echo $test_company['address'];
 }
 ?>
 </td>
</tr>
</table>
</div>

<br/>
             
<table width="80%" style="margin-left:30px; margin-top:70px;">
<tr>
<td width="50%">


<P class="p2 ft2" style="display:block; padding-top:0px; margin-top:0px;"><SPAN class="ft1">Date: </SPAN>
{{quote_detail.created_on}}</P>

<P class="p3 ft2" style="display:block;"><SPAN class="ft1">Quote No</SPAN>:
<NOBR>{{quote_detail.created_on_year}}{{quote_detail.created_on_day}}{{quote_detail.created_on_month}}-<?php echo substr($this->session->userdata('admin_name'), 0,1) ;?><?php echo substr($this->session->userdata('admin_lname'), 0,1) ; ?>{{quote_detail.id}}</NOBR></P>
<br>
<P class="p3 ft3" style="display:block;">Attn: &nbsp;
{{quote_detail.customer_f_name}}&nbsp;{{quote_detail.customer_l_name}} </p>

<table>
<tr><td style="padding-left:40px !important;">
<P class="ft3" style="display:block; padding-left:60px !important;"><?php echo isset($customer_company['company_name']) ? $customer_company['company_name'] : ''; ?></P></td></tr>
<tr><td style="padding-left:40px !important;">
<P class="ft2" style="display:block; padding-left:60px !important;">{{quote_detail.street_add}}</P></td></tr>
<tr><td style="padding-left:40px !important;">
<P class="ft2" style="display:block; padding-left:60px !important;"> Ph: {{quote_detail.phone_number}}</P></td></tr>
<tr><td style="padding-left:40px !important;">
<P class="ft2" style="display:block; padding-left:60px !important;">Email:

 {{quote_detail.email_add}}</P></td></tr></table>
 </td>
 <td width="50%" style="text-align:right;">
 </td>
 </tr>
</table>



<!-- right side of pdf contains company details --->


<table cellpadding="20" style="" >
<tr>
<td style="font:13px 'Arial' !important; padding-left:35px;">
 {{company_data.cover_text_1}}</td></tr>
</table>

<table cellspacing="0">
<?php
if(isset($quota_data['selling_point1']) && $quota_data['selling_point1'] != ''){
?>
<tr><td width="10%" style="text-align:right; color:red; font:bold; padding-left:77px;">√</td><td width="90%" style="padding-left:10px; font:13px 'Arial' !important ;"><?php echo isset($quota_data['selling_point1']) ? $quota_data['selling_point1'] : ''; ?></td></tr>
<?php
}
if(isset($quota_data['selling_point2']) && $quota_data['selling_point2'] != ''){
?>
<tr><td width="10%" style="text-align:right; color:red; padding-left:77px;">√ </td><td width="90%" style="padding-left:10px; font:13px 'Arial' !important;"><?php echo isset($quota_data['selling_point2']) ? $quota_data['selling_point2'] : ''; ?></td></tr>
<?php } if(isset($quota_data['selling_point3']) && $quota_data['selling_point3'] != ''){ ?>
<tr><td width="10%" style="text-align:right; color:red; padding-left:77px;">√ </td><td width="90%" style="padding-left:10px; font:13px 'Arial' !important;"><?php echo isset($quota_data['selling_point3']) ? $quota_data['selling_point3'] : ''; ?></td></tr>
<?php } ?>

<tr><td colspan=2 style="padding-left:35px; font:13px 'Arial' !important;">In addition to your requirements, ThoroughClean also offers</td></tr>

<tr><td width="10%" style="text-align:right; color:red; padding-left:77px;">√</td><td width="90%" style="padding-left:10px; font:13px 'Arial' !important;">Unrivalled whole of life value</td></tr>
<tr><td width="10%" style="text-align:right; color:red; padding-left:77px;">√</td><td width="90%" style="padding-left:10px; font:13px 'Arial' !important;">Personalised pre and <NOBR>post-sale</NOBR> Service</td></tr>
<tr><td width="10%" style="text-align:right; color:red; padding-left:77px;">√</td><td width="90%" style="padding-left:10px; font:13px 'Arial' !important;">Service technicians available at your disposal</td></tr>
<tr><td width="10%" style="text-align:right; color:red; padding-left:77px;">√</td><td width="90%" style="padding-left:10px; font:13px 'Arial' !important;">Maintenance spares, service and technical advice</td></tr>
<tr><td width="10%" style="text-align:right; color:red; padding-left:77px;">√</td><td width="90%" style="padding-left:10px; font:13px 'Arial' !important;">Operator training available</td></tr>

</table>

<table cellpadding="1" style="margin-top:20px;">
<tr><td style="padding-left:35px; font:13px 'Arial' !important;">
<!--<SPAN class="ft6">{{company_data.name}} </SPAN> --><?php echo isset($company_data_show['cover_text_2']) ? nl2br($company_data_show['cover_text_2']) : ''; ?></td>
</tr>
<!--<tr><td style="padding-left:35px; font:13px 'Arial' !important;">
We are pleased to submit the following proposal for your consideration.</td>
</tr>
<tr><td style="padding-left:35px; font:13px 'Arial' !important;">
I trust we have met your requirements, however please do not hesitate to call me if you have any questions.</td>
</tr>-->
</table>


<table cellpadding="5" align="left" style="margin-top:20px;">
<tr>
<td style="padding-left:35px; font:13px 'Arial' !important;">
Kind Regards</td> </tr>
<tr><td style="line-height:0px; padding-left:35px;">
<P class="ft9">{{quote_detail.name}} &nbsp {{quote_detail.last_name}}</P></td></tr>
<tr><td style="line-height:0px; padding-left:35px; font:13px 'Arial' !important;">
PH: {{quote_detail.phone_no}} / MOB: </td></tr>
<tr><td style="line-height:0px; padding-left:35px; font:13px 'Arial' !important;">
Email: {{quote_detail.email}}</td></tr>
</table>


</DIV>
</div>

<span style="display:none;">devide</span>


<div id="other_pages">
<DIV id="page_2">

<DIV class="dclr"></DIV>
<DIV id="id_1">
<?php //echo "<pre>"; print_r($product_data); die;?>
<!-- products detail -->
<div ng-repeat="product_data in product_data">

<table style="padding-top:\0px;">
<tr>
<td width="65%">
<table cellpadding=0 cellspacing=0>
<tr>
<td><P class="p20 ft10"><NOBR>{{product_data.product_name}} <br></NOBR></P></td>
</tr>
<TR>
	<TD class="td0" style="width:150px;"><P class="p21 ft12" style="display:inline;">INVESTMENT:&nbsp;&nbsp; <SPAN class="ft11">{{product_data.product_price}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </SPAN>plus GST</P></TD>
	
</TR>
<tr><td><P class="p22 ft1"><SPAN class="ft14">Product:&nbsp;&nbsp; </SPAN><NOBR>{{product_data.product_name}}</NOBR> Rental Spec:</P></td></tr>
<tr><td style="padding-left:60px;"><P class="ft1">•&nbsp;&nbsp;<SPAN class="ft16">13 HP Petrol engine</SPAN></P></td></tr>
<tr><td style="padding-left:60px;"><P class="ft1">•&nbsp;&nbsp;<SPAN class="ft16">Pump rated to {{product_data.rated_pressure}}</SPAN></P></td></tr>
<tr><td style="padding-left:60px;"><P class="ft1">•&nbsp;&nbsp;<SPAN class="ft16">Pump set to {{product_data.setup_pressure}}</SPAN></P></td></tr>
<tr><td style="padding-left:60px;"><P class="ft1">•&nbsp;&nbsp;<SPAN class="ft16">Water Temprature-{{product_data.water_temprature}}</SPAN></P></td></tr>
<tr><td style="padding-left:60px;"><P class="ft1">•&nbsp;&nbsp;<SPAN class="ft16">Size: {{product_data.size}}</SPAN></P></td></tr>
<tr><td style="padding-left:60px;"><P class="ft1">•&nbsp;&nbsp;<SPAN class="ft16">Weight: {{product_data.weight}}</SPAN></P></td></tr>
<?php //echo"<pre>"; print_r($uncoded_product_data); die;
if(isset($uncoded_product_data[0]['installation_cost']) 
   && $uncoded_product_data[0]['installation_cost'] != ''){
?>
<tr><td style="padding-left:60px;"><P class="ft1">•&nbsp;&nbsp;<SPAN class="ft16">Installation Cost: {{product_data.installation_cost}}</SPAN></P></td></tr>
<?php } ?>
<?php //echo"<pre>hi"; print_r($uncoded_product_data[0]['installation_desc']); die;
if(    isset($uncoded_product_data[0]['installation_desc']) 
    && trim($uncoded_product_data[0]['installation_desc'], ' ') != ''){
?>
<tr><td style="padding-left:60px;"><P class="ft1">•&nbsp;&nbsp;<SPAN class="ft16">Freight: {{product_data.installation_desc}}</SPAN></P></td></tr>
<?php } ?>
</table>
</td>
<td width="30%" style="padding-top:50px;">
<div ng-if="product_data.quote_product_image">
<img ng-src="{base_url}uploads/product_image/{{product_data.quote_product_image}}" style="width:250px; height:200px; margin-right:20px" />
 </div>
 <div ng-if="product_data.quote_product_image == ''" class="product_main_image">
<img ng-src="{base_url}uploads/product_image/{{product_data.product_image}}" style="width:250px; height:200px; margin-right:20px" />
 </div>
</td>
</tr>
</table>

<br>
</div>
<div ng-if="product_data[1]" >
<pagebreak />
</div>

<!-- end product detail -->
<div ng-if="product_deta[1]">
</div>
<table cellpadding="1" cellspacing="1" style="margin-top:50px;">
<?php //echo"<pre>hi"; print_r($uncoded_product_data[0]); die;
if(    isset($uncoded_product_data[0]['installation_cost']) 
    && ($uncoded_product_data[0]['installation_cost']) != ''){
?>
<tr><td><P class="p25 ft17">Installation Cost</P></td></tr>
<tr><td colspan="2" style="padding-left:40px; font:13px 'Arial' !important;"><?php echo $uncoded_product_data[0]['installation_cost'] ?></td></tr>
<?php } ?>

<tr><td><P class="p25 ft17">Freight</P></td></tr>
<?php //echo"<pre>hi"; print_r($uncoded_product_data[0]); die; 

if(    isset($uncoded_product_data[0]['installation_desc']) 
    && trim($uncoded_product_data[0]['installation_desc'], ' ') == ''){
?>
<tr><td colspan="2" style="padding-left:40px; font:13px 'Arial' !important;"><span style="color:red;">√ &nbsp;</span>To be confirmed <NOBR>Ex-Bundamba</NOBR> Qld</td></tr>
<?php } else{ ?>
	<tr><td colspan="2" style="padding-left:40px; font:13px 'Arial' !important;"><?php echo $uncoded_product_data[0]['installation_desc'] ?></td></tr>	
<?php } ?> 
<tr><td><P class="p25 ft17">Availability</P></td></tr>
<tr><td colspan="2" style="padding-left:40px; font:13px 'Arial' !important;"><span style="color:red;">√ &nbsp;</span><NOBR>Ex-stock</NOBR></td></tr>

<tr>
<td><P class="ft17" style="margin:0px; padding:0px;">Proposed Delivery Date:</P> </td>
<td style="text-align:left; padding-top:8px;">TBC</td>
</tr>
<tr>
<td><P class="ft17"  style="margin:0px; padding:0px;">Quote Validity:</P> </td>
<td style="text-align:left; padding-top:10px;">30 days</td>
</tr>
<tr>
<td ><SPAN class="ft17" style="margin:0px; padding:0px;">Payment Terms:  </SPAN></td>
<td style="text-align:left; padding-top:10px;">Payment required prior to shipping</td>
</tr>

<tr>
<td><SPAN class="ft17" style="margin:0px; padding:0px;">Warranties:</SPAN></td>
<td style="text-align:left; padding-top:10px;"> <NOBR>(Ex-Factory /</NOBR> Reseller Premises)</td>
</tr>

<tr>
<td style="text-align:right; color:red;">√</td>
<td style="text-align:left;"><SPAN style="padding:5px;">1 year ThoroughClean Manufacturer’s Warranty on build</SPAN></td>
</tr>
<tr>
<td style="text-align:right; color:red;">√</td>
<td style="text-align:left;"><SPAN style="padding:5px;">5 year ThoroughClean Manufacturer’s Structural Warranty on Galvanized frames & Reel</SPAN></td>
</tr>
<tr>
<td style="text-align:right; color:red;">√</td>
<td style="text-align:left;"><NOBR><SPAN style="padding:5px;">2-years</SPAN></NOBR> Manufacturer’s Warranty on Electric Motors</td>
</tr>
<tr>
<td style="text-align:right; color:red;">√</td>
<td style="text-align:left;"><NOBR><SPAN style="padding:5px;">3-years</SPAN></NOBR> or 2000 hours Manufacturer’s Warranty on all Petrol and Diesel Engines</td>
</tr>
<tr>
<td style="text-align:right; color:red;">√</td>
<td style="text-align:left;"><SPAN style="padding:5px;">12 months Warranty on Pressure Pump (Maintenance is not warranty. Excludes service and consumables required at specified maintenance intervals.)</SPAN></td></tr>
</table>


</DIV>

</DIV>
 <pagebreak />

<!-- product features -->


<!--<div ng-repeat="product_features in product_feature">
<div ng-repeat="product_featur in product_features">
<table border="0" cellspacing="5" cellpadding="10" style="margin-bottom:30px;">
<tr><td colspan="2" style="padding-left:250px; margin-bottom:-20px;"> 
<P class="ft24">{{product_featur.catlog_type}} <span id="model_number_{{$index}}">-{{product_featur.product_name}} </span></P>
</td></tr>
<tr><td colspan="2" style="margin:0px; padding:0px;"><hr style="border: 1px #ccc solid; margin:0px;"/></td></tr>
<tr>
<td padding="10" width="30%" style="padding-top:10px;">
<span ng-repeat="img in product_featur.title_image track by $index">
<img ng-src="{base_url}uploads/title_image/{{img}}" height="200px;" width="150px;" />
</span>
</td>
<td width="65%" style="padding-right:20px; padding-top:0px; padding-left:30px; font:15px;">
<P class="p36 ft24">{{product_featur.catlog_title}}</p>
<<P class="p28 ft25">Features & Benefits:</P>
<script>
var test = '{{product_featur.catlog_description|nl2br}}';
</script>
<P class="p37" style="padding-top:0px; font:15px !important;" id="feature_{{$index}}">{{product_featur.catlog_description|nl2br}}</P>
<span ng-bind-html-unsafe="$eval(product_featur.catlog_description)"></span>
</td>

</tr>
</table>
</div>
</div>-->

<?php
for($i = 0; $i<count($test_features); $i++)
{
//echo $test_features[$i];
for($j=0; $j<count($test_features[$i]); $j++)
{

$images = array();
$images = explode(',',$test_features[$i][$j]['title_image']);
?>
<table border="0" cellspacing="5" cellpadding="10" style="margin-bottom:60px;" width="100%">
<tr><td colspan="2" style="padding-left:250px; margin-bottom:-20px;"> 
<P class="ft24"><?php echo $test_features[$i][$j]['catlog_type']; ?><?php if(count($test_features)>1) { echo $test_features[$i]['product_name']; } ?></P>
</td></tr>
<tr><td colspan="2" style="margin:0px; padding:0px;"><hr style="border: 1px #ccc solid; margin:0px;"/></td></tr>
<tr>
<td padding="10" width="30%" style="padding-top:10px;">
<?php for($x=0; $x<count($images); $x++) { ?>
<img src="{base_url}uploads/title_image/<?php echo $images[$x]; ?>" style="display:block; max-width:150px; max-height:150px;" />
<?php } ?>
</td>
<td width="65%" style="padding-right:20px; padding-top:20px; padding-left:30px; font:15px;">

<P class="p28 ft25">Features & Benefits:</P>
<script>
var test = '{{product_featur.catlog_description|nl2br}}';
</script>
<P class="p37" style="padding-top:0px; font:15px !important;" id="feature_{{$index}}"><?php echo $test_features[$i][$j]['catlog_description']; ?></P>
</td>
</tr>
</table>
<?php
if($j==1 || $j==3 ||$j==5 || $j==7)
{
echo '<pagebreak />';
}
}}
?>

<!-- features ended -->



<!-- Optinal Extras -->


 <!--<div ng-if="addons[0].addon_keyword">-->
 <?php
 if(isset($test_addon) && !empty($test_addon))
 {
 ?>

 <h2 class="box-title" id="heading_color">Optional Extras</h2> 
 <div ng-repeat="addons in addons_data">
<table border="0" cellspacing="30" cellpadding="10">
 <span ng-if="quote_detail.optional_extra1 != ''">

<tr>
<td padding="10" width="30%">
<span ng-if="quote_detail.optional_extra1_img != addons.title_image">
<img ng-src="{base_url}uploads/quote_Optional_Extra/{{quote_detail.optional_extra1_img}}" height="100px;" width="150px;" />
</span>
<span ng-if="quote_detail.optional_extra1_img == addons.title_image">
<img ng-src="{base_url}uploads/title_image/{{addons.title_image}}" height="100px;" width="150px;" />
</span>
</td>
<td padding="10" width="70%">
<P class="p36 ft24">{{addons.addon_keyword}} - {{addons.catlog_title}}</P>
<hr style="border: 1px #ccc solid;"/>
<P class="p36 ft24">{{addons.addon_price}}</p>
<br/>
<P class="p37 ft2">{{addons.catlog_description}}</P>
</td>

</tr>
</span>
<span ng-if="quote_detail.optional_extra2 == ''">

<tr>
<td padding="10" width="30%">
<span ng-if="quote_detail.optional_extra2_img != addons.title_image">
<img ng-src="{base_url}uploads/quote_Optional_Extra/{{quote_detail.optional_extra2_img}}" height="100px;" width="150px;" />
</span>
<span ng-if="quote_detail.optional_extra2_img == addons.title_image">
<img ng-src="{base_url}uploads/title_image/{{addons.title_image}}" height="100px;" width="150px;" />
</span>
</td>
<td padding="10" width="70%">
<P class="p36 ft24">{{addons.addon_keyword}} - {{addons.catlog_title}}</P>
<hr style="border: 1px #ccc solid;"/>
<P class="p36 ft24">{{addons.addon_price}}</p>
<br/>
<P class="p37 ft2">{{addons.catlog_description}}</P>
</td>

</tr>
</span>
<span ng-if="quote_detail.optional_extra3 != ''">

<tr>
<td padding="10" width="30%">
<span ng-if="quote_detail.optional_extra3_img != addons.title_image">
<img ng-src="{base_url}uploads/quote_Optional_Extra/{{quote_detail.optional_extra3_img}}" height="100px;" width="150px;" />
</span>
<span ng-if="quote_detail.optional_extra3_img == addons.title_image">
<img ng-src="{base_url}uploads/title_image/{{addons.title_image}}" height="100px;" width="150px;" />
</span>
</td>
<td padding="10" width="70%">
<P class="p36 ft24">{{addons.addon_keyword}} - {{addons.catlog_title}}</P>
<hr style="border: 1px #ccc solid;"/>
<P class="p36 ft24">{{addons.addon_price}}</p>
<br/>
<P class="p37 ft2">{{addons.catlog_description}}</P>
</td>

</tr>
</span>

</table>
</div>
<?php } ?>
<!-- optinal extras ended -->







</DIV>

</DIV>
</div>
                    
  </div>
                </section>
                </div>
               
                <button name="" value="Generate PDF" id="save_pdf" class="btn btn-success btn-lg pull-left" onclick="generate_pdf()">Generate PDF</button>
                  
               <!-- </textarea>-->
                <div class="row">
                                
                                <!--<input type="submit" name="submit" value="Export PDF" class="btn btn-success btn-lg">-->
                                
                             
                             
                               
                            
                                 
                               <!--<button name="" value="View html" id="edit_html" class="btn btn-success btn-lg pull-right" onclick="view_html()">Edit Quote</button>
  
                           -->
                               
                            </div> 
               
</body>
</html>