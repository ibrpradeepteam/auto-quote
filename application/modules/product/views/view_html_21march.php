<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{page_title}</title>
{styles}
{scripts}
<script src="{base_url}ng-table/ng-table.js"></script>
<script src="<?php echo base_url('js/ckeditor');?>/ckeditor.js"></script>
<script src="<?php echo base_url('js');?>/adapters/jquery.js"></script>
<script>
//CKEDITOR.disableAutoInline = true;

$( document ).ready( function() {
	
	//$( '#edit_full_page').ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.	
	
	
} );
function view_html(){
	$( '#edit_full_page').ckeditor();
	 var $div=$('#editable_html'), isEditable=$div.is('.editable');
	 if(!isEditable)
	 {
		 alert('You can edit the quote');
	 }
	 else
	 {
		alert("You can't edit the quote"); 
	  }
     $('#editable_html').prop('contenteditable',!isEditable).toggleClass('editable')
	 $('#editable_html').prop('imageeditable',!isEditable).toggleClass('editable')	
	}
	
function generate_pdf(){	
		var full_page = $('#editable_html').html();
		var customer_name = $('#customer_name').val();
		$.ajax({
		type:'post',
		url:'{base_url}product/quote/file_write',
		data:{data:full_page},
		success:function(json){
			//alert(json);
		}	
		});
		//alert(customer_name);
		window.location.href='{base_url}product/quote/GeneratePDF/'+customer_name;
}

</script>
</head>

<body>
						<script>
						var quots_data = '{quote_data}';
						var companies_data = '{company_data}';
						
						var product_features = '{product_features}';
						var product_datas = '{product_data}';
						//var product_feature = product_features.replace(/\u/g, "");
						</script>
                       <!-- <textarea id="edit_full_page">-->
<section class="content" ng-app="editable" id="editable_html">

                    <div class="row" ng-controller="EditableCtrl" ng-init="init(quots_data,companies_data,product_features,product_datas)">
                    <form>
                   <input type="hidden" name="customer_name" value="{{quote_detail.customer_f_name}}&nbsp;{{quote_detail.customer_l_name}}" id="customer_name" />
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->   
                            <div class="row" id="edit_test">
                                <div class="col-md-4 pull-left">
                                    <h2 class="box-title" style="color:#09F;">{{company_data.name}}</h2>
                                    <div ng-if="company_data.logo">
                                    <img ng-src="{base_url}uploads/docs/{{company_data.logo}}" width="200px" height="100px" />
                                    </div>
                                </div>
                                <div class="col-md-8 pull-right">
                                      <h3>{{company_data.address}}<br />
                                      <br />Tel:{{quote_detail.phone_number}} </h3>
                                    </div>
                            </div>
                            <br />
                            <div class="row">
                             <div class="col-md-4 pull-left">
                                    <label class="box-title" style="font-size:18px;">Date:</label>
                                    <br />
                                    <label class="box-title" style="font-size:18px;">Quote No:</label>
                                    
                             </div>
                            
                         </div>
                            <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-3" style="float:left;">
                                  <label class="box-title" style="display:inline; font-size:18px;">Attn:&nbsp;</label>  
                                   
                                </div>
                                <div class="col-md-9" style="float:left;">
                                   <label style="display:inline; font-size:18px;">
                                    {{quote_detail.customer_f_name}}&nbsp;{{quote_detail.customer_l_name}}
                                    <br />
                                    {{quote_detail.company_name}}
                                    </label> 
                                    <div ng-if="quote_detail.street_add">
                                    <h4>{{quote_detail.street_add}}</h4>
                                   </div>
                                   <div ng-if="quote_detail.phone_number">
                                    <h4>Ph:{{quote_detail.phone_number}}</h4>
                                   </div>
                                   <div ng-if="quote_detail.email_add">
                                    <h4>Email:{{quote_detail.email_add}}</h4>
                                   </div>
                                </div>
                                </div>
                            </div>
                          <br />
                            <div class="row">
                                
                                <div class="col-md-12">
                                <div class="col-md-6">
                                 <p>
                                 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.  
                                 </p>
                                 </div>
                                </div>
                                
                            </div>
                         <br />
                            <div class="row">
                                <div class="col-md-4 pull-left">                                   
                                    <h4>Kind Regards</h4>
                                    <h3 style="color:#09F;">{{quote_detail.name}}</h3>
                                   
                                    <h3 ng-if="quote_detail.phone_no">Ph:{{quote_detail.phone_no}}</h3>
                                    
                                    <h3 ng-if="quote_detail.phone_no">Email:{{quote_detail.email}}</h3>
                                </div>
                            </div> 
                            
                            
                                                       
                            <hr style="  border-top: 1px solid #ccc;" />
                            
                            
                            
                            
                            <div class="row">
                            
                                <div class="col-md-4 pull-right"> 
                                 <h2 class="box-title" style="color:#09F;">{{company_data.name}}</h2>                                  
                                  <div ng-if="company_data.logo">
                                    <img ng-src="{base_url}uploads/docs/{{company_data.logo}}" width="200px" height="100px" />
                                    </div>   
                                </div>
                            </div>
                            <div class="row" ng-repeat="product_data in product_data">
                                <div class="col-md-4"> 
                                <h3 style="color:#06F;">{{product_data.product_name}}</h3>
                                <h3>Investment: {{product_data.price}}</h3>
                                </div>
                           
                               <div class="col-md-4">
                                <div class="col-md-3" style="float:left;">
                                  <label class="box-title" style="display:inline; font-size:22px;">Product:</label>  
                                   
                                </div>
                                <div class="col-md-9" style="float:left; margin-top:5px;">
                                   <label style="display:inline; font-size:18px;">
                                    {{product_data.product_name}}
                                  
                                    </label> 
                                   <li>Pump rated to {{product_data.max_pump_pressure}}</li>
                                   <li>Pump set to {{product_data.pump_set_pressure}}</li>
                                    <li>Water Temprature {{product_data.water_temprature}}</li>
                                     <li>Size: {{product_data.product_size}}</li>
                                      <li>Weight: {{product_data.product_weight}}</li>
                                </div>
                                </div>
                            </div>
                             <div class="row">
                             <div class="col-md-8 pull-left">
                               <h3 style="color:#09C;">Freight</h3>
                            
                                <h3 style="color:#09C;">Availability</h3>
                            
                                <h3 style="color:#09C;">Proposed Delivery Date:</h3>
                            
                                <h3 style="color:#09C;">Quote Validity:</h3>
                            
                                <h3 style="color:#09C;">Payment Terms:</h3>
                            
                                <h3 style="color:#09C;">Warranties:</h3>
                                <li>It is a long established fact that a reader</li>
                                <li> will be distracted by the readable content of a</li>
                                <li> page when looking at its layout. The point of using</li>
                                <li> Lorem Ipsum is that it has a more-or-less normal distribution </li>
                                <li>of letters, as opposed to using 'Content here, content here'</li>
                                <li>, making it look like readable English. Many desktop publishing</li>
                                <li> packages and web page editors now use Lorem Ipsum as their</li>
                                <li> default model text, and a search</li> 
                           
                                
                                </div>
                                <div class="col-md-4 pull-right" style="margin-top:50px;">
                                <div ng-if="quote_detail.product_image">
                                <img ng-src="{base_url}uploads/product_image/{{quote_detail.product_image}}" width="250px" height="250px" />
                                </div>
                                </div>
                            </div>
                            
                            
                            
                            <hr style="  border-top: 1px solid #ccc;" />
                            
                            
                            
                            <div class="row">
                              
                            <div class="col-md-4 pull-right"> 
                                 <h2 class="box-title" style="color:#09F;">{{company_data.name}}</h2>                                  
                                  <div ng-if="company_data.logo">
                                    <img ng-src="{base_url}uploads/docs/{{company_data.logo}}" width="200px" height="100px" />
                                    </div>   
                                </div>
                              </div>
                              
                                <h2 class="box-title" style="color:#09F;">Features and benefits</h2> 
                              <div class="row" ng-repeat="data in product_feature">
                              <div class="col-md-12">
                                 <div class="col-md-6 pull-left" style="width:60%;">
                                   
                                   <h3 style="color:#09F; margin:10px;">{{data.catlog_type}}</h3>
                                                             
                              		 <h4 style="color:#930; margin:10px;">{{data.catlog_title}}</h4>
                                
                                   {{data.catlog_description}}
                                  
                                 </div>
                                 <div class="col-md-6 pull-left" ng-if="data.title_image" style="width:25%"> 
                                   <img ng-src="{base_url}uploads/title_image/{{data.title_image}}" height="100px;" />                            
                                 </div>
                                 </div>
                                </div>
                           
                            
                           
                        </div><!--/.col (left) -->                        
                    </div>   <!-- /.row -->
                </section>
                <button name="" value="Generate PDF" id="save_pdf" class="btn btn-success btn-lg pull-left" onclick="generate_pdf()">Generate PDF</button>
                </form>   
               <!-- </textarea>-->
                <div class="row">
                                
                                <!--<input type="submit" name="submit" value="Export PDF" class="btn btn-success btn-lg">-->
                                
                             
                             
                               
                            
                                 
                               <button name="" value="View html" id="edit_html" class="btn btn-success btn-lg pull-right" onclick="view_html()">Edit Quote</button>
  
                           
                               
                            </div> 
               
</body>
</html>