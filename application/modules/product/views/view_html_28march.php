<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{page_title}</title>
{styles}
{scripts}
<script src="{base_url}ng-table/ng-table.js"></script>
<script src="<?php echo base_url('js/ckeditor');?>/ckeditor.js"></script>
<script src="<?php echo base_url('js');?>/adapters/jquery.js"></script>


<!--ckeditor api -->
<!--<link href="<?php //echo base_url('js/ckfinder');?>/sample.css" rel="stylesheet" type="text/css" />-->
<script src="<?php echo base_url('js/ckfinder');?>/ckeditor.js"></script>
<script src="<?php echo base_url('js/ckfinder');?>/ckfinder.js"></script>

<script>
//CKEDITOR.disableAutoInline = true;

$( document ).ready( function() {
	
	//alert($('#editor1').html());
	//$('#main_div').css('display','block');
	//$( '#edit_full_page').ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.	
	
	
} );
function view_html(){
	$( '#edit_full_page').ckeditor();
	 var $div=$('#main_div'), isEditable=$div.is('.editable');
	 if(!isEditable)
	 {
		 alert('You can edit the quote');
	 }
	 else
	 {
		alert("You can't edit the quote"); 
	  }
     $('#editable_html').prop('contenteditable',!isEditable).toggleClass('editable')
	 $('#editable_html').prop('imageeditable',!isEditable).toggleClass('editable')	
	}
	
function generate_pdf(){	
		//var full_page = $('#editor1').val();
		$('.pdf_header').css('display','block');
		$('.pdf_footer').css('display','block');
		var data = CKEDITOR.instances.editor1.getData();
		//var data = $('#main_div').html();
		var pdf_head = $('.pdf_header').html();
		var pdf_foot = $('.pdf_footer').html();
		var customer_name = $('#customer_name').val();
		var customer_email = $('#customer_email').val();
		var quote_id = $('#quote_id').val();
		$.ajax({
		type:'post',
		url:'{base_url}product/quote/file_write',
		data:{data:data,pdf_head:pdf_head,pdf_foot:pdf_foot},
		success:function(json){
			//alert(json);
		}	
		});
		//alert(customer_name);
		open_popup(customer_name,customer_email,quote_id);
		
		//window.location.href='{base_url}product/quote/GeneratePDF/?name='+customer_name+'&email='+customer_email+'&quote_id='+quote_id;
		
		
}

function open_popup(customer_name,customer_email,quote_id){
	jQuery.fancybox({
        type: 'iframe',
        href: '{base_url}product/quote/send_mail_to_manager/?name='+customer_name+'&email='+customer_email+'&quote_id='+quote_id,
        autoSize: false,
        closeBtn: true,
        width: 900,
        height: 500,
        closeClick: true,
        enableEscapeButton: true,
        beforeLoad: function () {},
    });		
}

</script>




</head>

<body>

<textarea id="editor1" name="editor1" rows="100" cols="80" style="width:100%; height:800px"></textarea>
                   
                   
                   <script type="text/javascript">
				   
$( document ).ready( function() {				   

// This is a check for the CKEditor class. If not defined, the paths must be checked.
if ( typeof CKEDITOR == 'undefined' )
{
	document.write(
		'<strong><span style="color: #ff0000">Error</span>: CKEditor not found</strong>.' +
		'This sample assumes that CKEditor (not included with CKFinder) is installed in' +
		'the "/ckeditor/" path. If you have it installed in a different place, just edit' +
		'this file, changing the wrong paths in the &lt;head&gt; (line 5) and the "BasePath"' +
		'value (line 32).' ) ;
}
else
{
	
	var config = {};
	config.height = '400px';
	config.resize_enabled = true;
	config.extraAllowedContent = ['div(col-md-4,col-md-8,col-md-12,col-md-2,container-fluid,row,pull-left,pull-right,title,title1,titl2,product_name,product_description,feature_desc,heading-feature,feature_img,heading_color,editable_html,main_div,top,pdf_header)','style'];
	config.allowedContent = true;
	
	config.contentsCss = ['{base_url}css/bootstrap.min.css','{base_url}css/auto_quote.css'];
	config['filebrowserBrowseUrl']='{base_url}ckeditor_images/quote/ckfinder.html';
	config['filebrowserImageBrowseUrl']='{base_url}ckeditor_images/quote/ckfinder.html?type=Images';
	config['filebrowserFlashBrowseUrl']='{base_url}ckeditor_images/quote/ckfinder.html?type=Flash';
	config['filebrowserUploadUrl'] = '{base_url}ckeditor_images/quote/core/connector/php/connector.php?command=QuickUpload&type=Files';
	config['filebrowserImageUploadUrl'] = '{base_url}ckeditor_images/quote/core/connector/php/connector.php?command=QuickUpload&type=Images';
	config['filebrowserFlashUploadUrl'] = '{base_url}ckeditor_images/quote/core/connector/php/connector.php?command=QuickUpload&type=Flash';
	
	var editor = CKEDITOR.replace( 'editor1',config);	
	
	//editor.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );
	var main_div = $('#main_div').html();
	$('#editor1').val(main_div);
	// Just call CKFinder.setupCKEditor and pass the CKEditor instance as the first argument.
	// The second parameter (optional), is the path for the CKFinder installation (default = "/ckfinder/").
	CKFinder.setupCKEditor( editor, '../' ) ;

	
	$('#main_div').css('display','none');
	
}

});
		</script>
						<script>
						var quots_data = '{quote_data}';
						var companies_data = '{company_data}';
						
						var product_features = '{product_features}';
						var product_datas = '{product_data}';
						
						var addons_data = '{addons_data}';
						//var product_feature = product_features.replace(/\u/g, "");
						</script>
                        
                       <!-- <textarea id="edit_full_page">-->
                       
 
 <div id="main_div" class="main_div">
    
<section class="content" ng-app="editable" id="editable_html" class="editable_html" style="margin-left:20px;">
  

                    <div class="row top" ng-controller="EditableCtrl" ng-init="init(quots_data,companies_data,product_features,product_datas,addons_data)">
                    
                  <input type="hidden" name="customer_id" value="{{quote_detail.customer_id}}" id="customer_id" />
                  
                  <input type="hidden" name="quote_id" value="{{quote_detail.id}}" id="quote_id" />
                  
                   <input type="hidden" name="customer_name" value="{{quote_detail.customer_f_name}}{{quote_detail.customer_l_name}}" id="customer_name" />
                   
                   <input type="hidden" name="customer_email" value="{{quote_detail.email_add}}" id="customer_email" />
                   
                  <div class="row pdf_header" style="display:none">
                  <div class="row" style="background-color:#8BCFF2;border: 1px #000 dashed; width:100%;  margin-right:0px;">
                                <div class="col-md-4 pull-right" style="float:right;">
                                   
                                    <div ng-if="company_data.logo">
                                    <img ng-src="{base_url}uploads/docs/{{company_data.logo}}" width="200px" height="100px"  />
                                    </div>
                                </div>
                                
                            </div>
                            </div>
                   
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->   
                            <div class="row" id="edit_test">
                                <div class="col-md-8 pull-left first_left_block" style="">
                                    <div class="heading_color"><h2 class="box-title heading_color" id="">{{company_data.name}}</h2></div>
                                    <div ng-if="company_data.logo">
                                    <img ng-src="{base_url}uploads/docs/{{company_data.logo}}" width="200px" height="100px" />
                                    </div>
                                </div>
                                <div class="col-md-4 pull-left first_right_block" style="">
                                      <h3>{{company_data.address}}<br />
                                      <br />Tel:{{quote_detail.phone_number}} </h3>
                                    </div>
                            </div>
                            <br />
                            <div class="row">
                             <div class="col-md-4 pull-left">
                                    <label class="box-title title" id="">Date:</label>
                                    <br />
                                    <label class="box-title title" id="">Quote No:</label>
                                    
                             </div>
                             
                             <style>
                           
							</style>
                         </div>
                            <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-3" style="float:left;">
                                  <label class="box-title title1" id="">Attn:&nbsp;</label>  
                                   
                                </div>
                                <div class="col-md-9" style="float:left;">
                                   <label class="title1">
                                    {{quote_detail.customer_f_name}}&nbsp;{{quote_detail.customer_l_name}}
                                    <br />
                                    {{quote_detail.company_name}}
                                    </label> 
                                    <div ng-if="quote_detail.street_add">
                                    <h4>{{quote_detail.street_add}}</h4>
                                   </div>
                                   <div ng-if="quote_detail.phone_number">
                                    <h4>Ph:{{quote_detail.phone_number}}</h4>
                                   </div>
                                   <div ng-if="quote_detail.email_add">
                                    <h4>Email:{{quote_detail.email_add}}</h4>
                                   </div>
                                </div>
                                </div>
                            </div>
                          <br />
                            <div class="row">
                                
                                <div class="col-md-12">
                                <div class="col-md-6">
                                 <p>
                                 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.  
                                 </p>
                                 </div>
                                </div>
                                
                            </div>
                         <br />
                            <div class="row">
                                <div class="col-md-4 pull-left">                                   
                                    <h4>Kind Regards</h4>
                                    <div class="heading_color"><h3 class="heading_color">{{quote_detail.name}} &nbsp {{quote_detail.last_name}}</h3></div>
                                   
                                    <h3 ng-if="quote_detail.phone_no">Ph:{{quote_detail.phone_no}}</h3>
                                    
                                    <h3 ng-if="quote_detail.phone_no">Email:{{quote_detail.email}}</h3>
                                </div>
                            </div> 
                            
                            
                                                       
                      
                            
                            
                            
                            
                           <!-- <div class="row">
                            
                                <div class="col-md-4 pull-right"> 
                                <div class="heading_color"> <h2 class="box-title heading_color" id="">{{company_data.name}}</h2></div>                                  
                                  <div ng-if="company_data.logo">
                                    <img ng-src="{base_url}uploads/docs/{{company_data.logo}}" width="200px" height="100px" />
                                    </div>   
                                </div>
                            </div> -->
                            <div class="row" ng-repeat="product_data in product_data">
                           
                                <div class="col-md-4"> 
                                <div class="heading_color"><h3 class="heading_color">{{product_data.product_name}}</h3></div>
                                <h3>Investment: {{product_data.price}}</h3>
                                </div>
                           <style>
						 
						   </style>
                               <div class="col-md-4">
                                <div class="col-md-3 product_name" id="">
                                  <label class="box-title">Product:</label>  
                                   
                                </div>
                                <div class="col-md-9" style="">
                                   <label class="title1">
                                    {{product_data.product_name}}
                                  
                                    </label> 
                                   <li>Pump rated to {{product_data.max_pump_pressure}}</li>
                                   <li>Pump set to {{product_data.pump_set_pressure}}</li>
                                    <li>Water Temprature {{product_data.water_temprature}}</li>
                                     <li>Size: {{product_data.product_size}}</li>
                                      <li>Weight: {{product_data.product_weight}}</li>
                                </div>
                                </div>
                            </div>
                             <div class="row">
                             <div class="col-md-8 pull-left">
                               <div class="heading_color"><h3 class="heading_color" id="">Freight</h3></div>
                            
                               <div class="heading_color"> <h3 class="heading_color">Availability</h3></div>
                            
                                <div class="heading_color"><h3 class="heading_color">Proposed Delivery Date:</h3></div>
                            
                                <div class="heading_color"><h3 class="heading_color">Quote Validity:</h3></div>
                            
                                <div class="heading_color"><h3 class="heading_color">Payment Terms:</h3></div>
                            
                                <div class="heading_color"><h3 class="heading_color">Warranties:</h3></div>
                                <li>It is a long established fact that a reader</li>
                                <li> will be distracted by the readable content of a</li>
                                <li> page when looking at its layout. The point of using</li>
                                <li> Lorem Ipsum is that it has a more-or-less normal distribution </li>
                                <li>of letters, as opposed to using 'Content here, content here'</li>
                                <li>, making it look like readable English. Many desktop publishing</li>
                                <li> packages and web page editors now use Lorem Ipsum as their</li>
                                <li> default model text, and a search</li> 
                           
                                
                                </div>
                                <div class="col-md-4 pull-right" style="margin-top:50px;">
                                <div ng-if="quote_detail.product_image">
                                <img ng-src="{base_url}uploads/product_image/{{quote_detail.product_image}}" width="250px" height="250px" />
                                </div>
                                </div>
                            </div>
                            
                            
                            
                            <!--<hr style="  border-top: 1px solid #ccc;" /> -->
                            
                            
                            
                           <!-- <div class="row">
                              
                            <div class="col-md-4 pull-right"> 
                                 <div class="heading_color"><h2 class="box-title heading_color" id="">{{company_data.name}}</h2></div>                                  
                                  <div ng-if="company_data.logo">
                                    <img ng-src="{base_url}uploads/docs/{{company_data.logo}}" width="200px" height="100px" />
                                    </div>   
                                </div>
                              </div> -->
                              
                                <h2 class="box-title" id="heading_color">Features and benefits</h2> 
                                
                              <div class="row">
                             
							
                            <div class="col-md-12" ng-repeat="product_features in product_feature">
                            
                            <div class="col-md-12" ng-repeat="product_feature in product_features">
                            
                            <div class="col-md-6 pull-left feature_img" ng-if="product_feature.title_image" style="margin:20px 5px 0px 5px;"> 
                                   <img ng-src="{base_url}uploads/title_image/{{product_feature.title_image}}" height="100px;" width="150px;" />                            
                                 </div> 
                                 
                                 <div class="col-md-6 pull-left feature_desc" id="">
                               
                                  <div class="heading-feature"> <h3 class="heading-feature" id="">{{product_feature.catlog_type}} - 
                                    {{product_feature.product_name}}
                                  
                                     </h3></div>
                                     
                                                           
                              		<div class="heading-feature">  <h4 class="heading-feature"  id="heading-feature">{{product_feature.catlog_title}}</h4></div>
                                
                                   {{product_feature.catlog_description}}
                                  </div>
                                
                                 
                                </div>
                                </div>
                                </div>
                                
                                
                                
                                
                                
                                <h2 class="box-title" id="heading_color">Optional Extras</h2> 
                                
                              <div class="row">
                             
							
                            <div class="col-md-12" ng-repeat="addons in addons_data">
                           
                            <div class="col-md-6 pull-left feature_img" ng-if="addons.title_image" style="margin:20px 5px 0px 5px;"> 
                                   <img ng-src="{base_url}uploads/title_image/{{addons.title_image}}" height="100px;" width="150px;" />                            
                                 </div> 
                                 
                                 <div class="col-md-6 pull-left feature_desc" id="">
                               
                                  <div class="heading-feature"> <h3 class="heading-feature" id="">{{addons.addon_keyword}} - 
                                    {{addons.catlog_title}}
                                  
                                     </h3></div>
                                     
                                                           
                              		<div class="heading-feature">  <h4 class="heading-feature"  id="heading-feature">Price - &nbsp;{{addons.addon_price}}</h4></div>
                                
                                   {{addons.catlog_description}}
                                  </div>
                                
                                 
                                </div>
                                </div>
                               
                           
                            
                           
                        </div><!--/.col (left) -->                        
                    </div>   <!-- /.row -->
                      
                    
  
                </section>
                </div>
               
                <button name="" value="Generate PDF" id="save_pdf" class="btn btn-success btn-lg pull-left" onclick="generate_pdf()">Generate PDF</button>
                  
               <!-- </textarea>-->
                <div class="row">
                                
                                <!--<input type="submit" name="submit" value="Export PDF" class="btn btn-success btn-lg">-->
                                
                             
                             
                               
                            
                                 
                               <!--<button name="" value="View html" id="edit_html" class="btn btn-success btn-lg pull-right" onclick="view_html()">Edit Quote</button>
  
                           -->
                               
                            </div> 
               
</body>
</html>