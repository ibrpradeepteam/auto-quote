<input type="text" id="color" value="<?php echo isset($color) ? $color : 'f5e48a'; ?>" name="color" style="border-color:#<?php echo isset($color) ? $color : 'f5e48a'; ?>"></input>
<script>
$(document).ready(function(e) {
  $('#color').colpick({
	layout:'hex',
	submit:0,
	colorScheme:'dark',
	onChange:function(hsb,hex,rgb,el,bySetColor) {
		$(el).css('border-color','#'+hex);
		$(el).val(hex);
		$('#previewNote').css('background-color','#'+hex);
		$('#previewNote').css('border-color','#'+hex);
		// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
		if(!bySetColor) $(el).val(hex);
	}
  }).keyup(function(){
	$(this).colpickSetColor(this.value);
  });
});
</script>
<style>
#color {
	margin: 0;
	padding: 0;
	border: 0;
	width: 0px;
	height: 28px;
	border-right: 99px solid green;
	line-height: 20px;
	margin-bottom: 10px;
	border-radius: 0;
	margin-left: 0px;
	cursor: crosshair;
}
</style>