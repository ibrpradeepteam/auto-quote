<html>
    <head>
        <title>Auto Quote | Dashboard</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">   
        <?php        
        $this->load->view('includes/styles');
        $this->load->view('includes/scripts');        
        ?>
</head>
<body class="skin-blue">
<?php
if($this->session->userdata('admin_id')){
  $this->load->view('includes/head');
  $this->load->view('includes/nav-bar');
}
?>
