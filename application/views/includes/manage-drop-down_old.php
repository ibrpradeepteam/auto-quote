<?php $this->load->view('include/style_and_script'); ?>
<?php ?>
<script>
var id = '<?= $id; ?>';
function manage_drop_down(action){
    var url = '<?= $url; ?>';
    var type = '<?= $type; ?>';
    var val = $("#add_"+id).val();
    var id1 = $("#edit_"+id).val();		
    $("#done_"+id+"_done").hide();
    $("#load_"+id+"_loader").show();
    if(action=='delete'){
        var confirmr = confirm('Are you sure you want to delete this '+project+'');	
    }else{
        var confirmr = true;	
    }
    if(confirmr){
    $.ajax({
        type: "POST",
        url: url, 
        data:{action:action,name:val,id:id1,type:type},
        success: function(json){
           var obj = jQuery.parseJSON(json);		   
           if(obj['success']){	
                $("#load_"+id+"_loader").hide();
                $("#done_"+id+"_done").show();		  
                if(action=='edit'){
                  $('#'+id+' option:selected').html(val);
                  var slectlength = $("#"+id+" option", parent.document).length;
                  for(var j=0;j<slectlength;j++){
                    if($("#"+id+" option:eq("+j+")", parent.document).val()==id1){
                        $("#"+id+" option:eq("+j+")", parent.document).html(val);
                    }
                  }	
                }else if(action=='add'){
                     $('#'+id).append(obj['option']);
                     $('#'+id, parent.document).append(obj['option']);  
                }else if(action=='delete'){
                    $('#'+id+' option:selected').remove();
                    var slectlength = $("#"+id+" option", parent.document).length;
                    for(var j=0;j<slectlength;j++){
                      if($("#"+id+" option:eq("+j+")", parent.document).val()==id1){
                          $("#"+id+" option:eq("+j+")", parent.document).remove();
                      }
                    }	
                    $('#edit_'+id).val('');
                    $("#add_"+id).val('');
                }
           } 
           else{
             $("#load_"+id+"").hide();
             alert("No change have made");
          } 	 
        }
    });	
    }
}
$(document).ready(function(e) {
    $('#'+id).change(function(e) {
       var val = $(this).val();
       var html = $('#'+id+' option:selected').html();	   
       if(val=='new'){
          $('#edit_'+id).val('');
          $("#add_"+id).val('');
          $('#add_button').show(); 
          $('#edit_button').hide();
          $('#delete_button').hide(); 
       }else{
          $('#edit_'+id).val(val);
          $("#add_"+id).val(html);
          $('#add_button').hide(); 
          $('#edit_button').show();
          $('#delete_button').show();   
       }
    });
});
</script>
<div class="row-fluid">
	<div class="span6">
		<?= $DropDown; ?>       
        <a href="javascript:;" onclick="manage_drop_down('delete')" id="delete_button" class="btn btn-danger" title="Delete" style="display:none;">Delete</a>       
    </div>
    <div class="span6">       
        <input type="text" name="add_<?= $id; ?>" id="add_<?= $id; ?>" value="" class="span12" >
        <input type="hidden" name="edit_<?= $id; ?>" id="edit_<?= $id; ?>" value="">
        <a href="javascript:;" class="btn btn-primary" onclick="manage_drop_down('add')" title="Add" id="add_button">SAVE</a>
        <a href="javascript:;" class="btn btn-primary" onclick="manage_drop_down('edit')" title="Edit" id="edit_button" style="display:none;">SAVE</a>       
    </div>
    <span id="<?= $id; ?>_loader" class="catelog_loader">
        <img src="<?php echo base_url();?>/images/loader.gif">    
    </span>
    <span id="<?= $id; ?>_done" class="catelog_loader">
            <img src="<?php echo base_url();?>/images/done.png">
    </span>
</div>
	