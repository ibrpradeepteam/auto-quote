<aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url('img') ?>/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, Jane</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="<?= base_url(); ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li  class="treeview <?= ($this->uri->segment(2) == 'siteuser' || $this->uri->segment(2) == 'company') ? "active" : ''; ?>">
                            <a href="#">
                                <i class="fa fa-users"></i> <span>Users</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu" style="display: none;">
                                <li class="<?= ($this->uri->segment(2) == 'siteuser') ? "active" : ''; ?>">
                                 <a href="<?php echo base_url('administration/siteuser'); ?>">
                                    <i class="fa fa-angle-double-right"></i> <span>Users</span>
                                </a>                                
                                </li> 
                                <li class="<?= ($this->uri->segment(2) == 'company') ? "active" : ''; ?>">
                                 <a href="<?php echo base_url('administration/company'); ?>">
                                    <i class="fa fa-angle-double-right"></i> <span>Company</span>
                                </a>                                
                                </li>                               
                            </ul>
                        </li> 
                        <li class="treeview <?= ($this->uri->segment(1) == 'product') ? "active" : ''; ?>">
                            <a href="#">
                                <i class="fa fa-th"></i> 
                                <span>Product (Machines)</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu" style="display: none;">
                                <li class="<?= ($this->uri->segment(2) == 'catlog') ? "active" : ''; ?>">
                                <a href="<?php echo base_url('product'); ?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Product List</a>
                                <a href="<?php echo base_url('product/add_product'); ?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Add product</a>
                                </li>                               
                            </ul>
                        </li> 
                        <li>
                            <a href="<?php echo base_url('product/quote'); ?>">
                                <i class="fa fa-th"></i> <span>Quote</span>
                            </a>
                        </li>
                       <li class="treeview <?= ($this->uri->segment(2) == 'catlog') ? "active" : ''; ?>">
                            <a href="#">
                                <i class="fa fa-gear fa-spin"></i>
                                <span>Settings</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu" style="display: none;">
                                <li class="<?= ($this->uri->segment(2) == 'catlog') ? "active" : ''; ?>"><a href="<?= base_url('administration/catlog')?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Catalogs</a></li>  
                                
                                 <li class="<?= ($this->uri->segment(2) == 'catlog') ? "active" : ''; ?>"><a href="<?= base_url('product/export_csv')?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> CSV</a></li>                                  
                            </ul>
                            
                           
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>