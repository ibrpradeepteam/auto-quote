<aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url('img') ?>/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $this->session->userdata('admin_name'); ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="<?= base_url(); ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        
                         <li>
                            <a href="<?php echo base_url('product/quote'); ?>">
                                <i class="fa fa-th"></i> <span>Quote</span>
                            </a>
                        </li>
                        
                         <li class="treeview <?= ($this->uri->segment(2) == 'customer') ? "active" : ''; ?>">
                            <a href="#">
                                <i class="fa fa-th"></i> 
                                <span>Customers</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu" style="display: none;">
                                <li class="<?= ($this->uri->segment(2) == 'catlog') ? "active" : ''; ?>">
                                <a href="<?php echo base_url('administration/customer'); ?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i>People</a>
                                <a href="<?php echo base_url('administration/customer/customers'); ?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i>Companies</a>
                                </li>                               
                            </ul>
                        </li> 
                        
                       
                        <li class="treeview <?= ($this->uri->segment(1) == 'product' && $this->uri->segment(2) != 'quote') ? "active" : ''; ?>">
                            <a href="#">
                                <i class="fa fa-th"></i> 
                                <span>Products</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu" style="display: none;">
                                <li class="<?= ($this->uri->segment(2) == 'catlog') ? "active" : ''; ?>">
                                <a href="<?php echo base_url('product'); ?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Products</a>
                                <a href="<?php echo base_url('product/add_product'); ?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Add product</a>
                                 <a href="<?php echo base_url('administration/catlog/categories'); ?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i>Categories</a>
                                 <?php if($this->session->userdata('admin_type') == 1 || $this->session->userdata('admin_type') == 2) { ?>
                                <a href="<?php echo base_url('administration/catlog'); ?>" style="margin-left: 10px;">
                                <i class="fa fa-angle-double-right"></i>Features and Benefits </a>
                                
                                <a href="<?php echo base_url('administration/catlog/extras'); ?>" style="margin-left: 10px;">
                                <i class="fa fa-angle-double-right"></i>Optional Extras</a>
                                <?php } ?>
                                </li>                               
                            </ul>
                        </li> 
                       
                       <?php if($this->session->userdata('admin_type') == 1 || $this->session->userdata('admin_type') == 2) { ?> 
                         <li  class="treeview <?= ($this->uri->segment(2) == 'siteuser' || $this->uri->segment(2) == 'company') ? "active" : ''; ?>">
                            <a href="#">
                                <i class="fa fa-users"></i> <span>Account Settings</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu" style="display: none;">
                                <li class="<?= ($this->uri->segment(2) == 'siteuser') ? "active" : ''; ?>">
                                 <a href="<?php echo base_url('administration/siteuser'); ?>">
                                    <i class="fa fa-angle-double-right"></i> <span>Users</span>
                                </a>                                
                                </li> 
                                <li class="<?= ($this->uri->segment(2) == 'company') ? "active" : ''; ?>">
                                 <a href="<?php echo base_url('administration/company'); ?>">
                                    <i class="fa fa-angle-double-right"></i> <span>Partner Company Setup</span>
                                </a>                                
                                </li>
                                <?php if($this->session->userdata('admin_type') == 1){ ?>
                                <li class="<?= ($this->uri->segment(2) == 'company') ? "active" : ''; ?>">
                                 <a href="<?php echo base_url('administration/siteuser/admin_setup'); ?>">
                                    <i class="fa fa-angle-double-right"></i> <span>Admin Setup</span>
                                </a>                                
                                </li>  
                                <?php } ?>                             
                            </ul>
                        </li> 
                        <?php } ?>
                       
                       <li class="treeview <?= ($this->uri->segment(2) == 'catlog') ? "active" : ''; ?>">
                            <a href="#">
                                <i class="fa fa-gear"></i>
                                <span>Settings</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu" style="display: none;">
                              <!--  <li class="<?= ($this->uri->segment(2) == 'catlog') ? "active" : ''; ?>"><a href="<?= base_url('administration/catlog')?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Catalogs</a></li>  -->
                             
                             <?php if($this->session->userdata('admin_type') == 1 || $this->session->userdata('admin_type') == 2) { ?>   
                                 <li class="<?= ($this->uri->segment(2) == 'catlog') ? "active" : ''; ?>"><a href="<?= base_url('administration/csv/')?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> CSV</a></li>
                                 <?php } ?>
                                 
                                  <li class="<?= ($this->uri->segment(2) == 'catlog') ? "active" : ''; ?>"><a href="<?= base_url('administration/csv/mail_setting')?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Manager Mail Setting</a></li>  
                                  
                                <?php if($this->session->userdata('admin_type') == 1 || $this->session->userdata('admin_type') == 2) { ?>      
                                  <li class="<?php echo ($this->uri->segment(2)=='smtp'?'active':'');?>"><a href="<?php echo base_url('administration/setting/smtp');?>" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> SMTP Setting</a></li> 
                                  <?php } ?>                               
                            </ul>
                            
                           
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>