<script>
var base_url = '<?php echo base_url(); ?>';
var segment = '<?php echo $this->uri->segment(2); ?>';
</script>
<!--Contain all js files-->

<script src="<?php echo base_url('js');?>/jquery.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url('js');?>/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url('js');?>/AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->        
        <script src="<?php echo base_url('js');?>/angular.min.js" type="text/javascript"></script> 
        <script src="<?php echo base_url('js');?>/auto_angular.js" type="text/javascript"></script> 
        
        
        
       
        <!-- jQuery UI 1.10.3 -->
        <script src="<?php echo base_url('js');?>/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url('js');?>/auto_quote.js" type="text/javascript"></script>
    
        <!-- Morris.js charts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="<?php echo base_url('js');?>/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="<?php echo base_url('js');?>/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="<?php echo base_url('js');?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('js');?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="<?php echo base_url('js');?>/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="<?php echo base_url('js');?>/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="<?php echo base_url('js');?>/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo base_url('js');?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url('js');?>/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
      
        
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?php echo base_url('js');?>/AdminLTE/dashboard.js" type="text/javascript"></script>     
        
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url('js');?>/AdminLTE/demo.js" type="text/javascript"></script>
        
        <!--DataTables-->
		<script src="<?php echo base_url('js/jquery.dataTables.min.js'); ?>"></script>
		<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
        
        <!-- fancybox -->
        <!-- Add mousewheel plugin (this is optional) -->
        <script type="text/javascript" src="<?php echo base_url('');?>fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
        
        <!-- Add fancyBox main JS and CSS files -->
        <script type="text/javascript" src="<?php echo base_url('fancybox');?>/source/jquery.fancybox.js?v=2.1.5"></script>
        <!-- fancybox-->
        
        <!-- chosen -->
        <script src="<?php echo base_url('js'); ?>/chosen.jquery.js" type="text/javascript"></script>
        <script>
        $(document).ready(function () { 
		 
	 var config = {
      '.chzn-select'           : {},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"},
	  '.chzn-select-height'    : {height:"50px"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
			});
        <!-- chosen -->
		</script>
		
		   <!--datetime picker-->
<script src="<?php echo base_url('js');?>/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('js'); ?>/jquery.timepicker.js"></script>
<script src="<?php echo base_url('js');?>/jquery.maskedinput.js" type="text/javascript"></script>

<script src="<?php echo base_url('js');?>/angular-nl2br.js" type="text/javascript"></script>

