<!--contain all style filespace-->



		<link href="<?php echo base_url('css');?>/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo base_url('css');?>/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo base_url('css');?>/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo base_url('css');?>/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?php echo base_url('css');?>/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="<?php echo base_url('css');?>/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?php echo base_url('css');?>/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo base_url('css');?>/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url('css');?>/AdminLTE.css" rel="stylesheet" type="text/css" />    
		<!--Datatables-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatables/datatable.css') ?>">
        
        <!--Fancybox-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('fancybox');?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />
        <style type="text/css">
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #222;
            }
        </style>
        <link href="<?php echo base_url('css');?>/chosen.css" rel="stylesheet">
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/auto_quote.css') ?>">
        
        <!--datepicker-->
<link href="<?php echo base_url('css');?>/datepicker.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css');?>/jquery.timepicker.css" />