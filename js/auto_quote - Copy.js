 $(document).ready(function () { 
  /* $('.dob').datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true
    });
    $(".date").mask("99/99/9999");
    $.mask.definitions['H'] = '[012]';
    $.mask.definitions['N'] = '[012345]';
    $.mask.definitions['n'] = '[0123456789]';
    $.mask.definitions['am'] = '[AM]';
    $.mask.definitions['am'] = '[PM]';
    $(".hour").mask("Hn:Nn am");
    $('#timepicker2').timepicker();
    $('.timepicker').timepicker({ 'scrollDefaultNow': true });
    $('#start_date').datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true,
      //startDate: _startDate,
      todayHighlight: true
    }).on('changeDate', function(e){ //alert('here');
        _endDate = new Date(e.date.getTime() ); //get new end date
        //alert(_endDate);
        $('#end_date').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
    });

    $('#end_date').datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true,
      //startDate: _endDate,
      todayHighlight: false
    }).on('changeDate', function(e){ 
        _endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
        //alert(_endDate);
        $('#start_date').datepicker(/*'setEndDate', _endDate*); //dynamically set new End date for #from
    });*/
    //Validate Form
    //$('form').h5Validate();     
   
    //Fancybox Initialization
    $('.fancybox').fancybox();
    //MultiSelect Dropdown Plugin
   /* var config = {
      '.chzn'           : {},
	  '.chzn-select'           : {width:"100%"},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }*/   
	
   
	
	// field nombering system by Ashvin Patel
	var no = 1;   
    $('span.enum').each(function(){
		$(this).html(no+'. ');
		no++;	
    });
	//disable department and location dropdown
	$('#rquest_form #department option:not(:selected)').attr('disabled', true);
	$('#rquest_form #location option:not(:selected)').attr('disabled', true);
	
	//multiselect validation	
	$('#marketing_person_chosen input, #receptionist_person_chosen input, #it_person_chosen input').blur(function(e) {
		var value = $(this).parent().parent().parent().parent().find('select').val();	
		var elem = $(this).parent().parent();
		validate_multiselect(value, elem);
	});
	$('#marketing_person, #receptionist_person, #it_person').change(function(e) {
		var value = $(this).val();	
		var elem = $(this).parent().find('.chosen-container .chosen-choices');
		validate_multiselect(value, elem);
	});  
	
	/*
	* Add company popup
	*/
	$('.add-company').click(function(e) {
		var url = base_url+'administration/company/add_company'
        viewFancybox(url, 300, 300);
    });
    
    $('.add-customer-company').click(function(e) {
		var url = base_url+'administration/customer/add_customer_company'
        viewFancybox(url, 300, 300);
    });
    
    $('.add-customer').click(function(e) {
		var url = base_url+'administration/customer/add_customer'
        viewFancybox(url, 400, 600);
    });
	
	$('.add-quote').click(function(e) {
		window.location.href = base_url+'product/quote/add_quote'
    });
	
	$('#update_product').click(function(){
	var products = [];
	var selected_name = $('#get_dropdown option:selected').text();
	var field_value = $("#common_field").val();
	$(".check").each(function(index, element){
	var product_id = $(element).val();
	
	if($(element).is(':checked')){
            	products.push($(element).val());
			}
	});
	
	if(products[0]){
	$.ajax({
	url:base_url+'product/update_bulk_products_text',
	type:'post',
	data:{product_ids:products,change_field:field_value,field_name:selected_name},
	success:function(){
	for(i = 0; i<products.length; i++){
	if(selected_name == 'Product Name'){
	$("#name_"+products[i]).html(field_value);
	}
	if(selected_name == 'Motor'){
	$("#motor_"+products[i]).html(field_value);
	}
	if(selected_name == 'Price'){
	$("#price_"+products[i]).html(field_value);
	}
	if(selected_name == 'Max Pump Pressure'){
	$("#max_pump_pressure_"+products[i]).html(field_value);
	}
	if(selected_name == 'Pump Set Pressure'){
	$("#pump_set_pressure_"+products[i]).html(field_value);
	}
	if(selected_name == 'Water Temprature'){
	$("#water_temp_"+products[i]).html(field_value);
	}
	if(selected_name == 'Size'){
	$("#size_"+products[i]).html(field_value);
	}
	if(selected_name == 'Weight'){
	$("#weight_"+products[i]).html(field_value);
	}
	}
	}
	});
	}
	else{
	alert("First Select Atleast one product");
	}
	
	});
	
	$("#get_dropdown").change(function(){
	var field = $(this).val();
	//console.log(field);
	if(field == 1){
	$("#common_field").css('display','block');
	$("#update_product").css('display','block');
	$("#common_dropdown").css('display','none');
	$("#common_dropdown").val('');
	}
	if(field != 1){
	$("#common_field").css('display','none');
	$("#update_product").css('display','none');
	$("#common_field").val('');
	}
	if((field != '') && (field != 1)){
	$.ajax({
	url:base_url+'product/get_catlog_dropdown/',
	type:'post',
	data:{selected:field},
	success:function(json){
	
	options = JSON.parse(json);
	$("#common_dropdown").css('display','block');
	$("#common_dropdown").html(options);
	}
	})
	}
	});
    
	$("#common_dropdown").change(function(){
	var products = [];
	var field_value = $(this).val();
	var selected_name = $('#common_dropdown option:selected').text();
	//console.log(name);
	var field_name = $("#get_dropdown").val();
	//console.log(field_name);
	//var i = 0;
	$(".check").each(function(index, element){
	var product_id = $(element).val();
	
	if($(element).is(':checked')){
            	products.push($(element).val());
			}
	});
	//console.log(products);
	if(products[0]){
	$.ajax({
	url:base_url+'product/update_bulk_products',
	type:'post',
	data:{product_ids:products,change_field:field_value,field_name:field_name},
	success:function(){
	for(i = 0; i<products.length; i++){
	if(field_name == 'Category'){
	$("#category_view_"+products[i]).html(selected_name);
	}
	if(field_name == 'Specification'){
	$("#feature1_view_"+products[i]).html(selected_name);
	}
	if(field_name == 'Motor / Engine'){
	$("#feature2_view_"+products[i]).html(selected_name);
	}
	if(field_name == 'Pump'){
	$("#feature3_view_"+products[i]).html(selected_name);
	}
	if(field_name == 'Boiler'){
	$("#feature4_view_"+products[i]).html(selected_name);
	}
	if(field_name == 'Frame'){
	$("#feature5_view_"+products[i]).html(selected_name);
	}
	if(field_name == 'Water Tank'){
	$("#feature6_view_"+products[i]).html(selected_name);
	}
	if(field_name == 'By-Pass'){
	$("#feature7_view_"+products[i]).html(selected_name);
	}
	if(field_name == 'Control Box'){
	$("#feature8_view_"+products[i]).html(selected_name);
	}
	if(field_name == 'Hose / Gun'){
	$("#feature9_view_"+products[i]).html(selected_name);
	}
	}
	}
	});
	}
	});
	
	
	// delete products
	$('#delete_products').click(function(e) {		
		var products = [];
		$('.check').each(function(index, element) {
			if($(element).is(':checked')){
				products.push($(element).val());
			}
		});			
		if(products[0]){
			if(confirm('Are you sure want to delete the selected Products!')){
				$.ajax({	  
					url : base_url+'product/delete_bulk_products',
					type : "post",
					data: {products:products},
					success: function(json){
						$.each(products, function(i, product_id){
							$('#product_row_'+product_id).remove();
						});
					}
				});
			}
		}else{
			alert('First Select atleast one Product!')	
		}
    });
   
	
});
/**
* Validate form multiselect dropdown on form submit
* or change drop down value
* 05/dec/2014
* Ashvin Patel
*/
function validate_multiselect(value, elem){
	if(elem){
		if(!value){
			elem.addClass('ui-state-error');
			return 1;					
		}else{
			elem.removeClass('ui-state-error');				
		}	
	}else{
		var count = 0;
		var i = 0
		$('select').each(function(index, element) {
			var id = $(this).attr('id');			
			if (id == 'marketing_person' || id == 'receptionist_person' || id == 'it_person'){
			  var value = $(this).val();	
			  var elem = $(this).parent().find('.chosen-container .chosen-choices');
			  count = validate_multiselect(value, elem);	
			  if(count){
				  i++;	
			  }
			}
		});		  
		if(i){
		  return false;	
		}
	}
}
function viewFancybox(url, width, height){
    jQuery.fancybox({
        type: 'iframe',
        href: url,
        autoSize: false,
        closeBtn: true,
        width: width,
        height: height,
        closeClick: true,
        enableEscapeButton: true,
        beforeLoad: function () {},
    });	
}


/**
delete functionality with checkbox selection
vinita-14march-2015
*/
function checkBox(id)
{
  var showcounter = 0;
	var checkcounter = 0;
	$('.check').each(function(index, element) { 		  
		if($(this).css('display') == 'none'){
			showcounter = parseInt(showcounter)+1; 		
		} 
		if($(this).is(':checked')){
			checkcounter = parseInt(checkcounter)+1;
		}
    });		
	if(showcounter==0){
		if(!checkcounter){
			alert("First select atleast one recored");
			return false;
		}else{
			if(confirm('Are you sure want to delete selected recored')){
				return true;	
			}else{
				return false;	
			}
		}
	}else{
		return false;
	}
 
}

function get_machines(category_id,index){
id_index = index.charAt(index.length-1);
		$.ajax({
				type:'post',
				url:base_url+'product/quote/get_category_machines',
				data:{cat_id:category_id},
				success:function(json){
				if(json){
					json = jQuery.parseJSON(json);
				$('#machine_dropdown_'+id_index).html(json);	
				$('#machine_dropdown_'+id_index).trigger("chosen:updated");
				}
				else{
				$('#machine_dropdown_'+id_index).html('<option value="">No products</option>');	
				}
				}
			});
}


function get_price1(addon_id)
{
  $.ajax({
				type:'post',
				url:base_url+'product/quote/get_addon_price',
				data:{id:addon_id,},
				success:function(json){
				if(json){
				var result = JSON.parse(json);
				console.log(result);
				$('#add_on1').val(result.addon_price);
				$('#add_on1').attr('value',result.addon_price);
				$('#add_on12').val(result.title_image);
				if(result.title_image != '')
				{
				$('#add_on12').attr('src','../../uploads/product_image/'+result.title_image);
				}else{
				$('#add_on12').attr('src','../../../uploads/title_image/'+result.title_image);
				}
				$('#add_ion12').attr('value',result.title_image);
				}
				
				}
			});
}

function get_price2(addon_id )
{
  $.ajax({
				type:'post',
				url:base_url+'product/quote/get_addon_price',
				data:{id:addon_id},
				success:function(json){
				if(json){
				var result = JSON.parse(json);
				$('#add_on2').val(result.addon_price);
				$('#add_on2').attr('value',result.addon_price);
				$('#add_on22').val(result.title_image);
				if(result.title_image != '')
				{
				$('#add_on22').attr('src','../../uploads/title_image/'+result.title_image);
				} else{
				$('#add_on22').attr('src','../../uploads/title_image/'+result.title_image);
				}
				$('#add_ion22').attr('value',result.title_image);
				
				}
				
				}
			});
}

function get_price3(addon_id)
{
  $.ajax({
				type:'post',
				url:base_url+'product/quote/get_addon_price',
				data:{id:addon_id},
				success:function(json){
				if(json){
				var result = JSON.parse(json);
				$('#add_on3').val(result.addon_price);
				$('#add_on3').attr('value',result.addon_price);
				$('#add_on32').val(result.title_image);
				if(result.title_image != '')
				{
				$('#add_on32').attr('src','../../uploads/title_image/'+result.title_image);
				}else{
				$('#add_on32').attr('src','../../../uploads/title_image/'+result.title_image);
				}
				$('#add_ion32').attr('value',result.title_image);
								}
				
				}
			});
}

function check_limit(product_id,index) {	
id_index = index.charAt(index.length-1);
//$("#product_detail_main").html('');
/*var selectArr = [];
        $('#machine_dropdown option:selected').each(function() {
    	selectArr.push($(this).val());
		});*/
		//console.log(selectArr); 
		$.ajax({
			 	type:'post',
				url:base_url+'product/get_product_features',
				data:{id : product_id},
				success:function(json){
					//console.log(json);
					if(json){
					
					 detail = JSON.parse(json);	
					 console.log(detail);
					var html = $("#save_html").html();
					
					var length = $('.detail_length').length;
					
					var length = length;
					
					var index = id_index;
					
						var final = html.replace(/N/g,index);
					   //console.log(detail);
					   if(length-1 == index)
					   {
						$("#product_detail_main").append(final);
					   }
					$(".product_detail_"+index).show();
					
					//$("#product_title_"+index).html();
					$("#product_title_"+index).html("Product Detail -"+detail.product_name);
					
					//$("#product_name_"+index).attr('value',detail.product_name);
					$("#product_name_"+index).val(detail.product_name);
					
					//$("#product_id_"+index).attr('value',detail.id);
					$("#product_id_"+index).val(detail.id);
					
					//$("#product_price_"+index).attr('value',detail.price);
					$("#product_price_"+index).val(detail.price);
					
					//$("#rated_pressure_"+index).attr('value',detail.max_pump_pressure);
					$("#rated_pressure_"+index).val(detail.max_pump_pressure);
					
					//$("#rated_pressure_"+index).attr('ng-model',detail.max_pump_pressure);
					//$("#setup_pressure_"+index).attr('value',detail.pump_set_pressure);
					
					$("#setup_pressure_"+index).val(detail.pump_set_pressure);
					
					$("#size_"+index).val(detail.product_size);
					$("#weight_"+index).val(detail.product_weight);
					
					$("#image_"+index).val(detail.product_image);
					$("#image_src_"+index).attr('src','../../uploads/product_image/'+detail.product_image);
					}
					
				}
					
					
			 });  
        
       /* if(selectArr.length >= 5)
		{
		 alert("you can't select more then 5 machines");
		 $(".chzn-select").chosen({max_selected_options: 5});
		 $('#machine_dropdown option').prop('disabled', true).trigger("chosen:updated");
		 $('#machine_dropdown option:selected').removeAttr('disabled').trigger("chosen:updated");
		}
		if(selectArr.length < 5)
		{
		 $(".chzn-select").chosen({max_selected_options: 5});
		 $('#machine_dropdown option').removeAttr('disabled').trigger("chosen:updated");
		}*/
      }
	  
	  function delete_image(product_id){
		  if(confirm("Are you sure want to delete this image?"))
		  {
		    $.ajax({
			type:'post',
			url: base_url+'product/delete_product_image',
			data:{id:product_id},
			success:function(json){
				
				if(json == 1)
				{
				$('#product_image').css('display','none');	
				}
			}	
			});
		  }
		  }
		  
		  function auto_fill(customer_id){		
			  if(customer_id !='' && customer_id !="add")
			  { 			 alert(customer_id);	 
			$("#edit_customer").removeAttr('disabled');
			$.ajax({
			type:'post',
			url:base_url+'product/quote/get_customer_detail',	
			data:{id:customer_id},
			success:function(json){
				console.log(json);
				customer_detail = JSON.parse(json);
				console.log(customer_detail.customer_f_name);
				
				// customer info 
				$('#customer_info_required').css('display','block');
				$('#contact_info').css('display','block');
				$('#customer_first_name').text(customer_detail.customer_f_name);
				$('#customer_last_name').text(customer_detail.customer_l_name);
				$('#owner').text(customer_detail.owner);
				$('#work_phone').text(customer_detail.work_phone);
				$('#phone_number1').text(customer_detail.phone_number);
				$('#email_add1').text(customer_detail.email_add);
				$('#address1').text(customer_detail.street_add);
				$('#address2').text(customer_detail.suburb+',');
				$('#address3').text(customer_detail.state+',');
				$('#address4').text(customer_detail.post_code);
				$('#customer_id').val(customer_detail.customer_id);
				
				/*$('#customer_f_name').val(customer_detail.customer_f_name);
				$('#customer_f_name').prop('readonly',true);
				
				
				//$('#customer_f_name').prop('readonly',true);
				
				$('#customer_l_name').val(customer_detail.customer_l_name);
				$('#customer_l_name').prop('readonly',true);
				//$('#customer_l_name').attr('value',customer_detail.customer_l_name);
				
				$('#company_name').val(customer_detail.company_name);
				$('#company_name').prop('readonly',true);
				//$('#company_name').attr('value',customer_detail.company_name);
				
				$('#phone_number').val(customer_detail.phone_number);
				$('#phone_number').prop('readonly',true);
				//$('#phone_number').attr('value',customer_detail.phone_number);
				
				$('#email_add').val(customer_detail.email_add);
				$('#email_add').prop('readonly',true);
				//$('#email_add').attr('value',customer_detail.email_add);
				
				$('#street_add').val(customer_detail.street_add);
				$('#street_add').prop('readonly',true);
				//$('#street_add').attr('value',customer_detail.street_add);
				
				$('#suburb').val(customer_detail.suburb);
				$('#suburb').prop('readonly',true);
				//$('#suburb').attr('value',customer_detail.suburb);
				
				$('#state').val(customer_detail.state);
				$('#state').prop('readonly',true);
				//$('#state').attr('value',customer_detail.state);
				
				$('#post_code').val(customer_detail.post_code);
				$('#post_code').prop('readonly',true);
				//$('#post_code').attr('value',customer_detail.post_code);*/
			}
			}); 
			  }
			  if(customer_id =='' || customer_id == 'add'){
				$('#customer_f_name').val('');
				$('#customer_f_name').attr('value','');
				$('#customer_f_name').removeAttr("readonly");
				
				
				$('#customer_l_name').val('');
				$('#customer_l_name').attr('value','');
				$('#customer_l_name').attr('readonly',false);
				
				$('#company_name').val('');
				$('#company_name').attr('value','');
				$('#company_name').prop('readonly',false);
				
				$('#phone_number').val('');
				$('#phone_number').attr('value','');
				$('#phone_number').prop('readonly',false);
				
				$('#email_add').val('');
				$('#email_add').attr('value','');
				$('#email_add').prop('readonly',false);
				
				$('#street_add').val('');
				$('#street_add').attr('value','');
				$('#street_add').prop('readonly',false);
				
				$('#suburb').val('');
				$('#suburb').attr('value','');
				$('#suburb').prop('readonly',false);
				
				$('#state').val('');
				$('#state').attr('value','');
				$('#state').prop('readonly',false);
				
				$('#post_code').val('');
				$('#post_code').attr('value','');
				$('#post_code').prop('readonly',false);
				 }
		       if(customer_id == 'add')
		       {
		      // $('#customer_f_name').attr('readonly',false);
		       //$('#customer_l_name').attr('readonly',false);
		       viewFancybox(base_url+'administration/customer/add_customer', 400, 600);
		       }
				 
				
		}
		function auto_fill_company(customer_id){
		var selected_customer_id = $("#customer_id").val();	
//alert(selected_customer_id);		
			  if(customer_id !='' && customer_id !="add")
			  { 			 
			$("#edit_customer").removeAttr('disabled');
			// customer info 
				$('#customer_info_required').css('display','block');
				$('#contact_info').css('display','block');
				$(".add-customer").css('display','none');
				$("input[name = delete]").css('display','none');
			$.ajax({
			type:'post',
			url:base_url+'product/quote/get_customer_company_detail',	
			data:{id:customer_id,customer_id:selected_customer_id},
			success:function(json){
				//console.log(json);
				customer_detail = JSON.parse(json);
				//console.log(customer_detail);
				var datavalue=[];
				$.each(customer_detail,function(key ,value){
				value = $.map(value, function(el) { return el; });
				datavalue.push(value);
				})
				
				console.log(datavalue);
				$('#data1').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>' );
				//$('#data1').html(datavalue);
				
				$('#example').dataTable( {				
				"data": datavalue,
				"columns": [
				 { "title": ""},
				 { "title": "Customer Name" },
				 { "title": "Company" },
				 { "title": "Phone No" },
				 { "title": "Email", "class": "center" },
				 { "title": "Address", "class": "center" }
				]
				
				});
				
				
			}
			}); 
			  }
			  
		       if(customer_id == 'add')
		       {
		      // $('#customer_f_name').attr('readonly',false);
		       //$('#customer_l_name').attr('readonly',false);
		       viewFancybox(base_url+'administration/customer/add_customer_company/', 400, 600);
			  
			
		       }
				 
				
		}
		function reset_values(){
 $('#customer_f_name').val('');
					$('#customer_f_name').attr('value','');
				
				$('#customer_l_name').val('');
				$('#customer_l_name').attr('value','');
				
				$('#company_name').val('');
				$('#company_name').attr('value','');
				
				$('#phone_number').val('');
				$('#phone_number').attr('value','');
				
				$('#email_add').val('');
				$('#email_add').attr('value','');
				
				$('#street_add').val('');
				$('#street_add').attr('value','');
				
				$('#suburb').val('');
				$('#suburb').attr('value','');
				
				$('#state').val('');
				$('#state').attr('value','');
				
				$('#post_code').val('');
				$('#post_code').attr('value','');
} 

function delete_catalog_image(catlog_id,img){
		  if(confirm("Are you sure want to delete this image?"))
		  {
		    $.ajax({
			type:'post',
			url: base_url+'administration/catlog/delete_catalog_image',
			data:{id:catlog_id,img:img},
			success:function(json){
				
				if(json == 1)
				{
				$('#'+img).css('display','none');	
				$('#'+img).css('display','none');	
				}
			}	
			});
		  }
		  }