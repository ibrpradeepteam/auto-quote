 $(document).ready(function () { 
  /* $('.dob').datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true
    });
    $(".date").mask("99/99/9999");
    $.mask.definitions['H'] = '[012]';
    $.mask.definitions['N'] = '[012345]';
    $.mask.definitions['n'] = '[0123456789]';
    $.mask.definitions['am'] = '[AM]';
    $.mask.definitions['am'] = '[PM]';
    $(".hour").mask("Hn:Nn am");
    $('#timepicker2').timepicker();
    $('.timepicker').timepicker({ 'scrollDefaultNow': true });
    $('#start_date').datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true,
      //startDate: _startDate,
      todayHighlight: true
    }).on('changeDate', function(e){ //alert('here');
        _endDate = new Date(e.date.getTime() ); //get new end date
        //alert(_endDate);
        $('#end_date').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
    });

    $('#end_date').datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true,
      //startDate: _endDate,
      todayHighlight: false
    }).on('changeDate', function(e){ 
        _endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
        //alert(_endDate);
        $('#start_date').datepicker(/*'setEndDate', _endDate*); //dynamically set new End date for #from
    });*/
    //Validate Form
    //$('form').h5Validate();     
   
    //Fancybox Initialization
    $('.fancybox').fancybox();
    //MultiSelect Dropdown Plugin
   /* var config = {
      '.chzn'           : {},
	  '.chzn-select'           : {width:"100%"},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }*/   
	
   
	
	// field nombering system by Ashvin Patel
	var no = 1;   
    $('span.enum').each(function(){
		$(this).html(no+'. ');
		no++;	
    });
	//disable department and location dropdown
	$('#rquest_form #department option:not(:selected)').attr('disabled', true);
	$('#rquest_form #location option:not(:selected)').attr('disabled', true);
	
	//multiselect validation	
	$('#marketing_person_chosen input, #receptionist_person_chosen input, #it_person_chosen input').blur(function(e) {
		var value = $(this).parent().parent().parent().parent().find('select').val();	
		var elem = $(this).parent().parent();
		validate_multiselect(value, elem);
	});
	$('#marketing_person, #receptionist_person, #it_person').change(function(e) {
		var value = $(this).val();	
		var elem = $(this).parent().find('.chosen-container .chosen-choices');
		validate_multiselect(value, elem);
	});  
	
	/*
	* Add company popup
	*/
	$('.add-company').click(function(e) {
		var url = base_url+'administration/company/add_company'
        viewFancybox(url, 300, 300);
    });
	
	$('.add-quote').click(function(e) {
		window.location.href = base_url+'product/quote/add_quote'
    });
	
});
/**
* Validate form multiselect dropdown on form submit
* or change drop down value
* 05/dec/2014
* Ashvin Patel
*/
function validate_multiselect(value, elem){
	if(elem){
		if(!value){
			elem.addClass('ui-state-error');
			return 1;					
		}else{
			elem.removeClass('ui-state-error');				
		}	
	}else{
		var count = 0;
		var i = 0
		$('select').each(function(index, element) {
			var id = $(this).attr('id');			
			if (id == 'marketing_person' || id == 'receptionist_person' || id == 'it_person'){
			  var value = $(this).val();	
			  var elem = $(this).parent().find('.chosen-container .chosen-choices');
			  count = validate_multiselect(value, elem);	
			  if(count){
				  i++;	
			  }
			}
		});		  
		if(i){
		  return false;	
		}
	}
}
function viewFancybox(url, width, height){
    jQuery.fancybox({
        type: 'iframe',
        href: url,
        autoSize: false,
        closeBtn: true,
        width: width,
        height: height,
        closeClick: true,
        enableEscapeButton: true,
        beforeLoad: function () {},
    });	
}


/**
delete functionality with checkbox selection
vinita-14march-2015
*/
function checkBox(id)
{
  var showcounter = 0;
	var checkcounter = 0;
	$('.check').each(function(index, element) { 		  
		if($(this).css('display') == 'none'){
			showcounter = parseInt(showcounter)+1; 		
		} 
		if($(this).is(':checked')){
			checkcounter = parseInt(checkcounter)+1;
		}
    });		
	if(showcounter==0){
		if(!checkcounter){
			alert("First select atleast one recored");
			return false;
		}else{
			if(confirm('Are you sure want to delete selected recored')){
				return true;	
			}else{
				return false;	
			}
		}
	}else{
		return false;
	}
 
}

function get_machines(category_id){
		$.ajax({
				type:'post',
				url:base_url+'product/quote/get_category_machines',
				data:{cat_id:category_id},
				success:function(json){
				if(json){
				$('#machine_dropdown').html(json);	
				$('#machine_dropdown').trigger("chosen:updated");
				}
				else{
				$('#machine_dropdown').html('<option value="">No products</option>');	
				}
				}
			});
}

function check_limit(product_id) {	

var selectArr = [];
        $('#machine_dropdown option:selected').each(function() {
    	selectArr.push($(this).val());
		//console.log(selectArr);
		//$('#product_title').append(selectArr.length).append(" -"+selectArr);
		}); 
		
		$.ajax({
			 	type:'post',
				url:base_url+'product/get_product_features',
				data:{id:selectArr[selectArr.length-1]},
				success:function(json){
					 detail = JSON.parse(json);	
					 console.log(detail);
					var html = $("#save_html").html();
					
					var length = $('.detail_length').length;
					
					var length = length;
					
					//var length = length+1;
					var final = html.replace(/N/g,length);
					//alert(final);
					
					$("#product_detail_main").append(final);
					$(".product_detail_"+length-1).show();
					
					$("#product_title_"+length).append(" -"+detail.product_name);
					$("#product_price_"+length).val(detail.price);
					$("#rated_pressure_"+length).val(detail.max_pump_pressure);
					$("#setup_pressure_"+length).val(detail.pump_set_pressure);
							
					}
					
					
			 });  
        
        if(selectArr.length >= 5)
		{
		 alert("you can't select more then 2 machines");
		 $(".chzn-select").chosen({max_selected_options: 5});
		 $('#machine_dropdown option').prop('disabled', true).trigger("chosen:updated");
		 $('#machine_dropdown option:selected').removeAttr('disabled').trigger("chosen:updated");
		}
		if(selectArr.length < 5)
		{
		 $(".chzn-select").chosen({max_selected_options: 5});
		 $('#machine_dropdown option').removeAttr('disabled').trigger("chosen:updated");
		}
      }
	  
	  function append(){
		    
		  }