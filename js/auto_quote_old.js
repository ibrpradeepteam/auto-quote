 $(document).ready(function () { 
  /* $('.dob').datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true
    });
    $(".date").mask("99/99/9999");
    $.mask.definitions['H'] = '[012]';
    $.mask.definitions['N'] = '[012345]';
    $.mask.definitions['n'] = '[0123456789]';
    $.mask.definitions['am'] = '[AM]';
    $.mask.definitions['am'] = '[PM]';
    $(".hour").mask("Hn:Nn am");
    $('#timepicker2').timepicker();
    $('.timepicker').timepicker({ 'scrollDefaultNow': true });
    $('#start_date').datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true,
      //startDate: _startDate,
      todayHighlight: true
    }).on('changeDate', function(e){ //alert('here');
        _endDate = new Date(e.date.getTime() ); //get new end date
        //alert(_endDate);
        $('#end_date').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
    });

    $('#end_date').datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true,
      //startDate: _endDate,
      todayHighlight: false
    }).on('changeDate', function(e){ 
        _endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
        //alert(_endDate);
        $('#start_date').datepicker(/*'setEndDate', _endDate*); //dynamically set new End date for #from
    });*/
    //Validate Form
    //$('form').h5Validate();     
   
    //Fancybox Initialization
    $('.fancybox').fancybox();
    //MultiSelect Dropdown Plugin
   /* var config = {
      '.chzn'           : {},
	  '.chzn-select'           : {width:"100%"},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }*/   
	
   
	
	// field nombering system by Ashvin Patel
	var no = 1;   
    $('span.enum').each(function(){
		$(this).html(no+'. ');
		no++;	
    });
	//disable department and location dropdown
	$('#rquest_form #department option:not(:selected)').attr('disabled', true);
	$('#rquest_form #location option:not(:selected)').attr('disabled', true);
	
	//multiselect validation	
	$('#marketing_person_chosen input, #receptionist_person_chosen input, #it_person_chosen input').blur(function(e) {
		var value = $(this).parent().parent().parent().parent().find('select').val();	
		var elem = $(this).parent().parent();
		validate_multiselect(value, elem);
	});
	$('#marketing_person, #receptionist_person, #it_person').change(function(e) {
		var value = $(this).val();	
		var elem = $(this).parent().find('.chosen-container .chosen-choices');
		validate_multiselect(value, elem);
	});  
	
	/*
	* Add company popup
	*/
	$('.add-company').click(function(e) {
		var url = base_url+'administration/company/add_company'
        viewFancybox(url, 300, 300);
    });
	
});
/**
* Validate form multiselect dropdown on form submit
* or change drop down value
* 05/dec/2014
* Ashvin Patel
*/
function validate_multiselect(value, elem){
	if(elem){
		if(!value){
			elem.addClass('ui-state-error');
			return 1;					
		}else{
			elem.removeClass('ui-state-error');				
		}	
	}else{
		var count = 0;
		var i = 0
		$('select').each(function(index, element) {
			var id = $(this).attr('id');			
			if (id == 'marketing_person' || id == 'receptionist_person' || id == 'it_person'){
			  var value = $(this).val();	
			  var elem = $(this).parent().find('.chosen-container .chosen-choices');
			  count = validate_multiselect(value, elem);	
			  if(count){
				  i++;	
			  }
			}
		});		  
		if(i){
		  return false;	
		}
	}
}
function viewFancybox(url, width, height){
    jQuery.fancybox({
        type: 'iframe',
        href: url,
        autoSize: false,
        closeBtn: true,
        width: width,
        height: height,
        closeClick: true,
        enableEscapeButton: true,
        beforeLoad: function () {},
    });	
}
