 $(document).ready(function () { 
   $('.dob').datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true
    });
    $(".date").mask("99/99/9999");
    $.mask.definitions['H'] = '[012]';
    $.mask.definitions['N'] = '[012345]';
    $.mask.definitions['n'] = '[0123456789]';
    $.mask.definitions['am'] = '[AM]';
    $.mask.definitions['am'] = '[PM]';
    $(".hour").mask("Hn:Nn am");
    $('#timepicker2').timepicker();
    $('.timepicker').timepicker({ 'scrollDefaultNow': true });
    $('#start_date').datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true,
      //startDate: _startDate,
      todayHighlight: true
    }).on('changeDate', function(e){ //alert('here');
        _endDate = new Date(e.date.getTime() ); //get new end date
        //alert(_endDate);
        $('#end_date').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
    });

    $('#end_date').datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true,
      //startDate: _endDate,
      todayHighlight: false
    }).on('changeDate', function(e){ 
        _endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
        //alert(_endDate);
        $('#start_date').datepicker(/*'setEndDate', _endDate*/); //dynamically set new End date for #from
    });
    //Validate Form
    $('form').h5Validate();  
    if(segment!='catlog'&&segment!='siteuser'){
    //Request Datatables Initialization
        $('#request_table').dataTable( {
			"order": [[ 4, "desc" ]]	
		} );
		 
    }
   
    //Fancybox Initialization
    $('.fancybox').fancybox();
    //MultiSelect Dropdown Plugin
    var config = {
      '.chzn'           : {},
	  '.chzn-select'           : {width:"100%"},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
    $('.open_request').click(function(){
        var r_id = $(this).attr('data-id');
        var u_type = $(this).attr('data-user-type');
		var r_for = $(this).attr('data-for');
        var url = base_url+'request/view_request/'+r_id+'/'+u_type+'/'+r_for;
        viewFancybox(url, 846, 482);
    });
	//get department managers list 
    $('.department').change(function(){
        var department_id = $(this).val();
        $.ajax({
            url: base_url+'request/get_department_managers/'+department_id,
            type: 'post',
            data: {},
            success: function(json){
                var data = $.parseJSON(json);
                
                if(!$.isEmptyObject(data)){
                    var html = '<ol>';
                    $.each(data, function(index, value){
                        html += "<li><label> "+value+" </label></li>";
                    });
                    html += '</ol>';
                }else{
                    var html = "<p><label> Select department</label></p>";
                }
                $('.show_managers').html('<span id="demo-html"><img src="'+base_url+'images/help.png"></span>');
                 $("#demo-html").tooltipster({
                    content: $(html),
                    width: 300,
                    position: 'right',
                    theme: 'tooltipster-light'
                });
            }
        });
    });
	//get location managers list By - ASHVIN PATEL 30/OCT/2014
	$('.location').change(function(){
        var location_id = $(this).val();
        $.ajax({
            url: base_url+'request/get_location_managers/'+location_id,
            type: 'post',
            data: {},
            success: function(json){
                var data = $.parseJSON(json);
                
                if(!$.isEmptyObject(data)){
                    var html = '<ol>';
                    $.each(data, function(index, value){
                        html += "<li><label> "+value+" </label></li>";
                    });
                    html += '</ol>';
                }else{
                    var html = "<p><label> Select location</label></p>";
                }
                $('.show_location_managers').html('<span id="demo-html-location"><img src="'+base_url+'images/help.png"></span>');
                 $("#demo-html-location").tooltipster({
                    content: $(html),
                    width: 300,
                    position: 'right',
                    theme: 'tooltipster-light'
                });
            }
        });
    });
	// field nombering system by Ashvin Patel
	var no = 1;   
    $('span.enum').each(function(){
		$(this).html(no+'. ');
		no++;	
    });
	//disable department and location dropdown
	$('#rquest_form #department option:not(:selected)').attr('disabled', true);
	$('#rquest_form #location option:not(:selected)').attr('disabled', true);
	
	//multiselect validation	
	$('#marketing_person_chosen input, #receptionist_person_chosen input, #it_person_chosen input').blur(function(e) {
		var value = $(this).parent().parent().parent().parent().find('select').val();	
		var elem = $(this).parent().parent();
		validate_multiselect(value, elem);
	});
	$('#marketing_person, #receptionist_person, #it_person').change(function(e) {
		var value = $(this).val();	
		var elem = $(this).parent().find('.chosen-container .chosen-choices');
		validate_multiselect(value, elem);
	});  
	
	
});
/**
* Validate form multiselect dropdown on form submit
* or change drop down value
* 05/dec/2014
* Ashvin Patel
*/
function validate_multiselect(value, elem){
	if(elem){
		if(!value){
			elem.addClass('ui-state-error');
			return 1;					
		}else{
			elem.removeClass('ui-state-error');				
		}	
	}else{
		var count = 0;
		var i = 0
		$('select').each(function(index, element) {
			var id = $(this).attr('id');			
			if (id == 'marketing_person' || id == 'receptionist_person' || id == 'it_person'){
			  var value = $(this).val();	
			  var elem = $(this).parent().find('.chosen-container .chosen-choices');
			  count = validate_multiselect(value, elem);	
			  if(count){
				  i++;	
			  }
			}
		});		  
		if(i){
		  return false;	
		}
	}
}
function viewFancybox(url, width, height){
    jQuery.fancybox({
        type: 'iframe',
        href: url,
        autoSize: false,
        closeBtn: true,
        width: width,
        height: height,
        closeClick: true,
        enableEscapeButton: true,
        beforeLoad: function () {},
    });	
}
